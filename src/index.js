import React from "react";
import ReactDOM from "react-dom";
// import "./index.css";

import "./assets/CSS/variable.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistedStore } from "./store/Store";
import NetworkInterceptor from "./utils/services/interceptor/interceptor";

NetworkInterceptor.setupInterceptors(store);
// import from './app.scss'
// import { ThemeProvider } from '@material-ui/core';
//const { store } = configureStore();

const ContainerApp = (props) => {
  return <div>{props.children}</div>;
};

ReactDOM.render(
  <ContainerApp>
    {/* <ThemeProvider theme={theme}> */}
    <Provider store={store}>
      <PersistGate persistor={persistedStore}>
        <App />
      </PersistGate>
    </Provider>
    {/* </ThemeProvider> */}
  </ContainerApp>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

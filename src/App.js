import OrderRouting from "./pages/order/routing";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/CSS/style.css";
import "./assets/CSS/custom.css";
import "./assets/CSS/checkout.css";
import './assets/CSS/theme.css';

function App() {
  // console.log = console.warn = console.error = () => {};
  return <OrderRouting />;
}

export default App;

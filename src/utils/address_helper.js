export function getCurrentLatLong() {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition((position) => {
      var lat = position.coords.latitude;
      var long = position.coords.longitude;
      return resolve({ lat, long })
    }, (error) => {
      reject(error)
    });
  })
}

export const getCity = (addressArray) => {
  let city = '';
  for (let i = 0; i < addressArray?.length; i++) {
    if (addressArray[i].types[0] && 'administrative_area_level_2' === addressArray[i].types[0]) {
      city = addressArray[i].long_name;
      return city;
    }
  }
};

export const getArea = (addressArray) => {
  let area = '';
  for (let i = 0; i < addressArray?.length; i++) {
    if (addressArray[i].types[0]) {
      for (let j = 0; j < addressArray[i].types.length; j++) {
        if ('sublocality_level_1' === addressArray[i].types[j] || 'locality' === addressArray[i].types[j]) {
          area = addressArray[i].long_name;
          return area;
        }
      }
    }
  }
};

export const getState = (addressArray) => {
  let state = '';
  for (let i = 0; i < addressArray?.length; i++) {
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0] && 'administrative_area_level_1' === addressArray[i].types[0]) {
        state = addressArray[i].long_name;
        return state;
      }
    }
  }
};
export const getPinCode = (addressArray) => {
  let pinCode = '';
  for (let i = 0; i < addressArray?.length; i++) {
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0] && 'postal_code' === addressArray[i].types[0]) {
        pinCode = addressArray[i].long_name;
        return pinCode;
      }
    }
  }
};
export const getLocality = (addressArray) => {
  let locality = '';
  for (let i = 0; i < addressArray?.length; i++) {
    for (let i = 0; i < addressArray.length; i++) {
      if (addressArray[i].types[0] && 'sublocality_level_2' === addressArray[i].types[0]) {
        locality = addressArray[i].long_name;
        return locality;
      }
    }
  }
};
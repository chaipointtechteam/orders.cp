import { headerKey, applicationId, oraginzationId, channelId } from "./constants";
/**
 * Creates the Application public API URL based on environment variables.
 *
 * @param {string} api
 */
export const createPlatformURL = (api) => {
  let publicUrl;

  publicUrl = `https://testing.chaipoint.com/qa3/online-ag/api/${api}`;
  return encodeURI(publicUrl);
};

export const createInternalPlatformURL = (api) => {
  let internalUrl;

  internalUrl = `https://chaipoint.com360degree.com/api/v1.0/${api}`;
  return encodeURI(internalUrl);
};

export const header = {
  channelId:channelId,
  "Content-Type": "application/json",
  "service.auth.key": headerKey,
  applicationId: applicationId,
  organizationId: oraginzationId,
};

/**
 * A collection of utilities to help securely connect to the platform
 */

export const handleSecureAjaxError = (
  error,
  dispatch,
  customerMessage = ""
) => {
  let formattedMessage = customerMessage;
  let platformResponse = undefined;

  try {
    if (error.response) {
      const { data, status, statusText, config } = error.response;
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(`Platform request failed: ${config.url}`);
      //log.error(data);
      //log.error(`Platform HTTP Status ${status} ${statusText}`);
      if (error.response.data) {
        platformResponse = data;
      }
      if (status) {
        platformResponse = Object.assign({}, platformResponse, {
          "Status Code": status,
        });
      }
      if (statusText) {
        platformResponse = Object.assign({}, platformResponse, {
          Status: statusText,
        });
      }
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      //log.error("Platform request error", error);
      formattedMessage.concat(" - Unexpected error occurred");
    } else {
      // Something happened in setting up the request that triggered an Error
      /// log.error("Error", error);
    }
  } catch (error) {
    console.log("Failed to process Error! whoops", error.message);
  }
  // log.debug(`Formatted Message ${formattedMessage}`);

  if (dispatch) {
    console.log(`platformResponse  ${platformResponse}`);
    // dispatch(addNotification({ title: formattedMessage, variant: "error", platformResponse }));
  }
};

//     - get sellers - /api/store/getsellerstores - working
//     - getStores - /api/seller/getSellers - working = returning services
//     - catalogue - /api/menu/v2/stores - working = catelogue
//     - inventory - /api/order/inventory
//     - Cart create - /api/cart/create
//     - Get create - /api/cart/getcart
//     - Refresh create - /api/cart/refreshcartitems
//     - otp generate - /api/login/otp/generate
//     - otp validate - /api/login/otp/verify
//     - Order create- /api/order/create
//     - confirm order without payment (for 0 total) - /api/order/confirm

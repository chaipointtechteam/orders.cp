/* eslint-disable no-param-reassign */
import Axios from "axios";
import { loader, showSnackBar } from "../../../actions//common.actions";
import { clearAuth } from "../../../actions/LoginAction";
import { header } from "../../apiUtil";
import toast from "../../../components/common/commonToast";
import { headerKey, applicationId, oraginzationId } from "../../constants";

/* Loader Show/Hide logic */
let count = 0;
const showLoader = (store) => {
  store.dispatch(loader(true));
  count++;
};

const hideLoader = (store) => {
  // console.log('** loaded **',)
  if (count <= 1) {
    store.dispatch(loader(false));
    count = 0;
  } else {
    count--;
  }
};

/* Error Show/Hide logic */
const handleError = (store, err = {}) => {
  // console.log('** err **', err)
  // store.dispatch(showSnackBar({ message: err.message, variant: 'error' }));
  const messgae = err.message || "Something went wrong";
 
  toast.warning(messgae, {duration: 2,
    maxCount: 10, closable: true });
  if (
    (err.status === 500 &&
      err.message === "INTERNAL SERVER ERROR: jwt expired!!") ||
    err.status === 401
  ) {
    store.dispatch(clearAuth());
  }
};

/* Function to validate|prepare|modify error object */
const prepareErrorObject = (error) => {
  let err = error.response ? error.response.data : error;
  if (typeof err === "object") {
    err.timestamp = Date.now();
    err.config = error.config;
  } else {
    err = {};
    err.message = "Something went wrong.";
    err.timestamp = Date.now();
  }
  return err;
};

const updateToken = (response = {}) => {
  // const { headers = {} } = response;
  // const token = headers[ACCESS_TOKEN];
  // if (token) {
  //   cookieServices.setCookie(ACCESS_TOKEN, token);
  // }
};

export default {
  setupInterceptors: (store) => {
    // Requests interceptor
    Axios.interceptors.request.use(
      (config) => {
        let headers = { ...config.headers };
        const { silent = true } = headers;
        const state = store.getState();
        const userObject = state?.userObject?.userObject;
        const { newHeader = {} } = userObject;
        headers["Content-Type"] = "application/json";
        headers["service.auth.key"] = headerKey;
        headers.applicationId = applicationId;
        headers.organizationId = oraginzationId;
        headers = { ...headers, ...newHeader }; // will overide header from redux state;
        headers["Access-Control-Allow-Methods"] =
          "POST, GET, PUT, DELETE, OPTIONS, PATCH";
        showLoader(store);
        if (headers.silent) {
          delete headers.silent;
        }
        return config;
      },
      (error) => {
        hideLoader(store);
        // handleError(store, error);
        return Promise.reject(error ? error["response"] : null);
      }
    );

    // Response interceptor
    Axios.interceptors.response.use(
      (response) => {
        const { data = {} } = response;
        // updateToken(response);
        hideLoader(store);
        if (data.status >= 400) {
          const err = prepareErrorObject(data);
          // handleError(store, err);
        }
        if (data.data && data.data.statusCode == "01") {
          // handleError(store, data.data);
        }
        return response;
      },
      (error) => {
        const err = prepareErrorObject(error);
        // updateToken(error.response);
        // handleError(store, err);
        hideLoader(store);
        return Promise.reject(error ? error["response"] : null);
      }
    );
  },
};

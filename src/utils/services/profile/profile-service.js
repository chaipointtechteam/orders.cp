import HttpClient from "../http-client/http-client";
import { BASE_URL } from "../../enviroments/enviroments";

class ProfileService {
  static async getAccountInfo(phoneNo, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}profile/v2/details`;
    return HttpClient.get(url, sessionId);
  }
  static async addUpdateAddress(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}profile/address/update`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async createCart(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}cart/create`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async createOrder(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}order/create`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async getCart(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}cart/getCart`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async defaultAddress(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}profile/address/default?addressId=${payload}`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async applyCoupon(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}coupon/apply`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async wallettCapture(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}wallet/capture`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async processOrder(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}order/process`;
    return await HttpClient.post(url, payload, sessionId);
  }
  static async RefreshCart(payload, sessionId) {
    const url = `${BASE_URL.BUSINESS_URL}cart/refreshcartitems`;
    return await HttpClient.post(url, payload, sessionId);
  }
}

export default ProfileService;
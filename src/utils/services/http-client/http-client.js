import Axios from "axios";
import { from } from "rxjs";
import { header, createPlateform } from "../../apiUtil";
import { map, fro } from "rxjs/operators";

class HttpService {
  get(url, sessionId) {
    const headers = { ...header, sessionId: sessionId };
    return Axios.get(url, { headers: headers })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return error;
        console.log(error, "erroermessage");
      });
  }

  post(url, data, sessionId) {
    const headers = { ...header, sessionId: sessionId };
    return Axios.post(url, data, { headers: headers })
      .then((response) => {
        return response.data;
      })
      .catch((response) => {
        return response;
      });
  }

  put(url, data, config = {}) {
    return from(Axios.put(url, data, config))
      .pipe(map((response) => response.data))
      .toPromise();
  }

  delete(url, data, config = {}) {
    return from(Axios.delete(url, data, config))
      .pipe(map((response) => response.data))
      .toPromise();
  }

  patch(url, data, config = {}) {
    return from(Axios.patch(url, data, config))
      .pipe(map((response) => response.data))
      .toPromise();
  }
}

const HttpClient = new HttpService();

export default HttpClient;

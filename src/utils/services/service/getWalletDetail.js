import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../../../utils/apiUtil";
import React, { useState } from "react";
import axios from "axios";
import { secretKey, authKey } from "../../constants";
import { connect } from "react-redux";
import ProfileService from "../profile/profile-service";
import * as Crypto from "crypto-js";
import { updatePayObj } from "../../../actions/PayAction";
export const getWalletDetails = async (props) => {
  const options = {
    headers: { ...header, sessionId: `${props.newHeader.sessionId}` },
  };
  let url = createPlatformURL("wallet/getWallet");
  var data = JSON.stringify({
    orgId: 1,
    phoneNumber: props.phoneNo,
  });
  var config = {
    method: "post",
    url: url,
    headers: options.headers,
    data: data,
  };
  return await axios(config)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
};
export const getPaymentsDetails = async (props) => {
  var payReq = "";
  var state = false;
  const key = Crypto.enc.Hex.parse(
    Crypto.SHA1(secretKey).toString().slice(0, 32).padEnd("0", 32)
  );
  var ciphertext = Crypto.AES.encrypt(authKey, key, {
    mode: Crypto.mode.ECB,
  }).toString();
  const options = {
    headers: { ...header, authKey: `${ciphertext}` },
  };
  let url = createPlatformURL("payment/v4/getPaymentDetail");
  var config = {
    method: "get",
    url: url,
    headers: options.headers,
  };

  await axios(config)
    .then(async function (response) {
      const sessionId = props.props.userObject.headerSessionId;
      if (props.props.Wallet.isUse) {
        const payload = {
          amount: props.props.Wallet.walletAmount,
          channelId: 11,
          cpOrderId: props.res.orderResponse.id,
          payableAmount: props.res.orderResponse.orderData.payableAmount,
          paymentRequestId: props.rString,
          pgId: response.data.pgId,
          sellerId: props.props.selectedChannel.sellerId,
          siteId: props.props.selectedStore.siteId,
        };
        await ProfileService.wallettCapture(payload, sessionId).then((res) => {
          if (res.status) {
            state = true;
            payReq = {
              payRequestId: payload.paymentRequestId,
              amount:
                props.res.orderResponse.orderData.payableAmount - res.amount,
            };
          }
        });
      }
      const name = [];
      props.props.cartObject.CreateCart.map((e) => {
        name.push(e.itemName);
      });
      // console.log(name.toString())
      var pUrl = Crypto.AES.decrypt(response.data.pgDetailMap.url, key, {
        mode: Crypto.mode.ECB,
      }).toString(Crypto.enc.Utf8);
      var pgKey = Crypto.AES.decrypt(response.data.pgDetailMap.key, key, {
        mode: Crypto.mode.ECB,
      }).toString(Crypto.enc.Utf8);
      var salt = Crypto.AES.decrypt(response.data.pgDetailMap.salt, key, {
        mode: Crypto.mode.ECB,
      }).toString(Crypto.enc.Utf8);
      const host = window.location.origin;
      var txnid = props.res.orderResponse.orderId;
      var amount = state
        ? payReq.amount
        : props.res.orderResponse.orderData.payableAmount;
      var product = 'orderTransaction';//name.toString();
      var userName = props.props.userObject.name;
      var email = props.props.userObject.email;
      // var surl = `${host}${process.env.REACT_APP_PAYMENT_SUCCESS_URL}`;
      // var furl = `${host}${process.env.REACT_APP_PAYMENT_FAILURE_URL}`;
      // var surl = "https://ptsv2.com/t/94ebo-1635421395/post";
      var surl = window.location.origin+"/paysuccess";
      var furl = window.location.origin+"/payunsuccess";
      //console.log(furl, surl);
      var phone = props.props.userObject.phoneNo;
      var udf1 = 11;
      var udf2 = props.res.orderResponse.id;
      var udf3 = response.data.pgId;
      var udf4 = state ? payReq.payRequestId : props.rString;
      var hash = Crypto.SHA512(
        `${pgKey}|${txnid}|${amount}|${product}|${userName}|${email}|${udf1}|${udf2}|${udf3}|${udf4}|||||||${salt}`
      ).toString();

      props.props.updatepayObject({
        url: pUrl,
        key: pgKey,
        txnid: txnid,
        amount: amount,
        firstname: userName,
        email: email,
        phone: phone,
        productinfo: product,        
        surl: surl,
        furl: furl,
        hash: hash,
        udf1: udf1,
        udf2: udf2,
        udf3: udf3,
        udf4: udf4,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};
export const createCart = async (props, session) => {
  const options = {
    headers: { ...header, sessionId: `${session}` },
  };
  var axios = require("axios");
  var url = createPlatformURL("cart/create");
  var config = {
    method: "post",
    url: url,
    headers: options.headers,
    data: props,
  };
  return axios(config)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {});
};
const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject || {};
  const userObject = reducerObj.userObject.userObject || {};
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const Wallet = reducerObj.Wallet;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  // console.log(Wallet, selectedStore, "asgdjahdgjdfljdffhdsjhlfdj");
  return {
    cartObject,
    userObject,
    selectedChannel,
    Wallet,
    selectedStore,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updatePayObj: (payload) => dispatch(updatePayObj(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(getPaymentsDetails);

export const filterTheMenuWithCategory = (productList, selectedCategory) => {
  var filteredMenu = productList?.filter(function (prod) {
    if (
      prod?.category &&
      prod.category?.categoryCode === selectedCategory?.categoryCode
    ) {
      return prod;
    }
  });
  return filteredMenu;
};
export const filterTheMenu = (productList, categoryList) => {
  var filterMenu = [];
  categoryList.map((item) => {
    var filtermenu = filterTheMenuWithCategory(
      productList,
      item
    );
    if (filtermenu.length !== 0) {
      filterMenu.push({ filterMenu: filtermenu, item: item })
    }

  })
  return filterMenu
}


export const filterTheMenuWithSubCategory = (
  productList,
  selectedSubCategory,
  selectedCategory
) => {
  var filteredMenu = productList?.filter(function (prod) {
    if (
      prod.subCategory &&
      prod.category &&
      prod.category.categoryCode === selectedCategory.categoryCode &&
      prod.subCategory.categoryCode === selectedSubCategory.categoryCode
    ) {
      return prod;
    }
  });
  return filteredMenu;
};

export const filterByValue = (productList, searchQuery) => {
  var filteredItems = productList?.filter(
    (o) =>
      o["displayName"]
        .toString()
        .toLowerCase()
        .includes(searchQuery.toLowerCase()) ||
      o["productCode"]
        .toString()
        .toLowerCase()
        .includes(searchQuery.toLowerCase())
  );
  return filteredItems;
};

export const containsObjectInCart = (obj, list) => {
  var i;
  for (i = 0; i < list.length; i++) {
    if (
      list[i].productId === obj.productId &&
      obj.productModifierList[0]?.modifierProductList.length > 0 &&
      obj.productModifierList[0]?.modifierProductList ===
        list[i].productModifierList[0]?.modifierProductList
    ) {
      //need to check if addons and variants are same or not
      return true;
    }
  }
  return false;
};

export const containsObject = (obj, list) => {
  var i;
  for (i = 0; i < list.length; i++) {
    if (list[i].productId === obj.productId) {
      return true;
    }
  }

  return false;
};

export const constainsObjectWithModifierList = (obj, list) => {
  var i;
  for (i = 0; i < list.length; i++) {
    if (obj.productId === list[i].productId) {
      
      var m = [];
      obj?.productModifierList.map((modifier) => {
        modifier?.modifierProductList.map((o, index) => {
          if (o?.isChecked === true) {
            m.push(o);
          }
        });
      });

      var sublist = list[i].productModifierList.modifierProductList;
      
      var k, j;
      var count = 0;
      if (sublist.length === m.length) {
        for (k = 0; k < m.length; k++) {
          for (j = 0; j < sublist.length; j++) {
            if (m[k].productId === sublist[j].productId) {
              count++;
            }
          }
        }
        if (count == m.length) {
          return true;
        }
      }
    }
  }

  return false;
};

export const reOrderObject = (obj, list) => {
  var i;
  for (i = 0; i < list.length; i++) {
    if (list[i].productId === obj.productId) {
      return list[i];
    }
  }
  return false;
};


export const updateSubTotal = (cartList, data) => {
  var requiredTotal = 0;
  var AddonPrice = 0;
  cartList.forEach((element) => {
    requiredTotal = requiredTotal + element.quantity * element.price;

    element.productModifierList.modifierProductList?.map((addOnItem) => {
      //

      if (addOnItem) {
        AddonPrice =
          AddonPrice + addOnItem.productName.price * element.quantity;
      }
    });
    requiredTotal = requiredTotal + AddonPrice;
    AddonPrice = 0;

    element.productVariantList.forEach((variant) => {
      if (variant.isChecked) {
        requiredTotal = requiredTotal + variant.price;
      }
    });
  });
  requiredTotal = parseFloat(requiredTotal).toFixed(2);
  return requiredTotal;
};

export const isValidEmail = (email) => {
  var reg = new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g);
  return reg.test(email);
};

export const getProductNameFromProductCode = (list, code) => {
  if (code && list) {
    var product = list.find(function (item, i) {
      if (item.productCode === code) {
        return item;
      }
    });
    if (product) {
      return product;
    }
  }
};

export const filterFromModal = (list, filterObject) => {
  
  var list1 = filterTheMenuWithCategory(list, filterObject.selectedCategory)
  var m = [];
  var filtered = list.map((prod1) => {
    var products=[];
        prod1.filterMenu.filter((prod)=>{
          if(
              (filterObject.isVeg && filterObject.isNonVeg) ||
              (!filterObject.isVeg && !filterObject.isNonVeg)
            ){
              if(
                  prod.price < filterObject.maxPrice &&
                  prod.price > filterObject.minPrice
                ){
                  return products.push(prod);
                }             
            }
          else if (            
            prod.price < filterObject.maxPrice &&
            prod.price > filterObject.minPrice &&
            prod.isVegetarian === filterObject.isVeg            
          ) {
            return products.push(prod);
          }
        })

        return m.push({filterMenu:products,item:prod1.item})
  });
    filtered=m;
    // console.log(filtered)
  return filtered;
};

export const CheckAvaibilityOfProduct = (list, product) => {
  
  var m = list?.find((item) => item.id === product.productId);
  if (!m?.isOutofStock && product?.quantity < m?.availableCount) {
    return true;
  }

  return false;
};

export const checkAvaibilityOfProductWithAddond = (list, product, cartList) => {
  //
  var m = list?.find((item) => item?.id === product?.productId);

  var cart = cartList?.filter((cartObj) => cartObj?.productId === m?.id);
  var productQuantity = 0;
  cart.map((prod) => {
    productQuantity = prod.quantity + productQuantity;
  });

  //
  if (!m?.isOutofStock && productQuantity < m?.availableCount) {
    var checkaddon = { check: true, addonItem: "" };
    product.productModifierList.modifierProductList?.map((addonItem) => {
      //

      var add = list?.find((item) => item?.id === addonItem?.productId);
      if (!add?.isOutofStock && product?.quantity < add?.availableCount) {
      } else {
        checkaddon = { check: false, addonItem: addonItem.productName };
      }
    });
    return checkaddon.check ? { check: true, addonItem: "" } : checkaddon;
  }
  return { check: false, addonItem: product };
};

export const ModifiersQuantity = (list, product, cartList) => {
  var m = list?.find((item) => item?.id === product?.productId);

  var cart = cartList.filter((cartObj) => cartObj.productId === m?.id);
  var productQuantity = 0;
  cart?.map((prod) => {
    productQuantity = prod?.quantity + productQuantity;
  });

  return productQuantity
}
export const VariantQuantity = (product,cartList)=>{
  // debugger

  var cart = cartList.filter((cartObj) => cartObj.selectedProduct?.productId == product.productId);
  var productQuantity = 0;
  cart?.map((prod) => {
    productQuantity = prod?.quantity + productQuantity
  });
  return productQuantity
} 
export const checkOutofStock = (list, product) => {
  
  var m = list?.find((item) => item?.id === product?.productId)
  
  if (product?.productVariantList?.length > 0) {
    return true
  } else if (!m?.isOutofStock) {
    return true
  }
  return false
}

export function deepEqual(object1, object2) {
  const keys1 = Object.keys(object1);
  const keys2 = Object.keys(object2);

  if (keys1.length !== keys2.length) {
    return false;
  }

  for (const key of keys1) {
    const val1 = object1[key];
    const val2 = object2[key];
    const areObjects = isObject(val1) && isObject(val2);
    if (
      areObjects && !deepEqual(val1, val2) ||
      !areObjects && val1 !== val2
    ) {
      return false;
    }
  }

  return true;
}

export function isObject(object) {
  return object != null && typeof object === 'object';
}

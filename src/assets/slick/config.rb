css_dir = "./assets/css/"
sass_dir = "./assets/sass/"
images_dir = "./assets/img/"
fonts_dir = "./assets/fonts"
relative_assets = true

output_style = :compact
line_comments = false

preferred_syntax = :scss
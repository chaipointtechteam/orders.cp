export const UPDATE_TANDC = "update-tandc";
export const ABOUT_US = "aboutUs";
export const PRIVACYPOLICY = "privacy-policy";
export const PARTNERS ="partners"
export const updateTandC = (data) => {
    return {
      type: UPDATE_TANDC,
      payload: data,
    };
  };
  export const updateAboutUs = (data) => {
    return {
      type: ABOUT_US,
      payload: data,
    };
};
export const updatePrivacyPolicy = (data) => {
    return {
      type: PRIVACYPOLICY,
      payload: data,
    };
};
export const updatePartners = (data) => {
  return {
    type: PARTNERS,
    payload: data,
  };
};
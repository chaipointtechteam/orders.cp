import axios from "axios";
import { BASE_URL } from "../utils/enviroments/enviroments";

const setLoading = () => {
  return async (dispatch) => {
    dispatch({
      type: "SET_LOADING",
      payload: {
        homeLoading: true,
      },
    });
  };
};

const unsetLoading = () => {
  return async (dispatch) => {
    dispatch({
      type: "SET_LOADING",
      payload: {
        homeLoading: false,
      },
    });
  };
};

const getHeaderItems = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "menu.json", {
        business_unit: "chaipoint.com",
      });

      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: [],
        },
      });
    }
  };
};

const getBanners = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "banner.json", {
        business_unit: "chaipoint.com",
        menu_id: 0,
      });

      return dispatch({
        type: "SET_BANNERS",
        payload: {
          banners: data.result.data,
          media_path: data.result.media_path,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_BANNERS",
        payload: {
          banners: [],
          media_path: "",
        },
      });
    }
  };
};

const getBannerText = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "banner.json", {
        business_unit: "order.chaipoint.com",
      });

      return dispatch({
        type: "SET_BANNER_TEXT",
        payload: {
          bannerText: data.result.bannerText,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_BANNER_TEXT",
        payload: {
          bannerText: "",
        },
      });
    }
  };
};

const getHowDoWeServeBanners = () => {
  return async (dispatch) => {
    try {
      const vaasData = await axios.post(BASE_URL.INTERNAL_URL + "cms.json", {
        business_unit: "chaipoint.com",
        cms_tag: "serve-customer-vaas",
      });

      const shopData = await axios.post(BASE_URL.INTERNAL_URL + "cms.json", {
        business_unit: "chaipoint.com",
        cms_tag: "serve-customer-shop",
      });

      const orderData = await axios.post(BASE_URL.INTERNAL_URL + "cms.json", {
        business_unit: "chaipoint.com",
        cms_tag: "serve-customer-order",
      });

      return dispatch({
        type: "SET_HDWSC_BANNERS",
        payload: {
          orderData: orderData.data.result.data,
          shopData: shopData.data.result.data,
          vaasData: vaasData.data.result.data,
          hdwscMediaPath: vaasData.data.result.base_path,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_HDWSC_BANNERS",
        payload: {
          orderData: [],
          shopData: [],
          vaasData: [],
          hdwscMediaPath: "",
        },
      });
    }
  };
};

const getProductLaunches = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(
        BASE_URL.INTERNAL_URL + "productAds.json",
        {
          business_unit: "chaipoint.com",
        }
      );

      return dispatch({
        type: "SET_PRODUCT_LAUNCHES",
        payload: {
          productLaunches: data.result.data,
          productLaunchMediaPath: data.result.media_path,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_PRODUCT_LAUNCHES",
        payload: {
          productLaunches: [],
          productLaunchMediaPath: "",
        },
      });
    }
  };
};

const getSocialImpact = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "cms.json", {
        business_unit: "chaipoint.com",
        cms_tag: "social-impact",
      });

      return dispatch({
        type: "SET_SOCIAL_IMPACT",
        payload: {
          socialImpact: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_SOCIAL_IMPACT",
        payload: {
          socialImpact: [],
        },
      });
    }
  };
};

const getBrandValuePurpose = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "cms.json", {
        business_unit: "chaipoint.com",
        cms_tag: "brand-value-purpose",
      });

      return dispatch({
        type: "SET_BRAND_VALUE",
        payload: {
          brandValues: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_BRAND_VALUE",
        payload: {
          brandValues: [],
        },
      });
    }
  };
};

const getNewStoreLaunches = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "cms.json", {
        business_unit: "chaipoint.com",
        cms_tag: "store-launches",
      });

      return dispatch({
        type: "SET_STORE_LAUNCHES",
        payload: {
          storeLaunches: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_STORE_LAUNCHES",
        payload: {
          storeLaunches: [],
        },
      });
    }
  };
};

const getPartnerTraining = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "cms.json", {
        business_unit: "chaipoint.com",
        cms_tag: "partner-training",
      });

      return dispatch({
        type: "SET_PARTNER_TRAINING",
        payload: {
          partnersTrainig: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_PARTNER_TRAINING",
        payload: {
          partnersTrainig: [],
        },
      });
    }
  };
};

export {
  getHeaderItems,
  getBanners,
  getBannerText,
  getHowDoWeServeBanners,
  getProductLaunches,
  getSocialImpact,
  getBrandValuePurpose,
  getNewStoreLaunches,
  getPartnerTraining,
  setLoading,
  unsetLoading,
};

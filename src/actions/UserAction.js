export const UPDATE_USER = "update-user";
export const RESET_USER = "reset-user";
export  const UPDATE_ADDRESS="update-address"

export const updateUserObject = (data, callBack) => {
  return (dispatch) => {
    dispatch(updateObject(data));
    callBack && callBack();
  };
};
export const updateObject = (data) => {
  return {
    type: UPDATE_USER,
    payload: data,
  };
};

export const updateAddress=(data)=>{
  // console.log(data)
   return{
     type:UPDATE_ADDRESS,
      payload:data
   }
}

export const resetUserObject = (data) => {
  return {
    type: RESET_USER,
  };
};

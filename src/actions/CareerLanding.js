import axios from "axios";
import { BASE_URL } from "../utils/enviroments/enviroments";
import _ from "lodash";

const setLoading = (isLoading) => {
  return async (dispatch) => {
    return dispatch({
      type: "SET_CAREER_LOADING",
      payload: {
        careerLoading: isLoading,
      },
    });
  };
};

const getJobCategories = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.get(
        BASE_URL.INTERNAL_URL + "jobCategory.json",
        {}
      );

      const jobCat = [];

      _.mapValues(data.result.data, (value, key) => {
        jobCat.push({ value, label: value });
      });

      return dispatch({
        type: "SET_CATEGORIES",
        payload: {
          jobCategories: jobCat,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_CATEGORIES",
        payload: {
          jobCategories: [],
        },
      });
    }
  };
};

const searchJobs = (details) => {
  return async (dispatch) => {
    const { category, jobType, keyword, location } = details;
    try {
      const { data } = await axios.post(
        BASE_URL.INTERNAL_URL + "jobSearch.json",
        {
          job_category: category,
          job_location: location,
          job_type: jobType,
          keyword: keyword,
        }
      );

      return dispatch({
        type: "SET_JOBS",
        payload: {
          jobs: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_JOBS",
        payload: {
          jobs: [],
        },
      });
    }
  };
};

const getJobs = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(
        BASE_URL.INTERNAL_URL + "listJob.json",
        {}
      );

      return dispatch({
        type: "SET_JOBS",
        payload: {
          jobs: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_JOBS",
        payload: {
          jobs: [],
        },
      });
    }
  };
};

const getHeaderItems = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "menu.json", {
        business_unit: "chaipoint.com",
      });

      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: [],
        },
      });
    }
  };
};

const getJobLocations = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.get(
        BASE_URL.INTERNAL_URL + "jobLocation.json"
      );

      return dispatch({
        type: "SET_JOB_LOCATIONS",
        payload: {
          jobLocations: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_JOB_LOCATIONS",
        payload: {
          jobLocations: [],
        },
      });
    }
  };
};

export {
  getHeaderItems,
  setLoading,
  getJobs,
  getJobCategories,
  searchJobs,
  getJobLocations,
};

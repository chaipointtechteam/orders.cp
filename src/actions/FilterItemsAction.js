/** Action Types */
export const SET_FILTERED_ITEM_LIST = "set-filtered-item-list";
export const UPDATE_FILTER_OBJECT = "update-filter-object";
export const RESET_FILTER = "reset-filter";

export const setFilteredItemList = (itemList, value) => {
  return {
    type: SET_FILTERED_ITEM_LIST,
    payload: itemList,
    value,
  };
};

export const updateFilterObject = (filterObject, value) => {  
  return {
    type: UPDATE_FILTER_OBJECT,
    payload: filterObject,
    value,
  };
};

export const resetFilter = () => {
  return {
    type: RESET_FILTER,
  };
};

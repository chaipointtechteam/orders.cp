export const UPDATE_PAY = "UPDATE_PAY";

export const REMOVE_PAY = "REMOVE_PAY";

// export const UPDATE_AMOUNT = "UPDATE_AMOUNT";

export const updatepayObject = (data, callBack) => {
    return (dispatch) => {
        dispatch(updatePayObj(data));
        callBack && callBack();
    };
};
export const updatePayObj = (data) => {
    return {
        type: UPDATE_PAY,
        payload: data,
    };
};

export const removPayObj = () => {
    return {
        type: REMOVE_PAY,
    };
};

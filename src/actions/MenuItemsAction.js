import axios from "axios";
//HC imports
import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../utils/apiUtil";

const options = {
  headers: header,
};

/** Action Types */
export const SET_MENU_ITEM_LIST = "set-menu-item-list";
export const SET_CATEGORY_MENU_LIST = "set-category-menu-list";
export const GET_STORE_INVENTORY_LIST = "get-store-inventory-list";
/**
 * Load Store List
 */
export const loadMenuItemList = (inputData, callback) => {
  let url = createPlatformURL("menu/v2/stores");
  return () => {
    axios
      .post(url, inputData, options) //store id will be in input data
      .then((res) => {
        callback && callback(res);
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const loadInventoryList = (inputData, callback) => {
  let url = createPlatformURL("order/inventory");
  return () => {
    axios
      .post(url, inputData, options) //store id will be in input data
      .then((res) => {
        callback && callback(res);
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const loadInventoryList1 = (inputData, callback) => {
  //
  let url = createPlatformURL("order/inventory");

  return axios
    .post(url, inputData, options) //store id will be in input data
    .then((res) => {
      return res;
    })
    .catch((error) => {
      handleSecureAjaxError(error, "ServiceActions loadService");
    });
};

export const setMenuItemList = (itemList, value) => {
  return {
    type: SET_MENU_ITEM_LIST,
    payload: itemList,
    value,
  };
};

export const setCategoryMenuList = (itemList, value) => {
  return {
    type: SET_CATEGORY_MENU_LIST,
    payload: itemList,
    value,
  };
};

export const updateMenuItem = (menuList, data, quantity, inventory) => {
  if (!data) {
    return (dispatch) => {
      dispatch(
        setMenuItemList({
          menuItemList: menuList,
        })
      );
    };
  }

  //if no data is available no need to perform any action
  //update menu item quantity
  menuList.forEach((element, index) => {
    if (element.productId === data.productId) {
      //
      if (element.productModifierList.length > 0) {
        //
        var m1 = data.productModifierList[0]?.modifierProductList.filter(
          (o, index) => o.isChecked === true
        );
      } else {
        element.quantity = quantity;
        return;
      }
    }
  });

  return (dispatch) => {
    dispatch(
      setMenuItemList({
        menuItemList: menuList,
        inventory: inventory,
      })
    );
  };
};

export const resetMenuItemList = (menuList, inventory) => {
  //update menu item quantity
  menuList.forEach((element) => {
    element.quantity = 0;
  });
  return (dispatch) => {
    dispatch(
      setMenuItemList({
        menuItemList: menuList,
        inventory: inventory,
      })
    );
  };
};

export const emptyMenuItemList = () => {
  return (dispatch) => {
    dispatch(
      setMenuItemList({
        menuItemList: [],
        inventory: [],
      })
    );
  };
};

export const upateMenuItemAfterCartItemRemoval = (
  menuList,
  data,
  inventory
) => {
  menuList.forEach((element, index) => {
    if (element.productId === data.productId) {
      if (element.productModifierList.length > 0) {
      } else {
        element.quantity = 0;
        return;
      }
    }
  });

  return (dispatch) => {
    dispatch(
      setMenuItemList({
        menuItemList: menuList,
        inventory: inventory,
      })
    );
  };
};

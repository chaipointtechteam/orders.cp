import axios from "axios";
import { BASE_URL } from "../utils/enviroments/enviroments";
import _ from "lodash";

const setLoading = (isLoading) => {
  return async (dispatch) => {
    return dispatch({
      type: "SET_JOB_LOADING",
      payload: { jobDetailLoading: isLoading },
    });
  };
};

const getHeaderItems = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "menu.json", {
        business_unit: "chaipoint.com",
      });

      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: [],
        },
      });
    }
  };
};

export { getHeaderItems, setLoading };

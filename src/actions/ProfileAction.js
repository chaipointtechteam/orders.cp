import axios from "axios";
//HC imports
import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../utils/apiUtil";
import ProfileService from '../utils/services/profile/profile-service';

const options = {
  headers: header,
};

/** Action Types */
export const SET_EMAIL_NAME = "set-email-name";
/**
 * Load Store List
 */
export const setEmailAndName = (inputData, callback) => {
  let url = createPlatformURL("profile/update");
  return (dispatch) => {
    axios
      .post(url, inputData, options) //email and name will be in input data
      .then((res) => {
        callback && callback();
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const getMenuItemList = (itemList, value) => {
  return {
    type: SET_EMAIL_NAME,
    payload: itemList,
    value,
  };
};
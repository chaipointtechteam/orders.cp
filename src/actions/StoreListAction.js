import axios from "axios";
//HC imports
import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../utils/apiUtil";
import moment from 'moment';
const options = {
  headers: header,
};

/** Action Types */
export const GET_STORE_LIST = "get-store-list";
export const UPDATE_SELECTED_STORE = "update-selected-store";
export const UPDATE_SELECTED_CHANNEL = "update-selected-channel";
export const UPDATE_SELECTED_LOCATION = "update-selected-location";
export const RESET_STORE = "reset-store";
export const RESET_CHANNEL = "reset-channel";
export const RESET_LOCATION = "reset-location";
export const resetStore = () => {
  return {
    type: RESET_STORE,
  };
};

export const resetChannel = () => {
  return {
    type: RESET_CHANNEL,
  };
};

export const resetLocation = () => {
  return {
    type: RESET_LOCATION,
  };
};

/**
 * Load Store List
 */
export const loadStoreList = (inputData, callback) => {
  
   var CurrentTime= moment().format("HH:mm:ss")
   var Currentdate= moment().format("L")
  let url = createPlatformURL("store/getsellerstores");
  return (dispatch) => {
    axios
      .post(url, inputData, options) //lat and lng will be in input
      .then((res) => {
        var dispData = []
        res.data.map((resData) =>
        {if (Date.parse(`${Currentdate} ${resData.startTime}`) < Date.parse(`${Currentdate} ${CurrentTime}`) && Date.parse(`${Currentdate} ${CurrentTime}`) < Date.parse(`${Currentdate} ${resData.endTime}`)) {
          dispData.push(resData)
          
        }}
        )
       
        callback && callback(dispData);
        dispatch(getStoreList(dispData));
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const getStoreList = (storeList, value) => {
  return {
    type: GET_STORE_LIST,
    payload: storeList,
    value,
  };
};

export const updateSelectedStore = (data) => {
  return {
    type: UPDATE_SELECTED_STORE,
    payload: data,
  };
};
export const updateSelectedLocation = (data) => {
  return {
    type: UPDATE_SELECTED_LOCATION,
    payload: data,
  };
};

export const updateSelectedChannel = (data) => {
  return {
    type: UPDATE_SELECTED_CHANNEL,
    payload: data,
  };
};

import axios from "axios";
import { BASE_URL } from "../utils/enviroments/enviroments";
import _ from "lodash";

const getBlogs = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "blog.json", {
        business_unit: "chaipoint.com",
      });

      return dispatch({
        type: "SET_BLOGS",
        payload: {
          blogs: data.result.data,
          media_path: data.result.media_path,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_BLOGS",
        payload: {
          blogs: [],
        },
      });
    }
  };
};

const getHeaderItems = () => {
  return async (dispatch) => {
    try {
      const { data } = await axios.post(BASE_URL.INTERNAL_URL + "menu.json", {
        business_unit: "chaipoint.com",
      });

      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: data.result.data,
        },
      });
    } catch (err) {
      return dispatch({
        type: "SET_MENU_ITEMS",
        payload: {
          menuItems: [],
        },
      });
    }
  };
};

export { getBlogs, getHeaderItems };

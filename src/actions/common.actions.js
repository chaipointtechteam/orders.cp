export const SHOW_ALERT = 'SHOW_ALERT';
export const HIDE_ALERT = 'HIDE_ALERT';
export const SHOW_SNACKBAR = 'SHOW_SNACKBAR';
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR';
export const CLEAR_ALL = 'CLEAR_ALL';
export const REFRESH = 'REFRESH';
export const AGENT = 'AGENT';

export function loader(action) {
  return {
    type: 'LOADER_VIEW',
    payload: action
  };
}

export function clearAll() {
  return {
    type: CLEAR_ALL
  };
}

export function setRefresh(refresh) {
  return {
    type: REFRESH,
    payload: refresh
  };
}

export function errorHandler(error) {
  return {
    type: 'ERROR_HANDLER',
    payload: error
  };
}

export const showAlert = (data) => ({
  type: SHOW_ALERT,
  payload: data
});

export const hideAlert = () => ({
  type: HIDE_ALERT
});

export const showSnackBar = (data) => ({
  type: SHOW_SNACKBAR,
  payload: data
});

export const hideSnackBar = () => ({
  type: HIDE_SNACKBAR
});

export const setAgent = (data) => ({
  type: AGENT,
  payload: data
});

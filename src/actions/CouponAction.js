export const UPDATE_COUPON = "UPDATE_COUPON";

export const REMOVE_COUPON = "REMOVE_COUPON";
export const APPLY_COUPON = "APPLY_COUPON";
export const REMOVE_APPLY_COUPON = "REMOVE_APPLY_COUPON";
export const REMOVE_PRODUCT = "REMOVE_PRODUCT";
export const UPDATE_PRODUCT = "UPDATE_PRODUCT";
export const UPDATE_REFRESHCART = "UPDATE_REFRESHCART";
export const REMOVE_REFRESHCART = "REMOVE_REFRESHCART"
export const updateCouponObject = (data) => {
  return {
    type: UPDATE_COUPON,
    payload: data,
  };
};

export const updateSelectedProduct = (data) => {
  return {
    type: UPDATE_PRODUCT,
    payload: data,
  };
};

export const removeSelectedProduct = () => {
  return {
    type: REMOVE_COUPON,
  };
};

export const removeCouponObject = () => {
  return {
    type: REMOVE_COUPON,
  };
};

export const applyCoupon = () => {
  return {
    type: APPLY_COUPON,
  };
};

export const updateRefreshCart = (data) => {
  return {
    type: UPDATE_REFRESHCART,
    payload: data
  };
};

export const removeRefreshCart = () => {
  return {
    type: REMOVE_REFRESHCART
  }
}

export const RemoveapplyCoupon = () => {
  return {
    type: REMOVE_APPLY_COUPON,
  };
};

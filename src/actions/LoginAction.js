import axios from "axios";
//HC imports
import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../utils/apiUtil";
import HttpClient from "../utils/services/http-client/http-client";
import { BASE_URL } from "../utils/enviroments/enviroments";
import ProfilService from "../utils/services/profile/profile-service";

const options = {
  headers: header,
};

/** Action Types */
export const GENERATE_OTP = "generate-otp";
export const VERIFY_OTP = "verify-otp";
export const CLEAR_AUTH = "CLEAR_AUTH";
export const SAVE_PROFILE = "SAVE_PROFILE";
/**
 * Load Store List
 */
export const generateOtp = (inputData, callback) => {
  let url = createPlatformURL("login/otp/generate");
  return () => {
    axios
      .post(url, inputData, options) //mobile no will be in input data
      .then((res) => {
        if (res.data) {
          callback && callback();
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const verifyOtp = (inputData, callback) => {
  let url = createPlatformURL("login/otp/verify");
  return () => {
    axios
      .post(url, inputData, options) //mobile no and otp will be in input data
      .then((res) => {
        callback && callback(res.data);
        //update header for all the next pages
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback(error.data);
      });
  };
};

export const logout = (newHeader, callback) => {
  let url = createPlatformURL("logout");
  const newOptions = {
    headers: newHeader,
  };
  return () => {
    axios
      .post(url, {}, newOptions) //session id will be in the header
      .then((res) => {
        
        callback && callback(res.data);
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const updateProfile = (newHeader, newProfile, callback) => {
  let url = createPlatformURL("profile/update");
  const newOptions = {
    headers: newHeader,
  };
  return () => {
    axios
      .post(url, newProfile, newOptions) //session id will be in the header
      .then((res) => {
        callback && callback(res.data);
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const getProfile = (phoneNo, sessionId) => {
  return async (dispatchEvent) => {
    const response = await ProfilService.getAccountInfo(phoneNo, sessionId).then((e) => {
      if (e?.data) {
        if (e.data.errorCode) {
          if (e.data.errorCode = "SESSION_EXPIRED") {
            
            return dispatchEvent({
                   type: CLEAR_AUTH
               });
                
          }
        }
      }
return dispatchEvent({
      type: SAVE_PROFILE,
      payload: e,
    });
      console.log(e)
    });
    
    
  };

  // const url = `${BASE_URL.BUSINESS_URL}profile/details`
  // return HttpClient.get(url, { params: { phoneNo } });
  // let url = createPlatformURL(`profile/details/phoneNo=${phoneNo}`);
  // return await (await axios.get(url, { headers: { ...headers, ...header } })).data
  // return () => {
  //   axios
  //     .post(url, newProfile, newOptions) //session id will be in the header
  //     .then((res) => {
  //       callback && callback(res.data);
  //     })
  //     .catch((error) => {
  //       handleSecureAjaxError(error, "ServiceActions loadService");
  //       callback && callback();
  //     });
  // };
};

export const clearAuth = () => ({
  type: CLEAR_AUTH,
});

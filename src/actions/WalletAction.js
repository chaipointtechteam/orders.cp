export const UPDATE_WALLET = "UPDATE_WALLET";

export const REMOVE_WALLET = "REMOVE_WALLET";

export const UPDATE_AMOUNT = "UPDATE_AMOUNT";

export const updateUserWallet = (data, callBack) => {
  return (dispatch) => {
    dispatch(updateWalletAmount(data));
    callBack && callBack();
  };
};
export const updateWalletAmount = (data) => {
  return {
    type: UPDATE_WALLET,
    payload: data,
  };
};

export const removeWallet = () => {
  return {
    type: REMOVE_WALLET,
  };
};

export const updateAmount = (data) => {
  return {
    type: UPDATE_AMOUNT,
    payload: data,
  };
};
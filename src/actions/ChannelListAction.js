import axios from "axios";
//HC imports
import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../utils/apiUtil";
import moment from "moment"
const options = {
  headers: header,
};

/** Action Types */
export const SET_SERVICE_LIST = "set-service-list";
/**
 * Load Service List
 */
export const loadServiceList = (inputData, callback) => {
  
  let url = createPlatformURL("seller/getSellers");
  return (dispatch) => {
    axios
      .post(url, inputData, options)
      .then((res) => {
        
        callback && callback();
        dispatch(setServiceList(res.data));
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const setServiceList = (serviceList, value) => {
  return {
    type: SET_SERVICE_LIST,
    payload: serviceList,
    value,
  };
};

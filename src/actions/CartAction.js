import { updateSubTotal } from "../utils/common";

export const UPDATE_CART = "update-cart";
export const RESET_CART = "reset-cart";

export const updateCartObject = (data) => {
  return {
    type: UPDATE_CART,
    payload: data,
  };
};

export const resetCartObject = () => {
  return {
    type: RESET_CART,
  };
};

function deepEqual(object1, object2) {
  const keys1 = Object.keys(object1);
  const keys2 = Object.keys(object2);

  if (keys1.length !== keys2.length) {
    return false;
  }

  for (const key of keys1) {
    const val1 = object1[key];
    const val2 = object2[key];
    const areObjects = isObject(val1) && isObject(val2);
    if (
      areObjects && !deepEqual(val1, val2) ||
      !areObjects && val1 !== val2
    ) {
      return false;
    }
  }

  return true;
}

function isObject(object) {
  return object != null && typeof object === 'object';
}


export const updateCartItem = (cartList, data, quantity) => {
  //  
  var cart = cartList.find((o) => o.productId === data.productId)

  if (cart.productModifierList?.modifierProductList?.length >= 0) {
    var m1 = []
    if (data.productModifierList?.length > 0) {
      data.productModifierList.map((modifier) => {
        modifier.modifierProductList.map((o, index) => {
          if (o.isChecked === true) {
            m1.push(o);
          }
        })
      })
    } else {
    
      m1 = data.productModifierList.modifierProductList
    }

    var select = m1 === undefined ? data : { ...data, productModifierList: { modifierProductList: m1 } }

    var cartitem = cartList.filter((o) => o.productId === cart.productId)
    if (cartitem.length > 1 && m1?.length > 0) {
      var i;
      cartitem.map((k) => {
        if (deepEqual(k.productModifierList, select.productModifierList)) {
          if(k.selectedProduct){
            k.selectedProduct.quantity =quantity >= k.quantity?k.selectedProduct.quantity+1: k.selectedProduct.quantity - 1;
          }
          k.quantity = quantity >= k.quantity ? k.quantity + 1 : k.quantity - 1;

        }
      })

    } else {
      cartitem.map((k) => {
        if (deepEqual(k.productModifierList, select.productModifierList)) {
          if(k.selectedProduct){
            k.selectedProduct.quantity =quantity >= k.quantity?k.selectedProduct.quantity+1: k.selectedProduct.quantity - 1;
          }
          k.quantity = quantity >= k.quantity ? k.quantity + 1 : k.quantity - 1;
        }
      })

    }
  } else {
    cart.quantity = quantity;
  }
  const m = cartList.map((item) => {
    var subItemList = [];
    
    subItemList= item.productModifierList?.modifierProductList?.map((adons) => {
      return {
        "productId": adons.productId,
        "productCode": adons.productCode,
        "maxCount": adons.maxCount,
        "minCount": adons.minCount,
        "defaultCount": adons.defaultCount,
        "sequence": adons.sequence,
        "displayName": adons.productName.displayName,
        "price": adons.productName.price,
        "isVegetarian": adons.productName.isVegetarian,
        "itemId": adons.productId,
        "quantity":item.quantity,
        "itemType": 1,
        "ofs": false,
        "disable": false,
        "parentName": "Addon Chai",
      }
    });

    return {
      itemId: item.productId,
      itemName: item.displayName,
      itemType: 1,
      price: item.price,
      quantity: item.quantity,
      parentId: 0,
      subItems: subItemList,
    };
  });

  return (dispatch) => {
    dispatch(
      updateCartObject({
        cartItems: cartList,
        CreateCart: m,
        subTotal: updateSubTotal(cartList),
      })
    );
  };
};

export const removeCartItem = (cartList, data) => {
  let requiredIndex;

  var cart = cartList.find((element) => element.productId === data.productId)

  if (cart.productModifierList?.modifierProductList?.length > 0) {

    var m1 = cartList.findIndex((element) => element.productId === data.productId &&
      deepEqual(element.productModifierList, data.productModifierList))

    requiredIndex = m1;
    cartList.splice(requiredIndex, 1);

  } else {
    var m1 = cartList.findIndex((element, index) => element.productId === data.productId)
    requiredIndex = m1
    cartList.splice(requiredIndex, 1);
  }
  const m = cartList.map((item) => {
    return {
      "discount": 0,
      "itemId": item.productId,
      "itemName": item.displayName,
      "itemType": 1,
      "price": item.price,
      "quantity": item.quantity,
      "parentId": 0,
      "subItems": [],
    };
  });
  return (dispatch) => {
    dispatch(
      updateCartObject({
        cartItems: cartList,
        CreateCart: m,
        subTotal: updateSubTotal(cartList),
      })
    );
  };
};
export const emptyCartItem = (cartList, data) => {
  let requiredIndex;
  cartList.forEach((element, index) => {
    if (
      element.productId === data.productId &&
      element.productAddonList === data.productAddonList
    ) {
      requiredIndex = index;
      return;
    }
  });
  cartList.splice(requiredIndex);

  return (dispatch) => {
    dispatch(
      updateCartObject({
        cartItems: [],
        CreateCart:[],
        subTotal: updateSubTotal(cartList),
      })
    );
  };
};

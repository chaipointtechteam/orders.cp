import axios from "axios";
//HC imports
import {
  createInternalPlatformURL,
  handleSecureAjaxError,
} from "../../utils/apiUtil";
import { businessUnit } from "../../utils/constants";
import { isMobile } from "react-device-detect";

/**
 * Load Store List
 */
export const getFooter = (callback) => {
  let url = createInternalPlatformURL("footer.json");
  return () => {
    axios
      .post(url, {
        business_unit: businessUnit,
      })
      .then((res) => {
        if (res.data.result.status === "success") {
          callback && callback(res.data.result.data);
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const getBannerImage = (callback, business_unit) => {
  let url = createInternalPlatformURL("banner.json");

  return () => {
    axios
      .post(url, {
        business_unit: business_unit,
      })
      .then((res) => {
        var bannerUrl = res.data.result.data;
        let media_path = res.data.result.media_path;
        // if (res.data.result.status === "success") {
        //   bannerUrl = isMobile
        //     ? res.data.result.media_path + res.data.result.data[0].mobile_banner
        //     : res.data.result.media_path + res.data.result.data[0].banner;
        // }

        callback && callback(bannerUrl, media_path);
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const getMarkettingOffers = (callback) => {
  let url = createInternalPlatformURL("marketingOffer.json");
  return () => {
    axios
      .post(url, {
        business_unit: businessUnit,
      })
      .then((res) => {
        if (res.data.result.status === "success") {
          callback &&
            callback(res.data.result.data, res.data.result.media_path);
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};
export const getproductAds = (callback) => {
  let url = createInternalPlatformURL("productAds.json");
  return () => {
    axios
      .post(url, {
        business_unit: businessUnit,
      })
      .then((res) => {
        if (res.data.result.status === "success") {
          callback &&
            callback(res.data.result.data, res.data.result.media_path);
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const subscribeLetter = (email, callback) => {
  let url = createInternalPlatformURL("subscribeNewsletter.json");
  return () => {
    axios
      .post(url, {
        business_unit: businessUnit,
        email: email,
      })
      .then((res) => {
        if (res.data.result.status === "success") {
          callback && callback(res.data.result.message);
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};
export const getCMS = (menuId, callback) => {
  let url = createInternalPlatformURL("cms.json");
  return () => {
    axios
      .post(url, {
        business_unit: businessUnit,
        menu_id: menuId,
      })
      .then((res) => {
        if (res.data.result.status === "success") {
          callback && callback(res.data.result);
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const getCMSShop = (menuId, business_unit, callback) => {
  let url = createInternalPlatformURL("cms.json");
  return () => {
    axios
      .post(url, {
        business_unit: business_unit,
        menu_id: menuId,
      })
      .then((res) => {
        if (res.data.result.status === "success") {
          callback && callback(res.data.result);
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

export const MenuItem = (business_unit, callback) => {
  let url = createInternalPlatformURL("menu.json");
  return () => {
    axios
      .post(url, {
        business_unit: business_unit,
        // menu_id: menuId
      })
      .then((res) => {
        if (res.data.result.status === "success") {
          callback && callback(res.data.result);
        }
      })
      .catch((error) => {
        handleSecureAjaxError(error, "ServiceActions loadService");
        callback && callback();
      });
  };
};

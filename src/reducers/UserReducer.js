import { UPDATE_USER, RESET_USER } from "../actions/UserAction";
import { CLEAR_AUTH, SAVE_PROFILE } from "../actions/LoginAction";
import { UPDATE_SELECTED_CHANNEL, UPDATE_SELECTED_STORE } from '../actions/StoreListAction';
import { UPDATE_ADDRESS } from "../actions/UserAction";

export const INITIAL_STATE = {
  isLoggedIn: false,
  selectedChannel: null,
  selectedStore: null,
  userObject: {
    showLoginPopup: false,
    showSignUpPopup: false,
    phoneNo: "",
    headerSessionId: "",
  },
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CLEAR_AUTH:
      return INITIAL_STATE;

    case UPDATE_USER:
      return {
        ...state,
        userObject: Object.assign({}, state.userObject, action.payload),
      };

    case SAVE_PROFILE:
      return {
        ...state,
        isLoggedIn: true,
        userObject: { ...state.userObject, ...action.payload },
      };

    case UPDATE_SELECTED_CHANNEL:
      return {
        ...state,
        selectedChannel: action.payload,
      };

    case UPDATE_SELECTED_STORE:
      return {
        ...state,
        selectedStore: action.payload,
      };

     case UPDATE_ADDRESS:
      
     return{
        ...state,
        userObject:action.payload
     };

    case RESET_USER:
      return {
        ...state,
        isLoggedIn: false,
        userObject: Object.assign(
          {},
          state.userObject,
          INITIAL_STATE.userObject
        ),
      };

    default:
      return state;
  }
};

import { combineReducers } from "redux";
import StoreListReducer from "./StoreListReducer";
import ServiceListReducer from "./ServiceListReducer";
import UserReducer from "./UserReducer";
import ChannelReducer from "./ChannelReducer";
import StoreReducer from "./StoreReducer";
import LocationReducer from "./LocationReducer";
import MenuItemsReducer from "./MenuItemsReducer";
import CategoryMenuReducer from "./CategoryMenuReducer";
import FilteredItemsReducer from "./FilteredItemsReducer";
import FilteredObjectReducer from "./FilteredObjectReducer";
import WalletReducer from "./WalletReducer";
import CouponReducer from "./CouponReducer";
import CartReducer from "./CartReducer";
import PayReducer from "./PayReducer";
import commonReducer from "./common.reducer";
import footerReducer from "./footerReducer";

import HomeReducer from "./HomeReducer";
import CareerLanding from "./CareerLanding";
import JobDetailsReducer from "./JobDetailsReducer";
import BlogsReducer from "./BlogsReducer";

export const reducers = combineReducers({
  storeList: StoreListReducer,
  channelList: ServiceListReducer,
  userObject: UserReducer,
  selectedChannel: ChannelReducer,
  selectedStore: StoreReducer,
  selectedLocation: LocationReducer,
  menuItemList: MenuItemsReducer,
  PayObject: PayReducer,
  categoryList: CategoryMenuReducer,
  filteredItemList: FilteredItemsReducer,
  filteredObject: FilteredObjectReducer,
  cartObject: CartReducer,
  CouponObject: CouponReducer,
  Wallet: WalletReducer,
  common: commonReducer,
  footer: footerReducer,

  home: HomeReducer,
  careerLanding: CareerLanding,
  jobDetails: JobDetailsReducer,
  blogs: BlogsReducer,
});

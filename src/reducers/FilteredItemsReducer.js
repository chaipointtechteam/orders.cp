import { SET_FILTERED_ITEM_LIST } from "../actions/FilterItemsAction";

export const INITIAL_STATE = {
  filteredItemList: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_FILTERED_ITEM_LIST:
      return {
        ...state,
        filteredItemList: action.payload,
      };
    default:
      return state;
  }
};

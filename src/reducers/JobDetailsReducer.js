const INITIAL_STATE = {
  menuItems: [],
  jobDetailLoading: true,
};

const HomeReducer = (state = INITIAL_STATE, action) => {
  const { payload, type } = action;
  switch (type) {
    case "SET_MENU_ITEMS":
      return {
        ...state,
        menuItems: payload.menuItems,
        bannerText:
          'Description of the "Lorem ipsum dolor sit amet" text that appears in Word Help',
      };

    case "SET_JOB_LOADING":
      return {
        ...state,
        jobDetailLoading: payload.jobDetailLoading,
      };

    default:
      return state;
  }
};

export default HomeReducer;

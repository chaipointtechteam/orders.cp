import {
  UPDATE_SELECTED_LOCATION,
  RESET_LOCATION,
} from "../actions/StoreListAction";

export const INITIAL_STATE = {
  selectedLocation: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_SELECTED_LOCATION:
      return {
        ...state,
        selectedLocation: action.payload,
      };
    case RESET_LOCATION:
      return {
        ...state,
        selectedChannel: Object.assign(
          {},
          state.selectedLocation,
          INITIAL_STATE.selectedLocation
        ),
      };
    default:
      return state;
  }
};

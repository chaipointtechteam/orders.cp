import { SET_SERVICE_LIST } from "../actions/ChannelListAction";

export const INITIAL_STATE = {
  channelList: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_SERVICE_LIST:
      return {
        ...state,
        channelList: action.payload,
      };
    default:
      return state;
  }
};

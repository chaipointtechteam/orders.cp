import { faBullseye } from "@fortawesome/free-solid-svg-icons";
import React from "react";
import {
  UPDATE_COUPON,
  REMOVE_COUPON,
  APPLY_COUPON,
  UPDATE_REFRESHCART,
  UPDATE_PRODUCT,
  REMOVE_PRODUCT,
  REMOVE_APPLY_COUPON,
  REMOVE_REFRESHCART
} from "../actions/CouponAction";
export const INITIAL_STATE = {
  isUse: false,
  CouponObject: "",
  selectedProduct: "",
  refreshCart: ""
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_COUPON:
      return {
        ...state,
        CouponObject: action.payload,
      };
    case UPDATE_PRODUCT:
      return {
        ...state,
        selectedProduct: action.payload
      }
    case REMOVE_PRODUCT:
      return {
        ...state,
        selectedProduct: ""
      }
    case REMOVE_COUPON:
      return {
        ...state,
        isUse: false,
        CouponObject: "",
      };

    case UPDATE_REFRESHCART:
      return {
        ...state,
        refreshCart: action.payload
      }

    case REMOVE_REFRESHCART:
      return {
        ...state,
        refreshCart: ""
      }

    case APPLY_COUPON:
      
      return {
        ...state,
        isUse: true,
      };

    case REMOVE_APPLY_COUPON:
      return {
        ...state,
        isUse: false,
      };

    default:
      return state;
  }
};

import { ABOUT_US, PARTNERS, PRIVACYPOLICY, UPDATE_TANDC } from "../actions/FooterAction";
export const INITIAL_STATE = {
  TandC: [],
  aboutUs: [],
  privacyPolicy: [],
  stateus: { TandC: false, aboutUs: false, privacyPolicy: false },
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_TANDC:
      return {
        ...state,
        TandC: Object.assign({}, action.payload),
        stateus:{ TandC: true, aboutUs: false, privacyPolicy: false }
      };
    case ABOUT_US:
      return {
        ...state,
        aboutUs: Object.assign({}, action.payload),
        stateus:{ TandC: false, aboutUs: true, privacyPolicy: false }
      };
    case PRIVACYPOLICY:
      return {
        ...state,
        privacyPolicy: Object.assign({}, action.payload),
        stateus:{ TandC: false, aboutUs: false, privacyPolicy: true }
      };
      case PARTNERS:
        return {
          ...state,
          partners: Object.assign({}, action.payload),
          stateus:{ TandC: false, aboutUs: false, privacyPolicy: true }
        };
    default:
      return state;
  }
};

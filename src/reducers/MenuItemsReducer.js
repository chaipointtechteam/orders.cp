import { SET_MENU_ITEM_LIST } from "../actions/MenuItemsAction";

export const INITIAL_STATE = {
  menuItemList: [],
  inventory: []
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_MENU_ITEM_LIST:
      return {
        ...state,
        menuItemList: action.payload,
      };
    default:
      return state;
  }
};

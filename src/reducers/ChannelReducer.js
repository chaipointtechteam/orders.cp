import {
  UPDATE_SELECTED_CHANNEL,
  RESET_CHANNEL,
} from "../actions/StoreListAction";

export const INITIAL_STATE = {
  selectedChannel: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_SELECTED_CHANNEL:
      return {
        ...state,
        selectedChannel: action.payload,
      };
    case RESET_CHANNEL:
      return {
        ...state,
        selectedChannel: {}
        // selectedChannel: Object.assign(
        //   {},
        //   state.selectedChannel,
        //   INITIAL_STATE.selectedChannel
        // ),
      };
    default:
      return state;
  }
};

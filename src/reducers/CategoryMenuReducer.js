import { SET_CATEGORY_MENU_LIST } from "../actions/MenuItemsAction";

export const INITIAL_STATE = {
  categoryList: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_CATEGORY_MENU_LIST:
      return {
        ...state,
        categoryList: action.payload,
      };
    default:
      return state;
  }
};

import {
  SHOW_ALERT, HIDE_ALERT, SHOW_SNACKBAR, HIDE_SNACKBAR, REFRESH, AGENT
} from '../actions/common.actions';

export default function CommonReducer(state = {
  loader: false, alert: { showAlert: false }, sideNav: false, error: null
}, action) {
  switch (action.type) {
    case 'LOADER_VIEW':
      return { ...state, loader: action.payload };
    case SHOW_ALERT:
      return {
        ...state,
        alert: {
          ...action.payload,
          showAlert: true
        }
      };

    case HIDE_ALERT:
      return {
        ...state,
        alert: {
          showAlert: false
        }
      };

    case SHOW_SNACKBAR:
      return {
        ...state,
        snackbar: {
          ...action.payload,
          show: true
        }
      };

    case HIDE_SNACKBAR:
      return {
        ...state,
        snackbar: {
          ...state.snackbar,
          show: false
        }
      };

    case REFRESH:
      return {
        ...state,
        refresh: action.payload
      };

    case AGENT:
      return {
        ...state,
        agent: action.payload
      };
    case 'ERROR_HANDLER':
      return { ...state, error: action.payload };
    default:
      return state;
  }
}

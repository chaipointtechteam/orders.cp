const INITIAL_STATE = {
  blogs: [],
  menuItems: [],
};

const BlogsReducer = (state = INITIAL_STATE, action) => {
  const { payload, type } = action;

  switch (type) {
    case "SET_BLOGS":
      return { ...state, blogs: payload.blogs, media_path: payload.media_path };

    case "SET_MENU_ITEMS":
      return { ...state, menuItems: payload.menuItems };

    default:
      return state;
  }
};

export default BlogsReducer;

import React from "react";
import {
  UPDATE_WALLET,
  REMOVE_WALLET,
  UPDATE_AMOUNT,
} from "../actions/WalletAction";
export const INITIAL_STATE = {
  isUse: false,
  walletAmount: "",
  useAmount: "",
  useasdjk: "",
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_WALLET:
      return {
        ...state,
        isUse: true,
        walletAmount: action.payload,
      };
    case REMOVE_WALLET:
      return {
        ...state,
        isUse: false,
        walletAmount: "",
      };
    case UPDATE_AMOUNT:
      return {
        ...state,
        useAmount: action.payload,
      };

    default:
      return state;
  }
};

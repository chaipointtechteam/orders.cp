import { UPDATE_CART, RESET_CART } from "../actions/CartAction";

export const INITIAL_STATE = {
  cartObject: {
    cartItems: [],
    CreateCart: [],
    subTotal: 0,
  },
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case UPDATE_CART:

      return {
        ...state,
        cartObject: Object.assign({}, state.cartObject, action.payload),
      };

    case RESET_CART:
      return {
        ...state,
        cartObject: Object.assign(
          {},
          state.cartObject,
          INITIAL_STATE.cartObject
        ),
      };
    default:
      return state;
  }
};

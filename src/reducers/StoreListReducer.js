import { GET_STORE_LIST } from "../actions/StoreListAction";

export const INITIAL_STATE = {
  storeList: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_STORE_LIST:
      return {
        ...state,
        storeList: action.payload,
      };
    default:
      return state;
  }
};

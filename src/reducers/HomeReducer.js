const INITIAL_STATE = {
  menuItems: [],
  banners: [{ banner: "" }],
  media_path: "",
  bannerText:
    'Description of the "Lorem ipsum dolor sit amet" text that appears in Word Help',
  productLaunches: [],
  orderData: {},
  shopData: {},
  vaasData: {},
  brandValues: [],
  storeLaunches: [],
  socialImpact: [],
  partnersTrainig: [],
  homeLoading: true,
};

const HomeReducer = (state = INITIAL_STATE, action) => {
  const { payload, type } = action;
  switch (type) {
    case "SET_MENU_ITEMS":
      return {
        ...state,
        menuItems: payload.menuItems,
        bannerText:
          'Description of the "Lorem ipsum dolor sit amet" text that appears in Word Help',
      };

    case "SET_BANNERS":
      return {
        ...state,
        banners: payload.banners,
        media_path: payload.media_path,
      };

    case "SET_PRODUCT_LAUNCHES":
      return {
        ...state,
        productLaunches: payload.productLaunches,
        productLaunchMediaPath: payload.productLaunchMediaPath,
      };

    case "SET_HDWSC_BANNERS":
      return {
        ...state,
        orderData: payload.orderData,
        shopData: payload.shopData,
        vaasData: payload.vaasData,
        hdwscMediaPath: payload.hdwscMediaPath,
      };

    case "SET_BRAND_VALUE":
      return {
        ...state,
        brandValues: payload.brandValues,
      };

    case "SET_STORE_LAUNCHES":
      return {
        ...state,
        storeLaunches: payload.storeLaunches,
      };

    case "SET_SOCIAL_IMPACT":
      return {
        ...state,
        socialImpact: payload.socialImpact,
      };

    case "SET_PARTNER_TRAINING":
      return {
        ...state,
        partnersTrainig: payload.partnersTrainig,
      };

    case "SET_LOADING":
      return {
        ...state,
        homeLoading: payload.homeLoading,
      };

    default:
      return state;
  }
};

export default HomeReducer;

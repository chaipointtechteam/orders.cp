const INITIAL_STATE = {
  menuItems: [],
  careerLoading: true,
  jobCategories: [],
  jobLocations: [],
};

const CareerLandingReducer = (state = INITIAL_STATE, action) => {
  const { payload, type } = action;

  switch (type) {
    case "SET_MENU_ITEMS":
      return { ...state, menuItems: payload.menuItems };

    case "SET_CAREER_LOADING":
      return {
        ...state,
        careerLoading: payload.careerLoading,
      };

    case "SET_CATEGORIES":
      return {
        ...state,
        jobCategories: payload.jobCategories,
      };

    case "SET_JOBS":
      return {
        ...state,
        jobs: payload.jobs,
      };

    case "SET_JOB_LOCATIONS":
      return {
        ...state,
        jobLocations: payload.jobLocations,
      };

    default:
      return state;
  }
};

export default CareerLandingReducer;

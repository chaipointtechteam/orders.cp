import {
  UPDATE_FILTER_OBJECT,
  RESET_FILTER,
} from "../actions/FilterItemsAction";

export const INITIAL_STATE = {
  //default filter
  filteredObject: {
    minPrice: null,
    maxPrice: null,
    isVeg: false,
    isNonVeg: false,
    searchQuery: "",
    activeKey: 0,
    selectedCategory: null,
    selectedSubCategory: null,
    isSubCategory: false,
  },
};

export default (state = INITIAL_STATE, action) => {
      
  switch (action.type) {
    case UPDATE_FILTER_OBJECT:
      return {
        ...state,
        filteredObject:{...state.filteredObject, ...action.payload}
      };
    case RESET_FILTER:
      return {
        ...state,
        filteredObject: Object.assign(
          {},
          state.filteredObject,
          INITIAL_STATE.filteredObject
        ),
      };
    default:
      return state;
  }
};

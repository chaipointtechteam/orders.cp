import { UPDATE_SELECTED_STORE, RESET_STORE } from "../actions/StoreListAction";

export const INITIAL_STATE = {
  selectedStore: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_SELECTED_STORE:
      return {
        ...state,
        selectedStore: action.payload,
      };
    case RESET_STORE:
      return {
        ...state,
        selectedStore: Object.assign(
          {},
          state.selectedStore,
          INITIAL_STATE.selectedStore
        ),
      };
    default:
      return state;
  }
};

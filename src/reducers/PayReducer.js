import React from "react";
import {
    UPDATE_PAY,
    REMOVE_PAY,

} from "../actions/PayAction";
export const INITIAL_STATE = {
    pay: {},
    name: ""
};
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UPDATE_PAY:
            return {
                ...state,
                pay: action.payload
            };
        case REMOVE_PAY:
            return {
                ...state,
                pay: {}
            };
        default:
            return state;
    }
}
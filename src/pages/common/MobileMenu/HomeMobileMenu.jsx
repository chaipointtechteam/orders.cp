import React, { useEffect, useState } from "react";
import _ from "lodash";
import { Drawer } from "antd";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  getCMS,
  getFooter,
  MenuItem,
  subscribeLetter,
} from "../../../actions/internalActions/LandingActions";

import "antd/lib/drawer/style/index.css";
import "./style.css";

const paths = {
  "chaiPoint.com": {
    "Contact Us": "Contact",
    "Privacy Policy": "PrivacyPolicy",
    "Terms  & Conditions": "TearmAndConditions",
    Partners: "Partners",
    "Help/FAQs": "FAQ",
    "About us": "aboutus",
    Careers: "Careers",
  },

  "shop.chaipoint.com": {
    "Contact Us": "Contact",
    "Privacy Policy": "PrivacyPolicy",
    "Terms  & Conditions": "TearmAndConditions",
    Partners: "Partners",
    "Help/FAQs": "FAQ",
    "About us": "aboutus",
    Careers: "Careers",
  },

  "order.chaipoint.com": {
    "Contact Us": "Contact",
    "Privacy Policy": "PrivacyPolicy",
    "Terms  & Conditions": "TearmAndConditions",
    Partners: "Partners",
    "Help/FAQs": "FAQ",
    "About us": "aboutus",
    Careers: "Careers",
  },
};

const MenuBar = (props) => {
  const [menuItems, setMenuItems] = useState([]);
  const [BU, setBU] = useState("");

  useEffect(() => {
    var BU = "order.chaipoint.com";
    var state = window.location.href.split("/");
    var index = state.length - 1;
    if (state[index] == "home") {
      BU = "chaiPoint.com";
    } else if (state[index] == "Shop") {
      BU = "shop.chaipoint.com";
    } else {
      BU = "order.chaipoint.com";
    }
    props.MenuItem(BU, (data) => {
      if (data) {
        setMenuItems(data.data);
        setBU(BU);
      }
    });
  }, []);

  const companyMenus = _.find(menuItems, ["name", "Company"]);
  const legalMenus = _.find(menuItems, ["name", "Legal"]);

  return (
    <Drawer
      className="drawer-items"
      title={`Hi, ${_.get(props, "userObject.name", "")}`}
      placement="left"
      visible={props.showMenu}
      onClose={props.onClose}
    >
      <section className="menu-section">
        {_.map(_.get(companyMenus, "child_menus", []), (ele) => (
          <Link to={`/${_.get(paths, [BU, ele.name])}`}>{ele.name}</Link>
        ))}
      </section>
      <section className="menu-section">
        {_.map(_.get(legalMenus, "child_menus", []), (ele) => (
          <Link to={`/${_.get(paths, [BU, ele.name])}`}>{ele.name}</Link>
        ))}
      </section>
    </Drawer>
  );
};

export default connect(null, {
  getFooter,
  subscribeLetter,
  getCMS,
  MenuItem,
})(withRouter(MenuBar));

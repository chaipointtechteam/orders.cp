import $ from "jquery";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getCMS } from "../../actions/internalActions/LandingActions";
import Logo from "../../assets/images/logo.png";
import Footer from "../../components/common/footer";
import LinkHeader from "../../components/common/linkHeader";
import ReactHelment from "../../components/common/ReactHelment";

function AboutUs(props) {
  const [Data, setData] = useState({});
  const [basePath, setBasePath] = useState("");
  const [check, setcheck]=useState(false)
  const cmsAboutUs = async () => {
    var menuId = 28;
    await props.getCMS(menuId, (data) => {
      if (data) {
        var basepath2 = `${data.base_path}${data.data[data.data.length - 1].media_path}`;
        setBasePath(data.base_path);
        setData(data.data[data.data.length - 1]);
        setcheck(true)
        var imagedata = [];
        var cssdata = [];
        var url = window.location.href.split("/");


        JSON?.parse(data.data[data.data.length - 1].media)?.image?.map(
          (img) => {
            imagedata.push(img);

          }
        );
        JSON?.parse(data.data[data.data.length - 1].media)?.css?.map((css) =>
          cssdata.push(css)
        );

        imagedata.map((img) => {
          $("section")
            .siblings()
            .each(function (index) {
              if (this.style.backgroundImage === `url("${img}")`) {
                this.style.backgroundImage = `url(${basepath2}${img}`;
              } else {
              }
            });
          $("div")
            .siblings()
            .each(function (index) {
              if (this.style.backgroundImage === `url("${img}")`) {
                this.style.backgroundImage = `url(${basepath2}${img}`;
              } else {
              }
            });
          $("img").each(function (index) {
            if (this.src === `${url[0]}//${url[2]}/${img}`) {
              this.src = `${basepath2}${img}`;
            } else {
            }
          });
        });

        cssdata.map((cssItem) => {
          $("link").each(function (index) {
            if (this.href === `${url[0]}//${url[2]}/${cssItem}`) {
              this.href = `${data.base_path}${data.data[data.data.length - 1].media_path
                }${cssItem}`;
            }
          });
        });
      }
    });
  };
  useEffect(() => {
    cmsAboutUs();
  }, []);

  return (
    <>
      <div>
      { check &&  <ReactHelment title={Data.meta_title} description={Data.meta_keyword}/>}
        <section className="service-banner headerFooter">

          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="col-md-6 col-sm-6 col-xs-12 HeaderHeight">
                  <div
                    className={
                      props.userObject.headerSessionId !== ""
                        ? "logo-after-login logo-display"
                        : "logo-display"
                    }
                  >
                    <Link to="/">
                      <img src={Logo} alt="Chai point" />
                    </Link>
                  </div>
                </div>
                <div style={{ marginTop: "10px" }}>
                  <LinkHeader />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <div
        id="mm"
        class="ee"
        dangerouslySetInnerHTML={{ __html: Data?.content }}
      ></div>
      <Footer />
    </>
  );
}
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, {
  getCMS,
})(AboutUs);

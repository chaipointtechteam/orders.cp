import $ from "jquery";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  getCMS,
  getCMSShop,
} from "../../actions/internalActions/LandingActions";
import Logo from "../../assets/images/logo.png";
import ReactHelment from "../../components/common/ReactHelment";
import { MenuOutlined, SearchOutlined } from "@ant-design/icons";
// import Logo1 from "../../../assets/images/chaipointMobile.svg";
import Logo1 from "../../../src/assets/images/chaipointMobile.svg";
import Basket from "../../../src/assets/images/basket.svg";

import MenuList from "../../pages/common/MobileMenu/HomeMobileMenu";
// import MenuList from "../../common/MobileMenu/HomeMobileMenu";

import LandingFooter from "../../components/landing/LandingFooter";

function AboutUsMobile(props) {
  const [Data, setData] = useState({});
  const [basePath, setBasePath] = useState("");
  const [check, setcheck] = useState(false);
  const [showMenu, setShowMenu] = useState(false);
  // const [BU,setBU] =useState("")
  const cmsAboutUs = async () => {
    var menuId = 26;
    var BU;
    var state = window.location.href.split("/");
    var index = state.length - 1;
    if (state[index] == "home") {
      BU = "chaiPoint.com";
    } else if (state[index] == "Shop") {
      BU = "shop.chaipoint.com";
    } else {
      BU = "order.chaipoint.com";
    }
    console.log(BU);
    await props.getCMSShop(menuId, BU, (data) => {
      if (data) {
        var basepath2 = `${data.base_path}${
          data.data[data.data.length - 1].media_path
        }`;
        setBasePath(data.base_path);
        setData(data.data[data.data.length - 1]);
        setcheck(true);
        var imagedata = [];
        var cssdata = [];
        var url = window.location.href.split("/");

        JSON?.parse(data.data[data.data.length - 1].media)?.image?.map(
          (img) => {
            imagedata.push(img);
          }
        );
        JSON?.parse(data.data[data.data.length - 1].media)?.css?.map((css) =>
          cssdata.push(css)
        );

        imagedata.map((img) => {
          $("section")
            .siblings()
            .each(function (index) {
              if (this.style.backgroundImage === `url("${img}")`) {
                this.style.backgroundImage = `url(${basepath2}${img}`;
              } else {
              }
            });
          $("div")
            .siblings()
            .each(function (index) {
              if (this.style.backgroundImage === `url("${img}")`) {
                this.style.backgroundImage = `url(${basepath2}${img}`;
              } else {
              }
            });
          $("img").each(function (index) {
            if (this.src === `${url[0]}//${url[2]}/${img}`) {
              this.src = `${basepath2}${img}`;
            } else {
            }
          });
        });

        cssdata.map((cssItem) => {
          $("link").each(function (index) {
            if (this.href === `${url[0]}//${url[2]}/${cssItem}`) {
              this.href = `${data.base_path}${
                data.data[data.data.length - 1].media_path
              }${cssItem}`;
            }
          });
        });
      }
    });
  };
  useEffect(() => {
    console.log(props);
    cmsAboutUs();
  }, []);

  return (
    <>
      <div>
        {check && (
          <ReactHelment
            title={Data.meta_title}
            description={Data.meta_keyword}
          />
        )}
        {/* <section className="service-banner headerFooter">
          <div className="container"></div>
        </section> */}
        <header className="home-page-header">
          <MenuOutlined
            style={{ fontSize: "2.6em" }}
            onClick={() => setShowMenu(true)}
          />
          <img src={Logo1} className="mobile-logo" />
          <div className="options-wrapper">
            <img src={""} className="sub-options" />
            <img src={Basket} className="sub-options" />
          </div>
        </header>
      </div>
      <div
        className="about-us-mobile"
        id="mm"
        dangerouslySetInnerHTML={{ __html: Data?.content }}
      ></div>
      <LandingFooter tab="2" />
      <MenuList showMenu={showMenu} onClose={() => setShowMenu(false)} />
    </>
  );
}
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, {
  getCMS,
  getCMSShop,
})(AboutUsMobile);

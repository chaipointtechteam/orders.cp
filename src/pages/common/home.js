import React, {Component} from "react"

export default class Home extends Component {
 render() {
     
     return(<div>
         Home page
         <ul>
             <li>
                 <a href="/product-detail">Product details </a>
             </li>
             <li>
                 <a href="/product-list">Product List </a>
             </li>
             <li>
                 <a href="/login">Login</a>
             </li>
             <li>
                 <a href="/blog" >Blog </a>
             </li>
         </ul>
     </div>)
 }
}
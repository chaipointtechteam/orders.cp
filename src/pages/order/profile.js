import React, { Component } from "react";
import ProfileDetails from "../../components/profile/profileDetails";
import ProfileHeader from "../../components/profile/profileHeader";
import ProfileTabs from "../../components/profile/profileTabs";
import MobileProfile from "../order/MobileProfile";

export default class Profile extends Component {
  
  state = {
    dimensions:window.innerWidth
  }
  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    this.setState({dimensions:window.innerWidth})
  }

  render() {

      return (
        <>
          <div className="detailHeader">
            <ProfileHeader />
            <ProfileDetails />
            {
              this.state.dimensions > 760 && (
                <ProfileTabs history={this.props.location.state} />
              )
            }            
          </div>
          {
              this.state.dimensions < 760 && (
                <MobileProfile model={ this.props.location?.state?.model}/>
              )
          }          
        </>
      );     
  }
}

import React, { Component } from "react";
import { isMobileOnly } from "react-device-detect";
import { connect } from "react-redux";

import { BrowserRouter, Redirect, Switch, Route } from "react-router-dom";

import { updateUserObject } from "../../actions/UserAction";
import MobileLogin from "../../components/common/Login/mobileLogin";
import MobileSignUp from "../../components/common/Login/mobileSignUp";
import LoginModal from "../../components/landing/LoginModal";
import SignUpModal from "../../components/landing/SignUpModal";
import MobileProductDetails from "../../components/product/MobileProductDetails";
import MobileFilter from "../../components/products/MobileFilter";
import MobileOrderList from "../../components/profile/mobileTabs/MobileOrderList";
import Address from "../../components/profile/tabs/addresses";
import Payment from "../../components/profile/tabs/payments";
import Wallet from "../../components/profile/tabs/wallet";
import AboutUs from "../common/AboutUs";
import Partners from "../common/Partners";
import PrivacyPolicy from "../common/PrivacyPolicy";
import TandC from "../common/TandC";
import Contact from "../common/Contact";
import Delivery from "./checkout/components/Delivery";
import MobileCoupon from "./checkout/components/mobileCoupon";
import PaymentModal from "./checkout/components/PaymentModal";
import PaymentSuccess from "./checkout/components/PaymentSuccess";
import CareerLanding from "../mother/Careers";
import JobDetails from "../mother/JobDetails";
import Blogs from "../mother/Blogs";
import BlogDetail from "../mother/BlogDetail";

import Home from "../mother/Home";

import Checkout from "./checkout/container/checkout-container";
import Landing from "./landing";
import MobileCart from "./MobileCart";
import PrivateRoute from "./PrivateRouting";
import ProductCatlog from "./productCatlog";
import Profile from "./profile";
import Faq from "../common/Faq";

import Shop from "../Shop/shop";

class Routing extends Component {
  state = {
    TandcRout: "",
    aboutUsRoute: "",
    PrivacyPolicyRout: "",
    partnersRoute: "",
    // object:[{name:"aboutus",BU:"chaipoint.com",menuId:26},{name:"Partners",BU:"chaipoint.com",menuId:38}]
  };
  closePopUp = () => {
    this.props.updateUserObject({
      showLoginPopup: false,
      showSignUpPopup: false,
    });
  };

  closePopUpAndOpenLogin = () => {
    this.props.updateUserObject({
      showLoginPopup: true,
      showSignUpPopup: false,
    });
  };

  closePopUpAndOpenSignUp = () => {
    this.props.updateUserObject({
      showLoginPopup: false,
      showSignUpPopup: true,
    });
  };

  componentDidMount = () => {
    if (!isMobileOnly) {
      const { footer } = this.props;
      setTimeout(() => {
        if (footer.TandC?.status === "success") {
          var tandc =
            footer.TandC?.data?.url_key?.split("/")[
              footer.TandC?.data?.url_key?.split("/").length - 1
            ];
          this.setState({ TandcRout: tandc });
        }
        if (footer.privacyPolicy?.status === "success") {
          var privacypolicy =
            footer.privacyPolicy?.data?.url_key?.split("/")[
              footer.privacyPolicy?.data?.url_key?.split("/").length - 1
            ];
          this.setState({ PrivacyPolicyRout: privacypolicy });
        }
        if (footer.aboutUs?.status === "success") {
          var aboutUs =
            footer.aboutUs?.data?.url_key?.split("/")[
              footer.aboutUs?.data?.url_key?.split("/").length - 1
            ];
          this.setState({ aboutUsRoute: aboutUs });
        }
        if (footer.partners?.status === "success") {
          var partners =
            footer.partners?.data?.url_key?.split("/")[
              footer.partners?.data?.url_key?.split("/").length - 1
            ];
          this.setState({ partnersRoute: partners });
        }
        if (footer.Contact?.status === "success") {
          var Contact =
            footer.Contact?.data?.url_key?.split("/")[
              footer.Contact?.data?.url_key?.split("/").length - 1
            ];
          this.setState({ ContactRoute: Contact });
        }
        if (footer.Faq?.status === "success") {
          var Faq =
            footer.Faq?.data?.url_key?.split("/")[
              footer.Faq?.data?.url_key?.split("/").length - 1
            ];
          this.setState({ FaqRoute: Faq });
        }
      }, 500);
    }
  };
  render() {
    const { userObject = {}, common, footer } = this.props;
    const { showLoginPopup = false, showSignUpPopup = false } = userObject;
    const { loader, showAlert, error } = common;

    return (
      <div>
        {/* {loader ? <Loader /> : []} */}

        <BrowserRouter>
          <LoginModal
            showModal={showLoginPopup}
            closePopUp={this.closePopUp}
            closePopUpAndOpenSignUp={this.closePopUpAndOpenSignUp}
          />
          <SignUpModal
            showModal={showSignUpPopup}
            closePopUp={this.closePopUp}
            closePopUpAndOpenLogin={this.closePopUpAndOpenLogin}
          />
          <Switch>
            <Route exact path="/Home" component={Home} />
            <Route exact path="/Careers" component={CareerLanding} />
            <Route exact path="/job-details" component={JobDetails} />
            <Route exact path="/blogs" component={Blogs} />
            <Route exact path="/blog-detail" component={BlogDetail} />

            {/* <Route exact path={`${this.state.PrivacyPolicyRout}`} component={PrivacyPolicy} />
        
            */}
            {/* { <div>
              {loader ? <Loader /> : []} */}
            <PrivateRoute exact path="/" Simple={true} component={Landing} />
            {/* </div>}  */}

            {/* <Route exact path="/products" component={ProductCatlog} />
            <Route exact path="/profile" component={Profile} />
            <Route exact path="/checkout" component={Checkout} /> */}
            <Route exact path="/cart" component={MobileCart} />
            <PrivateRoute
              exact
              path="/productDetails"
              component={MobileProductDetails}
            />
            {this.props.userObject.headerSessionId !== "" ? (
              <PrivateRoute
                exact
                path={"/profile"}
                Simple={true}
                component={Profile}
              />
            ) : (
              ""
            )}
            <PrivateRoute exact path={"/products"} component={ProductCatlog} />
            <PrivateRoute exact path={"/payment"} component={Payment} />
            <PrivateRoute exact path={"/filter"} component={MobileFilter} />
            <PrivateRoute exact path={"/wallet"} component={Wallet} />
            {this.props.userObject.headerSessionId === "" ? (
              <PrivateRoute
                exact
                path={"/login"}
                Simple={true}
                component={MobileLogin}
              />
            ) : (
              ""
            )}

            {this.props.userObject.headerSessionId === "" ? (
              <PrivateRoute
                exact
                path={"/signUp"}
                Simple={true}
                component={MobileSignUp}
              />
            ) : (
              ""
            )}

            {this.props.cartObject.cartItems.length !== 0 ? (
              <PrivateRoute
                exact
                path={"/checkout"}
                loadChange={true}
                component={Checkout}
              />
            ) : (
              <Redirect from="/checkout" to="/products" />
            )}
            <PrivateRoute
              exact
              path={"/payunsuccess"}
              component={PaymentModal}
            />
            <PrivateRoute
              exact
              path={"/paysuccess"}
              component={PaymentSuccess}
            />

            <PrivateRoute
              exact
              path={"/orders"}
              name={true}
              component={MobileOrderList}
            />
            <PrivateRoute
              exact
              path={"/address"}
              name={true}
              component={Address}
            />
            <PrivateRoute exact path={"/coupon"} component={MobileCoupon} />
            <PrivateRoute exact path={"/addressChange"} component={Delivery} />

            <PrivateRoute
              Simple={true}
              exact
              path={"/aboutus"}
              component={AboutUs}
            />
            <PrivateRoute
              Simple={true}
              exact
              path={"/TearmAndConditions"}
              component={TandC}
            />
            <PrivateRoute
              Simple={true}
              exact
              path={"/PrivacyPolicy"}
              component={PrivacyPolicy}
            />
            <PrivateRoute
              Simple={true}
              exact
              path={"/Partners"}
              component={Partners}
            />
            <PrivateRoute
              Simple={true}
              exact
              path={"/Contact"}
              component={Contact}
            />
            <PrivateRoute Simple={true} exact path={"/FAQ"} component={Faq} />

            <PrivateRoute Simple={true} exact path={"/Shop"} component={Shop} />

            <Redirect from="*" to="/" />
            {/* { !isMobileOnly ? <Route
                exact
                path={`${this.state.PrivacyPolicyRout}`}
                component={PrivacyPolicy}
              />:""}
              { !isMobileOnly ?  <Route
                exact
                path={`${this.state.aboutUsRoute}`}
                component={AboutUs}
              /> : ""}
              { !isMobileOnly ?  <Route
                exact
                path={`${this.state.partnersRoute}`}
                component={AboutUs}
              />:""} */}
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const selectedChannel = reducerObj.selectedChannel.selectedChannel;
  const selectedStore = reducerObj.selectedStore.selectedStore;
  const cartObject = reducerObj.cartObject.cartObject;
  const footer = reducerObj.footer;
  return {
    userObject,
    selectedChannel,
    selectedStore,
    cartObject,
    footer,
    common: reducerObj.common,
  };
};

export default connect(mapStateToProps, { updateUserObject })(Routing);

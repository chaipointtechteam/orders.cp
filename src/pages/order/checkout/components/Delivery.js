import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";
import { getProfile } from "../../../../actions/LoginAction";
import { updateAddress, updateUserObject } from "../../../../actions/UserAction";
import ProfileService from "../../../../utils/services/profile/profile-service";
import styles from "../styles/checkout.module.scss";
// map-marker-alt
class Footer extends Component {
  state = { status: false, changeAddress: false };

  setDefault = (id) => {
    let currentComponent = this;

    var phoneNo = this.props.userObject.phoneNo;
    var sessionId = this.props.userObject.headerSessionId;
    ProfileService.defaultAddress(id, sessionId)
      .then((response) => {
        currentComponent.props.getProfile(`${phoneNo}`, sessionId);
      })
      .catch((error) => {});
  };

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }


getAddressofUser=()=>{
  const { addresses = [], onAddAdress } = this.props;
    const { userObject = {} } = this.props;
    const { customerAddress = [] ,headerSessionId} = userObject;
    const { selectedLocation = {} } = this.props
    let obj = localStorage.getItem('persist:ChaiPoint')
    let obj2 = JSON.parse(obj);
    let location = JSON.parse(obj2.selectedLocation)


    let mainLatitude = location.selectedLocation.selectedLocation?.geometry?.location.lat;
    let mainLongitude = location.selectedLocation.selectedLocation?.geometry?.location.lng
    let errorText = ""
    var m =[]
  customerAddress.map((address) => {
    let addressText = [
      "streetAddress1",
      "streetAddress2",
      "cityName",
    ].map((i) => address[i]);
    addressText = addressText.join(", ");
    let dist = this.getDistanceFromLatLonInKm(mainLatitude, mainLongitude, address.latitude, address.longitude)
    if (dist > 5) {
      errorText = "Not Servicable"
    }
    else {
      errorText = ""
    }
    if (address.isDefault == true) {
      if(errorText){
        this.props.updateAddress({...this.props.userObject,DefaultAddress:true}) 
      }else{
        this.props.updateAddress({...this.props.userObject,DefaultAddress:false})
      }
}
  })
 
}
  componentDidMount=()=>{
    if(this.props.isLoggedIn){
      this.getAddressofUser();
    }
  }
  componentDidUpdate=(prevProps)=>{ 
    if(this.props.isLoggedIn){
    var m =this.props.userObject.customerAddress?.find((o)=>o.isDefault===true)
    var preM =prevProps.userObject.customerAddress?.find((o)=>o.isDefault===true)
    if(m?.addressId!==preM?.addressId){
      this.getAddressofUser();
    }
  }
  }
    
 

  //using Haversine formula.
  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  renderAddaddress = () => {
    const { addresses = [], onAddAdress } = this.props;
    const { userObject = {} } = this.props;
    const { customerAddress = [] ,headerSessionId} = userObject;
    const { selectedLocation = {} } = this.props
    let obj = localStorage.getItem('persist:ChaiPoint')
    let obj2 = JSON.parse(obj);
    let location = JSON.parse(obj2.selectedLocation)


    let mainLatitude = location.selectedLocation.selectedLocation?.geometry?.location.lat;
    let mainLongitude = location.selectedLocation.selectedLocation?.geometry?.location.lng
    let errorText = ""
    return (
      <>
        <div className="col-sm-12">
          {customerAddress.map((address) => {
            let addressText = [
              "streetAddress1",
              "streetAddress2",
              "cityName",
            ].map((i) => address[i]);
            addressText = addressText.join(", ");

            // let dist = this.getDistanceFromLatLonInKm(12.91163, 77.638014, 12.962643848864856, 77.64180821796876)
            let dist = this.getDistanceFromLatLonInKm(mainLatitude, mainLongitude, address.latitude, address.longitude)

            if (dist > 5) {
              errorText = "Not Servicable"
              
            }
            else {
              errorText = ""
              
            }


            if (address.isDefault == true) {
              return (
                <>
                  <div className="col-sm-12">
                    <div
                      style={{ border: "none", height: "100%" }}
                      className={styles["card"]}
                    >
                      <div className={styles["address-type"]}>
                        {address.addressTypeName}
                      </div>
                      <div className={styles["fulladdress"]}>{addressText}</div>
                      <div style={{
                        fontSize: '12px',
                        color: "red"
                      }}>{errorText}</div>
                      <div className={styles["action"]}>
                        {/* <Button>Deliver Here{address.isDefault}</Button> */}
                      </div>
                    </div>
                  </div>
                </>
              );
            }

            
          })}
          
        </div>
      </>
    );
  };

  renderAddressForChange = () => {
    const { addresses = [], onAddAdress } = this.props;
    const { userObject = {} } = this.props;
    const { customerAddress = [] } = userObject;
   
    return (
      <div className="col-sm-12">
        {customerAddress.map((address) => {
          let addressText = [
            "streetAddress1",
            "streetAddress2",
            "cityName",
          ].map((i) => address[i]);
          addressText = addressText.join(", ");

          if (address.isDefault == false) {
            return (
              <>
                <div className="col-sm-6 delievry-btn">
                  <div className={styles["card"]}>
                    <div className={styles["address-type"]}>
                      {address.addressTypeName}
                    </div>
                    <div className={styles["fulladdress"]}>{addressText}</div>
                    <div className={styles["action"]}>
                      <Button
                        onClick={() => {
                          this.setDefault(address.addressId);
                          this.setState({
                            changeAddress: !this.state.changeAddress,
                          });
                        }}
                      >
                        Deliver Here
                      </Button>
                    </div>
                  </div>
                </div>
              </>
            );
          }
        })}
        <div>
          <button
            type="primary"
            title="Login"
            className="add-btn "
            onClick={() => {
              onAddAdress()
              this.setState({
                changeAddress: !this.state.changeAddress,
              })
            }}
          >
            <span class="glyphicon glyphicon-plus"></span> Add Address
          </button>
        </div>
      </div>
    );
  };

  render() {
    const { addresses = [], onAddAdress, selectedChannel } = this.props;
    const { isLoggedIn = false, userObject = {} } = this.props;
    const { phoneNo, name ,customerAddress} = userObject;
    const isUser = isLoggedIn || phoneNo;
  
    return (
      <div className="detailHeader">
      <div className={styles["delivery-container"]}>
        <div className={styles["delivery-title"]}>
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <div
            // style={{ width: "90%" }}
            >
              <span className={styles["icon"]}>
                <FontAwesomeIcon icon={faMapMarkerAlt} size="20px" />
              </span>
              <span className={styles["heading"]}>Delivery Address</span>
            </div>
            <div>
              {selectedChannel.name === "DELIVERY" && isUser && customerAddress?.length>0 && (
                <>
                  {!this.state.changeAddress ? (
                    <span
                      style={{
                        cursor: "pointer",
                        marginRight: "10px",
                        fontSize: "15px",
                      }}
                      onClick={() =>
                        this.setState({
                          changeAddress: !this.state.changeAddress,
                        })
                      }
                    >
                      Change{" "}
                    </span>
                  ) : (
                    <span
                      style={{
                        cursor: "pointer",
                        marginRight: "10px",
                        fontSize: "15px",
                      }}
                      onClick={() =>
                        this.setState({
                          changeAddress: !this.state.changeAddress,
                        })
                      }
                    >
                      Cancel{" "}
                    </span>
                  )}
                </>
              )}
            </div>
          </div>
        </div>
        {selectedChannel.name === "DELIVERY"  && (
          <>
            <div className={styles["list"]}>
              
              {(isUser &&  (
                <>
                  
                  <div>{this.renderAddaddress()}</div>
                  <div>
                    {this.state.changeAddress ? (
                      <div>{this.renderAddressForChange()} </div>
                    ) : (
                      ""
                    )}{" "}
                  </div>
                  <br />

                </>
              )) ||
                ""}
            </div>
          </>
        )}
        {customerAddress?.length===0 ? 
         <div>
          <button
            type="primary"
            title="Login"
            className="add-btn "
            onClick={onAddAdress}
          >
            <span class="glyphicon glyphicon-plus"></span> Add Address
          </button>
        </div>:""}
        </div>
        </div>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const { isLoggedIn = false } = reducerObj.userObject;
  return { userObject, isLoggedIn, selectedChannel };
};
const mapDispatchToProps = (dispatchEvent, { sessionId }) => {
  return {
    getProfile: (payload, sessionId) =>
      dispatchEvent(getProfile(payload, sessionId)),
    updateUserObject: (payload) => dispatchEvent(updateUserObject(payload)),
     updateAddress:(payload)=>dispatchEvent(updateAddress(payload))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);

import React from "react";
import { Modal, Button } from "react-bootstrap";
import styles from "../styles/offermodal.module.scss";
import { Link } from "react-router-dom";
const PaymentModal = (props) => {
  const { showModal, data = {}, onHide } = props;

  return (
    <>
      <div className={styles["payment-modal-container"]}>
        <div className={styles["paycontainer"]}>
          <div className={styles["content"]}>
            {/* <div className={styles["title"]}>Payment Details</div> */}
            <div>
              <img
                height="180px"
                width="300px"
                src={
                  require("../../../../assets/images/PaymentUnsuccessful.svg")
                    .default
                }
              />
            </div>
            <div className={styles["title"]}>Oops Payment is Unsuccessful!</div>
            <div className={styles["description"]}>
              We are sorry! You’re payment was unsuccessful.
              <div> Please try again.</div>
            </div>
          </div>
          <div className={styles["footer"]}>
            <div >
              <Link to="/checkout">
                {" "}
                <button
                  type="primary"
                  title="Login"
                  className={styles["button2"]}
                  onClick={onHide}
                >
                  {" "}
                  Return to Checkout
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
    //   </Modal.Body>
    // </Modal>
  );
};

export default PaymentModal;

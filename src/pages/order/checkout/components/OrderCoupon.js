import React, { Component } from "react";
import { ICON_TYPES } from '../../../../common/constants';
import { ChaiAppIcons } from '../../../../components/icons/icons';

export default class OrderCoupon extends Component {
  render() {
    return (
      <div class="col-xs-12 loyalty-points seperate-order">
        <img src={require("../../../../assets/images/Coupon.svg").default} />
        {/* <ChaiAppIcons name={ICON_TYPES.COUPNS} /> */}
        <span>
          Apply Coupon
          <i class="fa fa-angle-right" aria-hidden="true"></i>
        </span>
      </div>
    );
  }
}

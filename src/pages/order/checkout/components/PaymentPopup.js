import React, { useState,useEffect,useRef } from "react";
import {useHistory} from "react-router-dom"
import ProfileService from "../../../../utils/services/profile/profile-service";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
const PaymentPopup = (props) => {
  const { payobject, selectedStore, selectedChannel, userObject } = props;
  const ref = useRef(null);
let history=useHistory()
  const proccess = async () => {
    const payload = {
      fulfilmentType: selectedChannel.name,
      orderId: payobject.udf2,
      outletId: selectedStore.orderId,
      siteId: selectedStore.siteId,
    };
    await ProfileService.processOrder(payload,userObject.headerSessionId).then((res) => {
      if (res.response===true) {
        
        history.push("/paysuccess")
      }
    });
  };
  const myfunc = () => {
    
  };
  useEffect(() => {
    setTimeout(() => {
      ref.current.click();
    }, 5000); //miliseconds
  }, []);
  return (
    <div className="add-new-address" >
      <label class="Edit-Profile">Payment Details</label>
      <label onClick={props.onHide} style={{float:"right"}}><FontAwesomeIcon
                icon={faTimes}
                size="lg"
                className="star-icon-plus"
              /></label>
      <div className="" style={{paddingTop:"20px"}}>
        <div class="row  d-flex align-items-center justify-content-center ml-5 ">
          <div style={{fontWeight:"bolder", fontSize:"11px"}}>
          <div class="col-sm-12 container mt-5 d-flex justify-content-center">
            <span class="col-sm-6">Username</span>
            <span class="col-sm-6">{payobject.firstname}</span>
          </div>
          <div class="col-sm-12 container mt-3 d-flex justify-content-center">
            <span class="col-sm-6">Phone</span>
            <span class="col-sm-6">+ {payobject.phone}</span>
          </div>
          <div class="col-sm-12 container mt-3 d-flex justify-content-center ">
            <span class="col-sm-6">Order ID</span>
            <span class="col-sm-6">{payobject.udf2}</span>
          </div>
          <div class="col-sm-12 container mt-3 d-flex justify-content-center ">
            <span class="col-sm-6">Order Type</span>
            <span class="col-sm-6">{selectedChannel.displayName }</span>
            </div>
            </div>
          {/* <div
            style={{
              borderBottom: "2px solid #12767f",
              width: "90%",
              marginRight: "70px",
            }}
            className="mt-3"
          ></div> */}
          <div class="col-sm-12 container profile-work d-flex justify-content-center mb-5 mt-3 " >
            <span class="col-sm-6" style={{marginLeft: "8px"}}>Total Bill</span>
            <span class="col-sm-6">₹{payobject.amount}</span>
          </div>
        </div>
        <form method="post" action={payobject.url}>
          <input type="hidden" name="key" value={payobject.key} />
          <input type="hidden" name="txnid" value={payobject.txnid} />
          <input type="hidden" name="amount" value={payobject.amount} />
          <input type="hidden" name="firstname" value={payobject.firstname} />
          <input type="hidden" name="email" value={payobject.email} />
          <input type="hidden" name="phone" value={payobject.phone} />
          <input
            type="hidden"
            name="productinfo"
            value={payobject.productinfo}
          />
          <input type="hidden" name="surl" value={payobject.surl} />
          <input type="hidden" name="furl" value={payobject.furl} />
          <input type="hidden" name="hash" value={payobject.hash} />
          <input type="hidden" name="udf1" value={payobject.udf1} />
          <input type="hidden" name="udf2" value={payobject.udf2} />
          <input type="hidden" name="udf3" value={payobject.udf3} />
          <input type="hidden" name="udf4" value={payobject.udf4} />

          {payobject.amount !== 0 ? (
            <button type="submit" class="btn btn-primary mt-5 " ref={ref} onClick={myfunc}>
            Proceed to pay
          </button>
          ) : (
            ''
          )}
        </form>
        {payobject.amount === 0 ? (
            <button class="btn btn-success mt-5 " onClick={proccess} >
              Place Order
            </button>
          ) : (
           ''
          )}
      </div>
    </div>
  );
};
const mapStateToProps = (reducerObj) => {
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const payobject = reducerObj.PayObject.pay;
  const userObject = reducerObj.userObject.userObject;
  return { selectedStore, payobject, selectedChannel ,userObject};
};

export default connect(mapStateToProps)(PaymentPopup);

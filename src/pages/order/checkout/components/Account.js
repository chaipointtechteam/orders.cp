import React, { Component } from "react";
import { connect } from "react-redux";
import { ICON_TYPES } from "../../../../common/constants";
import { ChaiAppIcons } from "../../../../components/icons/icons";
import { updateUserObject } from "../../../../actions/UserAction";
import { getProfile } from "../../../../actions/LoginAction";
// import { Button } from 'react-bootstrap';
import styles from "../styles/checkout.module.scss";
import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";
class Account extends Component {
  componentDidMount() {
    const { userObject, getProfile } = this.props;
    const { phoneNo = "",newHeader } = userObject;

    const headers = userObject?.newHeader || {};

    if (phoneNo) {
      getProfile(`${phoneNo}`, newHeader.sessionId);
    }
  }

  closePopUpAndOpenLogin = () => {
    this.props.updateUserObject({
      showLoginPopup: true,
      showSignUpPopup: false,
    });
  };

  renderLoginInstruct = () => {
    return (
      <div class="log_user">
        <div>
          To place your order now, login to your existing account or sign up
        </div>
        <Button
          type="primary"
          title="Login"
          className="loginBTn"
          onClick={this.closePopUpAndOpenLogin}
        >
          Login
        </Button>
        {/* <button type="button" className='btn btn-primary' onClick={this.closePopUpAndOpenLogin}>
        Login
      </button> */}
      </div>
    );
  };

  render() {
    const { isLoggedIn = false, userObject = {} } = this.props;
    const { phoneNo, name } = userObject;
    const isUser = isLoggedIn || phoneNo;
    const acc_card_class = isUser
      ? `${styles["account"]} ${styles["logged-in"]}`
      : `${styles["account"]}`;
    return (
      <div className="detailHeader">
        <div className={styles["account-container"]}>
          <div className={acc_card_class} style={{ border: "2px solid #5aadb1" }}>
            <div class="account-icon">
              <span>
                <img
                  src={
                    require("../../../../assets/images/Profile_Green.svg").default
                  }
                />
                {/* <ChaiAppIcons name={ICON_TYPES.ACCOUNT} /> */}
              </span>
              <span class="log_deliver">{isUser ? "Logged In" : "Account"}</span>
              {(isUser && (
                <div class="log_user1">
                  {name} | {phoneNo}
                </div>
              )) ||
                this.renderLoginInstruct()}
            </div>
          </div>
        </div>
        </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  
  const { isLoggedIn = false } = reducerObj.userObject;
  return { userObject, isLoggedIn };
};
const mapDispatchToProps = (dispatchEvent, { sessionId }) => {
  return {
    getProfile: (payload, sessionId) =>
      dispatchEvent(getProfile(payload, sessionId)),
    updateUserObject: (payload) => dispatchEvent(updateUserObject(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);

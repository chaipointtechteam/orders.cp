import React from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import {
  applyCoupon, updateCouponObject
} from "../../../../actions/CouponAction";
import { updateWalletAmount } from "../../../../actions/WalletAction";
import toast from "../../../../components/common/commonToast";
import ProfileService from "../../../../utils/services/profile/profile-service";
import styles from "../styles/offermodal.module.scss";
class OfferModal extends React.Component {
  state = {
    code: "",
    description: "",
    status: false,
    showSuccessModal: null,
    data: "",
    status2: false,
    useWalletPoint: null,
    errorWallet: null,
    invalidCouponMsg:""
  };

  notApply = (e) => {
    e.preventDefault();
    toast.warning("Please login and try again");
  };

   randomString=(length, chars)=>{
    var result = "";
    for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }


  onApply = async (e) => {
    e.preventDefault();    
    if (this.state.code !== "") {
      this.setState({
        status: !this.state.status,
      });
      var rString = this.randomString(
        32,
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
      );
      const payload = {

        cartRequestWebDTO: {
          cartItemList: this.props.CouponObject.cartRequestWebDTO,
          fulfilmentEntityId:
            this.props.CouponObject.fullfill.fulfilmentEntityId,
          fulfilmentEntityType:
            this.props.CouponObject.fullfill.fulfilmentEntityType,
          fulfilmentType: this.props.CouponObject.fullfill.fulfilmentType,
          lookupId: this.props.CouponObject.fullfill.lookupId,
          isApplying: true,
          oldUuid:null,
          totalAmount: this.props.CouponObject.fullfill.totalAmount,
          uuid:rString,
          couponCode: this.state.code,
          sellerId: this.props.CouponObject.fullfill.sellerId,
          // uuid:"asa32uuweuo",
          // oldUuid:"asa32uuweuo",
        },
        applyCouponResponse: null,
        refresh: false,
      };

      await ProfileService.applyCoupon(
        payload,
        this.props.sessionId.headerSessionId
      )
        .then((e) => {
          // debugger
          
          if (e.applyCouponResponse!==null) {
            // debugger

            toast.error("Invalid Coupon Code", { closable: true });

            this.props.updateCouponObject({
              ...this.props.CouponObject,
              applyCouponResponse: e.applyCouponResponse,
              chargeDtoObjects: e.cartResponseWebDTO.chargeDtoObjects,
              couponCode: e.couponCode,
              uuid:payload.uuid
            });

          } else {
           if(e.status=="SUCCESS"){
            toast.success("COUPON APPLIED SUCCESSFULLY", { closable: true });
           }else{
            if(e.status){
              this.setState({invalidCouponMsg:e.status})              
              // toast.error(e.status, { closable: true });
            }else{
              toast.error("INVALID COUPON", { closable: true });
            }
           }
            
            this.props.updateCouponObject({
              ...this.props.CouponObject,
              applyCouponResponse: e.applyCouponResponse,
              chargeDtoObjects: e.cartResponseWebDTO.chargeDtoObjects,
              couponCode: e.couponCode,
              uuid:payload.cartRequestWebDTO.uuid
            });
            if(e.status=="SUCCESS"){
              this.props.applyCoupon();
              if (this.props.wallet.isUse) {
                toast.warning("CP Wallet is removed as order value is updated. Please redeem the Wallet again.")
              }
           
            this.props.onApply({ code: e.couponCode });
            }
            // this.props.applyCoupon();
          }
         
        })
        .catch((e) => {
        //  toast.warning("invalid code")
          this.setState({ code: "" });
        });
    }
    this.setState({code:""});
  };

  handleChange = (e) => {
    if (e.target.name === "code") {
      this.setState({ code: e.target.value });
    }
  };

  usepoint = () => {
    const sessionId = this.props.sessionId.headerSessionId;
    const { wallet } = this.props;
    const { useAmount } = wallet;
    if (useAmount.value < this.props.data) {
      var amount = this.props.data - useAmount.value;
      this.props.updateWalletAmount(this.props.data - amount);
      this.props.onLoyalty({
        data: true,
        walletAmount: this.props.data - amount,
      });
    } else {
      console.log(this.props.data);
      this.props.updateWalletAmount(this.props.data);
      this.props.onLoyalty({ data: true, walletAmount: this.props.data });
    }
    // if (this.state.useWalletPoint <= this.props.data && this.state.useWalletPoint<=useAmount.value ) {
    //   this.props.updateWalletAmount(amount.toFixed(2));
    //   this.props.onLoyalty({ data: true, walletAmount: this.state.useWalletPoint });
    // } else {
    //   this.setState({ errorWallet: "Please Enter the Valid Amount" });
    // }
  };
  useWalletPoint = (e) => {
    this.setState({ useWalletPoint: e.target.value, errorWallet: "" });
  };
  renderLoyalty = () => {
    const { type = "", showModal = false, onHide, data } = this.props;
    return (
      <Modal
        className={styles["loyalty-modal-container"]}
        show={showModal}
        if="mysignModal"
        onHide={onHide}
        contentClassName={styles["modal-content"]}
      >
        <Modal.Body className={styles["loyalty-modal-body"]}>
          <Modal.Header closeButton style={{border:"none"}}> 
          </Modal.Header>
          <div>
            <img
              className={styles["loyalty-modal-image"]}
              src={
                require("../../../../assets/images/Loyalty Points.svg").default
              }
            />
          </div>
          <div className={styles["loyalty-text"]}>
            CP Wallet Balance: ₹ {data ?data.toFixed(2):0}
          </div>
          {data > 0 && this.props.sessionId? (
            <>
              <span>{this.state.errorWallet}</span>
              <div className={styles["loyalty-border"]}></div>
              <div className={styles["loyalty-btn-grp"]}>
                <div className={styles["button-cancle"]}>
                  {/* <button onClick={onHide}>Cancel</button> */}
                </div>
                <div className={styles["button-points"]}>
                  <button onClick={this.usepoint}>Use Wallet Balance</button>
                </div>
              </div>
            </>
          ) : null}
        </Modal.Body>
      </Modal>
    );
  };
  renderOffer = () => {
    const { type = "", showModal = false, onHide } = this.props;
    const { code } = this.state;
    return (
      <Modal
        className={styles["loyalty-modal-container"]}
        show={showModal}
        if="mysignModal"
        onHide={onHide}
        contentClassName={styles["modal-content"]}
      >
        <Modal.Body className={styles["loyalty-modal-body"]}>
          <div>
            <img
              className={styles["loyalty-modal-image"]}
              src={require("../../../../assets/images/Coupon.svg").default}
            />
          </div>
          <div className={styles["loyalty-text"]}>Apply Coupon</div>
          <div className={styles["loyalty-border"]}></div>
          <div className={styles["loyalty-btn-grp"]}>          
            <div className={styles["form-group"]}>
              <input
                className="input-coupon"
                value={code}
                autoFocus
                onChange={this.handleChange}
                placeholder={"Enter Coupon Code"}
                name={"code"}
                onKeyUp={(e)=>{
                  if(e.keyCode === 13){
                    if(this.props.sessionId.headerSessionId === "")
                      this.notApply(e)
                    else
                    this.onApply(e)
                  }
                }}
              />
              <div 
                style={{
                  color:"red",
                  margin:"10px 5px",
                  fontSize:"11px"
                }}
              >
                {this.state.invalidCouponMsg}
              </div>
              <div className={styles["button-cancle"]}>
                <button
                  style={{
                    marginLeft: "10px",
                    marginRight: "10px",
                    background: "#12767f",
                    color: "#ffff",
                  }}
                  onClick={()=>{
                    this.setState({invalidCouponMsg:""})
                    onHide()
                  }}
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  onClick={
                    this.props.sessionId.headerSessionId === ""
                      ? this.notApply
                      : this.onApply
                  }
                >
                  Apply
                </button>
              </div>
              {/* <Button
                variant="primary"
                onClick={this.onApply}
              >
                Apply
              </Button> */}
            </div>            
          </div>
        </Modal.Body>
      </Modal>
    );
  };
  render() {
    const { type = "", showModal = false, onHide } = this.props;

    return (
      <>
        {this.props.type === "loyalty" ? this.renderLoyalty(this.props) : null}
        {this.props.type === "coupon" ? this.renderOffer(this.props) : null}
      </>
      // <Modal
      //   className={styles["offer-modal-container"]}
      //   show={showModal}
      //   if="mysignModal"
      //   onHide={onHide}
      //   contentClassName={styles["modal-content"]}
      // >
      //   <Modal.Body className={styles["modal-body"]}>

      //   </Modal.Body>
      // </Modal>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const sessionId = reducerObj.userObject.userObject;
  const cartObject = reducerObj.cartObject.cartObject;
  const CouponObject = reducerObj.CouponObject.CouponObject;
  const cp = reducerObj.CouponObject;
  const wallet = reducerObj.Wallet;
  return { sessionId, cartObject, CouponObject, cp, wallet };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCouponObject: (payload) => dispatch(updateCouponObject(payload)),
    updateWalletAmount: (payload) => dispatch(updateWalletAmount(payload)),
    applyCoupon: () => dispatch(applyCoupon()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OfferModal);

import React, { Component } from "react";
import { connect } from "react-redux";
import IncrementDecrementAction from "../../../../components/products/incrementDecrementAction";

class OrderSummaryItem extends Component {
  state = { addOns: [] };
  componentDidMount = () => {
    const { cartItem } = this.props;
    var addOns = cartItem.productModifierList ? cartItem.productModifierList?.modifierProductList?.filter((k) => {
      if (k) return k;
    }) : '';
    this.setState({
      addOns: addOns,
    });
  };
  render() {
    const { cartItem, cartObject } = this.props;


    return (
      <>
    
        <div className=" np seperate-order">
          <div class="col-xs-12">
            <div class="col-xs-5">
              <span class="col-xs-2 veg-img">
                {cartItem.isVegetarian === true ? (
                  <img src={require("../../../../assets/images/Veg.svg").default} />
                ) : (
                  <img src={require("../../../../assets/images/Non Veg.svg").default} />
                )}
              </span>
              <div class="col-xs-10 order-names">
                <h4 class="product-name">
                  <strong>{cartItem.displayName}</strong>
                </h4>
              </div>
            </div>
            <div class="col-xs-4">
              <IncrementDecrementAction product={cartItem} />
            </div>

            <div class="col-xs-3 order-price">
              {cartObject.subTotal && (
                <h4 class="product-name">
                  <strong>
                    ₹{(cartItem.price * cartItem.quantity).toFixed(2)}
                  </strong>
                </h4>
              )}

            
            </div>
          </div>
          <div className="addOns-Summery" >
            <div class="col-xs-12 ">
              <div className="col-xs-10">
                {cartItem.productModifierList.modifierProductList?.length > 0 &&

                  this.state.addOns?.length > 0 &&
                  this.state.addOns.map((item, index) => (

                    <h5 key={index}>
                      <small> {item.productName.displayName}</small>
                    </h5>
                  ))}
                {/* {cartItem.productModifierList.modifierProductList?.length >= 0 && cartItem.productVariantList.length===0 &&
                  <h5>
                    <small style={{ color: '#5aadb1', fontWeight: 'bold' }}>Customize</small>
                  </h5>

                } */}
              </div>
              <div className="col-xs-2">
                {
                  cartItem.productModifierList?.modifierProductList?.length > 0 &&
                  this.state.addOns?.length > 0 &&
                  this.state.addOns.map((item, index) => (
                    <h5 key={index} style={{ marginLeft: "-35px" }}>
                      <small> ₹{item.productName.price * cartItem.quantity}</small>
                    </h5>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  return { cartObject };
};
export default connect(mapStateToProps)(OrderSummaryItem);

import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import {
  applyCoupon, updateCouponObject
} from "../../../../actions/CouponAction";
import { updateWalletAmount } from "../../../../actions/WalletAction";
import toast from "../../../../components/common/commonToast";
import ProfileService from "../../../../utils/services/profile/profile-service";
class MobileCoupon extends Component{
     
    state={
        code:""
    }

     handleChange=(event)=>{
       this.setState({
           [event.target.name] : event.target.value
       })
     }
  notApply = () => {
    this.props.history.push({
      pathname: "/checkout",
      name: "mobileCoupon"
    })
    toast.warning("Please login and try again");
  };

  randomString=(length, chars)=>{
    var result = "";
    for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  onApply = async () => {
    if (this.state.code !== "") {
      var rString = this.randomString(
        32,
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
      );
      const payload = {
        cartRequestWebDTO: {
          cartItemList: this.props.CouponObject.cartRequestWebDTO,
          fulfilmentEntityId:
            this.props.CouponObject.fullfill.fulfilmentEntityId,
          fulfilmentEntityType:
            this.props.CouponObject.fullfill.fulfilmentEntityType,
          fulfilmentType: this.props.CouponObject.fullfill.fulfilmentType,
          lookupId: this.props.CouponObject.fullfill.lookupId,
          isApplying: true,
          oldUuid:null,
          totalAmount: this.props.CouponObject.fullfill.totalAmount,
          uuid:rString,
          couponCode: this.state.code,
          sellerId: this.props.CouponObject.fullfill.sellerId,
        },
        applyCouponResponse: null,
        refresh: false,
      };

      await ProfileService.applyCoupon(
        payload,
        this.props.sessionId.headerSessionId
      )
        .then((e) => {
          if (e.applyCouponResponse!==null) {
            toast.error("Invalid Coupon Code", { closable: true });

            this.props.updateCouponObject({
              ...this.props.CouponObject,
              applyCouponResponse: e.applyCouponResponse,
              chargeDtoObjects: e.cartResponseWebDTO.chargeDtoObjects,
              couponCode: e.couponCode,
            });
          } else {
            if(e.status=="SUCCESS"){
              toast.success("COUPON APPLIED SUCCESSFULLY", { closable: true });
             }else{

              if(e.status){
                toast.error(e.status, { closable: true });
              }else{
                toast.error("INVALID COUPON", { closable: true });
              }

             }
            this.props.updateCouponObject({
              ...this.props.CouponObject,
              applyCouponResponse: e.applyCouponResponse,
              chargeDtoObjects: e.cartResponseWebDTO.chargeDtoObjects,
              couponCode: e.couponCode,
              uuid:payload.cartRequestWebDTO.uuid
            });
            if(e.status=="SUCCESS"){     
              if (this.props.wallet.isUse) {
                toast.warning("CP Wallet is removed as order value is updated. Please redeem the Wallet again.")
              }
            this.props.applyCoupon(e.couponCode);
            // this.props.onApply({ code: e.couponCode });
            if (this.props.wallet.isUse) {
              toast.warning("CP Wallet is removed as order value is updated. Please redeem the Wallet again.")
            }
             this.props.history.push({

              pathname: "/checkout",
              state: { name: "mobileCoupon" }
            })
            
          }
        }

        })
        .catch((e) => {
          this.setState({ code: "" });
        });
    }
  };

    render(){

    return (
        <div>
            <section class="mobile-coupon">
        <div class="row">
        <div class="coupon-title">
            <h2><Link to="/checkout"><FontAwesomeIcon
              
              icon={faArrowLeft}
              size="sm"
              
            /></Link>Apply Coupon</h2>
        </div>
        <div class="enter-coupon">
            <div id="subscription_area">
            <div class="input-group">
               <input
                type="text" 
                class="form-control" 
                name="code" 
                placeholder="Enter Coupon Code" 
                onChange={this.handleChange}
                value={this.state.code}
                />
               <span class="input-group-btn">
                    <button class="btn btn-default"
                     type="button"
                      onClick={
                        this.props.sessionId.headerSessionId === ""
                          ? this.notApply
                          : this.onApply
                      }>APPLY</button>
               </span>
            </div>
            </div>
            </div>
        </div>
    </section>
        </div>
    )
    }
}

const mapStateToProps = (reducerObj) => {
  const sessionId = reducerObj.userObject.userObject;
  const cartObject = reducerObj.cartObject.cartObject;
  const CouponObject = reducerObj.CouponObject.CouponObject;
  const cp = reducerObj.CouponObject;
  const wallet =reducerObj.Wallet;
  return { sessionId, cartObject, CouponObject, cp, wallet };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCouponObject: (payload) => dispatch(updateCouponObject(payload)),
    updateWalletAmount: (payload) => dispatch(updateWalletAmount(payload)),
    applyCoupon: () => dispatch(applyCoupon()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MobileCoupon));

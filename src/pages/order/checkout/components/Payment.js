import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import EditProfile from "../../../../components/profile/tabs/EditProfile"
import { updatepayObject } from "../../../../actions/PayAction";
import ProfileService from "../../../../utils/services/profile/profile-service";
import { getPaymentsDetails } from "../../../../utils/services/service/getWalletDetail";

export const deliveryCost = 50;
export const defaultTax = 45;

const Payment = (props) => {
  const ref = useRef(null);
  var history = useHistory();
  const [IsEnable, setIsEnable] = useState(false);
  const [AddModal, setAddModal] = useState(true);
  const [showModel, setShowModel] = useState(false);
  const [Isedit, setIsedit] = useState(false);
  const [notUser, setNotUser] = useState(false);
  const {
    isLoggedIn = false,
    userObject = {},
    selectedStore,
    payobject,
    selectedChannel,
  } = props;
  const { phoneNo, name } = userObject;
  const isUser = isLoggedIn || phoneNo;
  const [isEditProfile, setIsEditProfile] = useState(false);
  const flipAddProfile = () => {
    setIsEditProfile(false);
  };

  const flipAdd = () => {
    setIsedit(!Isedit);
  };
  const flipStore = () => {
    setShowModel(!IsEnable);
  };

  useEffect(() => {
    tandD();
    var id = clickkk();
    if (id == null && props.selectedChannel.sellerId===12) {
      setAddModal(false);
    } else setAddModal(true);
  }, []);

  useEffect(() => {
    var id = clickkk();
    if (id == null && props.selectedChannel.sellerId===12) {
      setAddModal(false);
    } else setAddModal(true);
  }, [props.userObject.customerAddress]);

  const setTimeDate = () => { };

  const tandD = () => {
    var m = setInterval(() => {
      var CurrentTime = moment().format("HH:mm:ss");
      var Currentdate = moment().format("L");

      if (
        Date.parse(`${Currentdate} ${selectedStore.startTime}`) <
        Date.parse(`${Currentdate} ${CurrentTime}`) &&
        Date.parse(`${Currentdate} ${CurrentTime}`) <
        Date.parse(`${Currentdate} ${selectedStore.endTime}`)
      ) {
        setTimeDate(moment().format("HH:mm:ss"));
        setIsEnable(false);
        setShowModel(false);
      } else {
        setIsEnable(true);
        setShowModel(true);
        clearInterval(m);
      }
    }, 1000);
  };
  function randomString(length, chars) {
    var result = "";
    for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }
  var rString = randomString(
    32,
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  );

  const dataPayload = {
    cartItemList: props.cartObject.CreateCart,
    fulfilmentEntityId: 76,
    fulfilmentEntityType: "STORE",
    fulfilmentType: props.selectedChannel.name,
    requestId: rString,
    sellerId: props.selectedChannel.sellerId,
  };
  const [subTotal, setSubTotal] = useState();

  useEffect(() => {
    var subtotal = parseFloat(props.cartObject.subTotal);
    var total = subtotal + deliveryCost + defaultTax;
    setSubTotal(total.toFixed(2));

    if (props.isLoggedIn) {
      if (props.userObject.name && props.userObject.email) {
        setNotUser(true);
      }
    }
  }, []);
  useEffect(() => {
    if (props.isLoggedIn) {
      if (props.userObject.name && props.userObject.email) {
        setNotUser(true);
      }
    }
  }, [props.isLoggedIn,props.userObject])
 
  const clickkk = () => {
    var id = null;
    props.userObject?.customerAddress?.map((data) => {
      if (data.isDefault === true) {
        id = data.addressId;
      }
    });
    return id;
  };



  const getPaymentDetails = async () => {


    if (props.userObject.name && props.userObject.email) {
      if (!IsEnable) {
        const payload = JSON.stringify(props.paymentData);
        const session = props.userObject.headerSessionId;
       
        await ProfileService.getCart(payload, session)
          .then(async (res) => {
            if (res) {
              const createCartData = {
                addressId: clickkk(),
                couponMetaData: [],
                instruction: "",
                lookupId: res.lookupId,
                requestType: res.fulfilmentType,
                sellerId: props.selectedChannel.sellerId,
              };
              var OrderPayload = JSON.stringify(createCartData);
              await ProfileService.createOrder(OrderPayload, session).then(
                async (res) => {
                  if (res) {
                    await getPaymentsDetails({ res, props, rString });
                  }
                }
              );
            }
          })
          .catch((e) => { });
        flipAdd();
      } else {
        setShowModel(true)
      }

    } else {
      alert("Please update your profile");
    }
    //
  };
  useEffect(() => {
    if (Isedit && props.payobject.amount === 0) {
      setTimeout(() => {
        proccess();
      }, 1000);
    } else {
      if (Isedit && props.payobject) {
        setTimeout(() => {
          ref.current.click();
        }, 1000);
      }
    }
  }, [props.payobject, Isedit]);

  const proccess = async () => {
    const payload = {
      fulfilmentType: selectedChannel.name,
      orderId: payobject.udf2,
      outletId: selectedStore.outletId,
      siteId: selectedStore.siteId,
    };
    await ProfileService.processOrder(payload, userObject.headerSessionId).then(
      (res) => {
        if (res.response === true) {
          history.push("/paysuccess");
        }
      }
    );
  };

  return (
    <>
      <form method="post" action={payobject.url}>
        <input type="hidden" name="key" value={payobject.key} />
        <input type="hidden" name="txnid" value={payobject.txnid} />
        <input type="hidden" name="amount" value={payobject.amount} />
        <input type="hidden" name="firstname" value={payobject.firstname} />
        <input type="hidden" name="email" value={payobject.email} />
        <input type="hidden" name="phone" value={payobject.phone} />
        <input type="hidden" name="productinfo" value={payobject.productinfo} />
        <input type="hidden" name="surl" value={payobject.surl} />
        <input type="hidden" name="furl" value={payobject.furl} />
        <input type="hidden" name="hash" value={payobject.hash} />
        <input type="hidden" name="udf1" value={payobject.udf1} />
        <input type="hidden" name="udf2" value={payobject.udf2} />
        <input type="hidden" name="udf3" value={payobject.udf3} />
        <input type="hidden" name="udf4" value={payobject.udf4} />

        <button
          type="submit"
          style={{ display: "none" }}
          class="btn btn-primary mt-5 "
          ref={ref}
        >
          Proceed to pay
        </button>
      </form>

      <div>
        {isEditProfile &&
          <Modal
            className="modal-opacity signup"
            id="addAddressModal"
            show={isEditProfile}
            onHide={flipAddProfile}
          >
            <Modal.Header closeButton></Modal.Header>
            <EditProfile onHide={flipAddProfile} />
          </Modal>
        }

      </div>






      <div className="detailHeader">
        {props.isLoggedIn && !notUser ? (
          <Modal
            className="modal-opacity"
            id="addAddressModal"
            show={!notUser}
            onHide={flipStore}
          >
            <div class="modal-header">
              <h5 class="modal-title">Edit Profile</h5>
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div class="modal-body">
              <h2>Please Update Your Profile Details.</h2>
            </div>
            <div class="modal-footer">
             
              <button
                type="button"
                class="btn btn-primary"
                onClick={() => {
                  if (window.innerWidth < 760) {
                    // history.goBack();
                    setIsEditProfile(true)
                  } else {
                    history.push("/profile");
                  }
                }}
              >
                Edit Profile
              </button>
            </div>
          </Modal>
        ) : (
          ""
        )}
        {IsEnable && window.innerWidth>760? (
          <Modal
            className="modal-opacity"
            id="addAddressModal"
            show={showModel}
            onHide={flipStore}
          >
            <div class="modal-header">
              <h5 class="modal-title">Store Closed</h5>
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div class="modal-body">
              <p>
                Store is currently unserviceable please change the store and try
                again.
              </p>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-bs-dismiss="modal"
                onClick={() => setShowModel(false)}
              >
                Close
              </button>
              <button
                type="button"
                class="btn btn-primary"
                onClick={() => history.push("/")}
              >
                Back To Home
              </button>
            </div>
          </Modal>
        ) : (
          ""
        )}
        
        <div>
          {isUser ? (
            <div class="log_userr">
              <button
                style={{
                  backgroundColor: !AddModal || !notUser||(props.userObject?.DefaultAddress && props.selectedChannel.sellerId==12) ? "grey" : "#12767F",
                }}
                type="primary"
                disabled={
                  (props.userObject?.DefaultAddress && props.selectedChannel.sellerId==12)||
                  !AddModal || !notUser}
                className="pay-btn"
                onClick={getPaymentDetails}
              >
                Proceed to Pay
              </button>
              {IsEnable ? (
                <div style={{ color: "red" }}>Alert : Store is closed</div>
              ) : !AddModal ? (
                <div style={{ color: "red" }}>
                  Alert : Please Select Delivery Address to Proceed
                </div>
              ) : !notUser ? (
                <div style={{ color: "red" }}>
                  Alert : Please Update Your Profile Details
                </div>
              ) : props.isLoggedIn && props.userObject?.DefaultAddress && props.selectedChannel.sellerId==12 ? (
                <div style={{ color: "red" }}>
                  Alert : This Address is not Seviceable
                </div>
              ) : (
                ""
              )}
            </div>
          ) : (
            ""
          )}
        </div>

      </div>
    </>
  );
};
const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject || {};
  const userObject = reducerObj.userObject.userObject || {};
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const { isLoggedIn = false } = reducerObj.userObject;
  const Wallet = reducerObj.Wallet;
  const payobject = reducerObj.PayObject.pay;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;

  return {
    payobject,
    cartObject,
    userObject,
    selectedChannel,
    Wallet,
    isLoggedIn,
    selectedStore,
  };
};

export default connect(mapStateToProps, { updatepayObject })(Payment);

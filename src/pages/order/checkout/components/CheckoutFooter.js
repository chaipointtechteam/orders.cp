import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import { updatepayObject } from "../../../../actions/PayAction";
import location from "../../../../assets/images/Location-01.svg";
import ProfileService from "../../../../utils/services/profile/profile-service";
import { getPaymentsDetails } from "../../../../utils/services/service/getWalletDetail";
function CheckoutFooter(props) {
  const ref = useRef(null);
  const [showModel, setShowModel] = useState(false);
  const [IsEnable, setIsEnable] = useState(false);
  const [Isedit, setIsedit] = useState(false);
  const [Address, setAddress] = useState(true);
  const [Total, setTotal] = useState(null);
  const [notUser, setNotUser] = useState(false);
  const [fullAddress, setFullAddress] = useState("");
  const {
    isLoggedIn = false,
    userObject = {},
    selectedChannel,
    selectedStore,
    payobject,
  } = props;
  const { phoneNo, name } = userObject;
  const isUser = isLoggedIn || phoneNo;
  let history = useHistory();
  const flipAdd = () => {
    setIsedit(!Isedit);
  };
  const flipStore = () => {
    setShowModel(!IsEnable);
  };

  useEffect(() => {
    if (props.userObject.name && props.userObject.email) {
      setNotUser(true);
    }
  }, [])

  useEffect(() => {
    if (props.Wallet.isUse) {
      setTotal(props.Wallet.useAmount?.value - props.Wallet.walletAmount);
    } else {
      setTotal(props.Wallet.useAmount?.value);
    }
  }, [props.Wallet.isUse,props.Wallet.useAmount]);

  // useEffect(() => {
  //   setTotal(props.Wallet.useAmount.value);
  // }, []);

  useEffect(() => {
    tandD()
    var id = clickkk();
    if (id == null && props.selectedChannel.sellerId==12) {
      setAddress(false);
    } else setAddress(true);
  }, []);

  const setTimeDate = () => { };

  const tandD = () => {
    var m = setInterval(() => {
      var CurrentTime = moment().format("HH:mm:ss");
      var Currentdate = moment().format("L");

      if (
        Date.parse(`${Currentdate} ${selectedStore.startTime}`) <
        Date.parse(`${Currentdate} ${CurrentTime}`) &&
        Date.parse(`${Currentdate} ${CurrentTime}`) <
        Date.parse(`${Currentdate} ${selectedStore.endTime}`)
      ) {
        setTimeDate(moment().format("HH:mm:ss"));
        setIsEnable(false);
        setShowModel(false);
      } else {
        setIsEnable(true);
        setShowModel(true);
        clearInterval(m);
      }
    }, 1000);
  };
  useEffect(() => {
    if (props.isLoggedIn) {
      var addressFind = props.userObject.customerAddress.find(
        (address) => address.isDefault == true
      );
      setFullAddress(addressFind);
    }
  }, [props.userObject]);

  //
  function randomString(length, chars) {
    var result = "";
    for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }
  var rString = randomString(
    32,
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  );

  const getPaymentDetails = async () => {
    if (props.userObject.email && props.userObject.name) {


      const payload = JSON.stringify(props.paymentData);
      const session = props.userObject.headerSessionId;
      if (!IsEnable) {
        await ProfileService.getCart(payload, session)
          .then(async (res) => {
            if (res) {
              const createCartData = {
                addressId: clickkk(),
                couponMetaData: [],
                instruction: "",
                lookupId: res.lookupId,
                requestType: res.fulfilmentType,
                sellerId: props.selectedChannel.sellerId,
              };
              var OrderPayload = JSON.stringify(createCartData);
              await ProfileService.createOrder(OrderPayload, session).then(
                async (res) => {
                  if (res) {
                    await getPaymentsDetails({ res, props, rString });
                  }
                }
              );
            }
          })
          .catch((e) => { });

        flipAdd();
      } else {
        setShowModel(true)
      }
    }
    else {
      history.push("/profile");
    }
  };
  useEffect(() => {
    var id = clickkk();
    if (id == null && props.selectedChannel.sellerId==12) {
      setAddress(false);
    } else setAddress(true);

  }, [props.userObject.customerAddress]);

  const clickkk = () => {
    var id = null;
    props.userObject?.customerAddress?.map((data) => {
      if (data.isDefault === true) {
        id = data.addressId;
      }
    });
    return id;
  };
  useEffect(() => {
    if (Isedit && props.payobject.amount === 0) {
      setTimeout(() => {
        proccess();
      }, 1000);
    } else {
      if (Isedit && props.payobject) {
        setTimeout(() => {
          ref.current.click();
        }, 1000);
      }
    }
  }, [props.payobject, Isedit]);

  const proccess = async () => {
    const payload = {
      fulfilmentType: selectedChannel.name,
      orderId: payobject.udf2,
      outletId: selectedStore.outletId,
      siteId: selectedStore.siteId,
    };
    await ProfileService.processOrder(payload, userObject.headerSessionId).then(
      (res) => {
        if (res.response === true) {
          history.push("/paysuccess");
        }
      }
    );
  };

  return (
    <>
      <form method="post" action={payobject.url}>
        <input type="hidden" name="key" value={payobject.key} />
        <input type="hidden" name="txnid" value={payobject.txnid} />
        <input type="hidden" name="amount" value={payobject.amount} />
        <input type="hidden" name="firstname" value={payobject.firstname} />
        <input type="hidden" name="email" value={payobject.email} />
        <input type="hidden" name="phone" value={payobject.phone} />
        <input type="hidden" name="productinfo" value={payobject.productinfo} />
        <input type="hidden" name="surl" value={payobject.surl} />
        <input type="hidden" name="furl" value={payobject.furl} />
        <input type="hidden" name="hash" value={payobject.hash} />
        <input type="hidden" name="udf1" value={payobject.udf1} />
        <input type="hidden" name="udf2" value={payobject.udf2} />
        <input type="hidden" name="udf3" value={payobject.udf3} />
        <input type="hidden" name="udf4" value={payobject.udf4} />

        <button
          type="submit"
          style={{ display: "none" }}
          class="btn btn-primary mt-5 "
          ref={ref}
        >
          Proceed to pay
        </button>
      </form>

      <div className="showFooter">

        <section class="footer-payment1">
          {isLoggedIn ? (
            <div class="payment-withlogin">
              <div class="loc-footer">
                <div class="loc-home" style={{ width: "100%" }}>
                  <div>
                    {fullAddress ? (
                      <>
                      {props.selectedChannel.name==="DELIVERY"?
                      <>
                      <div className=""></div>
                      <div>
                        <img src={location} />
                      </div>
                      <div
                        class="footer-home-details"
                        style={{ width: "87%" }}
                      >
                        <span>{fullAddress.addressTypeName}</span>
                        <div
                          class="loc-change"
                          onClick={() =>
                            history.push({
                              pathname: "/address",
                              setDefalut: true,
                            })
                          }
                        >
                          <span>CHANGE</span>
                        </div>
                        <div

                        >
                          <small>
                            {fullAddress.streetAddress1},{fullAddress.streetAddress2}
                          </small>
                          {props.userObject?.DefaultAddress &&
                          <small style={{color:"red",fontSize:"9px"}}>
                              This Address is not ServiceAble
                            </small>}
                        </div>
                      </div>
                      </>:null
                    }
                        
                      </>
                    ) : (
                      <div>
                        <div>
                          <img src={location} />
                        </div>
                        <div class="footer-home-details">
                          <span style={{ fontSize: 15 }}>
                            Please change delivery address to proceed
                          </span>
                          <div
                            class="loc-change"
                            onClick={() =>
                              history.push({
                                pathname: "/address",
                                setDefalut: true,
                              })
                            }
                          >
                            {props.userObject?.customerAddress?.length > 0 ? <span>CHANGE</span> : <span>ADD</span>}
                          </div>
                        </div>
                      </div>
                    )}


                    {IsEnable && window.innerWidth<760 ? (
                      <Modal
                        className="modal-opacity"
                        id="addAddressModal"
                        show={showModel}
                        onHide={flipStore}
                      >
                        <div class="modal-header">
                          <h5 class="modal-title">Store Closed</h5>
                          {/* <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                          ></button> */}
                        </div>
                        <div class="modal-body">
                          <p>
                            Store is currently unserviceable please change the store and try
                            again.
                          </p>
                        </div>
                        <div class="modal-footer">
                          <button
                            type="button"
                            class="btn btn-secondary"
                            data-bs-dismiss="modal"
                            onClick={() => setShowModel(false)}
                          >
                            Close
                          </button>
                          <button
                            type="button"
                            class="btn btn-primary"
                            onClick={() => history.push("/")}
                          >
                            Back To Home
                          </button>
                        </div>
                      </Modal>
                    ) : (
                      ""
                    )}

                    {/* <div>
                    <div>
                      <img src={location} />
                    </div>
                    <div class="footer-home-details">
                      <span>Please Select Dilevery address</span>
                      <div
                        class="loc-change"
                        onClick={() =>
                          history.push({
                            pathname: "/address",
                            setDefalut: true,
                          })
                        }
                      >
                        <span>CHANGE</span>
                      </div>
                      </div>
                      </div> */}
                  </div>
                </div>
                <div class="buttons-payment">
                  <button>
                    ₹
                    {props.Wallet.isUse
                      ? (
                        props.Wallet.useAmount?.value -
                        props.Wallet.walletAmount
                      ).toFixed(2)
                      : props.Wallet.useAmount.value}
                  </button>
                  <button
                    style={{ backgroundColor: Address || !notUser||props.userObject?.DefaultAddress&& props.selectedChannel.sellerId==12 ? "#DC492F" : "grey" }}
                    onClick={getPaymentDetails}
                    disabled={
                      props.userObject?.DefaultAddress&& props.selectedChannel.sellerId==12||
                      !Address ||
                      !notUser 
                      // selectedChannel.captureCustomerAddress == false
                    }
                  >
                    Make payment
                  </button>
                </div>
              </div>
            </div>
          ) : (
            <div class="payment-withoutlogin">
              <div class="loc-footer">
                <div class="loc-home">
                  <div className="addressDiv">
                    <div class="footer-home-details" style={{width:"100%"}}>
                      <span>Almost There</span>
                      <small style={{paddingTop:"3%"}}>Login or Signup to place your order now</small>
                    </div>
                  </div>
                </div>
                <div class="buttons-payment">
                  <button onClick={() => history.push("/login")} style={{background:"#DC492F"}}>
                    Continue
                  </button>
                </div>
              </div>
            </div>
          )}
        </section>
      </div>
    </>
  );
}
const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject || {};
  const userObject = reducerObj.userObject.userObject || {};
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const { isLoggedIn = false } = reducerObj.userObject;
  const Wallet = reducerObj.Wallet;
  const payobject = reducerObj.PayObject.pay;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;

  return {
    payobject,
    cartObject,
    userObject,
    selectedChannel,
    Wallet,
    isLoggedIn,
    selectedStore,
  };
};

export default connect(mapStateToProps, { updatepayObject })(CheckoutFooter);

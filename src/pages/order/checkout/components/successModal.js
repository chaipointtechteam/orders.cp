import React from "react";
import { Modal } from "react-bootstrap";
import styles from "../styles/offermodal.module.scss";

export const SuccessModal = (props) => {
  const { showModal, data = {}, onHide } = props;
  return (
    <>
      <div className={styles["content"]}>
        <div className={styles["title"]}>{data.code}</div>
        <div className={styles["description"]}>{data.description}</div>
      </div>
      <div className={styles["footer"]}></div>
    </>
  );
};

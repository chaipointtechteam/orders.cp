import React, { useEffect, useState } from "react";
import { Modal, Button } from "react-bootstrap";
import { connect } from "react-redux";
import styles from "../styles/offermodal.module.scss";
import { Link } from "react-router-dom";
import { emptyCartItem } from "../../../../actions/CartAction";
import { removeCouponObject } from "../../../../actions/CouponAction";
import { removPayObj } from "../../../../actions/PayAction";
import { resetMenuItemList } from "../../../../actions/MenuItemsAction";
import toast from "../../../../components/common/commonToast";
import ProfileService from "../../../../utils/services/profile/profile-service";
import { isMobileOnly } from "react-device-detect";
const PaymentSuccess = (props) => {
  const { showModal, data = {}, onHide } = props;
  const [order, setOrder] = useState(props.PayObject)
  useEffect(() => {

    props.cartObject.cartItems.forEach((cart) => {
      emptyCartItem(props.cartObject.cartItems, cart);
    });
    props.resetMenuItemList(props.menuItemList);

    if (props.CouponObject.isUse) {
      removeCoupon();
    }
    // props.removPayObj();
  }, []);

  const removeCoupon = async () => {

    const payload = {
      cartRequestWebDTO: {
        cartItemList: props.CouponObject.CouponObject.cartRequestWebDTO,
        fulfilmentEntityId:
          props.CouponObject.CouponObject.fullfill.fulfilmentEntityId,
        fulfilmentEntityType:
          props.CouponObject.CouponObject.fullfill.fulfilmentEntityType,
        fulfilmentType: props.CouponObject.CouponObject.fullfill.fulfilmentType,
        lookupId: props.CouponObject.CouponObject.fullfill.lookupId,
        isApplying: false,
        totalAmount: props.CouponObject.CouponObject.fullfill.totalAmount,
        couponCode: props.CouponObject.CouponObject.couponCode,
        sellerId: props.CouponObject.CouponObject.fullfill.sellerId,
      },
      applyCouponResponse: props.CouponObject.CouponObject.applyCouponResponse,
      refresh: false,
    };

    await ProfileService.applyCoupon(payload, props.userObject.headerSessionId)
      .then((e) => {

        props.removeCouponObject();
        // props.RemoveapplyCoupon();
      })
      .catch((e) => {

        props.RemoveapplyCoupon();
      });
  };

  return (
    <>
      <div className={styles["payment-modal-container"]}>
        <div className={styles["paycontainer"]}>
          <div className={styles["content"]}>
            {/* <div className={styles["title"]}>Payment Details</div> */}
            <div>
              <img
                height="180px"
                width="300px"
                src={
                  require("../../../../assets/images/PaymentSuccessful.svg")
                    .default
                }
              />
            </div>
            <div className={styles["title"]}> Payment Successful!</div>
            <div className={styles["description"]}>
              Thank you for ordering. We received your order and will begin
              processing it soon. Your order information appears below.
            </div>
          </div>
          <div className={styles["order-number"]}>Order No. {" "}
            {order.pay.txnid}
            {" "}|{" "}<span className={styles["spanText"]}>
              {props.selectedChannel.displayName}
            </span> </div>
          <div className={styles["footer"]}>

            <div >
           <Link to={isMobileOnly?"/orders":"/profile"}>
                {" "}
                <button
                  className={styles["button1"]}
                  onClick={() => props.removPayObj()}
                >
                  {" "}
                  My Order
                </button>
              </Link>
            </div>
            <div>
              <Link to="/products">
                {" "}
                <button
                  type="primary"
                  title="Login"
                  className={styles["button"]}
                  onClick={() => props.removPayObj()}
                >
                  {" "}
                  Return to Menu
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
    //   </Modal.Body>
    // </Modal>
  );
};

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const CouponObject = reducerObj.CouponObject;
  const userObject = reducerObj.userObject.userObject;
  const PayObject = reducerObj.PayObject;
  const selectedChannel = reducerObj.selectedChannel.selectedChannel.selectedChannel;
  return { cartObject, menuItemList, CouponObject, userObject, PayObject, selectedChannel };
};
export default connect(mapStateToProps, {
  emptyCartItem,
  resetMenuItemList,
  removeCouponObject,
  removPayObj
})(PaymentSuccess);

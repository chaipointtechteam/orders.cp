import React, { Component } from "react";
import OrderSummaryItem from "./OrderSummaryItem";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

class OrderedItemList extends Component {
  render() {
    const { cartObject } = this.props;
    return (
      <>
        {cartObject &&
          cartObject.cartItems &&
          cartObject.cartItems.map((item, index) => (
            <OrderSummaryItem key={index} cartItem={item} />
          ))}
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  return { cartObject };
};

export default connect(mapStateToProps, {})(OrderedItemList);

OrderedItemList.propTypes = {
  cartObject: PropTypes.object.isRequired,
};

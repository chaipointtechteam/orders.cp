import React, { Component } from "react";
import { deliveryCost, defaultTax } from "../../../../common/constants";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

class OrderBillDetails extends Component {
  calculateTotal = () => {
    var subtotal = parseFloat(this.props.cartObject.subTotal);
    var total = subtotal + deliveryCost + defaultTax;
    return total;
  };

  render() {
    return (
      <div class="col-xs-12 cart-subtotal">
        <h4 class="product-name order-bill">
          <strong>Bill Details</strong>
        </h4>
        <div class="item-total">
          <div class="col-xs-9">
            <h4 class="product-name">Item Total</h4>
          </div>
          <div class="col-xs-3">
            <h4 class="product-subtotal">₹{this.props.cartObject.subTotal}</h4>
          </div>
        </div>
        <div class="item-total">
          <div class="col-xs-9">
            <h4 class="product-name">Delivery Charges</h4>
          </div>
          <div class="col-xs-3">
            <h4 class="product-subtotal">₹{deliveryCost}</h4>
          </div>
        </div>
        <div class="taxes_bill">
          <div class="col-xs-9">
            <h4 class="product-name">Taxes & Charges</h4>
          </div>
          <div class="col-xs-3">
            <h4 class="product-subtotal">₹{defaultTax}</h4>
          </div>
        </div>
        <div class="total-bill">
          <div class="col-xs-9">
            <h4 class="product-name">Total</h4>
          </div>
          <div class="col-xs-3">
            <h4 class="product-subtotal">₹{this.calculateTotal()}</h4>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  return { cartObject };
};

export default connect(mapStateToProps, {})(OrderBillDetails);

OrderBillDetails.propTypes = {
  cartObject: PropTypes.object.isRequired,
};

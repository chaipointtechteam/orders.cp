import { faChevronRight, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { isMobileOnly } from "react-device-detect";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  applyCoupon,
  RemoveapplyCoupon,
  removeCouponObject,
  updateCouponObject,
  updateRefreshCart
} from "../../../../actions/CouponAction";

import { removeWallet, updateAmount } from "../../../../actions/WalletAction";
import toast from "../../../../components/common/commonToast";
import ProfileService from "../../../../utils/services/profile/profile-service";
import { getWalletDetails } from "../../../../utils/services/service/getWalletDetail";
import Payment from "../components/Payment";
import styles from "../styles/checkout.module.scss";
import OfferModal from "./offermodal";
import OrderedItemList from "./OrderedItemList";
export const deliveryCost = 0;
export const defaultTax = 0;

export const OFFER_MODAL = {
  COUPON: "coupon",
  LOYALITY: "loyalty"
};
const OrderSummary = (props) => {
  const [showModal, setshowModal] = useState("");
  const [status, setstatus] = useState(false);
  const [successCode, setsuccessCode] = useState(null);
  const [walletPoint, setwalletPoint] = useState("");
  const [amountData, setamountData] = useState({});
  const [amountData2, setamountData2] = useState([]);
  const [walletAmount, setwalletAmount] = useState("");
  const [walletModel, setwalletModel] = useState("");
  const [cart, setCart] = useState(0);
  const [apply, setApply] = useState(0);
  const [coupon, setCoupon] = useState(false);
  const [paymentData, setPaymentData] = useState({
    lookupId: "",
    sellerId: "",
  });
  // create cart>aplyc>orderS>creatyorder>
  // carecart>createorder>

  let history = useHistory();

  useEffect(() => {
    getWalletDetails(props.sessionId)
      .then((data) => {
        setwalletPoint(data.walletAmount);
      })
      .catch((e) => { });

    if (props.Wallet.isUse) {
      setwalletAmount("");
      props.removeWallet();
    }
    if (props.CouponObject.isUse) {
      if (props.state?.name !== "mobileCoupon") {
        removeApply();
      }
    }
  }, [props.isLoggedIn]);

  useEffect(() => {
    if (props.state?.name !== "mobileCoupon") {
      calculateTotal();
    } else {
      RefreshCart();
    }
    setCoupon(false);
    setCart(cart + 1);
  }, []);

  useEffect(() => {

    if (cart > 0 && props.CouponObject.refreshCart) {
      RefreshCart();
    }
    if (props.Wallet.isUse) {
      setwalletAmount("");
      props.removeWallet();
    }
  }, [props.cartObject.subTotal, props.CouponObject.isUse]);

  useEffect(() => {
    if (props.CouponObject.isUse) {
      if (cart > 0 && coupon) {        
        // applyCouponRefresh();
      }
    }
  }, [props.CouponObject.CouponObject.cartRequestWebDTO]);

  const handleApplyCoupon = () => {
    if (isMobileOnly) {
      history.push({
        pathname: "/coupon",
      });
    } else {
      setshowModal(OFFER_MODAL.COUPON);
    }
    setCoupon(true);
  };

  const handleLoyalityModal = () => {
    // this.setState({ walletModel: true })
    setshowModal(OFFER_MODAL.LOYALITY);
  };

  const RefreshCart = () => {
    const payload = {
      cartItemList: props.cartObject.CreateCart.map((k, index) => {
        console.log(props.CouponObject)
        return {
          discount: k.discount,
          itemId: k.itemId,
          // "itemName": k.displayName,
          itemType: k.itemType,
          price: k.price,
          quantity: k.quantity,
          parentId: k.parentId,
          subItems: k.subItems,
          items: props.CouponObject.refreshCart.cartItemList[index].items,
          id: props.CouponObject.refreshCart.cartItemList[index].id,
          name: props.CouponObject.refreshCart.cartItemList[index].name,
          chargeList:
            props.CouponObject.refreshCart.cartItemList[index].chargeList,
        };
      }),
      couponcode: props.CouponObject.isUse
        ? props.CouponObject.CouponObject.couponCode
        : "",
      fulfilmentEntityId: props.CouponObject.refreshCart.fulfilmentEntityId,
      isApplying: props.CouponObject.isUse ? true : "",
      fulfilmentEntityType: props.CouponObject.refreshCart.fulfilmentEntityType,
      fulfilmentType: props.CouponObject.refreshCart.fulfilmentType,
      lookupId: props.CouponObject.refreshCart.lookupId,
      sellerId: props.selectedChannel.sellerId,
      // "refresh": true,
    };
    ProfileService.RefreshCart(payload, props.sessionId.headerSessionId).then(
      (response) => {
        props.updateCouponObject({
          ...props.CouponObject.CouponObject,
          cartRequestWebDTO: response.cartItems,
          fullfill: {
            fulfilmentEntityId: response.fulfilmentEntityId,
            fulfilmentEntityType: response.fulfilmentEntityType,
            fulfilmentType: response.fulfilmentType,
            lookupId: response.lookupId,
            sellerId: props.selectedChannel.sellerId,
            totalAmount: response.chargeDtoObjects.find(
              (o) => o.name === "Bill Total"
            ).value,
          },
        });
        if (!props.CouponObject.isUse) {
          setamountData2(response.chargeDtoObjects);
          var dis = response.chargeDtoObjects.find(
            (o) => o.name === "Discount"
          );
          var cgst = response.chargeDtoObjects.find((o) => o.name === "CGST");
          var sgst = response.chargeDtoObjects.find(
            (o) => o.name === "SGST/UTGST"
          );

          var bill = response.chargeDtoObjects.find(
            (o) => o.name === "Bill Total"
          );
          var taxes = cgst.value + sgst.value;
          var discount = dis?.value;
          var disc = Math.abs(discount);
          setamountData({
            discount: disc?.toFixed(2),
            tax: taxes.toFixed(2),

            bill: bill.value.toFixed(2),
          });
          props.updateAmount(bill);
        } else {
          var dis = props.CouponObject.CouponObject.chargeDtoObjects.find(
            (o) => o.name === "Discount"
          );

          var cgst = props.CouponObject.CouponObject.chargeDtoObjects.find(
            (o) => o.name === "CGST"
          );
          // var delivery = props.CouponObject.CouponObject.chargeDtoObjects.find(
          //   (o) => o.name === "Delivery Charges"
          // );
          var bill = props.CouponObject.CouponObject.chargeDtoObjects.find(
            (o) => o.name === "Bill Total"
          );
          var sgst = props.CouponObject.CouponObject.chargeDtoObjects.find(
            (o) => o.name === "SGST/UTGST"
          );
          var taxes = cgst.value + sgst.value;
          // var discount = dis.value;
          var disc = Math.abs(discount);
          setamountData2(props.CouponObject.CouponObject.chargeDtoObjects);
          setamountData({
            discount: disc.toFixed(2),
            tax: taxes.toFixed(2),
            // delivery: delivery.value.toFixed(2),
            bill: bill.value.toFixed(2),
          });
          props.updateAmount(bill);
        }
      }
    );
  };

  const applyCouponRefresh = () => {
    var rString = randomString(
      32,
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    );
    const payload = {
      cartRequestWebDTO: {
        cartItemList: props.CouponObject.CouponObject.cartRequestWebDTO,
        fulfilmentEntityId:
          props.CouponObject.CouponObject.fullfill.fulfilmentEntityId,
        fulfilmentEntityType:
          props.CouponObject.CouponObject.fullfill.fulfilmentEntityType,
        fulfilmentType: props.CouponObject.CouponObject.fullfill.fulfilmentType,
        lookupId: props.CouponObject.CouponObject.fullfill.lookupId,
        isApplying: true,
        uuid: rString,
        oldUuid: props.CouponObject.CouponObject.uuid,
        totalAmount: props.CouponObject.CouponObject.fullfill.totalAmount,
        couponCode: props.CouponObject.CouponObject.couponCode,
        sellerId: props.CouponObject.CouponObject.fullfill.sellerId,
      },
      applyCouponResponse: props.CouponObject.CouponObject.applyCouponResponse,
      refresh: true,
    };

    ProfileService.applyCoupon(payload, props.sessionId.headerSessionId)
      .then((e) => {
        if (e.applyCouponResponse !== null) {
          // toast.error("Invalid Coupon Code", { closable: true });
        } else {
          props.updateCouponObject({
            ...props.CouponObject.CouponObject,
            applyCouponResponse: e.applyCouponResponse,
            chargeDtoObjects: e.cartResponseWebDTO.chargeDtoObjects,
          });
          props.applyCoupon();
          var dis = e.cartResponseWebDTO.chargeDtoObjects.find(
            (o) => o.name === "Discount"
          );

          var cgst = e.cartResponseWebDTO.chargeDtoObjects.find(
            (o) => o.name === "CGST"
          );
          // var delivery = e.cartResponseWebDTO.chargeDtoObjects.find(
          //   (o) => o.name === "Delivery Charge"
          // );
          var bill = e.cartResponseWebDTO.chargeDtoObjects.find(
            (o) => o.name === "Bill Total"
          );
          var sgst = e.cartResponseWebDTO.chargeDtoObjects.find(
            (o) => o.name === "SGST/UTGST"
          );
          var taxes = cgst.value + sgst.value;
          // var discount = dis.value;
          // var disc = Math.abs(discount);
          setamountData2(e.cartResponseWebDTO.chargeDtoObjects);
          setamountData({
            // discount: disc.toFixed(2),
            tax: taxes.toFixed(2),
            //delivery: delivery.value.toFixed(2),
            bill: bill.value.toFixed(2),
          });
          props.updateAmount(bill);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const removeApply = async () => {
    var rString = randomString(
      32,
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    );
    const payload = {
      cartRequestWebDTO: {
        cartItemList: props.CouponObject.CouponObject.cartRequestWebDTO,
        fulfilmentEntityId:
          props.CouponObject.CouponObject.fullfill.fulfilmentEntityId,
        fulfilmentEntityType:
          props.CouponObject.CouponObject.fullfill.fulfilmentEntityType,
        fulfilmentType: props.CouponObject.CouponObject.fullfill.fulfilmentType,
        lookupId: props.CouponObject.CouponObject.fullfill.lookupId,
        isApplying: false,
        oldUuid: props.CouponObject.CouponObject.uuid,
        // oldUuid: null,
        uuid: rString,
        totalAmount: props.CouponObject.CouponObject.fullfill.totalAmount,
        couponCode: props.CouponObject.CouponObject.couponCode,
        sellerId: props.CouponObject.CouponObject.fullfill.sellerId,
      },
      applyCouponResponse: props.CouponObject.CouponObject.applyCouponResponse,
      refresh: false,
    };

    await ProfileService.applyCoupon(payload, props.sessionId.headerSessionId)
      .then((e) => {
        // e.applyCouponResponse.status === "FAILURE"
        //   ? toast.error(e.status, { closable: true })
        //   : toast.success(e.status, { closable: true });
        props.updateCouponObject({
          ...props.CouponObject.CouponObject,
          applyCouponResponse: "",
          chargeDtoObjects: "",
          couponCode: "",
        });
        props.RemoveapplyCoupon();
        if (props.Wallet.isUse) {
          
          toast.warning("CP Wallet is removed as order value is updated. Please redeem the Wallet again.")
        }
        toast.info("COUPON REMOVED SUCCESSFULLY", { closable: true })
      })
      .catch((e) => {
        props.RemoveapplyCoupon();
      });

    setsuccessCode(null);
    setshowModal("");
  };

  const handleCloseModal = () => {
    setshowModal("");
  };

  function randomString(length, chars) {
    var result = "";
    for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  // const applyCoupon = async () => {

  // }

  // const calculateAfterApplyCoupon = () => {

  //   var dis = props.CouponObject.CouponObject.chargeDtoObjects.find((o) => o.name === "Discount");

  //   var cgst = props.CouponObject.CouponObject.chargeDtoObjects.find(
  //     (o) => o.name === "CGST"
  //   );
  //   var delivery = props.CouponObject.CouponObject.chargeDtoObjects.find(
  //     (o) => o.name === "Delivery Charge"
  //   );
  //   var bill = props.CouponObject.CouponObject.chargeDtoObjects.find(
  //     (o) => o.name === "Bill Total"
  //   );
  //   var sgst = props.CouponObject.CouponObject.chargeDtoObjects.find(
  //     (o) => o.name === "SGST/UTGST"
  //   );
  //   var taxes = cgst.value + sgst.value
  //   var discount = dis.value
  //   var disc = Math.abs(discount)
  //   setamountData({
  //     discount: disc.toFixed(2),
  //     tax: taxes.toFixed(2),
  //     delivery: delivery.value.toFixed(2),
  //     bill: bill.value.toFixed(2),
  //   })
  //   props.updateAmount(bill)
  // }

  const calculateTotal = async () => {
    var rString = randomString(
      32,
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    );
    const dataPayload = {
      cartItemList: props.cartObject.CreateCart,
      fulfilmentEntityId: props.selectedStore.outletId,
      fulfilmentEntityType: "STORE",
      fulfilmentType: props.selectedChannel.name,
      requestId: rString,
      sellerId: props.selectedChannel.sellerId,
    };

    await ProfileService.createCart(dataPayload, props.sesssionId).then(
      (res) => {
        if (res) {
          props.updateCouponObject({
            cartRequestWebDTO: res.cartItems,
            fullfill: {
              fulfilmentEntityId: res.fulfilmentEntityId,
              fulfilmentEntityType: res.fulfilmentEntityType,
              fulfilmentType: res.fulfilmentType,
              lookupId: res.lookupId,
              sellerId: props.selectedChannel.sellerId,
              totalAmount: res.chargeDtoObjects?.find(
                (o) => o.name === "Bill Total"
              ).value,
            },
          });

          props.updateRefreshCart({
            cartItemList: props.cartObject.CreateCart.map((k, index) => {
              return {
                id: res?.cartItems[index]?.id,
                name: res?.cartItems[index]?.name,
                items: res?.cartItems[index]?.items,
                chargeList: res?.cartItems[index]?.chargeList,
              };
            }),

            fulfilmentEntityId: res.fulfilmentEntityId,
            fulfilmentEntityType: res.fulfilmentEntityType,
            fulfilmentType: res.fulfilmentType,
            lookupId: res.lookupId,
            sellerId: props.selectedChannel.sellerId,
          });

          if (res.chargeDtoObjects) {
            var cgst = res.chargeDtoObjects.find((o) => o.name === "CGST");
            var delivery = res.chargeDtoObjects.find(
              (o) => o.name === "Delivery Charge"
            );
            var deliverys = res.chargeDtoObjects.find(
              (o) => o.name === "Delivery Charges"
            );

            var bill = res.chargeDtoObjects.find(
              (o) => o.name === "Bill Total"
            );
            var sgst = res.chargeDtoObjects.find(
              (o) => o.name === "SGST/UTGST"
            );
            var taxes = cgst.value + sgst.value;
            setamountData2(res.chargeDtoObjects);
            setamountData({
              tax: taxes.toFixed(2),
              delivery:
                delivery !== undefined ? delivery.value : deliverys.value,
              bill: bill.value.toFixed(2),
            });
            props.updateAmount(bill);
            props.PatmentData(res.lookupId, dataPayload.sellerId);
            setPaymentData({
              lookupId: res.lookupId,
              sellerId: dataPayload.sellerId,
            });
          } else {
          }
        }
      }
    );
  };

  const renderBillDetails = () => {
    return (
      <div class="col-xs-12 cart-subtotal cart-responsive ">
        <div className={styles["bill-container"]}>
          <h4 className={styles["product-name"]}>
            <strong>Bill Details</strong>
          </h4>
          {/* <div className={styles["item-total"]}>
            <div class="col-xs-9">
              <h4 className={styles["product-name"]}>Item Total</h4>
            </div>
            <div style={{ marginTop: "8px" }} class="col-xs-3">
              <h4 className={styles["product-subtotal"]}>
                ₹{props.cartObject.subTotal}
              </h4>
            </div>
          </div> */}
          {amountData2.map((charges) => (
            <>
              {charges.name !== "Round off" &&
                charges.name !== "Bill Total" &&
                charges.value !== 0 ? (
                <>
                  <div class="col-xs-9">
                    <h4 className={styles["product-name"]}>{charges.name}</h4>
                  </div>
                  <div class="col-xs-3">
                    <h4 className={styles["product-subtotal"]}>
                      {charges.value < 0 ? "-₹" : "₹"}
                      {Math.abs(charges.value.toFixed(2))}
                    </h4>
                  </div>
                </>
              ) : null}
            </>
          ))}
          {amountData.discount !== undefined &&
            amountData.discount !== "NaN" ? (
            <div className={styles["item-total"]}>
              <div class="col-xs-9">
                <h4 className={styles["product-name"]}>Coupon Discount</h4>
              </div>
              <div class="col-xs-3">
                <h4 className={styles["product-subtotal"]}>
                  -₹{amountData.discount}
                </h4>
              </div>
            </div>
          ) : (
            ""
          )}
          {props.Wallet.isUse ? (
            <div className={styles["item-total"]}>
              <div class="col-xs-9">
                <h4 className={styles["product-name"]}>Wallet Amount</h4>
              </div>
              <div class="col-xs-3 walletAmount">
                <h4 className={styles["product-subtotal"]}>
                  -₹{props.Wallet.walletAmount}
                </h4>
              </div>
            </div>
          ) : (
            ""
          )}
          {/* {amountData.delivery > 0 ? (
            <div className={styles["item-total"]}>
              <div class="col-xs-9">
                <h4 className={styles["product-name"]}>Delivery Charges</h4>
              </div>
              <div style={{ marginTop: "8px" }} class="col-xs-3">
                <h4 className={styles["product-subtotal"]}>
                  ₹{amountData.delivery}
                </h4>
              </div>
            </div>
          ) : (
            ""
          )} */}

          <div className={styles["taxes-bill"]}>
            {/* <div class="col-xs-9">
              <h4
                style={{ marginTop: "8px" }}
                className={styles["product-name"]}
              >
                Taxes & Charges
              </h4>
            </div> */}

            {/* <div style={{ marginTop: "8px" }} class="col-xs-3">
              <h4 className={styles["product-subtotal"]}>₹ {amountData.tax}</h4>
            </div> */}

            <div className={styles["total-bill"]}>
              <div className="totalTop">
                <h4 className={styles["product-name"]}>Total</h4>
              </div>
              <div className="totalTop totalBill">
                <h4 className={styles[""]}>
                  ₹
                  {props.Wallet.isUse
                    ? (amountData.bill - props.Wallet.walletAmount).toFixed(2)
                    : amountData.bill}
                </h4>
              </div>
            </div>
          </div>
        </div>
        <Payment paymentData={paymentData} />
      </div>
    );
  };

  const handleSuccess = (code) => {
    setsuccessCode(code);
    setshowModal("");
  };

  const handleLoyal = (data) => {
    setstatus(data.data);
    setwalletAmount(data.walletAmount);
    setshowModal("");
  };

  // const { showModal = "", successCode } = this.state;
  return (
    <div class="col-lg-5">
      {
        <OfferModal
          showModal={!!showModal}
          data={walletPoint}
          onLoyalty={handleLoyal}
          type={showModal}
          onApply={handleSuccess}
          onHide={handleCloseModal}
        />
      }
      <div className={styles["order-summary-container"]}>
        <div class="order-mobile-header">
          <h4
            className={styles["order-title"]}
            style={{ marginTop: isMobileOnly ? "5%" : "" }}
          >
            Order Summary{" "}
            <label 
              onClick={() => {
                props.CouponObject.CouponObject.couponCode && removeApply()                
                history.push("/products")
              }}
            >
              Back to Menu
            </label>
          </h4>
        </div>
        <div className={styles["summary-body"]}>
          <div className="summary-itemscroll">
            <OrderedItemList />
          </div>

          <div className="seperateforMobile">
            {!props.sessionId.headerSessionId == "" && walletPoint > 0 ? (
              <div className="col-xs-12 seperate-order seperate-loyalty" onClick={handleLoyalityModal}>
                <div className={styles["loyalty-points"]} > 
                  <div style={{display:"flex",alignItems:"center"}}>
                    <img
                      src={
                        require("../../../../assets/images/Loyalty Points.svg")
                          .default
                      }
                    />
                    {/* {props.Wallet.isUse ? (
                      <span>{props.Wallet.walletAmount}</span>
                    ) : (
                      ""
                    )} */}
                    
                    {!props.Wallet.isUse && (<span>Use CP Wallet</span>)}
                    {props.Wallet.isUse && (
                      <>
                      <span>
                         
                        <div>
                      <div>
                          <span style={{fontSize:"15px"}}>Redeem CP wallet : ₹{walletPoint}</span>
                          </div>
                          <div >
                            <span style={{fontSize:"14px",fontWeight:"normal",color:"#6d6e71"}}>
                            {props.Wallet.isUse?
                            `${props.Wallet.walletAmount} will be debited` 
                            :""}
                             </span>
                          </div>
                          </div>
                      </span>
                       </>
                      )
                    }
                    </div>
                  <div className={styles["oncursor"]}>
                    <div className={styles["oncursor"]}>
                      {!props.Wallet.isUse ? (
                        <FontAwesomeIcon
                          onClick={handleLoyalityModal}
                          size="lg"
                          icon={faChevronRight}
                        />
                      ) : (
                        <FontAwesomeIcon
                          icon={faTimes}
                          size="2x"
                          className="star-icon"
                          onClick={() => {
                            setwalletAmount("");
                            props.removeWallet();
                            setstatus(!status);
                            setshowModal("");
                          }}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            ) : null}

            {!props.sessionId.headerSessionId == "" ? (
              <div
              style={{top:"-18px"}}
                class="col-xs-12 seperate-order seperate-coupon"
                onClick={
                  !props.CouponObject.isUse ? handleApplyCoupon : removeApply
                }
              >
                <div className={styles["loyalty-points"]}>
                  <div style={{display:"flex",alignItems:"center"}}>
                    <img
                      height={"30px"}
                      width={"35px"}
                      src={
                        require("../../../../assets/images/Coupon.svg").default
                      }
                    />
                    {!props.CouponObject.isUse && (<span>Apply Coupon</span>)}
                    {props.CouponObject.isUse && (
                      <>
                        <span> 
                          <div>
                          <span style={{fontSize:"15px"}}>Coupon Applied - {props.CouponObject.CouponObject.couponCode}</span>
                          </div>
                          <div >
                            <span style={{color:"#6d6e71",fontSize:"14px",fontWeight:"normal"}}>
                            {props?.CouponObject?.CouponObject?.chargeDtoObjects?
                            `Total Discount: ₹ ${Math.abs(props?.CouponObject?.CouponObject.chargeDtoObjects?.find(o=>o.name=="Discount").value)  }`
                            :""}
                             </span>
                          </div>
                        </span>
                      </>
                    )}
                  </div>
                  

                  <div className={styles["oncursor"]}>
                    {!props.CouponObject.isUse ? (
                      <FontAwesomeIcon
                        onClick={handleApplyCoupon}
                        size="lg"
                        icon={faChevronRight}
                      />
                    ) : (
                      <FontAwesomeIcon
                        icon={faTimes}
                        size="2x"
                        className="star-icon"
                      // onClick={removeApply}
                      />

                      // <Button
                      //   style={{
                      //     float: "right",
                      //     borderRadius: "4px",
                      //     borderTopLeftRadius: "4px",
                      //     borderBottomLeftRadius: "4px",
                      //     padding: "5% 10%",
                      //   }}
                      //   variant="primary"
                      //   onClick={removeApply}
                      //   // setsuccessCode(null);
                      //   // setshowModal("");
                      // >
                      //   REMOVE
                      // </Button>
                    )}
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
          </div>
          {/* <OrderCoupon /> */}
          {renderBillDetails()}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (reducerObj) => {
  const isLoggedIn = reducerObj.userObject;
  const sessionId = reducerObj.userObject.userObject;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;

  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const cartObject = reducerObj.cartObject.cartObject;
  const Wallet = reducerObj.Wallet;
  const CouponObject = reducerObj.CouponObject;
  return {
    isLoggedIn,
    sessionId,
    selectedChannel,
    cartObject,
    Wallet,
    CouponObject,
    selectedStore,
  };
};
// const mapDispatchToProps=(dispatch)=>{
//   return{
//     removeWallet:()=>dispatch(removeWallet())
//   }
// }
export default connect(mapStateToProps, {
  removeWallet,
  updateCouponObject,
  removeCouponObject,
  applyCoupon,
  updateAmount,
  updateRefreshCart,
  RemoveapplyCoupon,
})(OrderSummary);

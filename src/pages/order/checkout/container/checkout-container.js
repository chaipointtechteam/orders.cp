import React, { Component } from "react";
import { connect } from "react-redux";
import { getProfile } from "../../../../../src/actions/LoginAction";
import { updateUserObject } from "../../../../../src/actions/UserAction";
import AddressModal from "../../../../components/common/address/address-container";
import DetailsHeader from "../../../../components/products/detailsHeader";
import MobileHeader from "../../../../components/products/MobileHeader";
import Account from "../components/Account";
import CheckoutFooter from "../components/CheckoutFooter";
import Delivery from "../components/Delivery";
import OrderSummary from "../components/OrderSummary";
import styles from "../styles/checkout.module.scss";

class Checkout extends Component {
  state = {
    
    showAddressModal: false,
    paymentData: { lookupId: "", sellerId: "" },
  };
  handleOnAddAdress = () => {
    this.setState({ showAddressModal: !this.state.showAddressModal });
  };
  setPatmentData = (lookupId, sellerId) => {
    this.setState({ paymentData: { lookupId: lookupId, sellerId: sellerId } });
  };
  
  componentDidMount = () => {
    const { userObject } = this.props;
    if (!(userObject.headerSessionId === "")) {
      const { phoneNo, headerSessionId } = userObject;
      this.props.getProfile(`${phoneNo}`, headerSessionId).then((res) => {
        this.props.updateUserObject(res);
      });
    }
  };
  render() {
    const { showAddressModal } = this.state;
    const { cartObject, userObject, selectedChannel } = this.props;
    const { headerSessionId } = userObject;

    return (
      <>
        <MobileHeader />
        <div className={styles["checkout-container"]}>
          <AddressModal
            show={showAddressModal}
            userObject={userObject}
            onClose={this.handleOnAddAdress}
          />
          <DetailsHeader />
          <section className={styles["category-header"]}>

            <div class="container category-mobile">

              <div class="row">
                <div class="col-xs-12 col-lg-12 col-sm-12 col-ms-12">
                  <div class="col-lg-7">
                    <Account />

                    {headerSessionId && (
                      <>
                        {selectedChannel.name === "DELIVERY" && (
                          <Delivery setServiceAble={this.setServiceAble} onAddAdress={this.handleOnAddAdress} />
                        )}
                      </>
                    )}

                    {/* need to check whether the selected channel is delivery or not 
                  show this only if its delivery */}
                    {/* <Payment paymentData={this.state.paymentData} /> */}
                  </div>
                  <OrderSummary
                    
                    cartObject={cartObject}
                    PatmentData={this.setPatmentData}
                    state={this.props.location.state}
                  />
                </div>
              </div>
            </div>
          </section>
        </div>
        <CheckoutFooter paymentData={this.state.paymentData} />
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject || {};
  const userObject = reducerObj.userObject.userObject || {};
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  return { cartObject, userObject, selectedChannel };
};
const mapDispatchToProps = (dispatchEvent, { sessionId }) => {
  return {
    getProfile: (payload, sessionId) =>
      dispatchEvent(getProfile(payload, sessionId)),
    updateUserObject: (payload) => dispatchEvent(updateUserObject(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Checkout);

import React, { Component } from "react";
import DetailsHeader from "../../components/products/detailsHeader";
import Account from "../../components/checkout/Account";
import Delivery from "../../components/checkout/Delivery";
import Payment from "../../components/checkout/Payment";
import OrderSummary from "../../components/checkout/OrderSummary";

export default class Checkout extends Component {
  render() {
    return (
      <>
        <DetailsHeader />
        <section class="Category-header fffffffffff">
          <div class="container hghg">
            <div class="row">
              <div class="col-xs-12 col-lg-12 col-sm-12 col-ms-12">
                <div class="col-lg-7">
                  <Account />
                  <Delivery />
                  {/* need to check whether the selected channel is delivery or not 
                  show this only if its delivery */}
                  <Payment />
                </div>
                <OrderSummary />
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

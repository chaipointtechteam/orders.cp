import React, { Component } from "react";
import Cart from "../../components/products/cart";
import LandingFooter from "../../components/landing/LandingFooter";
export default class MobileCart extends Component {
  render() {
    return (
      <>
        <section class="Category-header">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-lg-12 col-sm-12 col-ms-12">
                <Cart />
              </div>
            </div>
          </div>
        </section>
        <LandingFooter tab="1"/>
      </>
    );
  }
}

import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Loader from "../../components/loader/loader";
import { PropTypes } from "prop-types";
import { ConsoleSqlOutlined, TrophyOutlined } from "@ant-design/icons";

class PrivateRoute extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    const { userObject, component: Component, ...props } = this.props;
    const condition =
      // this.props.selectedChannel &&
      // this.props.selectedChannel.sellerId &&
      this.props.selectedStore && this.props.selectedStore.siteId;

    const condition1 = this.props.userObject.headerSessionId;
    const { loader, showAlert, error } = this.props.common;

    return (
      <>
        {this.props.name == true ? (
          <div>
            {loader ? <Loader /> : []}
            <Route
              {...props}
              render={(props) =>
                condition1 ? <Component {...props} /> : <Redirect to="/" />
              }
            />
          </div>
        ) : this.props.loadChange === true ? (
          <div>
            {loader ? <Loader changeTitle={true} /> : []}
            <Route
              {...props}
              render={(props) =>
                condition ? <Component {...props} /> : <Redirect to="/" />
              }
            />
          </div>
        ) : this.props.Simple === true ? (
          <div>
            {loader ? <Loader /> : []}
            <Route {...props} render={(props) => <Component {...props} />} />
          </div>
        ) : (
          <div>
            {loader ? <Loader /> : []}
            <Route
              {...props}
              render={(props) =>
                condition ? <Component {...props} /> : <Redirect to="/" />
              }
            />
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const userObject = reducerObj.userObject.userObject;
  return {
    selectedStore,
    selectedChannel,
    userObject,
    common: reducerObj.common,
  };
};

export default connect(mapStateToProps, {})(PrivateRoute);

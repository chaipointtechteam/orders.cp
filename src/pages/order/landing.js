import React, { Component } from "react";
import Banner from "../../components/landing/banner";
import LandingMainMenu from "../../components/landing/landingMainMenu";
import { googleMapApi } from "../../utils/constants";
import ReactDependentScript from "react-dependent-script";
import Recommend from "../../components/landing/recommend";
import Footer from "../../components/common/footer";
import LandingFooter from "../../components/landing/LandingFooter";
import { isMobileOnly } from "react-device-detect";

export default class Landing extends Component {
  render() {
    return (
      <>
        <ReactDependentScript
          loadingComponent={<div>google maps script is loading...</div>}
          scripts={[
            `https://maps.googleapis.com/maps/api/js?key=${googleMapApi}&libraries=places`,
          ]}
        >
          <Banner />
          <div style={{ marginBottom: isMobileOnly ? "7rem" : "" }}>
            <Recommend />
          </div>
        </ReactDependentScript>
        <LandingMainMenu />
        <Footer />
        <LandingFooter tab="0" />
      </>
    );
  }
}

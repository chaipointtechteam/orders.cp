import $ from "jquery";
import React, { Component } from "react";
import Recommend from "../../components/landing/recommend";
import Catlog from "../../components/products/catlog";
import CatlogHeader from "../../components/products/catlogHeader";
import DetailsHeader from "../../components/products/detailsHeader";
import Footer from "../../components/common/footer";
import MobileFooterCart from "../../components/products/MobileFooterCart";
import MobileHeader from "../../components/products/MobileHeader";
export default class ProductCatlog extends Component {
  componentDidUpdate() {
    if(window.innerWidth>760)
    {$(window).scroll(function(){
      var sticky = $('.sticky'),
          scroll = $(window).scrollTop();
      if (scroll >=100) sticky.addClass('fixed');
      else sticky.removeClass('fixed');
    });
    }
    if (window.innerWidth < 760) {
      $(window).scroll(function () {
        var sticky = $('.Category-header'),
          scroll = $(window).scrollTop();
        if (scroll >= 250) { sticky.addClass('mobileFix').removeClass('Category-header'); }
        else {
          var sticky2 = $('.mobileFix'),
            scroll = $(window).scrollTop();
          if (scroll <= 250) { sticky2.addClass('Category-header').removeClass('mobileFix'); }
        }
      });
    }
  }
  render() {
    
    
    return (
      <>
        <MobileHeader />
        <DetailsHeader />
        <Recommend />
        <section className="product-sticky">
          <div className="sticky-list sticky">
              <div className="sticky" style={{zIndex:"9999999999"}}>
                <CatlogHeader />
              </div>
              <Catlog />
          </div>
        </section>
        <Footer />           
        <MobileFooterCart />
      </>
    );
  }
}

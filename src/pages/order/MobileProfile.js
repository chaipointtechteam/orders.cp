import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { getProfile } from "../../../src/actions/LoginAction";
import { updateUserObject } from "../../../src/actions/UserAction";
import { emptyCartItem, resetCartObject } from "../../actions/CartAction";
import { loader } from "../../actions/common.actions";
import { clearAuth, logout } from "../../actions/LoginAction";
import { emptyMenuItemList, resetMenuItemList } from "../../actions/MenuItemsAction";
import {
  resetChannel, resetLocation, resetStore,
  updateSelectedChannel, updateSelectedLocation, updateSelectedStore
} from "../../actions/StoreListAction";
import { resetUserObject } from "../../actions/UserAction";
import toast from "../../components/common/commonToast";
import LandingFooter from "../../components/landing/LandingFooter";
import EditProfile from "../../components/profile/tabs/EditProfile";
import { header } from "../../utils/apiUtil";




const MobileProfile = (props) => {

  let history = useHistory();
  useEffect(() => {
    console.log("adskhasdasdgjkladgj")
    props.loader(false)
    console.log(props)
    if (props.model) {
      setIsedit(true)
    }
  }, [])

  const resetTheCart = () => {

    props.cartObject.cartItems.forEach((cart) => {
      props.emptyCartItem(props.cartObject.cartItems, cart);
    });
    // this.props.resetMenuItemList(this.props.menuItemList);

  };


  const logout = () => {
    const { userObject } = props;
    const newHeader = header;
    newHeader.sessionId = userObject.headerSessionId;
    console.log( userObject.headerSessionId,newHeader)
    
    props.logout(newHeader, (data) => {
      if (data) {
        props.clearAuth();
        resetTheCart()
        props.resetUserObject();
        props.emptyMenuItemList();
        props.resetCartObject();
        props.resetStore();
        props.resetChannel();
        props.resetLocation();
        props.updateSelectedStore({
          selectedStore: {},
        });
        props.updateSelectedChannel({
          selectedChannel: {},
        });
        props.updateSelectedLocation({
          selectedLocation: {},
        });
        localStorage.clear();
        toast.success("Logged out successfully", {});
        props.history.push("/");
             }

      })
    
    
   
    // //reset the cart
    //reset the user object
    //logout the user
    //redirect to home page
  };
  const [Isedit, setIsedit] = useState(false);
  const flipAdd = () => {
    setIsedit(false);
  };
  return (
    <>
      {Isedit ? (
        <Modal
          className="modal-opacity signup"
          id="addAddressModal"
          show={Isedit}
          onHide={flipAdd}
        >
          <Modal.Header closeButton></Modal.Header>
          <EditProfile onHide={flipAdd} />
        </Modal>
      ) : (
        ""
      )}
      <div className="showFooter">
        <div className="container">
          <div
            style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }}
            className=""
          > <div style={{ display: "flex", flexDirection: "row", padding: "3%", alignItems: "center", width: "93%" }}>
              <Link to="/">
                <div>
                  <img
                    className="back-arrow"
                    src={require("../../assets/images/left-arrow.svg").default}
                  />{" "}
                </div>
              </Link>
              <div style={{ marginLeft: "2%" }}>
                <div className="UserName">{props.userObject.name}</div>
                <div className="contact-details">
                  <span className="">
                    {props.userObject.phoneNo} | {props.userObject.email}
                  </span>
                </div>
              </div>
            </div>
            <div style={{ width: "7%" }}>
              <FontAwesomeIcon
                onClick={() => {
                  setIsedit(true);
                }}
                icon={faEdit}
                size="2x"
                className="editProfile"
              />
            </div>
          </div>
          <div className="header-Line"></div>
          <div className="item-list">
            <Link to="/orders" className="Link">
              <div className="menu-card">
                <span className="menu-item">Orders</span>
                <img
                  className="right-arrow"
                  src={require("../../assets/images/Right-arrow.svg").default}
                />
              </div>
            </Link>
            {/* <Link to="/payment" className="Link">
            <div className="menu-card">
              <span className="menu-item">Payments</span>
              <img
                className="right-arrow "
                src={require("../../assets/images/Right-arrow.svg").default}
              />
            </div>
          </Link> */}
            <Link to="/address" className="Link">
              <div className="menu-card">
                <span className="menu-item">Addresses</span>
                <img
                  className="right-arrow"
                  src={require("../../assets/images/Right-arrow.svg").default}
                />
              </div>
            </Link>
            {/* <div className="menu-card">
            <span className="menu-item">Loyalty-Points</span>
            <span className="points">500</span>
          </div> */}
          
              <div className="menu-card">
                <span onClick={logout} className="menu-item">Logout</span>
                <img
                  className="right-arrow"
                  onClick={logout}
                  src={require("../../assets/images/Right-arrow.svg").default}
                />
              </div>
        
          </div>
        </div>
        <LandingFooter tab="2" />
      </div>
    </>
  );
};

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const common = reducerObj.common.loader;
  const cartObject = reducerObj.cartObject.cartObject;
  return { userObject, common, cartObject };

};
export default connect(mapStateToProps, {
  loader, resetMenuItemList,
  getProfile,
  updateUserObject,
  resetUserObject,
  resetCartObject,
  resetStore,
  resetChannel,
  emptyMenuItemList,
  updateSelectedChannel,
  updateSelectedStore,
  updateSelectedLocation,
  logout,
  resetLocation,
  emptyCartItem,
  clearAuth
})(MobileProfile);

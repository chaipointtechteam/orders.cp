import React from 'react'
import Banner from './banner'
import BestSeller from './bestSeller'
import InstantChaiTea from './InstantChaiTea'
import ShopProducts from './ShopProducts'
import "./shopStyle.css"
import ShopText from "./shopText"
function shop() {
    return (
        <>
        <Banner/>
         <ShopText />
         <InstantChaiTea/>
        <BestSeller/>
        </>  
    ) 
}

export default shop

import React from 'react'
import "./shopStyle.css"
import ShopProducts from './ShopProducts'

function bestSeller() {
    return (
       
             <section class="shop-service-banner1" >
   <div class="container">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12 mobile-section">
            <h1 style={{fontWeight: "600",textAlign: "center"}}>Best Sellers</h1>
            <p style={{textAlign: "center"}}>Sweet & savory accompaniments for your chilling experience </p>
         </div>
         <div class="col-md-12 col-sm-12 col-xs-12 mobile-section" style={{padding: "20px 30px"}}>
         <ShopProducts />
          <ShopProducts />
          <ShopProducts/>
          <ShopProducts/>
         </div>
         
        </div>
        </div>
        
       </section>
       
    )
}

export default bestSeller

import React from 'react'
import image from "../../assets/images/Combos_Website_shopcp-01_540x.jpg"
import "./shopStyle.css"
function ShopProducts() {
    return (
        
            <div class="col-sm-3">
           <div class="panel panel-default text-center">
             <img src={image}
              style={{width:"100%"}} />
             <div class="panel-heading shop-basic-product" 
             style=
             {{padding: "10px",
             textAlign: "start",
             marginTop: "-20px"}}
             >
               <h4 style={{fontSize:"20px"}}>Assam Masala</h4>
               <p style={{fontSize:"14px",paddingTop:"3px"}}>Lorem ipsum dolor sit amet.</p>
             </div>
             <div class="panel-body shop-rate" >
               <input type="radio" id="star5" name="rate" value="5" />
                <label for="star5" title="text">5 stars</label>
                <input type="radio" id="star4" name="rate" value="4" />
                <label for="star4" title="text">4 stars</label>
                <input type="radio" id="star3" name="rate" value="3" />
                <label for="star3" title="text">3 stars</label>
                <input type="radio" id="star2" name="rate" value="2" />
                <label for="star2" title="text">2 stars</label>
             </div>
             <div class="row" style={{width:"100%",paddingTop:"3px",mrginTop:"2px"}}>
                <div class="col-sm-12 col-xs-12 shop-price-tags" style={{padding: "10px", marginTop:"15px"}}>
                   <div class="col-md-6" style={{marginTop: "-32px",textAlign: "start"}}>
                     <h4 style={{fontSize:"17px"}}>Rs. 699.00</h4>
                     <span style={{textDecoration: "line-through",fontSize:"13px"}}>Rs. 699.00</span>
                   </div>
                   <div class="col-md-6" style={{marginTop:"-20px"}}>
                     <button class="shop-btn" type="submit">Add to cart</button>
                   </div>
               </div>
             </div>
           </div>          
         </div>
        
    )
}

export default ShopProducts

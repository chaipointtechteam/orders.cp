import React from 'react'

function InstantChaicatalog({data,basepath}) {

    return (
        <div class="col-sm-6">
            {/* {console.log(basepath,data.media_path,JSON.parse(data.media))} */}
        <div class=" panel text-center garden-section" style={{backgroundColor: "transparent", border: "2px solid white"}}>
         <div class="col-sm-12 col-xs-12 gardenHgt">
            <div class="col-md-7 panel-heading ">
               <h1 class="collection-title">Chai <br /> Experience</h1>
           
               <p style={{textAlign: "center"}} class="collection-content">
                   
                  <div  dangerouslySetInnerHTML={{ __html: data?.content }}></div>
                    </p>
           
             <div class="col-sm-12 col-xs-12 seeAllBtn">
           <button class="btn btn-primary" type="submit">See all</button>
         </div>
             </div>
            <div class="col-md-5">
        <img src={`${basepath}${data.media_path}${JSON.parse(data.media).image[0]}`} class="gardenImg"/> 
         </div>
         </div>
        
        </div>          
      </div>
    )
}

export default InstantChaicatalog

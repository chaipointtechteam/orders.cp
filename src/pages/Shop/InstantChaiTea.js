import React from 'react'
import {useEffect,useState} from "react"
import { getCMSShop } from '../../actions/internalActions/LandingActions'
import {connect} from "react-redux"
import InstantChaicatalog from './InstantChaicatalog'
import { data } from 'jquery'
import ShopProducts from './ShopProducts'
 
function InstantChaiTea(props) {

  const [garden, setGarden] = useState([])
  useEffect(()=>{
    cmsShopGardenToGlass();
  },[])
 
  const cmsShopGardenToGlass = async () => {
    var menuId = 61;
    await props.getCMSShop(menuId,"Shop.Chaipoint.com", (data) => {
       console.log(data)
      setGarden(data)
    })
  }
    return (
      <section class="price-table secPadding tea-gallery">
         <div class="container"> 
             <div class="row panel-bg"> 
        <div class="col-sm-12 col-xs-12">
      <div class="panel-heading">
         <h1 style={{textAlign: "center",
         color: "#1ba0b5",
       fontWeight: "bolder",marginTop: "60px"}}>Instant Tea Collection</h1>
         <p style={{textAlign: "center"}}>Sweet & savory accompaniments for your chilling experience </p>
       </div>
       </div>
               <div class="col-sm-12 col-xs-12">
                 {console.log(garden)}
               {  garden.data?.map((data)=>{
                 return(
                   <>
                     <InstantChaicatalog
                     basepath={garden.base_path}
                     data={data}
                    />
                    <ShopProducts/>
                    <ShopProducts/>
                    <ShopProducts/>
                    <ShopProducts/>
                    <InstantChaicatalog
                     basepath={garden.base_path}
                     data={data}
                    />
                    </>
                 )
                 })}
                
               </div>
       </div>
       </div>
       </section>
    )
}
 const mapStateToProps=()=>{

 }
export default connect(mapStateToProps,{getCMSShop})(InstantChaiTea)

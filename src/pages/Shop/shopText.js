import React from 'react'


function shopText() {
    return (
         <section class="shop-bright-section">
       <div class="container">
           <div class="row">
           <div class="col-lg-12">
               
               <div class="shop-brightness-cup">
               <span>To serve a great cup that brightens up lives and brings people together</span>
               
               </div>
               
               </div>
           
           </div>
           
           
           </div>
       
       
       </section>
    )
}

export default shopText
import React, { useEffect, useState } from 'react'
import { connect } from "react-redux";
import Logo from "../../assets/images/logo.png"
import ShopIcon1 from "../../assets/images/ShopIcons01.svg";
import ShopIcon2 from "../../assets/images/ShopIcons02.svg";
import { getBannerImage } from "../../actions/internalActions/LandingActions";

const Banner = (props) => {
    const [bannerUrl, setbannerUrl] = useState(Logo)

    useEffect(() => {
        var sata = props.getBannerImage((url) => {

            if (url) {
                console.log("url", url)
                setbannerUrl(
                    url
                );
            }
        }, "shop.chaipoint.com");
    }, [])


    return (
        <div>
            <section class="shop-service-banner" style={{ backgroundImage: `url(${bannerUrl})` }}
            >
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 shop-mobile-section shop-banner-section">
                            <div class="col-md-6 col-sm-6 col-xs-9 shop-for-mobile">
                                <div class="shop-logo-display">
                                    <img src={Logo} />
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-3">
                                <div class="shop-login-service">
                                    {/* <!-- <button class="service-login" id="myBtn">login</button> --> */}
                                    <ul class="shop-header">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Shop Now</a></li>
                                        <li><a href="#">Category</a></li>
                                        <li><img src={ShopIcon1} /></li>
                                        <li><button id="signBtn" class="shop-service-signup" ><img src={ShopIcon2} />Login</button></li>
                                    </ul>

                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="shop-search-location">
                                <h6>Introducing</h6>
                                <h2>Instant Chai</h2>
                                <p>Authentic Chai for an authentic you simply dummy text of the printing and typesetting industry.</p>
                                <ul>
                                    <li>Masala</li>
                                    <li>Ginger</li>
                                    <li>Caradom</li>

                                </ul>

                                <a href="#" class="shop-searchsee">See all</a>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    )
}
const mapStateToProps = () => {

}
export default connect(mapStateToProps, { getBannerImage })(Banner);
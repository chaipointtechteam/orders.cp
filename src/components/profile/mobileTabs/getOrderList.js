import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../../../utils/apiUtil";
import axios from "axios";

export const getOrderDetails = async (sessionData) => {
  const options = {
    headers: { ...header, sessionId: `${sessionData}` },
  };

  let url = createPlatformURL("order/getOrders?limit=100&offset=0&isMsite=true");

  var data = "";
  var config = {
    method: "get",
    url: url,
    headers: options.headers,
    data: data,
  };
  return await axios(config)
    .then(function (response) {
     
      return response.data;
    })
    .catch(function (error) {
      
    });
};

import moment from "moment";
import { PropTypes } from "prop-types";
import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { emptyCartItem, updateCartItem, updateCartObject } from "../../../actions/CartAction";
import { setServiceList } from "../../../actions/ChannelListAction";
import { updateFilterObject } from "../../../actions/FilterItemsAction";
import { getProfile } from "../../../actions/LoginAction";
import {
  loadInventoryList,
  loadMenuItemList,

  setCategoryMenuList, setMenuItemList, upateMenuItemAfterCartItemRemoval, updateMenuItem
} from "../../../actions/MenuItemsAction";
import {
  loadStoreList, updateSelectedChannel,
  updateSelectedStore
} from "../../../actions/StoreListAction";
import toast from "../../../components/common/commonToast";
import { containsObject, filterTheMenuWithCategory, reOrderObject, updateSubTotal } from "../../../utils/common";
import { oraginzationId } from "../../../utils/constants";
import ProfileService from "../../../utils/services/profile/profile-service";
import { getOrderDetails } from "./getOrderList";
import ViewMobileOrder from "./ViewMobileOrder";

const MobileOrderList = (props) => {
  let history = useHistory();

  const [ScheduleList, setScheduleList] = useState([]);
  const [completedList, setcompletedList] = useState();
  const [state, setState] = useState(false);
  const [List, setList] = useState({});
  const [item, setItem] = useState();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {

    var data = await getOrderDetails(props.selectedStoreh1);

    setScheduleList(data?.orderList?.Scheduled);
    setcompletedList(data?.orderList?.Completed);
    setList(data?.totalOrders);
  };
  const updateCartWithProduct = (item) => {
    if (
      item.productAddonList.length === 0 &&
      item.productVariantList.length === 0
    ) {
      var cartList = props.selected.cartItems;

      if (!containsObject(item, cartList)) {
        cartList.push(item);
        item.quantity = item.quantity + 1;
        updateCartAndMenuList(item.quantity, item);
        // toast.success("Item added to Cart", {closable:true});
      } else {
        updateCartAndMenuList(item.quantity + 1, item);
      }

      props.updateCartObject({
        cartItems: cartList,
        subTotal: updateSubTotal(cartList),
      });
    } else {
      var cartList = props.selected.cartItems;

      if (!containsObject(item, cartList)) {
        cartList.push(item);
        item.quantity = item.quantity + 1;
        updateCartAndMenuList(item.quantity, item);
        // toast.success("Item added to Cart", {closable:true});
      } else {
        updateCartAndMenuList(item.quantity + 1, item);
      }

      props.updateCartObject({
        cartItems: cartList,
        subTotal: updateSubTotal(cartList),
      });

      // console.log("error");
    }
  };

  const updateCartAndMenuList = (quantity, item) => {
    props.updateMenuItem(props.menuItemList, item, quantity);

    props.updateCartItem(props.cartItemList, item, quantity);
  };
  const setStore = (id) => {
    const store = props.storeList.find((o) => o.outletId === id)
    // console.log(channel, "asdkjgakdsgjal")
    props.updateSelectedStore({
      selectedStore: store
    })
  }


  const setChannel = (id) => {


    const channel = props.channelList?.find((o) => o.sellerId === id);

    props.updateSelectedChannel({
      selectedChannel: channel,
    });
  };

  const LocationLoad = (lat, long) => {
    props.loadStoreList(
      {
        latitude: lat,
        longitude: long,
      },
      (data) => {
        
      }
    );
  }

  const setDefault = (id) => {
    // let currentComponent = this;

    var phoneNo = props.userObject.phoneNo;
    var sessionId = props.userObject.headerSessionId;
    ProfileService.defaultAddress(id, sessionId)
      .then((response) => {
        props.getProfile(`${phoneNo}`, sessionId);
      })
      .catch((error) => { });
  };


  const getAllAvailableMenuItems = () => {
    //Getting the  menu items will be called
    props.loadInventoryList(
      {
        sellerId: props.selectedChannel?.sellerId,
        siteId: props.selectedStore?.siteId,
      },
      (inventoryData) => {
        props.loadMenuItemList(
          {
            cityId: props.selectedStore?.cityId,
            includeProductRecipe: true,
            organizationId: oraginzationId,
            sellerIdList: [props.selectedChannel?.sellerId],
            siteId: props.selectedStore?.siteId,
          },
          (catLogData) => {
            if (catLogData) {
              // debugger
              var categoryList =
                catLogData.data.sellerProductCatalogueList[0].productCatalogue
                  .categoryList;
              props.setCategoryMenuList({
                categoryList: categoryList,
              });

              var menuItemList = [];
              catLogData.data.sellerProductCatalogueList[0].productCatalogue.productList.forEach(
                (product) => {
                  inventoryData.data.forEach((inventory) => {
                    if (
                      product.productCode === inventory.code &&
                      !inventory.isOutofStock
                    ) {
                      product.quantity = 0;
                      menuItemList.push(product);
                    }
                  });

                }
              );
              categoryList.forEach((category) => {
                category.productCodes.forEach((productCode) => {
                  menuItemList.forEach((product) => {
                    if (product.productCode === productCode) {
                      product.category = category;
                    }
                  });
                });
                category.subCategories.forEach((subCategory) => {
                  subCategory.productCodes.forEach((productCode) => {
                    menuItemList.forEach((product) => {
                      if (product.productCode === productCode) {
                        product.subCategory = subCategory;
                        product.category = category;
                      }
                    });
                  });
                });
              });
              var filtercate = [];
              var filterindx = [];
              props.categoryList?.map((category, index) => {
                var filter = [];
                filter = filterTheMenuWithCategory(menuItemList, category)

                if (filter && filter.length > 0) {
                  filtercate.push(category)
                  filterindx.push(index)
                }

              })
              // console.log(filtercate, "asdglanskdgnadkflgnadkflgnsa asdgnjkadngka aksdngjkasdgnak")
              // var filtercate = filterTheMenuWithCategory(menuItemList,category)

              props.updateFilterObject({
                activeKey: filterindx[0],
                selectedCategory: filtercate[0],
                selectedSubCategory: null,
                isSubCategory: null,
              })
              props.setMenuItemList({
                menuItemList: menuItemList,
              });


            }
            else {
              // this.props.setCategoryMenu{
              //   categoryList: [],
              // });List(
              props.setMenuItemList({
                menuItemList: [],
              });
            }


            // debugger
            //here the complete process will take place of concatination of all the inventory items and menu items
            //this.props.history.push("/products"); //as we are already on that page
          }
        );
      }
    );
  };
  
  const setChannelList = (item) => {

    props.setServiceList({
      channelList: item[0].sellerOptions
    })


  }
  const reOrder = (item) => {

    setDefault(item.addressId);
    LocationLoad(
      item.orderFulfilmentEntity.lat,
      item.orderFulfilmentEntity.lng
    );

    let locationData = localStorage.getItem('locationData')
    locationData = JSON.parse(locationData)
    if (locationData.length > 0) {

      var requiredStore = locationData.map((sd) => {
        if (item.orderFulfilmentEntity.lat === sd.latitude && item.orderFulfilmentEntity.lng === sd.longitude) {
          return sd
        }
      })
      requiredStore = requiredStore.filter((rs) => {
        return rs !== undefined
      })
    }
    else {
      LocationLoad(
        item.orderFulfilmentEntity.lat,
        item.orderFulfilmentEntity.lng
      );
    }


 props.selected.cartItems.forEach((cart) => {
      props.emptyCartItem(props.selected.cartItems, cart);
    });
    setChannelList(requiredStore)
    setStore(item.orderFulfilmentEntity.id);
    setChannel(item.orderSeller.sellerId);
    getAllAvailableMenuItems(item, requiredStore);

    var cartList = props.menuItemList;
    item.orderItems.map((items) => {
      const t = reOrderObject(items, cartList);

      if (t) {
        setDefault(item.addressId)
        LocationLoad(item.orderFulfilmentEntity.lat, item.orderFulfilmentEntity.lng)
        setStore(item.orderFulfilmentEntity.id)
        setChannel(item.orderSeller.sellerId)
        if (items.items.length > 0) {
          updateCartWithProduct(t, items.quantity, items.items);
        } else {
          updateCartWithProduct(t, items.quantity, []);
        }
        history.push("/checkout");
      } else {
        toast.warning("The product is currently unavailable", { closable: true })
      }
    });

  };

  
    return (
      <>
        {/* <div className="container">
          <div className="header-user">
            <div style={{display:"flex",flexDirection:"row",alignItems:"center"}} className="">
              <Link to="/profile">
                <span>
                  <img
                    className="back-arrow"
                    src={
                      require("../../../assets/images/left-arrow.svg").default
                    }
                  />{" "}
                </span>
              </Link>
              <span className="ORDERS">ORDERS</span>
            </div>
          </div>
        </div>
        <div className="myOrder">
          {" "}
          <div className="myOrderText">My Order</div>
        </div> */}

        <>
          {state ? (
            <>
              <div className="container">
                <div className="header-user">
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                    className=""
                  >
                    <span
                      onClick={() => {
                        setState(false);
                      }}
                    >
                      <img
                        className="back-arrow"
                        src={
                          require("../../../assets/images/left-arrow.svg")
                            .default
                        }
                      />{" "}
                    </span>

                    <span className="ORDERS">ORDERS SUMMARY</span>
                  </div>
                </div>
              </div>
              <div className="myOrder">
                {" "}
                <div className="myOrderText">My Order</div>
              </div>
              <ViewMobileOrder item={item} reOrder={reOrder} />
            </>
          ) : (
            <>
              <div className="container">
                <div className="header-user">
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                    className=""
                  >
                    <Link to="/profile">
                      <span>
                        <img
                          className="back-arrow"
                          src={
                            require("../../../assets/images/left-arrow.svg")
                              .default
                          }
                        />{" "}
                      </span>
                    </Link>
                    <span className="ORDERS">ORDERS</span>
                  </div>
                </div>
              </div>
              <div className="myOrder">
                {" "}
                <div className="myOrderText">My Order</div>
              </div>
              {List === 0 ? (
                <h2 className="noOrder">No order found</h2>
              ) : (
                <>
                  {ScheduleList ? (
                    <>
                      {ScheduleList.map((item) => {
                        return (
                          <div key={item.id}>
                            <Card style={{ width: "100%" }}>
                              <div
                                style={{
                                  display: "flex",
                                  flexDirection: "row",
                                }}
                              >
                                {item.orderSeller.fulfilmentType ===
                                  "DELIVERY" ? (
                                  <img
                                    className="product_img"
                                    src={
                                      require("../../../assets/images/delivery.svg")
                                        .default
                                    }
                                  />
                                ) : (
                                  <>
                                    {item.orderSeller.fulfilmentType ===
                                      "PICKUP" ? (
                                      <img
                                        className="product_img"
                                        src={
                                          require("../../../assets/images/pickup.svg")
                                            .default
                                        }
                                      />
                                    ) : (
                                      <img
                                        className="product_img"
                                        src={
                                          require("../../../assets/images/walkin.svg")
                                            .default
                                        }
                                      />
                                    )}
                                  </>
                                )}
                                <>
                                  {/* {item.orderData.discount > 0 ? (
                                    <span className="offer-order ">
                                      {item.orderData.discount}% OFF
                                    </span>
                                  ) : (
                                    ""
                                  )} */}
                                </>
                                <Card.Body style={{ margin: "2% 1%" }}>
                                  <Card.Title className="order-Details">
                                    Order No. <span>{item.orderId}</span>
                                  </Card.Title>
                                  <div onClick={()=>{
                                      setState(true);
                                      setItem(item);
                                  }}>
                                  <img
                                    className="right-arrow"
                                    src={
                                      require("../../../assets/images/Right-arrow.svg")
                                        .default
                                    }
                                  />
                                  </div>
                                  <div>
                                    <Card.Text className="card-text amount">
                                      {" "}
                                      ₹ {item.orderData.payableAmount}
                                    </Card.Text>
                                    <Card.Text className="card-text address">
                                    {item.customerAddress&&item.fulfilmentType === "DELIVERY"
                                      ? `${item.customerAddress
                                        .streetAddress2 !== null
                                        ? item.customerAddress
                                          .streetAddress2
                                        : ""
                                      } ${item.customerAddress
                                        .streetAddress1 !== null
                                        ? item.customerAddress
                                          .streetAddress1
                                        : ""
                                      } ${item.customerAddress.landmark !== null
                                        ? item.customerAddress.landmark
                                        : ""
                                      }`
                                      : ""}
                                    </Card.Text>
                                    <Card.Text className="card-text address">
                                      {moment(item.orderTime).format(
                                        "ddd Do MMM  YYYY, h:mm a"
                                      )}
                                    </Card.Text>
                                  </div>
                                  <div className="Card-Buttons">
                                    <Button
                                      variant=""
                                      className="buttons1"
                                      onClick={() => {
                                        setState(true);
                                        setItem(item);
                                      }}
                                    >
                                      <span className="VIEW-ORDER">VIEW ORDER</span>
                                    </Button>
                                    <Button
                                      variant=""
                                      className="buttons2"
                                      onClick={() => reOrder(item)}
                                    >
                                      <span className="VIEW-ORDER">REORDER</span>
                                    </Button>
                                  </div>
                                </Card.Body>
                              </div>

                            </Card>
                          </div>
                        );
                      })}
                    </>
                  ) : (
                    ""
                  )}
                  {completedList ? (
                    <>
                      {completedList.map((item) => {
                        return ( <div key={item.id}>
                          <Card style={{ width: "100%" }}>
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "row",
                              }}
                            >
                              {item.orderSeller.fulfilmentType ===
                                "DELIVERY" ? (
                                <img
                                  className="product_img"
                                  src={
                                    require("../../../assets/images/delivery.svg")
                                      .default
                                  }
                                />
                              ) : (
                                <>
                                  {item.orderSeller.fulfilmentType ===
                                    "PICKUP" ? (
                                    <img
                                      className="product_img"
                                      src={
                                        require("../../../assets/images/pickup.svg")
                                          .default
                                      }
                                    />
                                  ) : (
                                    <img
                                      className="product_img"
                                      src={
                                        require("../../../assets/images/walkin.svg")
                                          .default
                                      }
                                    />
                                  )}
                                </>
                              )}
                              <>
                                {/* {item.orderData.discount > 0 ? (
                                  <span className="offer-order ">
                                    {item.orderData.discount}% OFF
                                  </span>
                                ) : (
                                  ""
                                )} */}
                              </>
                              <Card.Body style={{ margin: "2% 1%" }}>
                                <Card.Title className="order-Details">
                                  Order No. <span>{item.orderId}</span>
                                </Card.Title>
                                <img
                                  className="right-arrow"
                                  src={
                                    require("../../../assets/images/Right-arrow.svg")
                                      .default
                                  }
                                />
                                <div>
                                  <Card.Text className="card-text amount">
                                    {" "}
                                    ₹ {item.orderData.payableAmount}
                                  </Card.Text>
                                  <Card.Text className="card-text address">
                                  {item.customerAddress&&item.fulfilmentType === "DELIVERY"
                                    ? `${item.customerAddress
                                      .streetAddress2 !== null
                                      ? item.customerAddress
                                        .streetAddress2
                                      : ""
                                    } ${item.customerAddress
                                      .streetAddress1 !== null
                                      ? item.customerAddress
                                        .streetAddress1
                                      : ""
                                    } ${item.customerAddress.landmark !== null
                                      ? item.customerAddress.landmark
                                      : ""
                                    }`
                                    : ""}
                                  </Card.Text>
                                  <Card.Text className="card-text address">
                                    {moment(item.orderTime).format(
                                      "ddd Do MMM  YYYY, h:mm a"
                                    )}
                                  </Card.Text>
                                </div>
                                <div className="Card-Buttons">
                                  <Button
                                    variant=""
                                    className="buttons1"
                                    onClick={() => {
                                      setState(true);
                                      setItem(item);
                                    }}
                                  >
                                    <span className="VIEW-ORDER">VIEW ORDER</span>
                                  </Button>
                                  <Button
                                    variant=""
                                    className="buttons2"
                                    onClick={() => reOrder(item)}
                                  >
                                    <span className="VIEW-ORDER">REORDER</span>
                                  </Button>
                                </div>
                              </Card.Body>
                            </div>

                          </Card>
                        </div>
                      
                        );
                      })}
                    </>
                  ) : (
                    ""
                  )}
                </>
              )}
            </>
          )}
        </>
      </>
    );
 
};
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const selectedStoreh1 = reducerObj.userObject.userObject.newHeader.sessionId;
  const storeList = reducerObj.storeList.storeList;
  const selected = reducerObj.cartObject.cartObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const selectedChannel = reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const cartItemList = reducerObj.cartObject.cartObject.cartItems;
  const channelList = reducerObj.channelList.channelList.channelList;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  return { selectedStoreh1, userObject, selected, menuItemList, cartItemList, storeList, channelList, categoryList, selectedChannel, selectedStore };
};

export default connect(mapStateToProps, {
  updateCartObject,
  updateCartItem,
  updateMenuItem,
  loadMenuItemList,
  setMenuItemList,
  setCategoryMenuList,
  loadInventoryList,
  updateFilterObject,
  getProfile,
  updateSelectedChannel,
  updateSelectedStore,
  loadStoreList,
  setServiceList,
  emptyCartItem,
  upateMenuItemAfterCartItemRemoval,
})(MobileOrderList);
MobileOrderList.propTypes = {
  cartObject: PropTypes.object.isRequired,
  updateCartObject: PropTypes.func.isRequired,
  setServiceList: PropTypes.func.isRequired,
};

import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Button, Card } from "react-bootstrap";
import call from "../../../assets/images/svg.svg";
import { header } from "../../../utils/apiUtil";
const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: 0,
  },
}));

function ViewMobileOrder(props) {
  console.log(props)
  const [statusData, setStatusData] = useState("");
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [phone, setPhone] = useState("");
const {item}=props
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // var url = `https://testing.chaipoint.com/qa3/online-ag/api/order/getOrderStatus?orderId=${item.id}`;
  const options = {
    headers: { ...header },
  };

  useEffect(() => {
    getStatus();
  }, []);

  useEffect(() => {
    if (props) {
      var i = setInterval(() => getStatus(), 10000);
    }
    return () => {
       clearInterval(i)
    }
  }, [props]);
  const getStatus = async () => {
    var config = {
      method: "post",
      url: `https://testing.chaipoint.com/qa3/online-ag/api/order/getOrderStatus?orderId=${props.item.id}`,
      headers: options.headers,
    };
    await axios(config).then((e) => {
      setStatusData(e.data);
      setPhone(`tel:${e.data.riderPhoneNumber}`);
    });
  };

  return (
    <>
      <div className="container">
        <div style={{ marginTop: "5%" }} className="OrderDetails">
          Order No. {item.orderId} |{" "}
          <span className="Delivery">{item.fulfilmentType}</span>
        </div>
      </div>
      <div className="cardStyle">
        <Card>
          <Card.Body>
            <div className="flexContent">
              <div className="myOrderText2">Order Details</div>
              <div className="">
                {moment(statusData.orderStatusUIResponse?.time).format(
                  "ddd, Do MMM YYYY | "
                )}{" "}
                {statusData.orderStatusUIResponse?.uiTitle}
                {" at "}
                {moment(statusData.orderStatusUIResponse?.time).format(
                  "h:mm a"
                )}
              </div>
            </div>
            <div style={{ marginTop: "10px" }} className="flexContent">
              <div style={{ fontSize: "10px" }} className="card-text address">
                <div className="MobileOrderAddress">
                  {item.customerAddress && item.fulfilmentType === "DELIVERY"
                    ? `${
                        item.customerAddress.streetAddress2 !== null
                          ? item.customerAddress.streetAddress2
                          : ""
                      } ${
                        item.customerAddress.streetAddress1 !== null
                          ? item.customerAddress.streetAddress1
                          : ""
                      } ${
                        item.customerAddress.landmark !== null
                          ? item.customerAddress.landmark
                          : ""
                      }`
                    : ""}
                </div>
                <div className="MobileOrderAddress">
                  {moment(item.orderTime).format(`ddd, Do MMM YYYY | `)}
                  {"Ordered at"}
                  {moment(item.orderTime).format(` h:mm a`)}
                </div>
                <div>
                  <div style={{ alignSelf: "center" }}>
                    {statusData.deliveryStatusUIResponse?.uiTitle}{" "}
                    {statusData.riderPhoneNumber && (
                      <>
                        {" | "}Call the Rider{" "}
                        <a href={phone}>
                          <img
                            src={call}
                            height="10px"
                            style={{ color: "#12767f", margin: "10px" }}
                          />
                        </a>
                      </>
                    )}
                  </div>
                </div>
                <div className="MobileOrderAddress"></div>
              </div>
              <div className="points">
                ₹ {item.orderData.payableAmount.toFixed(2)}
              </div>
            </div>

            {item.qrCode ? (
              <>
                <img
                  style={{
                    width: "100px",
                    height: "100px",
                  }}
                  onClick={handleOpen}
                  src={`data:image/png;base64,${item.qrCode?.qrCodeImage}`}
                />
                <Modal
                  aria-labelledby="transition-modal-title"
                  aria-describedby="transition-modal-description"
                  className={classes.modal}
                  open={open}
                  onClose={handleClose}
                  closeAfterTransition
                  BackdropComponent={Backdrop}
                  BackdropProps={{
                    timeout: 500,
                  }}
                >
                  <Fade in={open}>
                    <div className={classes.paper}>
                      <img
                        style={{
                          width: "250px",
                          height: "250px",
                        }}
                        src={`data:image/png;base64,${item.qrCode?.qrCodeImage}`}
                      />
                    </div>
                  </Fade>
                </Modal>
              </>
            ) : null}
            <div className="header-Line2"></div>
            <div style={{ marginTop: "2%" }} className="flexContent">
              <div style={{ fontSize: "15px" }} className="myOrderText2">
                Order Summary
              </div>
              <div>
                {" "}
                <Button variant="">
                  <span className="VIEW-ORDER" onClick={() => props.reOrder(item)}>
                    REORDER
                  </span>
                </Button>
              </div>
            </div>

            <div style={{ margin: "3% 0%" }}>
              {item.orderItems.map((item) => {
                return (
                  <>
                    <Card style={{ marginTop: "2%", height: "50px" }}>
                      <div style={{ display: "flex", flexDirection: "row" }}>
                        {/* <img
                          className="product_img2"
                          src={
                            require("../../../assets/images/Offers-05.jpg")
                              .default
                          }
                        /> */}
                        <div></div>
                        <img
                          style={{
                            margin: "2%",
                            height: "15px",
                            width: "15px",
                          }}
                          src={
                            require("../../../assets/images/Veg.svg").default
                          }
                        />
                        <div className="CardBody">
                          <div style={{ width: "40%" }}>{item.productName}</div>
                          <div>
                            {item.quantity}x{item.price}
                          </div>
                          <div>₹ {item.price.toFixed(2)}</div>
                        </div>
                      </div>
                    </Card>
                  </>
                );
              })}

              <div style={{ marginTop: "5%" }}>
                <div style={{ fontSize: "15px" }} className="myOrderText2">
                  Bill Details
                </div>
                <div style={{ marginTop: "3%" }}>
                  {item.orderData.subTotal>0 && 
                  <div style={{ margin: "1% 5%" }} className="flexContent">
                    <div
                      style={{ fontSize: "10px" }}
                      className=" address"
                    >
                      Item Total
                    </div>
                    <div
                      style={{ fontSize: "10px" }}
                      className=" address"
                    >
                      ₹ {item.orderData.subTotal.toFixed(2)}
                    </div>
                    </div>}
                  {item.orderData.totalCharges>0 &&
                  <div style={{ margin: "1% 5%" }} className="flexContent">
                    <div
                      style={{ fontSize: "10px" }}
                      className=" address"
                    >
                      Delivery Charges
                    </div>
                    <div
                      style={{ fontSize: "10px" }}
                      className=" address"
                    >
                      ₹ {item.orderData.totalCharges.toFixed(2)}
                    </div>
                    </div>}
                  {item.orderData.totalTax > 0 &&
                    <div style={{ margin: "1% 5%" }} className="flexContent">
                      <div
                        style={{ fontSize: "10px" }}
                        className=" address"
                      >
                        Tax and Charges
                      </div>
                      <div
                        style={{ fontSize: "10px" }}
                        className=" address"
                      >
                        {item.orderData.totalTax.toFixed(2)} %
                      </div>
                    </div>}
                  {item.orderData.discount > 0 &&
                    <div style={{ margin: "1% 5%" }} className="flexContent">
                      <div
                        style={{ fontSize: "10px" }}
                        className=" address"
                      >
                        Discount
                      </div>
                      <div
                        style={{ fontSize: "10px" }}
                        className=" address"
                      >
                        ₹ {item.orderData.discount.toFixed(2)}
                      </div>
                    </div>}
                  <div className="header-Line2"></div>
                  <div style={{ margin: "1% 5%" }} className="flexContent">
                    <div className="points">Total</div>
                    <div className="points">
                      ₹ {item.orderData.payableAmount.toFixed(2)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Card.Body>
        </Card>
      </div>
    </>
  );
}

export default ViewMobileOrder;

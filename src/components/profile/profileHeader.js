import React, { Component } from "react";
import ProfileDropdown from "../common/profileDropdown";
import "./profile.css";
import CartModal from "../landing/CartModal";
import { Link } from "react-router-dom";
export default class ProfileHeader extends Component {
  state = { showPopup: false };
  openPopup = () => {
    this.setState({ showPopup: true });
  };
  closePopUp = () => {
    this.setState({
      showPopup: false,
    });
  };
  render() {
    return (
      <section class="category-nav">
        <div id="contain" class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div id="main" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="logo-display">
                  <Link to="/">
                    <img
                      src={require("../../assets/images/logo.png").default}
                    />
                  </Link>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="category-section">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div
                        id="main"
                        class="col-md-4 col-sm-4 col-xs-12 search-profile1 u-p"
                      ></div>
                      <div
                        id="main"
                        class="col-md-3 col-sm-3 col-xs-12 search-profile u-p"
                      >
                        <div class="simplemenu">
                          <div class="dropbtn" onClick={this.openPopup}>
                            <span>
                              <img
                                src={
                                  require("../../assets/images/Cart2.svg")
                                    .default
                                }
                              />
                            </span>
                            <span>Cart</span>
                          </div>
                        </div>
                      </div>
                      <ProfileDropdown />
                    </div>
                  </div>
                </div>
              </div>
           
            </div>
          </div>
          <CartModal
            showModal={this.state.showPopup}
            closePopUp={this.closePopUp}
          />
        </div>
      </section>
    );
  }
}

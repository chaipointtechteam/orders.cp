import React, { Component } from "react";
import { connect } from "react-redux";
import { updateProfile } from "../../actions/LoginAction";
import { updateUserObject } from "../../actions/UserAction";
import { isValidEmail } from "../../utils/common";
import toast from "../common/commonToast";
import "./profile.css"
import { Modal } from "react-bootstrap";

class ProfileDetails extends Component {
  state = {
    detail: false,
    allowToEdit: false,
    name: this.props.userObject.name,
    email: this.props.userObject.email,
    phoneNo: this.props.userObject.phoneNo,
  };

  componentDidMount = () => {
    if (this.props.isLoggedIn) {

      if (this.props.userObject.name === "" || this.props.userObject.email === "") {
        this.setState({
          detail: true,
        });
        console.log(this.props.userObject.name)
      }

    }
  }
  componentDidUpdate = (prevProps) => {
    if(prevProps.isLoggedIn!==this.props.isLoggedIn){
      if (this.props.isLoggedIn) {
        
        if (this.props.userObject.name === "" || this.props.userObject.email === "") {
          
          this.setState({
            detail: true,
          });
          console.log(this.props.userObject.name)
        }
  
      }
    }
  }

  updateInputValue = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  allowToEditTheProfile = () => {
    this.setState({
      allowToEdit: true,
    });
    this.close();
  };
  validate = () => {

    return (
      this.state.email && this.state.name.length > 3 && isValidEmail(this.state.email)
    );
  };
  saveTheProfile = () => {
    if (this.state.name.length > 3) {
      if (this.validate()) {
        this.props.updateProfile(
          this.props.userObject.newHeader,
          this.state,
          (responseData) => {
            this.props.updateUserObject({
              phoneNo: responseData.phoneNo,
              email: responseData.email,
              name: responseData.name,
              customerId: responseData.customerId,
            });
            this.setState({
              allowToEdit: false,
            });
            toast.success("Profile updated successfully", {});
          }
        );
      } else {
        toast.error("Please enter valid email", {});
      }
    } else {
      toast.error("Name must be greater then 3 characters", {});
    }
  };
  close = () => {
    this.setState({
      detail: false,
    });
  }
  render() {
    const { userObject } = this.props;

    return (
      <>
        {this.state.detail ? (
          <Modal
            className="modal-opacity"
            id="addAddressModal"
            show={this.state.detail}

          >
            <div class="modal-header">
              <h5 class="modal-title">Edit Profile</h5>
              <button
                onClick={() => this.close()}
                type="button"
                class="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div class="modal-body">
              <p>As you have directly logged in to ChaiPoint , You Need To  Update Your Profile Details First in Order To Continue</p>
            </div>
            <div style={{ textAlign: "center", padding: 20, paddingTop: 0 }}>
              <button style={{ width: "20%", textAlign: "center", padding: 8, }}
                type="button"
                class="btn btn-primary"
                onClick={() => this.allowToEditTheProfile()}
              >
                Ok
              </button>
            </div>

          </Modal>
        ) : (
          ""
        )}
        <section id="main" class="profile-details">
          <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12 profile-header">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="profile-name">
                  {!this.state.allowToEdit && <label>{userObject.name}</label>}
                  {this.state.allowToEdit && (
                    <div class="form-group">
                      <input
                        type="text"
                        class="form-control"
                        name="name"
                        placeholder="Name"
                        onChange={this.updateInputValue}
                        value={this.state.name}
                        maxLength={50}
                      />
                    </div>
                  )}

                  <div class="email_details">
                    <span>+ {userObject.phoneNo}</span>

                    {!this.state.allowToEdit && <span>{userObject.email}</span>}
                    {this.state.allowToEdit && (
                      <div class="form-group">
                        <input
                          type="text"
                          class="form-control"
                          name="email"
                          placeholder="Email"
                          required=""
                          onChange={this.updateInputValue}
                          value={this.state.email}
                          maxLength={200}
                        />
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 edit-button">
                <div class="edit-profile">
                  {!this.state.allowToEdit && (
                    <button
                      class="btn btn-primary"
                      onClick={this.allowToEditTheProfile}
                    >
                      Edit Profile
                    </button>
                  )}
                  {this.state.allowToEdit && (
                    <button class="btn btn-primary" onClick={this.saveTheProfile}>
                      Save Profile
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const isLoggedIn = reducerObj.userObject.isLoggedIn;
  return { userObject, isLoggedIn };
};
export default connect(mapStateToProps, { updateProfile, updateUserObject })(
  ProfileDetails
);

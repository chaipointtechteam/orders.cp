import { faSearch, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import moment from "moment";
import { PropTypes } from "prop-types";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router";
import {
  emptyCartItem,
  updateCartItem,
  updateCartObject,
} from "../../../actions/CartAction";
import { setServiceList } from "../../../actions/ChannelListAction";
import { updateFilterObject } from "../../../actions/FilterItemsAction";
import { getProfile } from "../../../actions/LoginAction";
import {
  loadInventoryList,
  loadMenuItemList,
  setCategoryMenuList,
  setMenuItemList,
  upateMenuItemAfterCartItemRemoval,
  updateMenuItem,
} from "../../../actions/MenuItemsAction";
import {
  loadStoreList,
  updateSelectedChannel,
  updateSelectedStore,
} from "../../../actions/StoreListAction";
import toast from "../../../components/common/commonToast";
import {
  createPlatformURL,
  handleSecureAjaxError,
  header,
} from "../../../utils/apiUtil";
import {
  CheckAvaibilityOfProduct,
  checkAvaibilityOfProductWithAddond,
  constainsObjectWithModifierList,
  containsObject,
  filterTheMenuWithCategory,
  getProductNameFromProductCode,
  reOrderObject,
  updateSubTotal,
} from "../../../utils/common";
import { oraginzationId } from "../../../utils/constants";
import ProfileService from "../../../utils/services/profile/profile-service";
import "../profile.css";
import ViewOrderDetails from "./viewOrderDetails";

const Orders = (props) => {
  let history = useHistory();
  const [totalOrders, setToatalOrder] = useState(0);
  const [state, setState] = useState(false);
  const [messsage, setMessage] = useState("");
  const [CompleteList, setCompleteList] = useState([]);
  const [ScheduleList, setScheduleList] = useState([]);
  const [Loading, setLoading] = useState(false);
  const [resData, setResData] = useState("");
  const [item, setItem] = useState(null);
  const [fullFil, setfullFil] = useState("");
  const [orderTime, setOrderTime] = useState(null);
  const [DeliverTime, setDeliverTime] = useState(null);
  const [orderItem, setorderItem] = useState([]);
  const [total, setTotal] = useState(null);
  const [address, setAddress] = useState(null);
  const [orderData, setOrderData] = useState({});
  const [Data, setData] = useState({});
  const [SelectedStoreData, setSelectStoreData] = useState([]);
  const [Search, setSearch] = useState(true);
  const [SearchValue, setSearchValue] = useState("");
  const [SearchNo, setSearchNo] = useState("");
  const getOrderDetails = () => {
    setToatalOrder(0);

    setCompleteList([]);
    setScheduleList([]);
    const options = {
      headers: { ...header, sessionId: `${props.selectedStoreh1}` },
    };
    let url = createPlatformURL(
      "order/getOrders?limit=100&offset=0&isMsite=true"
    );

    setLoading(true);
    axios
      .get(url, options)
      .then((response) => {
        setLoading(false);

        setResData(response.data);
        const count = response.data.totalOrders;
        setToatalOrder(response.data.totalOrders);
        setCompleteList(response.data.orderList.Completed);
        setScheduleList(response.data.orderList.Scheduled);
        if (count <= 0) {
          setMessage("Sorry, you have no past orders!");
        }
      })
      .catch((error) => {
        setLoading(false);
        handleSecureAjaxError(error, "ServiceActions loadService");
      });
  };

  const getOrderByOrerNo = (orderID) => {
    setLoading(false);
    // setToatalOrder(0);
    // setCompleteList([]);
    // setScheduleList([]);
    const matchesScheduled = [];
    //console.log(SearchValue);
    console.log(resData);
    
    resData.orderList && resData.orderList.Scheduled.find((data, index) => {
      if (data.orderId.includes(SearchValue)) {
        matchesScheduled.push(data);
      }
    });
    const matchesComplete = [];
    resData.orderList && resData.orderList.Completed.find((data, index) => {
      if (data.orderId.includes(SearchValue)) {
        matchesComplete.push(data);
      }
    });
    if (matchesComplete.length + matchesScheduled.length > 0) {
      setToatalOrder(matchesScheduled.length + matchesComplete.length);
      setCompleteList(matchesComplete);
      setScheduleList(matchesScheduled);
    } else {
      setToatalOrder(0);
      setMessage("Sorry, we could not find a matching order for your search");
    }
    const options = {
      headers: { ...header, sessionId: `${props.selectedStoreh1}` },
    };
    let url = `https://testing.chaipoint.com/qa3/online-ag/api/order/id?isGenerated=true&isMsite=true&orderId=${orderID}`;

    // setLoading(true);
    // axios
    //   .get(url, options)
    //   .then((response) => {
    //     setLoading(false);
    //     console.log(response.data)
    //     setToatalOrder(1);
    //     setScheduleList([response.data]);
    //   })
    //   .catch((error) => {
    //     setMessage("Sorry, we could not find a matching order for your search");
    //     setLoading(false);
    //   });
  };
  useEffect(() => {
    if (SearchValue !== "") {
      getOrderByOrerNo(SearchValue);
    }
  }, [SearchValue]);

  useEffect(() => {
    if (Search != true) {
      getOrderByOrerNo();
    } else {
      getOrderDetails();
    }
  }, [Search]);

  const updateCartWithProduct = (
    item,
    quantity,
    itemModifier,
    inventory,
    menuItemList
  ) => {
    if (
      item.productModifierList.length === 0 &&
      item.productVariantList.length === 0
    ) {
      var cartList = props.selected.cartItems;

      if (!containsObject(item, cartList)) {
        if (CheckAvaibilityOfProduct(inventory, item)) {
          cartList.push(item);
          item.quantity = item.quantity + quantity;
          updateCartAndMenuList(item.quantity, item, menuItemList, inventory);
          // toast.success("Item added to Cart", { closable: true });
        } else {
          toast.warning(
            `you can't add ${item.displayName},it's quantity is  now out of stock`
          );
        }
      } else {
        updateCartAndMenuList(item.quantity + 1, item, menuItemList, inventory);
      }

      props.updateCartObject({
        cartItems: cartList,
        subTotal: updateSubTotal(cartList),
      });
    } else {
      var cartList = props.selected.cartItems;
      var m = [];
      item.productModifierList.map((modifier) => {
        itemModifier.map((k) => {
          modifier.modifierProductList.map((o, index) => {
            if (o.productId === k.productId) {
              o.productName = getProductNameFromProductCode(
                menuItemList,
                o.productCode
              );
              m.push(o);
            }
          });
        });
      });

      if (m.length !== undefined) {
        var select = {
          ...item,
          quantity: quantity - 1,
          productModifierList: {
            modifierProductList: m !== undefined ? m : [],
          },
        };

        if (!constainsObjectWithModifierList(item, cartList)) {
          var message = checkAvaibilityOfProductWithAddond(
            inventory,
            select,
            props.cartItemList
          );
          if (message.check) {
            cartList.push(select);
            item.quantity = item.quantity + 0;
            updateCartAndMenuList(
              select.quantity + 1,
              select,
              menuItemList,
              inventory
            );
            // toast.success("Item added to Cart", { closable: true });
          } else {
            toast.warning(
              `you can't add ${message.addonItem.displayName},it's quantity is  now out of stock`
            );
          }
        }
      }
      props.updateCartObject({
        cartItems: cartList,
        subTotal: updateSubTotal(cartList),
      });
    }
  };
  const updateCartAndMenuList = (quantity, item, menuItemList, inventory) => {
    props.updateMenuItem(menuItemList, item, quantity, inventory);
    props.updateCartItem(props.cartItemList, item, quantity);
  };

  const setDefault = (id) => {
    // let currentComponent = this;

    var phoneNo = props.userObject.phoneNo;
    var sessionId = props.userObject.headerSessionId;
    ProfileService.defaultAddress(id, sessionId)
      .then((response) => {
        props.getProfile(`${phoneNo}`, sessionId);
      })
      .catch((error) => {});
  };

  const getAllAvailableMenuItems = (selectedChannel, selectedStore, item) => {
    //Getting the  menu items will be called
    props.loadInventoryList(
      {
        sellerId: selectedChannel.sellerId,
        siteId: selectedStore.siteId,
      },

      (inventoryData) => {
        props.loadMenuItemList(
          {
            cityId: selectedStore.cityId,
            includeProductRecipe: true,
            organizationId: oraginzationId,
            sellerIdList: [selectedChannel.sellerId],
            siteId: selectedStore.siteId,
          },
          (catLogData) => {
            if (catLogData) {
              //
              var categoryList =
                catLogData.data.sellerProductCatalogueList[0].productCatalogue
                  .categoryList;
              props.setCategoryMenuList({
                categoryList: categoryList,
              });

              var menuItemList = [];
              var inventory = [];
              catLogData.data.sellerProductCatalogueList[0].productCatalogue.productList.forEach(
                (product) => {
                  inventory = inventoryData.data;
                  if (product.productVariantList.length > 0) {
                    product.quantity = 0;
                    menuItemList.push(product);
                  }
                  inventoryData.data.forEach((inventory) => {
                    if (
                      product.productCode === inventory.code &&
                      !inventory.isOutofStock
                    ) {
                      product.quantity = 0;
                      menuItemList.push(product);
                    }
                  });
                }
              );
              categoryList.forEach((category) => {
                category.productCodes.forEach((productCode) => {
                  menuItemList.forEach((product) => {
                    if (product.productCode === productCode) {
                      product.category = category;
                    }
                  });
                });
                category.subCategories.forEach((subCategory) => {
                  subCategory.productCodes.forEach((productCode) => {
                    menuItemList.forEach((product) => {
                      if (product.productCode === productCode) {
                        product.subCategory = subCategory;
                        product.category = category;
                      }
                    });
                  });
                });
              });
              var filtercate = [];
              var filterindx = [];
              categoryList?.map((category, index) => {
                var filter = [];
                filter = filterTheMenuWithCategory(menuItemList, category);

                if (filter && filter.length > 0) {
                  filtercate.push(category);
                  filterindx.push(index);
                }
              });

              props.updateFilterObject({
                activeKey: filterindx[0],
                selectedCategory: filtercate[0],
                selectedSubCategory: null,
                isSubCategory: null,
              });
              props.setMenuItemList({
                menuItemList: menuItemList,
                inventory: inventory,
              });
              // debugger
              var cartList = menuItemList;
              item.orderItems.map((items) => {
                const t = reOrderObject(items, cartList);
                // debugger
                if (t) {
                  if (items.items.length > 0) {
                    updateCartWithProduct(
                      t,
                      items.quantity,
                      items.items,
                      inventory,
                      menuItemList
                    );
                  } else {
                    updateCartWithProduct(
                      t,
                      items.quantity,
                      [],
                      inventory,
                      menuItemList
                    );
                  }

                  toast.success("Item added to Cart", {});
                  history.push("/checkout");
                } else {
                  toast.warning("The product is currently unavailable", {
                    closable: true,
                  });
                }
              });
            } else {
              // this.props.setCategoryMenu{
              //   categoryList: [],
              // });List(
              props.setMenuItemList({
                menuItemList: [],
              });
            }

            //
            //here the complete process will take place of concatination of all the inventory items and menu items
            //this.props.history.push("/products"); //as we are already on that page
          }
        );
      }
    );
  };

  const reOrder = (item) => {
    // debugger

    setDefault(item.addressId);

    props.loadStoreList(
      {
        latitude: item.orderFulfilmentEntity.lat,
        longitude: item.orderFulfilmentEntity.lng,
      },
      (data) => {
        var store = data.find(
          (o) => o.outletId === item.orderFulfilmentEntity.id
        );
        if (store) {
          props.updateSelectedStore({
            selectedStore: store,
          });

          props.setServiceList({
            channelList: store.sellerOptions,
          });
          var channel = store.sellerOptions.find(
            (o) => o.sellerId === item.orderSeller.sellerId
          );
          if (channel) {
            props.updateSelectedChannel({
              selectedChannel: channel,
            });
            if (store && channel) {
              getAllAvailableMenuItems(channel, store, item);
            }
          }
        } else {
          toast.warning("store is currently unavailable");
        }
      }
    );
  };

  return (
    <>
      <div id="main" className="sb-example-1">
        {!state ? (
          <div className="search">
            <input
              onInput={(event) => {
                if (event.target.name === "orderNo") {
                  setSearchValue(event.target.value.trim());
                  getOrderByOrerNo(event.target.value.trim());

                  // setSearch(!Search);
                  setSearch(false);
                }
              }}
              type="number"
              className="searchTerm"
              name="orderNo"
              onChange={(event) => {
                if (event.target.name === "orderNo") {
                  setSearchValue(event.target.value.trim());
                  getOrderByOrerNo(event.target.value.trim());

                  // setSearch(!Search);
                  setSearch(false);
                }
              }}
              // onPaste={(event) => {
              //   console.log(event);
              //   if (event.target.name === "orderNo") {
              //     setSearchValue(event.target.value.trim());
              //     console.log("paste: ", event.clipboardData.getData("text"));
              //     getOrderByOrerNo(event.target.value.trim());
              //     setSearch(false);
              //   }
              // }}
              placeholder="Search by Order No."
              value={SearchValue}
            />

            {Search ? (
              <button
                type="button"
                onClick={() => {
                  setSearch(!Search);
                  getOrderByOrerNo();
                }}
                className="searchButton"
              >
                <FontAwesomeIcon
                  icon={faSearch}
                  size="sm"
                  className="star-icon"
                />
              </button>
            ) : (
              <button
                type="button"
                onClick={() => {
                  setSearchValue("");
                  setSearch(!Search);
                }}
                className="searchButton"
              >
                <FontAwesomeIcon
                  icon={faTimes}
                  size="sm"
                  className="star-icon"
                />
              </button>
            )}
          </div>
        ) : (
          ""
        )}
      </div>
      {state ? (
        <>
          <div>
            <div
              style={{
                marginLeft: "20px",
                float: "right",
                fontSize: "28px",
                cursor: "pointer",
              }}
            >
              <span className="x" onClick={() => setState(false)}>
                {" "}
                <FontAwesomeIcon
                  icon={faTimes}
                  size="sm"
                  className="star-icon"
                />
              </span>
            </div>
            <ViewOrderDetails
              ResData={Data}
              resData={resData}
              item={item}
              fullFil={fullFil}
              orderItems={orderItem}
              orderTime={orderTime}
              DeliverTime={DeliverTime}
              orderData={orderData}
              Total={total}
              reOrder={reOrder}
            />
          </div>
        </>
      ) : (
        <>
          {Loading ? (
            <h1> </h1>
          ) : (
            <>
              {totalOrders == 0 ? (
                <div>
                  <h1>No Order Found</h1>
                </div>
              ) : (
                <>
                  {ScheduleList ? (
                    <>
                      {ScheduleList.map((item) => {
                        // const time = item.orderStatusLog.find((element) => {
                        //   if (element.statusId === item.currentStatusId) {
                        //     return <h1>{element.time}</h1>;
                        //   }
                        // });

                        return (
                          <div id="order" class="w3-container city">
                            <div id="content" class="col-lg-12 main-order">
                              <div class="col-lg-3 col-md-4 col-xs-12 col-sm-5 order-img">
                                {item.fulfilmentType === "DELIVERY" ? (
                                  <img
                                    src={
                                      require("../../../assets/images/delivery.svg")
                                        .default
                                    }
                                  />
                                ) : (
                                  <>
                                    {item.fulfilmentType === "TAKEAWAY" ? (
                                      <img
                                        src={
                                          require("../../../assets/images/pickup.svg")
                                            .default
                                        }
                                      />
                                    ) : (
                                      <img
                                        src={
                                          require("../../../assets/images/walkin.svg")
                                            .default
                                        }
                                      />
                                    )}
                                  </>
                                )}
                              </div>
                              <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 order-details">
                                <div class="order-number">
                                  <label>
                                    Order No. <span>{item.orderId}</span>
                                    <span style={{ color: "#0e07881" }}>
                                      {" "}
                                      | {item.fulfilmentType}{" "}
                                    </span>
                                  </label>
                                  <small style={{ color: "#007881" }}>
                                    {item.orderStatusUIResponsesLog.map(
                                      (time) => {
                                        return (
                                          <>
                                            {time.statusId ===
                                            item.currentStatusId
                                              ? `${time.uiTitle} | `
                                              : null}
                                            <span style={{ color: "#007881" }}>
                                              {time.statusId ===
                                              item.currentStatusId
                                                ? moment(time.time).format(
                                                    "ddd Do MMM YYYY, h:mm a"
                                                  )
                                                : null}
                                            </span>
                                          </>
                                        );
                                      }
                                    )}
                                    <img
                                      src={
                                        require("../../../assets/images/Tick.svg")
                                          .default
                                      }
                                    />
                                  </small>
                                </div>
                                <div class="order-address">
                                  <div className="addressContainer">
                                    <span>
                                      {item.customerAddress &&
                                      item.fulfilmentType === "DELIVERY"
                                        ? `${
                                            item.customerAddress
                                              .streetAddress2 !== null
                                              ? item.customerAddress
                                                  .streetAddress2
                                              : ""
                                          } ${
                                            item.customerAddress
                                              .streetAddress1 !== null
                                              ? item.customerAddress
                                                  .streetAddress1
                                              : ""
                                          } ${
                                            item.customerAddress.landmark !==
                                            null
                                              ? item.customerAddress.landmark
                                              : ""
                                          }`
                                        : ""}
                                    </span>
                                  </div>
                                  <div class="order-date">
                                    <small>
                                      {/* {item.orderTime} */}
                                      {moment(item.orderTime).format(
                                        "ddd Do MMM  YYYY, h:mm a"
                                      )}
                                    </small>
                                    <a>
                                      <button
                                        className="ViewOrder"
                                        onClick={() => {
                                          setState(true);
                                          setData(item);
                                          setItem(item.id);
                                          setDeliverTime(
                                            item.orderStatusLog.map((time) => {
                                              return (
                                                <span
                                                  style={{ color: "#007881" }}
                                                >
                                                  {time.statusId ===
                                                  item.currentStatusId
                                                    ? moment(time.time).format(
                                                        "ddd Do MMM YYYY, h:mm a"
                                                      )
                                                    : null}
                                                </span>
                                              );
                                            })
                                          );
                                          setOrderTime(
                                            moment(item.orderTime).format(
                                              "ddd Do MMM  YYYY, h:mm a"
                                            )
                                          );
                                          setorderItem(item.orderItems);
                                          setTotal(
                                            item.orderData.payableAmount
                                          );
                                          setOrderData(item.orderData);
                                          setfullFil(item.fulfilmentType);
                                          if (
                                            item.fulfilmentType === "DELIVERY"
                                          ) {
                                            setAddress(
                                              `${
                                                item.customerAddress
                                                  ?.streetAddress2 !== null
                                                  ? item.customerAddress
                                                      .streetAddress2
                                                  : ""
                                              } ${
                                                item.customerAddress
                                                  .streetAddress1 !== null
                                                  ? item.customerAddress
                                                      .streetAddress1
                                                  : ""
                                              } ${
                                                item.customerAddress
                                                  .landmark !== null
                                                  ? item.customerAddress
                                                      .landmark
                                                  : ""
                                              }`
                                            );
                                          }
                                        }}
                                      >
                                        View Order Details
                                      </button>
                                    </a>
                                  </div>
                                </div>
                                <div className="row orderforfirefox">
                                  <div class="order-name col-8">
                                    {item.orderItems.map((item) => {
                                      return (
                                        <span>
                                          {item.productName} x {item.quantity}
                                        </span>
                                      );
                                    })}
                                    <span>
                                      {}
                                      {/* Lemongrass Chai X2, Veg Biriyani Combo X 1, Raspberry
                          Chai x 3 */}
                                    </span>
                                  </div>
                                  <div class="order-name col-4">
                                    <label>
                                      Total{" "}
                                      <span>
                                        <i
                                          class="fa fa-inr"
                                          aria-hidden="true"
                                        ></i>
                                        ₹ {item.orderData.payableAmount}{" "}
                                      </span>
                                    </label>
                                  </div>
                                </div>
                                <div class="reorder-btn">
                                  <button
                                    class=" re-order"
                                    onClick={() => reOrder(item)}
                                  >
                                    REORDER
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </>
                  ) : (
                    " "
                  )}
                  {CompleteList ? (
                    <>
                      {CompleteList.map((item) => {
                        return (
                          <div id="order" class="w3-container city">
                            <div id="content" class="col-lg-12 main-order">
                              <div class="col-lg-3 col-md-4 col-xs-12 col-sm-5 order-img">
                                {item.fulfilmentType === "DELIVERY" ? (
                                  <img
                                    src={
                                      require("../../../assets/images/delivery.svg")
                                        .default
                                    }
                                  />
                                ) : (
                                  <>
                                    {item.fulfilmentType === "TAKEAWAY" ? (
                                      <img
                                        src={
                                          require("../../../assets/images/pickup.svg")
                                            .default
                                        }
                                      />
                                    ) : (
                                      <img
                                        src={
                                          require("../../../assets/images/walkin.svg")
                                            .default
                                        }
                                      />
                                    )}
                                  </>
                                )}
                              </div>
                              <div class="col-lg-9 col-md-8 col-sm-7 col-xs-12 order-details">
                                <div class="order-number">
                                  <label>
                                    Order No. <span>{item.orderId}</span>
                                    <span style={{ color: "#007881" }}>
                                      {" "}
                                      | {item.fulfilmentType}{" "}
                                    </span>
                                  </label>
                                  <small style={{ color: "#007881" }}>
                                    {item.orderStatusUIResponsesLog.map(
                                      (time) => {
                                        return (
                                          <>
                                            {time.statusId ===
                                            item.currentStatusId
                                              ? `${time.uiTitle} | `
                                              : null}
                                            <span style={{ color: "#007881" }}>
                                              {time.statusId ===
                                              item.currentStatusId
                                                ? moment(time.time).format(
                                                    "ddd Do MMM YYYY, h:mm a"
                                                  )
                                                : null}
                                            </span>
                                          </>
                                        );
                                      }
                                    )}
                                    <img
                                      src={
                                        require("../../../assets/images/Tick.svg")
                                          .default
                                      }
                                    />
                                  </small>
                                </div>
                                <div class="order-address">
                                  <span className="adddressContainer">
                                    {item.customerAddress &&
                                    item.fulfilmentType === "DELIVERY"
                                      ? `${
                                          item.customerAddress
                                            .streetAddress2 !== null
                                            ? item.customerAddress
                                                .streetAddress2
                                            : ""
                                        } ${
                                          item.customerAddress
                                            .streetAddress1 !== null
                                            ? item.customerAddress
                                                .streetAddress1
                                            : ""
                                        } ${
                                          item.customerAddress.landmark !== null
                                            ? item.customerAddress.landmark
                                            : ""
                                        }`
                                      : ""}
                                  </span>
                                  <div class="order-date">
                                    <small>
                                      {/* {item.orderTime} */}
                                      {moment(item.orderTime).format(
                                        "ddd Do MMM  YYYY, h:mm a"
                                      )}
                                    </small>
                                    <a>
                                      <button
                                        className="ViewOrder"
                                        onClick={() => {
                                          setData(item);
                                          setState(true);
                                          setItem(item.id);
                                          setDeliverTime(
                                            moment(
                                              item.orderStatusLog[2].time
                                            ).format("ddd Do MMM YYYY, h:mm a")
                                          );
                                          setOrderTime(
                                            moment(item.orderTime).format(
                                              "ddd Do MMM  YYYY, h:mm a"
                                            )
                                          );
                                          setorderItem(item.orderItems);
                                          setTotal(
                                            item.orderData.payableAmount
                                          );
                                          setOrderData(item.orderData);
                                          if (
                                            item.fulfilmentType === "DELIVERY"
                                          ) {
                                            setAddress(
                                              `${
                                                item.customerAddress
                                                  .streetAddress2 !== null
                                                  ? item.customerAddress
                                                      .streetAddress2
                                                  : ""
                                              } ${
                                                item.customerAddress
                                                  .streetAddress1 !== null
                                                  ? item.customerAddress
                                                      .streetAddress1
                                                  : ""
                                              } ${
                                                item.customerAddress
                                                  .landmark !== null
                                                  ? item.customerAddress
                                                      .landmark
                                                  : ""
                                              }`
                                            );
                                          }
                                          setfullFil(item.fulfilmentType);
                                        }}
                                      >
                                        View Order Details
                                      </button>
                                    </a>
                                  </div>
                                </div>
                                <div class="order-name">
                                  {item.orderItems.map((item) => {
                                    return (
                                      <span>
                                        {item.productName} x {item.quantity}
                                      </span>
                                    );
                                  })}
                                  <span>
                                    {}
                                    {/* Lemongrass Chai X2, Veg Biriyani Combo X 1, Raspberry
                          Chai x 3 */}
                                  </span>
                                  <label>
                                    Total{" "}
                                    <span>
                                      <i
                                        class="fa fa-inr"
                                        aria-hidden="true"
                                      ></i>
                                      ₹ {item.orderData.payableAmount}{" "}
                                    </span>
                                  </label>
                                </div>
                                <div class="reorder-btn">
                                  <button
                                    class="re-order"
                                    onClick={() => reOrder(item)}
                                  >
                                    REORDER
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </>
                  ) : (
                    " "
                  )}
                </>
              )}
            </>
          )}
        </>
      )}
    </>
  );
};

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const selectedStoreh1 = reducerObj.userObject.userObject.newHeader.sessionId;
  const storeList = reducerObj.storeList.storeList;
  const selected = reducerObj.cartObject.cartObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const inventory = reducerObj.menuItemList.menuItemList.inventory;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const cartItemList = reducerObj.cartObject.cartObject.cartItems;
  const channelList = reducerObj.channelList.channelList.channelList;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  return {
    selectedStoreh1,
    userObject,
    selected,
    menuItemList,
    inventory,
    cartItemList,
    storeList,
    channelList,
    categoryList,
    selectedChannel,
    selectedStore,
  };
};

export default connect(mapStateToProps, {
  updateCartObject,
  updateCartItem,
  updateMenuItem,
  loadMenuItemList,
  setMenuItemList,
  setCategoryMenuList,
  loadInventoryList,
  setServiceList,
  updateFilterObject,
  getProfile,
  updateSelectedChannel,
  updateSelectedStore,
  loadStoreList,
  emptyCartItem,
  upateMenuItemAfterCartItemRemoval,
})(Orders);
Orders.propTypes = {
  cartObject: PropTypes.object.isRequired,
  setServiceList: PropTypes.func.isRequired,
  updateCartObject: PropTypes.func.isRequired,
};

import React, { useState } from "react";
import { getProfile } from "../../../../src/actions/LoginAction";
import { updateUserObject } from "../../../../src/actions/UserAction";
import { connect } from "react-redux";
import { isValidEmail } from "../../../utils/common";
import toast from "../../common/commonToast";
import ProfileService from "../../../utils/services/profile/profile-service";
import { updateProfile } from "../../../../src/actions/LoginAction";

const EditProfile = (props) => {
  const [phoneNo, setPhone] = useState(props.userObject.phoneNo);
  const [email, setEmail] = useState(props.userObject.email);
  const [name, setName] = useState(props.userObject.name);
  const handleChange = (event) => {
    if (event.target.name === "phoneNo") {
      setPhone(event.target.value);
    } else if (event.target.name === "email") {
      setEmail(event.target.value);
    } else if (event.target.name === "name") {
      setName(event.target.value);
    }
  };

  const SaveProfile = () => {
    if (name.length > 3) {
      if (isValidEmail(email)) {
        props.updateProfile(
          props.userObject.newHeader,
          { phoneNo, name, email },
          (responseData) => {
            props.updateUserObject({
              phoneNo: responseData.phoneNo,
              email: responseData.email,
              name: responseData.name,
              customerId: responseData.customerId,
            });

            toast.success("Profile updated successfully", {});
            props.onHide();
          }
        );
      } else {
        toast.error("Please enter velid email", {});
      }
    } else {
      toast.error("Name must be greater then 3 characters", {});
    }
  };
  return (
    <div className="add-new-address">
      <div class="Edit-Profile">Edit Profile</div>
      <div>
        <div class="form-group">
          <label for="exampleInputEmail1">Name</label>
          <input
            name={"name"}
            value={name}
            onChange={handleChange}
            type="Name"
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            placeholder="Enter Name"
          />
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Email</label>
          <input
            name={"email"}
            value={email}
            onChange={handleChange}
            type="email"
            class="form-control"
            id="exampleInputPassword1"
            placeholder="Enter Email"
          />
        </div>

        <button
          type="submit"
          class="btn btn-primary"
          onClick={() => {
            SaveProfile();
          }}
        >
          Submit
        </button>
      </div>
    </div>
  );
};
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, { updateProfile, updateUserObject })(
  EditProfile
);

import React, { Component } from "react";
import { isMobileOnly } from "react-device-detect";
import {Link, useHistory} from "react-router-dom"
import {Redirect} from "react-router-dom"
import img from "../../../";

export default class Payments extends Component {
  
  render() {
    // let history =useHistory();
    return (
      <>
      {
        isMobileOnly? 
        <> 
        <div className="container">
        <div className="header-user">
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
            }}
            className=""
          >
            <Link to="/profile">
            <span>
              <img
                className="back-arrow"
                src={
                  require("../../../assets/images/left-arrow.svg")
                    .default
                }
              />{" "}
            </span>
            </Link>

            <span className="ORDERS">PAYMENT</span>
          </div>
        </div>
      </div>
      <div className="myOrder">
        {" "}
        <div className="myOrderText">My Payment</div>
      </div>
      </>:<Redirect to="/profile"/>
      }
      <div id="payments" class="w3-container city payments-profile">

        <div className="payment-header">Saved Cards</div>
        <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12 payments-history">
          <div class="col-xs-12 payment-details">
            <div class="col-xs-12 col-lg-5 col-sm-12 col-md-6 payment-options">
              <div class="payment-visa">
                <img
                  src={require("../../../assets/images/Payment-02.svg").default}
                />
              </div>
              <div class="card-details">
                <label> 1234 xxx xxxx 2345</label>
                <span>
                  Valid Till <small>04/2023</small>
                </span>
              </div>
              <div class="payment-delete">
                <button class="btn btn-primary delete-button-cart payment-delete-btn">
                  Delete
                </button>
              </div>
            </div>
            <div class="col-xs-12 col-lg-5 col-sm-12 col-md-6 payment-options">
              <div class="payment-visa">
                <img
                  src={require("../../../assets/images/Payment-02.svg").default}
                />
              </div>
              <div class="card-details">
                <label> 1234 xxx xxxx 2345</label>
                <span>
                  Valid Till <small>04/2023</small>
                </span>
              </div>
              <div class="payment-delete">
                <button class="btn btn-primary delete-button-cart payment-delete-btn">
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="payment-header">Linked Wallets</div>
        <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12 payments-history">
          <div class="col-xs-12 payment-details">
            <div class="col-xs-12 col-lg-5 col-sm-12 col-md-6 payment-options">
              <div class="payment-visa">
                <img
                  src={require("../../../assets/images/Payment-02.svg").default}
                />
              </div>
              <div class="card-details">
                <label> 1234 xxx xxxx 2345</label>
                <span>
                  Valid Till <small>04/2023</small>
                </span>
              </div>
              <div class="payment-delete">
                <button class="btn btn-primary delete-button-cart payment-delete-btn">
                  Delete
                </button>
              </div>
            </div>
            <div class="col-xs-12 col-lg-5 col-sm-12 col-md-6 payment-options">
              <div class="payment-visa">
                <img
                  src={require("../../../assets/images/Payment-02.svg").default}
                />
              </div>
              <div class="card-details">
                <label> 1234 xxx xxxx 2345</label>
                <span>
                  Valid Till <small>04/2023</small>
                </span>
              </div>
              <div class="payment-delete">
                <button class="btn btn-primary delete-button-cart payment-delete-btn">
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      </>
    );
  }
}

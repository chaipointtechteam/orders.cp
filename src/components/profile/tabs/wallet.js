import React, { Component } from "react";
import { isMobileOnly } from "react-device-detect";
import {Link, useHistory} from "react-router-dom"
import {Redirect} from "react-router-dom"

export default class Wallet extends Component {
  render() {
    return (
      <>
      {
       isMobileOnly? 
       <> 
       <div className="container">
       <div className="header-user">
         <div
           style={{
             display: "flex",
             flexDirection: "row",
             alignItems: "center",
           }}
           className=""
         >
           <Link to="/profile">
           <span>
             <img
               className="back-arrow"
               src={
                 require("../../../assets/images/left-arrow.svg")
                   .default
               }
             />{" "}
           </span>
           </Link>

           <span className="ORDERS">WALLET</span>
         </div>
       </div>
     </div>
     <div className="myOrder">
       {" "}
       <div className="myOrderText">Manage Wallet</div>
       
     </div>
     </>:<Redirect to="/profile"/>
     }
      <div id="wallet" class="w3-container city">
        <h2>Tokyo</h2>
        <p>Tokyo is the capital of Japan.</p>
      </div>
      </>
    );
  }
}

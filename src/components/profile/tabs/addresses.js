import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import { isMobileOnly } from "react-device-detect";
import { connect } from "react-redux";
import { Redirect, withRouter } from "react-router-dom";
// import { connect } from "react-redux";
import { getProfile } from "../../../../src/actions/LoginAction";
import AddressModal from "../../../components/common/address/address-container";
import ProfileService from "../../../utils/services/profile/profile-service";
import AddAddress from "./addAddress";



class Addresses extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      showAddAddressModal: false,
      edit: "",
      Modal: false,
      setDefault:props.location.setDefalut||null
    };
  }


  setDefault = (id) => {
    let currentComponent = this;

    var phoneNo = this.props.userObject.phoneNo;
    var sessionId = this.props.userObject.headerSessionId;
    ProfileService.defaultAddress(id, sessionId)
      .then((response) => {
        currentComponent.props.getProfile(`${phoneNo}`, sessionId);
        if (this.state.setDefault) {this.props.history.goBack()}
      })
      .catch((error) => {});
  };


  flipAddAddressState = () => {
    this.setState((state, props) => ({
      showAddAddressModal: !state.showAddAddressModal,
    }));
  };

  flipAdd = () => {
    this.setState((state, props) => ({
      Modal: false,
    }));
  };

  saveOrUpdateAddress = (addressObj) => {
    this.flipAddAddressState();
  };

  DeleteAction = async (item) => {

    const payload = {
      addressId: item.addressId,
      addressTypeId: item.addressTypeId,
      fullName: item.fullName,
      landmark: item.landmark,
      latitude: item.latitude,
      longitude: item.longitude,
      phoneNo: Number(item.mobileNumber),
      pincode: Number(item.zipcode),
      streetAddress1: item.streetAddress1,
      isDefault: item.isDefault,
      isActive: false,
      streetAddress2: item.streetAddress2,
    };
    const headerSessionId = this.props.userObject.headerSessionId;
    const op = await ProfileService.addUpdateAddress(payload, headerSessionId)
      .then((e) => {
      

        this.props.getProfile(
          `${this.props.userObject.phoneNo}`,
          headerSessionId
        );
      })
      .catch((e) => {});
  };

  render() {
    const { customerAddress } = this.props.userObject;
   
    return (
      <>
        {isMobileOnly || window.innerWidth < 760 ? (
          <>
            <div class="showFooter">
              <div className="container">
                <div className="header-user">
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      alignItems: "center",
                    }}
                    className=""
                  >
                    <span onClick={() => this.props.history.goBack()}>
                      <img
                        className="back-arrow"
                        src={
                          require("../../../assets/images/left-arrow.svg")
                            .default
                        }
                      />{" "}
                    </span>

                    <span className="ORDERS">ADDRESS</span>
                  </div>
                </div>
              </div>
              <div className="myOrder">
                {" "}
                <div className="myOrderText">Manage Addresses</div>
              </div>
            </div>
          </>
        ) : (
          <Redirect to="/profile" />
        )}
        <div id="address" class="w3-container  manage-address-section">
          <div class="header-add">Manage Addresses</div>
          <div className="">
            <a
              onClick={this.flipAddAddressState}
              href="#"
              className="add-addresses detailHeader"
            >
              <h2 class="">Add Address</h2>
            </a>
            {this.state.showAddAddressModal ? (
              <AddressModal
                show={this.state.showAddAddressModal}
                userObject={this.props.userObject}
                onClose={this.flipAddAddressState}
                checkout={this.props.location.setDefalut}
                // Edit={this.state.edit}
              />
            ) : (
              ""
            )}

            {this.state.Modal ? (
              <Modal
                className="modal-opacity signup"
                id="addAddressModal"
                show={this.state.Modal}
                onHide={this.flipAdd}
              >
                <AddAddress
                  onClose={this.flipAdd}
                  userObject={this.props.userObject}
                  saveOrUpdateAddress={this.saveOrUpdateAddress}
                  Edit={this.state.edit}
                />
              </Modal>
            ) : (
              ""
            )}
          </div>
          <div class="col-xs-24 multiple-address">
            {customerAddress.length > 0 ? (
              <>
                {customerAddress.map((item) => {
                  return (
                    <>
                      <div class="col-xs-12 col-lg-5 col-sm-6 col-md-6 single-address">
                        <div class="manage-address">
                          <label>
                            <img
                              src={
                                require("../../../assets/images/Location-01.svg")
                                  .default
                              }
                            />
                            {item.addressTypeName}
                          </label>
                          <span>
                            {item.streetAddress1} {item.streetAddress2}{" "}
                            {item.zipcode}
                          </span>

                          <span class="address-buttons">
                               <div style={{display:"flex",justifyContent:"space-between", margin:"0px 6px"}}>
                              <div>
                              <button
                                class="btn btn-primary edit-button-cart"
                                onClick={() => {
                                  this.setState({
                                    edit: item,
                                    Modal: !this.state.Modal,
                                  });
                                }}
                              >
                                Edit
                              </button>
                              </div>
                              <div>
                              <button
                                class="btn btn-primary delete-button-cart"
                                onClick={() => this.DeleteAction(item)}
                              >
                                Delete
                              </button>
                              </div>
                              </div>
                          </span>
                        </div>
                        {item.isDefault == true ? "" :
                          <div class="showFooter">
                            <button
                              style={{
                                marginTop: "10px",
                                width: "100%",
                                padding: "8px",
                                borderRadius: "8px",
                              }}
                              class="btn btn-primary Default-button-cart"
                              onClick={() => this.setDefault(item.addressId)}
                            >
                              Deliver Here
                            </button>
                          </div>}
                      </div>
                    </>
                  );
                })}
              </>
            ) : (
              <h1>No Address is added</h1>
            )}

            {/* <div class="col-xs-12 col-lg-5 col-sm-6 col-md-6 single-address">
              <div class="manage-address">
                <label>
                  <img
                    src={
                      require("../../../assets/images/Location-01.svg").default                    }
                  />
                  Home
                </label>
                <span>
                {}
                </span>

                <span class="address-buttons">
                  <button class="btn btn-primary edit-button-cart">Edit</button>
                  <button class="btn btn-primary delete-button-cart">
                    Delete
                  </button>
                </span>
              </div>
            </div> */}
          </div>
        </div>
        <div class="btn-group-fab" style={{ marginRight: "0px", right: "0px", left: "88%" }} role="group" aria-label="FAB Menu">

          <div>
            <button
              type="button"
              onClick={this.flipAddAddressState}
              class="btn btn-main btn-primary has-tooltip"
              data-placement="left"
              title="Menu"
            >
              {" "}
              <FontAwesomeIcon
                icon={faPlus}
                size="lg"
                className="star-icon-plus"
              />
            </button>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, { getProfile })(withRouter(Addresses));

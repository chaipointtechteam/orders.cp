import React from "react";
import { getProfile } from "../../../../src/actions/LoginAction";
import { updateUserObject } from "../../../../src/actions/UserAction";
import { connect } from "react-redux";
import ProfileService from "../../../utils/services/profile/profile-service";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faArrowLeft } from "@fortawesome/free-solid-svg-icons";

import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";
import styles from "../../common/address/styles/address-modal.module.scss";
import Map from "../../common/address/map";
import { getCurrentLatLong } from "../../../utils/address_helper";

class AddAddress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addressLine: this.props.Edit.streetAddress2,
      streetAddress1: this.props.Edit.streetAddress1,
      // landmark: this.props.Edit.landmark,
      zipcode: this.props.Edit.zipcode,
      addressType: this.props.Edit.addressTypeId,
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
  }

  // componentDidMount = async () => {
  //   try {
  //     const { lat, long } = await getCurrentLatLong();
  //     this.setState({ lat, long, ...this.state });
  //   } catch (error) { }
  // };

  handleChange(event) {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  }
  handleInputChange = ({ target = {} }) => {
    const { name = "" } = target;
    const { current = {} } = this.state;
    let value = target.value;
    if (name === "pincode" && isNaN(target.value)) {
      value = current.pincode;
    }

    const newCurrent = { ...current, [name]: value };
    this.setState({ current: newCurrent });

  };

  isInvalidForm = () => {
    const { current = {} } = this.state;
    ["streetAddress1", "streetAddress2", "pincode", "landmark"].forEach((i) => {
      if (!current[i] || !current[i].trim()) {
        return true;
      }
    });
    if (current["streetAddress1"]?.length == 0 || current["streetAddress1"] == null) {
      return true;
    }
    else if (current["streetAddress2"].length == 0) {
      return true;
    }
    else if (current["landmark"]?.length == 0 || current["landmark"] == null) {
      return true;
    }
    else if (current["pincode"].length != 6) {
      return true;
    }
    else
      return false;
  };


  async onSubmitForm() {

    const { Edit } = this.props;
    const { userObject = {} } = this.props;
    const { email, phoneNo, name, headerSessionId } = userObject;

    const {
      addressType,
      streetAddress1,
      streetAddress2,
      pincode,
      fullName,
      landmark,
      lat,
      lng,
    } = this.state.current;
    const payload = {
      addressId: Edit.addressId,
      addressTypeId: this.state.addressType,
      emailId: email,
      fullName: name,
      landmark: landmark,
      latitude: lat,
      longitude: lng,
      phoneNo: Number(phoneNo),
      pincode: Number(pincode),
      streetAddress2: streetAddress2,
      isDefault: false,
      streetAddress1: streetAddress1,
    };
    const { sessionId } = this.props.userObject.newHeader;

    const op = await ProfileService.addUpdateAddress(payload, headerSessionId)

      .then((e) => {

        ProfileService.defaultAddress(e, headerSessionId)
        setTimeout(() => {
          this.props.getProfile(`${phoneNo}`, headerSessionId);
        }, 200);

        if (e.status === 400) {
          this.setState({ error: e.data.message });
        } else {
          this.props.onClose();

        }


      })
      .catch((e) => {

      });

    // this.props.saveOrUpdateAddress(this.state);
  }

  render() {
    const { show, onClose } = this.props;
    const { current = {}, onSave } = this.state;
    const isInvalidForm = this.isInvalidForm();

    return (
      <div className="add-new-address">
        <div style={{ paddingBottom: "5%" }} >
          <label onClick={this.props.onClose} style={{ marginRight: "20px" }}>
            <FontAwesomeIcon
              icon={faArrowLeft}
              size="lg"
              className=""
            /></label>
          <label>Edit Address</label>

        </div>
        <div className="add-new-address" style={{ paddingLeft: 0, paddingRight: 0 }}>
          <Map
            height="150px"
            zoom={15}
            editData={this.props.Edit}
            onAddressChange={(current) => {

              this.setState({
                current: {
                  ...current,
                  streetAddress2: current.address,
                  pincode: current.pincode,

                },
              });
            }}
            current={current}
          />
        </div>
        <div className="mapAddress">
          {[
            { field: "streetAddress2", placeholder: "Full Address" },
            { field: "streetAddress1", placeholder: "Flat no/ Door No" },
            { field: "landmark", placeholder: "Landmark" },
            { field: "pincode", placeholder: "Pincode", maxlength: "6" },
          ].map((i) => {
            const { field, ...rest } = i;

            return (
              <div>
                <input
                  required
                  {...rest}
                  value={current[field]}
                  name={field}
                  onChange={this.handleInputChange}
                />
              </div>
            );
          })}

          <div className="Address-category bbb">
            {this.props.Edit.addressTypeId === 1 ?
              <input
                type="radio"
                id="home"
                name="addressType"
                value={1}
                checked
                onChange={this.handleInputChange}
              /> : <input
                type="radio"
                id="home"
                name="addressType"
                value={1}

                onChange={this.handleChange}
              />}
            <label htmlFor="home">Home</label>
            {this.props.Edit.addressTypeId === 2 ?
              <input
                type="radio"
                id="work"
                name="addressType"
                value={2}
                checked
                onChange={this.handleInputChange}
              /> : <input
                type="radio"
                id="work"
                name="addressType"
                value={2}

                onChange={this.handleChange}
              />}
            <label htmlFor="work">Office</label>
            {this.props.Edit.addressTypeId === 3 ?
              <input
                type="radio"
                id="other"
                name="addressType"
                value={3}
                checked
                onChange={this.handleInputChange}
              />
              : <input
                type="radio"
                id="other"
                name="addressType"
                value={3}

                onChange={this.handleChange}
              />}
            <label htmlFor="other">Other</label>
          </div>
          <div>
            <Button
              variant="primary"
              onClick={this.onSubmitForm}
              disabled={isInvalidForm}
            >
              Save Address
            </Button>
            <p style={{ color: "red" }}>{this.state.error}</p>
          </div>
        </div>
        {/* <div style={{ marginTop: "10%" }}>
          <div>
            <input
              type="text"
              name="addressLine"

              value={this.props.addressLine}
              placeholder="Full Address"
              onChange={this.handleChange}
            ></input>
          </div>
          <br />
          <div>
            <input
              required
              type="text"
              name="flatNumber"
              defaultValue={this.props.Edit.streetAddress2}
              value={this.state.flatNumber}
              placeholder="Flat No/Door No"
              onChange={this.handleChange}
            ></input>
          </div>
          <br />
          <div>
            <input
              type="text"
              name="landmark"

              value={this.state.landmark}
              placeholder="Landmark"
              onChange={this.handleChange}
            ></input>
          </div>
          <br />
          <div>
            <input
              type="number"
              name="zipcode"

              value={this.state.zipcode}
              placeholder="zipcode"
              onChange={this.handleChange}
            ></input>
          </div>
          <br />
        </div>
        <div className="Address-category">
      

          {this.props.Edit.addressTypeId === 1 ? <input
            type="radio"
            id="home"
            name="addressType"
            value={1}
            checked
            onChange={this.handleChange}
          /> : <input
            type="radio"
            id="home"
            name="addressType"
            value={1}
            onChange={this.handleChange}
          />}
          <label htmlFor="home">Home</label>


          {this.props.Edit.addressTypeId === 2 ?
            <input
              type="radio"
              id="work"
              name="addressType"
              value={2}
              checked
              onChange={this.handleChange}
            /> : <input
              type="radio"
              id="work"
              name="addressType"
              value={2}

              onChange={this.handleChange}
            />}
          <label htmlFor="work">Work</label>
          {this.props.Edit.addressTypeId === 3 ?
            <input
              type="radio"
              id="other"
              name="addressType"
              value={3}
              checked
              onChange={this.handleChange}
            /> : <input
              type="radio"
              id="other"
              name="addressType"
              value={3}

              onChange={this.handleChange}
            />}
          <label htmlFor="other">Other</label>
        </div>
        <div>
          <button
            title="Save Address"
            onClick={this.onSubmitForm}
            name="saveAddress"
          >
            Save Address
          </button>
        </div> */}
      </div>
    );
  }
}
const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatchEvent) => {
  return {
    getProfile: (payload, sessionId) =>
      dispatchEvent(getProfile(payload, sessionId)),
    updateUserObject: (payload) => dispatchEvent(updateUserObject(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddAddress);

import React, { Component, useState, useEffect } from "react";
import moment from "moment";
import axios from "axios";
import { header } from "../../../utils/apiUtil";

const ViewOrderDetails = (props) => {
  const [statusData, setStatusData] = useState("");
  const [Qr, setQr] = useState("");

  const options = {
    headers: { ...header, sessionId: `${props.selectedStoreh1}` },
  };
  useEffect(() => {
    getStatus();
  }, []);

  useEffect(() => {
    if (props) {
      var i = setInterval(() => getStatus(), 10000);
    }
    return () => {
       clearInterval(i)
    }
  }, [props]);




  const getStatus = async () => {
    var config = {
      method: "post",
      url: `https://testing.chaipoint.com/qa3/online-ag/api/order/getOrderStatus?orderId=${props.ResData.id}`,
      headers: options.headers,
    };
    await axios(config).then((e) => setStatusData(e.data));
  };

  return (
    <>
      <div className="Order-No-123456-Delivery">
        Order No. {props.ResData.orderId} |
        <span className="text-style-1"> {props.fullFil}</span>
      </div>

      <div className="col-lg-12 Rectangle-2">
        <div
          style={{ borderBottom: "2px solid", borderBottomColor: "#b6b8ba" }}
        >
          <div className="row">
            {props.ResData.fulfilmentType === "DELIVERY" ? (
              ""
            ) : (
              <div
                className="col"
                style={{
                  paddingBottom: "3%",
                  paddingTop: "2%",
                  paddingLeft: "3.3%",
                }}
              >
                <img
                  style={{
                    width: "152px",
                    height: "150px",
                  }}
                  src={`data:image/png;base64,${props.ResData.qrCode?.qrCodeImage}`}
                />
                {/* < QRCode style={{ width: "152px", height: "150px" }} value={props.ResData.qrCode.qrCodeImage} /> */}
              </div>
            )}
            <div
              style={{
                marginRight:
                  props.ResData.fulfilmentType === "DELIVERY" ? "0%" : "5%",
                marginLeft:
                  props.ResData.fulfilmentType === "DELIVERY" ? "3%" : "0%",
              }}
              class={
                props.ResData.fulfilmentType === "DELIVERY"
                  ? "col-lg-11 col-md-8 col-sm-7 col-xs-12 order-details"
                  : "col-11 col-md-8 col-sm-7 col-xs-12 order-details"
              }
            >
              <div class="order-number">

                <label style={{ fontFamily: "NeueHaasUnica-regular" }}>
                  Order Details
                </label>
                <small style={{ color: "#007881" }}>
                  {/* {props.ResData.orderStatusUIResponsesLog.map((time) => {
                    return (
                      <>
                        {time.statusId === props.ResData.currentStatusId
                          ? `${time.uiTitle} |`
                          : null}
                        <span style={{ color: "#007881" }}>
                          {" "}
                          {time.statusId === props.ResData.currentStatusId
                            ? moment(time.time).format(
                                "ddd Do MMM YYYY, h:mm a"
                              )
                            : null}
                        </span>
                      </>
                    );
                  })} */}
                  {moment(statusData.orderStatusUIResponse?.time).format(
                    "ddd, Do MMM YYYY | "
                  )}
                  {statusData.orderStatusUIResponse?.uiTitle}
                  {" at "}
                  {moment(statusData.orderStatusUIResponse?.time).format(
                    "h:mm a"
                  )}

                  <img
                    src={require("../../../assets/images/Tick.svg").default}
                  />
                </small>
              </div>
              <div class="order-address">
                <span></span>
                <div class="order-date">
                  <div style={{ fontFamily: "NeueHaasUnica-regular" }}>
                    {props.ResData.deliveryStatusUIResponse.uiTitle}
                  </div>
                  <div style={{ fontFamily: "NeueHaasUnica-regular" }}>
                    {moment(props.ResData.orderTime).format(
                      `ddd, Do MMM YYYY | `
                    )}
                    {"Ordered at"}
                    {moment(props.ResData.orderTime).format(` h:mm a`)}
                  </div>
                  <div style={{ fontFamily: "NeueHaasUnica-regular" }}>
                    {statusData.deliveryStatusUIResponse?.uiTitle}
                    {statusData.riderPhoneNumber
                      ? ` | Rider Phone : +91 ${statusData.riderPhoneNumber}`
                      : null}
                  </div>
                  <div style={{ fontFamily: "NeueHaasUnica-regular" }}></div>
                </div>
              </div>
              <div class="order-name">
                <label style={{ fontFamily: "NeueHaasUnica-regular" }}>
                  Total{" "}
                  <span>
                    <i class="fa fa-inr" aria-hidden="true"></i>₹{" "}
                    {props.Total.toFixed(2)}{" "}
                  </span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div
          style={{ margin: "2% 3%", marginRight: "5%", fontSize: 24 }}
          class="order-number"
        >
          <label style={{ fontFamily: "NeueHaasUnica-regular" }}>
            Order Summary
          </label>
          <button
            class=" re-order"
            onClick={() => props.reOrder(props.ResData)}
          >
            REORDER
          </button>
        </div>
        <div style={{ marginBottom: "2%" }}>
          {props.orderItems.map((item) => {
            return (
              <>
                <div className="">
                  <div style={{ padding: 0 }} className="col-lg-12">
                    {/* <div
                    style={{ padding: 0 }}
                    class="col-lg-3 col-md-4 col-xs-12 col-sm-5"
                  >
                    <img
                      className="Rectangle-9"
                      src={
                        require("../../../assets/images/Offers-05.jpg").default
                      }
                    />
                  </div> */}
                    <div
                      style={{
                        marginTop: "2%",
                        padding: "2% 0% 2% 2%",
                        border: "solid 0.5px #b6b8ba",
                        borderRadius: "7.6px",
                      }}
                      class="col-lg-12 col-md-8 col-sm-7 col-xs-12"
                    >
                      <div
                        style={{ padding: 0, marginTop: "14px", width: "20px" }}
                        className="col-lg-1 col-md-4 col-xs-12 col-sm-5"
                      >
                        <img
                          class="Rectangle-13"
                          src={
                            require("../../../assets/images/Veg.svg").default
                          }
                        />
                      </div>
                      <div className="col-lg-11 col-md-8 col-sm-7 col-xs-12 bill-header">
                        <table
                          style={{ margin: 0, padding: 0 }}
                          class="table table-borderless "
                        >
                          <thead style={{ borderStyle: "hidden" }}>
                            <tr>
                              <div className="row">
                                <div className="col-6">
                                  <th style={{ textAlign: "left" }} scope="col">
                                    <label
                                      style={{
                                        fontFamily: "NeueHaasUnica-regular",
                                        fontSize: "16px",
                                      }}
                                    >
                                      {" "}
                                      {item.productName}
                                    </label>
                                  </th>
                                </div>
                                <div className="col-4">
                                  <th style={{ textAlign: "left" }} scope="col">
                                    <label
                                      style={{
                                        fontFamily: "NeueHaasUnica-regular",
                                        fontSize: "16px",
                                      }}
                                    >
                                      {item.quantity} x {item.price}
                                    </label>
                                  </th>
                                </div>
                                <div className="col-2 bill-seperation">
                                  <th
                                    style={{ textAlign: "right" }}
                                    scope="col"
                                  >
                                    <label
                                      style={{
                                        fontFamily: "NeueHaasUnica-regular",
                                        fontSize: "16px",
                                      }}
                                    >
                                      ₹ {item.quantity * item.price}{" "}
                                    </label>
                                  </th>
                                </div>
                              </div>
                            </tr>

                            {item.items.map((item) => {
                              return (
                                <div
                                  className="row"
                                  style={{
                                    paddingLeft: "30px",
                                    color: "GrayText",
                                  }}
                                >
                                  <div className="col-sm-6">
                                    {item.productName}
                                  </div>
                                  <div
                                    className="col-sm-3"
                                    style={{ paddingLeft: "20px" }}
                                  >
                                    {item.quantity} x {item.price}
                                  </div>
                                  <div
                                    className="col-sm-3"
                                    style={{ paddingLeft: "130px" }}
                                  >
                                    ₹{item.quantity * item.price}
                                  </div>
                                </div>
                              );
                            })}
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </>
            );
          })}
        </div>
        <div style={{ marginBottom: "2%" }}>
          <div style={{ marginBottom: "2%" }} className="text2">
            Bill Details
          </div>
          <div>
            <div>
              <table className="table table-borderless bill-table">
                <tbody style={{ borderStyle: "hidden" }}>
                  <tr style={{ borderTop: "none" }}>
                    <td style={{ textAlign: "left" }} scope="col">
                      <label className="text3">Item Total</label>
                    </td>
                    <td style={{ textAlign: "right" }} scope="col">
                      <label className="text3">
                        ₹ {props.orderData.subTotal.toFixed(2)}
                      </label>
                    </td>
                  </tr>
                  <tr style={{ borderTop: "none" }}>
                    <td style={{ textAlign: "left" }} scope="col">
                      <label className="text3">Delivery Charges</label>
                    </td>
                    <td style={{ textAlign: "right" }} scope="col">
                      <label className="text3">
                        ₹ {props.orderData.totalCharges.toFixed(2)}
                      </label>
                    </td>
                  </tr>
                  <tr style={{ borderTop: "none" }}>
                    <td style={{ textAlign: "left" }} scope="col">
                      <label className="text3">Taxes and Charges</label>
                    </td>
                    <td style={{ textAlign: "right" }} scope="col">
                      <label className="text3">
                        ₹ {props.orderData.totalTax.toFixed(2)}
                      </label>
                    </td>
                  </tr>
                  <tr style={{ borderTop: "none" }}>
                    <td style={{ textAlign: "left" }} scope="col">
                      <label className="text3">Discount</label>
                    </td>
                    <td style={{ textAlign: "right" }} scope="col">
                      <label className="text3">
                        ₹ {props.orderData.discount.toFixed(2)}
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div
            style={{
              border: "none",
              marginTop: "4%",
              borderTop: "solid 2px",
              borderTopColor: "#12767f",
              borderTopLeftRadius: "none",
              borderTopRightRadius: "none",
              height: "60px",
            }}
            className="Rectangle-151"
          >
            <table
              style={{ margin: 0, padding: 0 }}
              className="table table-borderless"
            >
              <tbody>
                <tr style={{ border: "none" }}>
                  <td style={{ textAlign: "left" }} scope="col">
                    <label className="text4">TOTAL</label>
                  </td>
                  <td style={{ textAlign: "right" }} scope="col">
                    <label className="text4">₹ {props.Total.toFixed(2)}</label>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewOrderDetails;

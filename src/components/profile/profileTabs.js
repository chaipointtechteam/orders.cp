import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faTimes } from "@fortawesome/free-solid-svg-icons";

import Tabs from "react-responsive-tabs";

// IMPORTANT you need to include the default styles
import "react-responsive-tabs/styles.css";
import Payments from "./tabs/payments";
import Addresses from "./tabs/addresses";
import Orders from "./tabs/orders";
import LoyaltyPoints from "./tabs/loyaltyPoints";
import Wallet from "./tabs/wallet";
import "./profile.css";

const Profile = (props) => {
  const [orderNo, setOrderNo] = useState("");
  const [date, setDate] = useState(props.history);
  const [state, setState] = useState(true);

  const [tabs, setTabs] = useState([
    { name: "Orders", content: <Orders /> },
    // { name: "Payments", content: <Payments /> },
    { name: "Addresses", content: <Addresses /> },
    // { name: "Loyalty Points", content: <LoyaltyPoints /> },
    // { name: "Wallet", content: <Wallet /> },
  ]);

  function getTabs() {
    
    return tabs.map((tab, index) => ({
      title: tab.name,
      getContent: () => tab.content,
      /* Optional parameters */
      key: index,
      tabClassName: "w3-bar-item w3-button tablinkab",
      panelClassName: "w3-container city",
    }));
  }

  const getSearchByOrederNo = (e) => {
    setTabs([
      { name: "Orders", content: <Orders name={orderNo} /> },
      // { name: "Payments", content: <Payments /> },
      // { name: "Addresses", content: <Addresses /> },
      // { name: "Loyalty Points", content: <LoyaltyPoints /> },
      // { name: "Wallet", content: <Wallet /> },
    ]);
  };

  useEffect(() => {
    getTabs();
  }, [state]);

  return (
    <div className="container profile-container">
      <div id="content" className="w3-container profile-tabs">
        {/* <div id="main" className="sb-example-1">
          <div className="search">
            <input
              type="text"
              className="searchTerm"
              name="orderNo"
              onChange={(event) => {
                if (event.target.name === "orderNo") {
                  setOrderNo(event.target.value.trim());
                }
              }}
              placeholder="Search by Order No."
              value={orderNo}
            />
            {state ? (
              <button
                type="button"
                onClick={() => {
                  if (orderNo !== "") {
                    setState(!state);
                    getSearchByOrederNo();
                  }
                }}
                className="searchButton"
              >
                <img src={require("../../assets/images/Search.svg").default} />
              </button>
            ) : (
              <button
                type="button"
                onClick={() => {
                  setState(!state);
                  setTabs([
                    { name: "Orders", content: <Orders /> },
                    // { name: "Payments", content: <Payments /> },
                    { name: "Addresses", content: <Addresses /> },
                    // { name: "Loyalty Points", content: <LoyaltyPoints /> },
                    // { name: "Wallet", content: <Wallet /> },
                  ]);
                }}
                className="searchButton"
              >
                <FontAwesomeIcon
                  icon={faTimes}
                  size="sm"
                  className="star-icon"
                />
              </button>
            )}
          </div>
        </div> */}

        <Tabs
          items={getTabs()}
          selectedTabKey={date ? date.Tab : 0}
        />
      </div>
    </div>
  );
};
export default Profile;

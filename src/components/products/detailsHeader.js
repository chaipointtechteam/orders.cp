import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import ProfileDropdown from "../common/profileDropdown";
import HeaderChannelSelector from "./headerChannelSelector";
import HeaderStoreSelector from "./headerStoreSelector";
import {Link} from "react-router-dom"
class DetailsHeader extends Component {
  render() {
    return (
      <div className="detailHeader">
      <section className="category-nav">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div className="logo-display">
                  <Link to="/">
                    <img
                      src={require("../../assets/images/logo.png").default}
                      alt="Chai point"
                    />
                  </Link>
                </div>
              </div>
              <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12 category-list">
                <div className="category-component">
                  <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                      <div className="col-md-4 col-sm-6 col-xs-6">
                        <div
                          className="nav-location"
                          style={{ color: "#2b2728" }}
                        >
                          <HeaderStoreSelector />
                        </div>
                      </div>
                      <div className="col-md-3 col-sm-4 col-xs-6">
                        <div className="nav-location">
                          <HeaderChannelSelector />
                        </div>
                      </div>
                      <div className="col-md-5 col-sm-2 col-xs-2">
                      <ProfileDropdown /></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </section>
        
        </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const selectedLocation =
    reducerObj.selectedLocation.selectedLocation.selectedLocation;
  return { selectedStore, selectedChannel, selectedLocation };
};

export default connect(mapStateToProps, {})(DetailsHeader);

DetailsHeader.propTypes = {
  selectedChannel: PropTypes.object,
  selectedStore: PropTypes.object,
  selectedLocation: PropTypes.object,
};

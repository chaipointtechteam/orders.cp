import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import Select from "react-select";
import { emptyCartItem, resetCartObject } from "../../actions/CartAction";
import { setServiceList } from "../../actions/ChannelListAction";
import { resetMenuItemList } from "../../actions/MenuItemsAction";
import { updateSelectedStore } from "../../actions/StoreListAction";
import toast from "../common/commonToast";
const customStyles = {
  menu: (styles) => ({
    ...styles,
    zIndex: 9999,
    cursor: "pointer",
    "@media only screen and (max-width: 762px)": {
      ...styles["@media only screen and (max-width: 762px)"],
      fontSize: "12px",
    },
  }),
  control: (styles) => ({
    ...styles,
    cursor: "pointer",
    width: "100%",
    paddingLeft: 30,
    border: "none",
    color: "#279599",
    borderRadius: "0px",
    boxShadow: "none",
   
    "@media only screen and (max-width: 762px)": {
      ...styles["@media only screen and (max-width: 762px)"],
      paddingLeft: 0,
    },
    
  }),
  singleValue: (styles) => ({
    ...styles,
    cursor: "pointer",
    color: "#279599",
    "@media only screen and (max-width: 762px)": {
      ...styles["@media only screen and (max-width: 762px)"],
      fontSize: "12px",
    },
  }),

  option: () => {
    return {
      cursor: "pointer",
      listStyle: "none",
      padding: "12px 20px",
      borderBottom: "1px solid #f3f4f4",
      color: "#279599",
    };
  },
  indicatorsContainer: () => {
    return {
      color: "#279599",
    };
  },
  dropdownIndicator: () => {
    return {
      color: "#279599",
    };
  },
};

class HeaderStoreSelector extends Component {
  state = {
    selectedStore: null,
    selectedValue: {},
  };
  componentDidMount = () => {
    this.updateOptions();
    const { selectedStore, storeList } = this.props;
    storeList.forEach((element, index) => {
      if (element.siteId === selectedStore.siteId) {
        this.setState({
          selectedValue: {
            value: element.siteId,
            label: element.name,
            name: "selectedStore",
            index: index,
          },
        });
      }
    });
  };
  componentDidUpdate(prevProps) {
    if (this.props.storeList !== prevProps.storeList) {
      this.updateOptions();
    }
  }
  updateOptions = () => {
    var options = [];
    this.props.storeList.forEach((element, index) => {
      options.push({
        value: element.siteId,
        label: element.name,
        name: "selectedStore",
        index: index,
      });
    });
    this.setState({
      options: options,
    });
  };
  resetTheCart = () => {
    this.props.cartObject.cartItems.forEach((cart) => {
      this.props.emptyCartItem(this.props.cartObject.cartItems, cart);
    });
    this.props.resetMenuItemList(this.props.menuItemList);
  };
  handleStoreChange = (event) => {
    this.setState({
      [event.name]: this.props.storeList[event.index],
      selectedValue: {
        value: this.props.storeList[event.index].siteId,
        label: this.props.storeList[event.index].name,
        name: "selectedStore",
        index: event.index,
      },
    });
    if (event.name === "selectedStore") {
      this.setState({
        sellerOptions: this.props.storeList[event.index].sellerOptions,
      });
      this.props.updateSelectedStore({
        selectedStore: this.props.storeList[event.index],
      });
      this.props.setServiceList({
        channelList: this.props.storeList[event.index].sellerOptions,
      });
      toast.warning("Your previous cart will be removed");
      //need to empty the cart if any item in cart preset
      this.resetTheCart();
      // this.props.resetCartObject();
      // this.props.resetCartObject(); //called 2 times intenstionally as one time was not removing the data
      this.props.resetMenuItemList(this.props.menuItemList);
    }
  };

  render() {
    return (
      <>
        <span className="detailHeader">
          <img src={require("../../assets/images/Store.svg").default} />
        </span>
        <div className="header-custom-select">
          <Select
            isSearchable={false}
            value={this.state.selectedValue}
            options={this.state.options}
            styles={customStyles}
            onChange={this.handleStoreChange}
          />
        </div>
        {/* <select
          id="location-disp"
          name="selectedStore"
          onChange={this.handleStoreChange}
          value={this.state.selectedValue}
        >
          <option>Select Store</option>
          {storeList.map((option, index) => (
            <option key={index} value={index}>
              {option.name}
            </option>
          ))}
        </select> */}
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const storeList = reducerObj.storeList.storeList;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  return { storeList, selectedStore, menuItemList, cartObject };
};

export default connect(mapStateToProps, {
  updateSelectedStore,
  setServiceList,
  resetCartObject,
  resetMenuItemList,
  emptyCartItem,
})(HeaderStoreSelector);

HeaderStoreSelector.propTypes = {
  storeList: PropTypes.array,
  updateSelectedStore: PropTypes.func.isRequired,
  setServiceList: PropTypes.func.isRequired,
};

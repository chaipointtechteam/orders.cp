import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import React, { Component } from "react";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import styled from "styled-components";
import {
  resetFilter, updateFilterObject
} from "../../actions/FilterItemsAction";

const RangeSliderContainer = styled.div`
  .input-range__track--active,
  .input-range__slider {
    background: ${(props) => props.color};
    border-color: ${(props) => props.color};
  }
  .input-range__label--max .input-range__label-container {
    display: none;
  }
  .input-range__label--min .input-range__label-container {
    display: none;
  }
`;

class MobileFilter extends Component {


  state = { showPrice: false, isVeg: true, isNonVeg: false, filteredObject:{},applyFilteredObject:{}};
  componentDidMount = () => {
    const { filteredObject } = this.props;
    console.log('filteredObject', filteredObject)
    this.setState({
      filteredObject:filteredObject
    })
    this.setState({
      isVeg: filteredObject.isVeg !== undefined ? filteredObject.isVeg : this.state.isVeg,
      isNonVeg: filteredObject.isNonVeg
    })
    const { menuItemList } = this.props;
    if (menuItemList) {
      var min = menuItemList && menuItemList.length > 0 ? menuItemList.reduce(function (res, obj) {
        return obj.price < res.price ? obj : res;
      }) : null;

      var max = menuItemList && menuItemList.length > 0 ? menuItemList.reduce(function (res, obj) {
        return obj.price > res.price ? obj : res;
      }) : null;

      if (min === null && max === null) {

      } else {
        this.setState({
          value: { min: min.price?.toFixed(), max: max.price?.toFixed() },
          min: min.price,
          max: max.price.toFixed(0),
        });
      }

    }
    if (filteredObject?.minPrice !== undefined && filteredObject?.maxPrice !== undefined) {
      var value = { min: filteredObject.minPrice, max: filteredObject.maxPrice }
      this.updateRangeValue(value)
      // this.onVegNonVegSelection();
      this.setState({applyFilteredObject:{
        minPrice: min,
        maxPrice: max,
        isVeg: this.state.isVeg,
        isNonVeg: this.state.isNonVeg,
        searchQuery: "",
        activeKey: filteredObject.activeKey,
        selectedCategory: filteredObject.selectedCategory,
        selectedSubCategory: filteredObject.selectedSubCategory,
        isSubCategory: filteredObject.isSubCategory,
      }});
    }

  };

  resetAllFilter = (e) => {
    e.preventDefault();

    const { menuItemList } = this.props;
    var min =
        menuItemList && menuItemList.length > 0
          ? menuItemList.reduce(function (res, obj) {
              return obj.price < res.price ? obj : res;
            })
          : null;

      var max =
        menuItemList && menuItemList.length > 0
          ? menuItemList.reduce(function (res, obj) {
              return obj.price > res.price ? obj : res;
            })
          : null;

    this.props.updateFilterObject({
      isVeg: false,
      isNonVeg: false,
      minPrice: min.price,
      maxPrice: max.price.toFixed(0),
      activeKey: this.props.filteredObject.activeKey,
      selectedCategory: this.props.filteredObject.selectedCategory,
      selectedSubCategory: this.props.filteredObject.selectedSubCategory,
      isSubCategory: this.props.filteredObject.isSubCategory,
    });
    // this.props.resetFilter();
    this.setState({
      isVeg: false,
      isNonVeg: false,
      value: { min: this.state.min, max: this.state.max },
    });
    this.props.history.goBack();
  };


  // onVegNonVegSelection = () => {
  //   this.setState(
  //     {
  //       isVeg: !this.state.isVeg,
  //       isNonVeg: !this.state.isNonVeg,
  //     },
  //     () => {        
  //       const { filteredObject } = this.props;
  //       //update the filter value with veg and non veg
  //       this.setState({applyFilteredObject:{
  //         minPrice: this.state.min,
  //         maxPrice: this.state.max,
  //         isVeg: this.state.isVeg,
  //         isNonVeg: this.state.isNonVeg,
  //         searchQuery: "",
  //         activeKey: filteredObject.activeKey,
  //         selectedCategory: filteredObject.selectedCategory,
  //         selectedSubCategory: filteredObject.selectedSubCategory,
  //         isSubCategory: filteredObject.isSubCategory,
  //       }});        

  //     }
  //   );
  // };

  onVegNonVegSelection = (e) => {
    e.target.id==='veg'?this.setState(
      {
        isVeg: !this.state.isVeg,
        isNonVeg: this.state.isNonVeg,
      },
      () => {
        const { filteredObject } = this.props;
        //update the filter value with veg and non veg
        this.setState({applyFilteredObject:{
          minPrice: this.state.value.min,
          maxPrice: this.state.value.max,
          isVeg: this.state.isVeg,
          isNonVeg: this.state.isNonVeg,
          searchQuery: "",
          activeKey: filteredObject.activeKey,
          selectedCategory: filteredObject.selectedCategory,
          selectedSubCategory: filteredObject.selectedSubCategory,
          isSubCategory: filteredObject.isSubCategory,
        }});
      }
    ):this.setState(
      {
        isVeg: this.state.isVeg,
        isNonVeg: !this.state.isNonVeg,
      },
      () => {
        const { filteredObject } = this.props;
        //update the filter value with veg and non veg
        this.setState({applyFilteredObject:{
          minPrice: this.state.value.min,
          maxPrice: this.state.value.max,
          isVeg: this.state.isVeg,
          isNonVeg: this.state.isNonVeg,
          searchQuery: "",
          activeKey: filteredObject.activeKey,
          selectedCategory: filteredObject.selectedCategory,
          selectedSubCategory: filteredObject.selectedSubCategory,
          isSubCategory: filteredObject.isSubCategory,
        }});
      }
    )
  };

  updateRangeValue = (value) => {
    this.setState({ value }, () => {
      const { filteredObject } = this.props;
      //update filter object with min and max value
      this.setState({applyFilteredObject:{
        isVeg: this.state.isVeg,
        isNonVeg: this.state.isNonVeg,
        searchQuery: "",
        activeKey: filteredObject.activeKey,
        selectedCategory: filteredObject.selectedCategory,
        selectedSubCategory: filteredObject.selectedSubCategory,
        isSubCategory: filteredObject.isSubCategory,
        minPrice: value.min,
        maxPrice: value.max,
      }});
    });
    // this.setState({min:value.min})
  };

  backFilter=()=>{
    this.props.updateFilterObject(this.state.filteredObject)
  }

  applyFilter=()=>{
    this.props.updateFilterObject(this.state.applyFilteredObject)
    this.props.history.goBack();
  }

  render() {
    return (
      <section class="mobile-sort">
        <div class="row">
          <div class="sort-mobile-header">
            <h4>
              <Link to={"products"}>
                
                <FontAwesomeIcon
                  icon={faArrowLeft}
                  size="sm"
                  onClick={()=>this.backFilter()}
                  className="FilterBack"
                />
              </Link>
              Filter
            </h4>
          </div>
          <div class="sort-by-mobile">
            <div class="tab">
              <button
               style={{fontSize:16}}
                class="tablinks"
                onClick={() => {
                  this.setState({ showPrice: false });
                }}
                id="defaultOpen"
              >
              Veg\Non-veg
              </button>
              <button
                class="tablinks"
                style={{fontSize:16}}
                onClick={() => {
                  this.setState({ showPrice: true });
                }}
              >
                Price
              </button>
            </div>

            {this.state.showPrice ? (
              <div id="Paris" class="tabcontent">
                <div class="price-range">
                  {/* <h4 style={{fontSize:15}}>Price </h4> */}
                  <div class="food-type">
                    <RangeSliderContainer color="#349b9f">
                      <InputRange
                        step={2}
                        draggableTrack={false}
                        formatLabel={(value) => value}
                        maxValue={this.state.max}
                        minValue={this.state.min}
                        value={this.state.value}
                        onChange={(value) => this.updateRangeValue(value)}
                      />
                    </RangeSliderContainer>
                  </div>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      marginLeft: "10px",
                      fontWeight: 500,
                      
                    }}
                  >
                    <div>{`₹${this.state.min?.toFixed()}`}</div>
                    <div>{`₹${this.state.max}`}</div>
                  </div>
                </div>
              </div>
            ) : (
              <div id="London" class="tabcontent">
                {/* <h3  style={{fontSize:15}} >VEG\NONVEG</h3> */}
                <div class="radio-sizes">
                  <div class="for-sizes">
                    <label class="radio-inline" style={{fontSize:16}}>
                      <input 
                        type="checkbox" 
                        name="optradio"
                        id="veg" 
                        defaultChecked={this.state.isVeg}
                        checked={this.state.isVeg}
                        onClick={this.onVegNonVegSelection} 
                      />
                      <span>Veg</span>
                    </label>
                    <label class="radio-inline" style={{fontSize:16}}>
                      <input 
                        type="checkbox" 
                        name="optradio" 
                        id="nonveg"
                        defaultChecked={this.state.isNonVeg}
                        checked={this.state.isNonVeg}
                        onClick={this.onVegNonVegSelection} 
                      />
                      <span >Non Veg</span>
                    </label>
                  </div>
                </div>
              </div>
            )}
          </div>
          <button class="applyFilter" onClick={() => this.applyFilter()}>
            Apply Filter
          </button>
          <button class="resetFilter" onClick={this.resetAllFilter}>
            Reset Filter
          </button>
        </div>

      </section>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const filteredObject = reducerObj.filteredObject.filteredObject;
  return { menuItemList, filteredObject };
};

export default connect(mapStateToProps, { updateFilterObject, resetFilter })(
  withRouter(MobileFilter)
);

MobileFilter.propTypes = {
  updateFilterObject: PropTypes.func.isRequired,
};

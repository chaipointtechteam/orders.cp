import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  setFilteredItemList,
  updateFilterObject,
} from "../../actions/FilterItemsAction";
import { setCategoryMenuList } from "../../actions/MenuItemsAction";
import {
  filterByValue,
  filterFromModal,
  filterTheMenu,
  filterTheMenuWithCategory,
  filterTheMenuWithSubCategory,
} from "../../utils/common";
import ProductModal from "../product/ProductModal";
import {CatlogProduct} from "./catlogProductItem";

class CatlogProducts extends Component {
  
  scrollTimer;

  state = {
    isShow: false,
    index: "",
    myItemList: [],
    filteredItemList: [],
    showProductModal: false,
    isVeg: "",
    isNonVeg: "",
    minPrice: "",
    maxPrice: "",
  };
  
  setItems = () => {
    const { menuItemList, filteredObject, categoryList } = this.props;
    
    const {
      selectedCategory,
      selectedSubCategory,
      searchQuery,
      minPrice,
      maxPrice,
      isVeg,
      isNonVeg,
    } = filteredObject;
    // console.log('Filtered Object', filteredObject);
    this.setState({
      isVeg: isVeg,
      isNonVeg: isNonVeg,
      minPrice: minPrice,
      maxPrice: maxPrice,
    });

    
    let myItemList = filterTheMenu(this.props.menuItemList, categoryList);
    let filteredItemList = myItemList;    
    
    // console.log('Search Query ', searchQuery, searchQuery.length)

    if (searchQuery && searchQuery !== "") {
      filteredItemList = filterByValue(menuItemList, searchQuery);
      myItemList = filterTheMenu(filteredItemList, categoryList);
    }

    // console.log('filteredItemList after search query', filteredItemList)
    if (minPrice && maxPrice) {
      // debugger
      filteredItemList = filterFromModal(myItemList, {
        minPrice,
        selectedCategory,
        maxPrice,
        isVeg,
        isNonVeg,
      });
      this.props.setFilteredItemList({
        filteredItemList: filteredItemList,
      });
      // console.log(filteredItemList)
    }

    // console.log('filteredItemList after max min price', filteredItemList)
    if (isVeg && isNonVeg) {
      filteredItemList = filterFromModal(myItemList, {
        selectedCategory,
        maxPrice,
        minPrice,
        isVeg,
        isNonVeg,
      });
    }
    // console.log('filteredItemList after veg non veg', filteredItemList)

    this.setState({
      filteredItemList: filteredItemList,
      // myItemList: myItemList
    });

    // console.log('filteredItemList after set State', filteredItemList)

    this.props.setFilteredItemList({
      filteredItemList: filteredItemList,
    });
  };
    
  navHighlighter = () => {
    const { menuItemList} = this.props;
    
    const sections = document.querySelectorAll("h1[id]");
    // Get current scroll position
    let scrollY = window.pageYOffset;

    // Now we loop through sections to get height, top and ID values for each
    sections.forEach((current) => {
      const sectionHeight = current.offsetHeight;
      const sectionTop = current.offsetTop - 100;

      var sectionId = current.getAttribute("class");
      var activeKey = parseInt(sectionId);
      var position = window.innerWidth < 760 ? scrollY - 350 : scrollY - 650;
      // console.log(scrollY,"adngsad",sectionTop,"asdf;aks",sectionHeight)
      if (position > sectionTop) {
        var filtercate = [];
       
        var filterindx = [];
        this.props.categoryList.map((category, index) => {
          var filter = filterTheMenuWithCategory(menuItemList, category);

          if (filter && filter.length > 0) {
            filtercate.push(category);
            filterindx.push(index);
          }
        });
       
        window.clearTimeout(this.isScrolling );

        this.isScrolling = setTimeout(()=> {
            this.props.updateFilterObject({
            activeKey: filterindx[activeKey],
            selectedCategory: filtercate[activeKey],
            selectedSubCategory: null,
            isSubCategory: false,
            // isVeg: this.state.isVeg,
            // isNonVeg: this.state.isNonVeg,
            // minPrice: this.state.minPrice,
            // maxPrice: this.state.maxPrice,
          });
        }, 66);
        
      } else {
      }
    });
  };
 
  topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  } 
  componentDidMount = () => {    
    this.setItems();
    window.addEventListener("scroll", this.navHighlighter);    
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.filteredObject !== prevProps.filteredObject ||
      this.props.menuItemList !== prevProps.menuItemList ||
      this.props.cartObject !== prevProps.cartObject
    ) {
      // console.log('in component update')
      this.setItems();
    }
  }
  askVariant = (item) => {
    this.setState({
      showProductModal: true,
      selectedItem: item,
    });
  };
  closeTheModal = () => {
    this.setState({
      showProductModal: false,
    });
  };

  render() {
    const { filteredItemList, myItemList } = this.state;
     
    
    var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
    // console.log("filtered list", filteredItemList);

    return (
      <div class="col-md-6 col-sm-12 col-xs-12 catalogProducts sticky">
        <div class="tab-content ">
          <div class="tab-pane active" id="recommnd-tab">
            <ProductModal
              showModal={this.state.showProductModal}
              closeModal={this.closeTheModal}
              selectedProduct={this.state.selectedItem}
            />
            {filteredItemList && filteredItemList.length == 0 && (
              <p class="no-product">
                We are sorry, there are no products in this category now
              </p>
            )}
            {/* {minPrice ||
              maxPrice ||
              isVeg ||
              isNonVeg} */}
            {filteredItemList &&
              filteredItemList.length > 0 &&
              filteredItemList.map((item, index) => {
                if (item.filterMenu !== undefined) {
                  return (
                    <>
                      {item.item.subCategories?.length > 0 &&
                      item.filterMenu.length > 0 ? (
                        <>
                          <h1
                            className={index}
                            id={`${item.item.name}`}
                            style={{ marginTop: index === 0 ? "" : "0px" }}
                          >
                            {" "}
                            {""}
                          </h1>
                          <div
                            style={{
                              marginTop:
                                window.innerWidth < 760 && index !== 0
                                  ? "63px"
                                  : "-11px",
                            }}
                          >
                            {item.item.subCategories.map((sub, index) => {
                              var filtermenuList = filterTheMenuWithSubCategory(
                                this.props.menuItemList,
                                sub,
                                item.item
                              );
                              if (filtermenuList) {
                                var m = item.filterMenu;

                                return (
                                  <>
                                    <h4
                                      id={`${sub.name}${index}`}
                                      class={`${index}`}
                                      //  style={{padding:"10px"}}
                                      // style={{marginTop:"-113px"}}
                                    >
                                      {" "}
                                    </h4>
                                    <div>
                                      {filtermenuList.map((item, index) => {
                                        var k = m.find(
                                          (o) => o.productId == item.productId
                                        );
                                                                        
                                        if (k !== undefined) {
                                          return (
                                            <CatlogProduct
                                              clickImage={() =>
                                                this.askVariant(item)
                                              }
                                              key={item.id}
                                              product={item}
                                              index={index}
                                            />
                                          );
                                        }
                                      })}
                                    </div>
                                  </>
                                );
                              }
                            })}
                          </div>
                        </>
                      ) : (
                        <>
                          <h1
                            className={index}
                            id={`${item.item.name}`}
                            style={{ marginTop: "0px" }}
                          >
                            {""}
                          </h1>
                          <div
                            style={{
                              marginTop:
                                window.innerWidth < 760 && index !== 0
                                  ? "63px"
                                  : "33px",
                            }}
                          >
                            {item.filterMenu.map((item, index) => {
                              return (
                                <CatlogProduct
                                  clickImage={() => this.askVariant(item)}
                                  key={item.id}
                                  product={item}
                                  index={index}
                                />
                              );
                            })}
                          </div>
                        </>
                      )}
                    </>
                  );
                } else {
                  return (
                    <CatlogProduct
                      clickImage={() => this.askVariant(item)}
                      key={item.id}
                      product={item}
                      index={index}
                    />
                  );
                }
              })}
            <div
              name="123"
              className="stopScroll"
              style={{ paddingTop: "220px" }}
            ></div>
          </div>
        </div>

        <button onClick={this.topFunction} id="myBtn" title="Go to top"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const isLoggedIn = reducerObj.userObject.isLoggedIn;
  const userObject = reducerObj.userObject.userObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const filteredObject = reducerObj.filteredObject.filteredObject;
  const filteredItemList = reducerObj.filteredItemList.filteredItemList;
  const cartObject = reducerObj.cartObject.cartObject;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  return {
    menuItemList,
    filteredObject,
    cartObject,
    categoryList,
    filteredItemList,
  };
};

export default connect(mapStateToProps, {
  setFilteredItemList,
  updateFilterObject,
  setCategoryMenuList,
})(CatlogProducts);

CatlogProducts.propTypes = {
  menuItemList: PropTypes.array,
  filteredObject: PropTypes.object,
};

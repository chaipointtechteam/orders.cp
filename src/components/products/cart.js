import React, { Component } from "react";
import EmptyCart from "./emptyCart";
import MyCart from "./myCart";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

class Cart extends Component {
  render() {
    const { cartObject } = this.props;
   
    return (
      <div class="col-md-3 col-sm-12 col-xs-12 cart-d">
        {cartObject &&
          cartObject.cartItems &&
          cartObject.cartItems.length === 0 && <EmptyCart show={this.props.show}/>}
        {cartObject &&
          cartObject.cartItems &&
          cartObject.cartItems.length > 0 && <MyCart/>}
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  return { cartObject };
};

export default connect(mapStateToProps, {})(Cart);

Cart.propTypes = {
  cartObject: PropTypes.object.isRequired,
};

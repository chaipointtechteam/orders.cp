import React, { Component } from "react";
import Cart from "./cart";
import CatlogProducts from "./catlogProducts";
import MenuCategory from "./menuCategory";
export default class Catlog extends Component {
  render() {
   
    return (
      <section class="Category-tabs">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div className="sticky menusticky" >
                {" "}
                <MenuCategory />
              </div>

              <CatlogProducts />
              <div className="cartAction sticky">
                <Cart show="true" />
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import { connect } from "react-redux";
import { updateFilterObject } from "../../actions/FilterItemsAction";
import styles from "../../components/common/address/styles/address-modal.module.scss";
import {
  filterTheMenuWithCategory
} from "../../utils/common";
class CategoryModel extends Component {
  state = {
    selectedValue: null,
    options: null,
    showPopup: false,
    theposition: window.pageYOffset,
    sticky: null,
    isShow: false,
    catag:[]
  };
  componentDidMount = () => {
    

    let options = [];
    this.props.categoryList.map((categoryItem, index) => {
    
      var filtermenu = filterTheMenuWithCategory(this.props.menuItemList,categoryItem);
      if (filtermenu?.length > 0) {
     
        options.push({
          key: index,
          value: categoryItem.name,
          label: categoryItem.name,
          categoryItem,
        });
      }
    });
    this.setState({ catag: options });
  }
  componentDidUpdate = (prevProps) => {
    if (prevProps.categoryList !== this.props.categoryList) {
      let options = [];
      this.props.categoryList.map((categoryItem, index) => {
        var filtermenu = filterTheMenuWithCategory(this.props.menuItemList,categoryItem);
        if (filtermenu?.length > 0) {
          options.push({
            key: index,
            value: categoryItem.name,
            label: categoryItem.name,
            categoryItem,
          });
        }
      });
      this.setState({ catag: options });
    }
  }
  handleChangeSelect = ( key, selectedCategory, isSubCat, selectedSubCategory) => {

    this.props.updateFilterObject({
      activeKey: key,
      selectedCategory: selectedCategory,
      selectedSubCategory: selectedSubCategory,
      isSubCategory: isSubCat,
    });
    this.props.onClose();
  };

  render() {
    const { show, onClose, filteredObject ,categoryList} = this.props;

    return (
      <div id="style-2">
      <Modal
        className={styles["modal-category"]}
        show={show}
        onHide={onClose}
        centered
        contentClassName={styles["modal-content"]}
      >
        {/* <Modal.Header closeButton>
          <Modal.Title>Select Category</Modal.Title>
        </Modal.Header> */}
        <Modal.Body>
          
            {/* <MenuCategory close={onClose}/> */}

            {this.state.catag.map((item, index) => {
               
              return (<>
                {item.categoryItem.subCategories.length ==0?(<div
                    style={{ padding: "8px", fontSize: "13px" }}
                    onClick={() => this.handleChangeSelect(index,item.categoryItem,null,null)}
                  >
                    <a href={`#${item.categoryItem.name}` } style={{color:"black"}}> <label for={index} >{item.categoryItem.name}</label></a>
                    {filteredObject.activeKey === item.key ? (<>
                     <label style={{ float: "right", color: "#12767f" }}>
                        <FontAwesomeIcon
                          icon={faCheckCircle}
                          style={{ color: "#12767f" }}
                          size="lg"
                          className="filter-icon"
                        />
                      </label></>
                    ) : (
                      ""
                    )}
                  </div>):null}
              {item.categoryItem.subCategories.map((subItem,index) => {
                return (
                  <div
                    style={{ padding: "8px", fontSize: "13px" }}
                    onClick={() => this.handleChangeSelect(index,item.categoryItem,true,subItem)}
                  >
                   <a href={`#${subItem.name}${index}`} style={{color:"black"}}>  <label for={index}>{subItem.name}</label></a>
                    {filteredObject.selectedSubCategory?.categoryCode === subItem.categoryCode ? (
                      <label style={{ float: "right", color: "#12767f" }}>
                        <FontAwesomeIcon
                          icon={faCheckCircle}
                          style={{ color: "#12767f" }}
                          size="lg"
                          className="filter-icon"
                        />
                      </label>
                    ) : (
                      ""
                    )}
                  </div>
                );
              })}
              </>)
            })}
            
          
        </Modal.Body>
        </Modal>
        </div>                                            
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const selectedChannel =reducerObj.selectedChannel.selectedChannel
  const filteredObject = reducerObj.filteredObject.filteredObject;
  const categoryList = reducerObj.categoryList.categoryList.categoryList
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList
  return { filteredObject ,selectedChannel,categoryList,menuItemList};
};

export default connect(mapStateToProps, {
  updateFilterObject,
  
})(CategoryModel);

import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { emptyCartItem, resetCartObject } from "../../actions/CartAction";
import { setServiceList } from "../../actions/ChannelListAction";
import { resetMenuItemList } from "../../actions/MenuItemsAction";
import { updateSelectedStore } from "../../actions/StoreListAction";
import Home from "../../assets/images/Shop_Mobile Assets-03.svg";
import HeaderChannelSelector from "../products/headerChannelSelector";
import HeaderStoreSelector from "./headerStoreSelector";

class MobileHeader extends Component {


  render() {
    return (


      <div className="showFooter">
        <section className="category-nav" style={{ zIndex: "999999" }}>
          <div className="container" style={{ margin: "0px", padding: "0px" }}>

            <div className="navbar mobileHeader">
              <div className="mobile-delivery-selction">
                <div>
                  <Link to="/">
                    <img height={30} width={25} src={Home} style={{ marginTop: "9px" }} />
                  </Link>
                </div>


                <div className="mobile-store-selector" style={{ marginTop: "5px" }} >
                  <HeaderStoreSelector />
                </div>
              </div>


              <div className="mobile-channel-selector">
                <HeaderChannelSelector />
              </div>
            </div>

          </div>
        </section>
      </div>


    );
  }
}


const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const storeList = reducerObj.storeList.storeList;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  return { storeList, selectedStore, menuItemList, cartObject };
};

export default connect(mapStateToProps, {
  updateSelectedStore,
  setServiceList,
  resetCartObject,
  resetMenuItemList,
  emptyCartItem
})(MobileHeader);

MobileHeader.propTypes = {
  storeList: PropTypes.array,
  updateSelectedStore: PropTypes.func.isRequired,
  setServiceList: PropTypes.func.isRequired,
};



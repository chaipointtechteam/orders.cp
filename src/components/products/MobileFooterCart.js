import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import image from "../../assets/images/viewcart.svg";
import { withRouter } from "react-router-dom";

class MobileFooterCart extends Component {
  redirectToCart = () => {

    this.props.history.push("/cart");
  };
  render() {
    const { cartObject } = this.props;
    return (
      <>
        {cartObject && cartObject.cartItems && cartObject.cartItems.length > 0 && (
          <div className="footer-cart">
            <ul className="viewcart-footer">
              <li>{cartObject.cartItems.length} Items</li>
              <li>₹{cartObject.subTotal}</li>
            </ul>
            <a onClick={this.redirectToCart}>
              View Cart <img style={{ marginTop: "-5px" }} src={image} />
            </a>
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  return { cartObject };
};

export default connect(mapStateToProps, {})(withRouter(MobileFooterCart));

MobileFooterCart.propTypes = {
  cartObject: PropTypes.object.isRequired,
};

import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import styled from "styled-components";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import {
  updateFilterObject,
  resetFilter,
  setFilteredItemList,
} from "../../actions/FilterItemsAction";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { filterTheMenu } from "../../utils/common";
const RangeSliderContainer = styled.div`
  .input-range__track--active,
  .input-range__slider {
    background: ${(props) => props.color};
    border-color: ${(props) => props.color};
  }
  .input-range__label--max .input-range__label-container {
    display: none;
  }
  .input-range__label--min .input-range__label-container {
    display: none;
  }
`;

class FilterModal extends Component {
  state = { isVeg: true, isNonVeg: false };
  componentDidMount = () => {
    // debugger
    const { filteredObject } = this.props;
    this.setState({
      isVeg:
        filteredObject.isVeg !== undefined
          ? filteredObject.isVeg
          : this.state.isVeg,
      isNonVeg: filteredObject.isNonVeg,
    });

    const { menuItemList } = this.props;
    if (menuItemList) {      
      var min =
        menuItemList && menuItemList.length > 0
          ? menuItemList.reduce(function (res, obj) {
              return obj.price < res.price ? obj : res;
            })
          : null;

      var max =
        menuItemList && menuItemList.length > 0
          ? menuItemList.reduce(function (res, obj) {
              return obj.price > res.price ? obj : res;
            })
          : null;

      if (min === null && max === null) {
      } else {
        this.setState({
          value: { min: min.price?.toFixed(), max: max.price?.toFixed() },
          min: min.price,
          max: max.price.toFixed(0),
        });
      }
    }
    if (
      filteredObject?.minPrice !== undefined &&
      filteredObject?.maxPrice !== undefined
    ) {
      var value = {
        min: filteredObject.minPrice,
        max: filteredObject.maxPrice,
      };
      this.updateRangeValue(value);
    }
  };

  resetAllFilter = (e) => {
    e.preventDefault();

    const { menuItemList } = this.props;
    var min =
        menuItemList && menuItemList.length > 0
          ? menuItemList.reduce(function (res, obj) {
              return obj.price < res.price ? obj : res;
            })
          : null;

      var max =
        menuItemList && menuItemList.length > 0
          ? menuItemList.reduce(function (res, obj) {
              return obj.price > res.price ? obj : res;
            })
          : null;

    this.props.updateFilterObject({
      isVeg: false,
      isNonVeg: false,
      minPrice: min.price,
      maxPrice: max.price.toFixed(0),
      activeKey: this.props.filteredObject.activeKey,
      selectedCategory: this.props.filteredObject.selectedCategory,
      selectedSubCategory: this.props.filteredObject.selectedSubCategory,
      isSubCategory: this.props.filteredObject.isSubCategory,
    });

    let filteredItemList = filterTheMenu(
      this.props.menuItemList,
      this.props.categoryList
    );
    this.props.setFilteredItemList({
      filteredItemList: filteredItemList,
    });

    // this.props.resetFilter();
    this.setState({
      isVeg: false,
      isNonVeg: false,
      value: { min: this.state.min, max: this.state.max },
    });
    this.props.closePopUp()
  };

  onVegNonVegSelection = (e) => {
    e.target.id==='veg'?this.setState(
      {
        isVeg: !this.state.isVeg,
        isNonVeg: this.state.isNonVeg,
      },
      () => {
        const { filteredObject } = this.props;
        //update the filter value with veg and non veg
        this.props.updateFilterObject({
          minPrice: this.state.value.min,
          maxPrice: this.state.value.max,
          isVeg: this.state.isVeg,
          isNonVeg: this.state.isNonVeg,
          searchQuery: "",
          activeKey: filteredObject.activeKey,
          selectedCategory: filteredObject.selectedCategory,
          selectedSubCategory: filteredObject.selectedSubCategory,
          isSubCategory: filteredObject.isSubCategory,
        });
      }
    ):this.setState(
      {
        isVeg: this.state.isVeg,
        isNonVeg: !this.state.isNonVeg,
      },
      () => {
        const { filteredObject } = this.props;
        //update the filter value with veg and non veg
        this.props.updateFilterObject({
          minPrice: this.state.value.min,
          maxPrice: this.state.value.max,
          isVeg: this.state.isVeg,
          isNonVeg: this.state.isNonVeg,
          searchQuery: "",
          activeKey: filteredObject.activeKey,
          selectedCategory: filteredObject.selectedCategory,
          selectedSubCategory: filteredObject.selectedSubCategory,
          isSubCategory: filteredObject.isSubCategory,
        });
      }
    )

    // this.setState(
    //   {
    //     isVeg: !this.state.isVeg,
    //     isNonVeg: !this.state.isNonVeg,
    //   },
    //   () => {
    //     const { filteredObject } = this.props;
    //     //update the filter value with veg and non veg
    //     this.props.updateFilterObject({
    //       minPrice: this.state.value.min,
    //       maxPrice: this.state.value.max,
    //       isVeg: this.state.isVeg,
    //       isNonVeg: this.state.isNonVeg,
    //       searchQuery: "",
    //       activeKey: filteredObject.activeKey,
    //       selectedCategory: filteredObject.selectedCategory,
    //       selectedSubCategory: filteredObject.selectedSubCategory,
    //       isSubCategory: filteredObject.isSubCategory,
    //     });
    //   }
    // );
  };
  updateRangeValue = (value) => {
    this.setState({ value }, () => {
      const { filteredObject } = this.props;
      //update filter object with min and max value
      this.props.updateFilterObject({
        isVeg: this.state.isVeg,
        isNonVeg: this.state.isNonVeg,
        searchQuery: "",
        activeKey: filteredObject.activeKey,
        selectedCategory: filteredObject.selectedCategory,
        selectedSubCategory: filteredObject.selectedSubCategory,
        isSubCategory: filteredObject.isSubCategory,
        minPrice: value.min,
        maxPrice: value.max,
      });
    });
    // this.setState({min:value.min})
  };
  render() {
    return (
      <Modal
        className="modal-opacity signup productFilter "
        show={this.props.showModal}
        onHide={this.props.closePopUp}
      >
        <div onClick={this.props.closePopUp}>
          {" "}
          <label
            style={{ float: "right", marginRight: "10px", marginTop: "10px" }}
          >
            <FontAwesomeIcon
              icon={faTimes}
              size="2x"
              className="star-icon-plus"
            />
          </label>
        </div>
        <div class="filter-options ">
          <form>
            <div class="food-type">
              <div class="checkbox top">
                <label>
                  <input
                    class="filter-checkbox"
                    type="checkbox"
                    id="veg"
                    defaultChecked={this.state.isVeg}
                    checked={this.state.isVeg}
                    onClick={this.onVegNonVegSelection}
                  />
                  <img src={require("../../assets/images/Veg.svg").default} />
                  Veg
                </label>
              </div>
              <div class="checkbox top">
                <label>
                  <input
                    type="checkbox"
                    id="nonveg"
                    class="filter-checkbox"
                    defaultChecked={this.state.isNonVeg}
                    checked={this.state.isNonVeg}
                    onClick={this.onVegNonVegSelection}
                  />
                  <span>
                    <img
                      src={require("../../assets/images/Non Veg.svg").default}
                    />
                  </span>
                  Non veg
                </label>
              </div>
            </div>
            <div class="price-range">
              <h4>price range</h4>
              <div class="food-type">
                <RangeSliderContainer color="#349b9f">
                  <InputRange
                    step={2}
                    draggableTrack={false}
                    formatLabel={(value) => value}
                    maxValue={this.state.max}
                    minValue={this.state.min}
                    value={this.state.value}
                    onChange={(value) => this.updateRangeValue(value)}
                  />
                </RangeSliderContainer>
              </div>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  // marginTop: "1rem",
                  fontWeight: 500,
                  fontSize: "18px",
                  // color: "#8f23b3"
                }}
              >
                <div>{`₹${this.state.min?.toFixed()}`}</div>
                <div>{`₹${this.state.max}`}</div>
              </div>
            </div>

            <div class="reset-btn-div">
              <button class="Clear-All " onClick={this.resetAllFilter}>
                Clear All
              </button>
            </div>
          </form>
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const filteredObject = reducerObj.filteredObject.filteredObject;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  return { menuItemList, filteredObject, categoryList };
};

export default connect(mapStateToProps, {
  updateFilterObject,
  resetFilter,
  setFilteredItemList,
})(FilterModal);

FilterModal.propTypes = {
  updateFilterObject: PropTypes.func.isRequired,
};

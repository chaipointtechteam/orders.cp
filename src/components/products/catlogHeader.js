import { faSlidersH } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import $ from "jquery";
import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  setFilteredItemList,
  updateFilterObject,
} from "../../actions/FilterItemsAction";
import {
  filterTheMenu,
  filterTheMenuWithCategory,
  filterTheMenuWithSubCategory,
} from "../../utils/common";
import CategoryModel from "./CategoryModel";
import FilterModal from "./filterModal";

import _ from "lodash";

const customStyles = {
  menu: (styles) => ({ ...styles, zIndex: 9999, width: "50%" }),

  control: (styles) => ({
    ...styles,
    cursor: "pointer",
    width: "50%",
    fontSize: "16px",
    border: "none",
    color: "#279599",
    border: "1px solid #b4b6b8",
    borderRadius: "5px",
    boxShadow: "none",
    "&:hover": {
      // borderBottom: "1px solid transperent",
    },
  }),
  singleValue: (styles) => ({
    ...styles,
    cursor: "pointer",
    color: "#279599",
  }),
  option: () => {
    return {
      listStyle: "none",
      cursor: "pointer",
      padding: "12px 20px",
      borderBottom: "1px solid #f3f4f4",
      color: "#279599",
    };
  },
  indicatorsContainer: () => {
    return {
      color: "#279599",
    };
  },
  dropdownIndicator: () => {
    return {
      color: "#279599",
    };
  },
};
const styles = {
  width: "20rem",
  bottom: "-40px",
  borderRadius: "40px",
  background: "#dc492f",
  border: "1px solid #dc492f",
  fontSize: "16px",
};
const styles2 = {
  bottom: "-80px",
  width: "20rem",
  borderRadius: "40px",
  background: "#DC492F",
  border: "1px solid #dc492f",
  fontSize: "16px",
};
class CatlogHeader extends Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }
  state = {
    selectedValue: null,
    options: null,
    showPopup: false,
    theposition: window.pageYOffset,
    sticky: null,
    isShow: false,
    isSticky: null,
    filteredLength: [],
  };

  flipAddAddressState = () => {
    this.setState((state, props) => ({
      isShow: !state.isShow,
    }));
  };

  myFunction = () => {
    var header = document.getElementById("myHeader");
    if (window.pageYOffset > header.offsetTop) {
      this.setState({ stick: true });
    } else {
      this.setState({ stick: false });
    }
  };

  componentDidMount = () => {
    const { filteredObject, categoryList, menuItemList } = this.props;
    if (
      (filteredObject.maxPrice &&
        filteredObject.minPrice &&
        filteredObject.isVeg) ||
      filteredObject.searchQuery
    ) {
      this.setState({
        filteredLength: this.props.filteredItemList,
      });
    } else {
      if (filteredObject.selectedSubCategory !== null) {
        let filteredLength = filterTheMenuWithSubCategory(
          menuItemList,
          filteredObject.selectedSubCategory,
          filteredObject.selectedCategory
        );
        this.setState({
          filteredLength: filteredLength,
        });
      } else {
        let filteredLength = filterTheMenuWithCategory(
          menuItemList,
          filteredObject.selectedCategory
        );
        this.setState({
          filteredLength: filteredLength,
        });
      }
    }
    let options = [];
    categoryList.map((categoryItem, index) => {
      var filtermenu = filterTheMenuWithCategory(menuItemList, categoryItem);
      if (filtermenu?.length > 0) {
        options.push({
          key: index,
          value: categoryItem.name,
          label: categoryItem.name,
          categoryItem,
        });
      }
    });
    this.setState({ options: options });
  };

  openFilterModal = () => {
    this.setState({ showPopup: true });
  };

  closePopUp = () => {
    this.setState({
      showPopup: false,
    });
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.filteredObject !== prevProps.filteredObject ||
      this.props.filteredItemList !== prevProps.filteredItemList
    ) {
      const { filteredObject, categoryList, menuItemList } = this.props;
      // debugger
      if (
        (filteredObject.minPrice && filteredObject.maxPrice) ||
        filteredObject.searchQuery
      ) {
        this.setState({
          filteredLength: this.props.filteredItemList,
        });
      } else {
        if (filteredObject.selectedSubCategory != null) {
          let filteredLength = filterTheMenuWithSubCategory(
            menuItemList,
            filteredObject.selectedSubCategory,
            filteredObject.selectedCategory
          );
          this.setState({
            filteredLength: filteredLength,
          });
        } else {
          let filteredLength = filterTheMenuWithCategory(
            menuItemList,
            filteredObject.selectedCategory
          );
          this.setState({
            filteredLength: filteredLength,
          });
        }
      }
    }
    const { filteredObject, updateFilterObject, menuItemList } = this.props;
    var filtercate = [];
    var filterindx = [];
    this.props.categoryList.map((category, index) => {
      var filter = filterTheMenuWithCategory(menuItemList, category);

      if (filter && filter.length > 0) {
        filtercate.push({ ...category, index });
        filterindx.push(index);
      }
    });

    if (window.innerWidth <= 760) {
      $(".form-control").click(function () {
        $([document.documentElement, document.body]).animate(
          {
            scrollTop: $(".Category-header").offset()?.top,
          },
          10
        );
      });
    }
  }
  updateTheSearchQuery = (event) => {
    console.log('event.target.value', event.target.value)
    this.setState({
      searchQuery: event.target.value,
    });

    const { filteredObject } = this.props;
    if (event.target.value !== "") {
      this.props.updateFilterObject({
        searchQuery: event.target.value,
        activeKey: 0,
        selectedCategory: null,
        selectedSubCategory: null,
        isSubCategory: null,
      });
      let filteredItemList = filterTheMenu(
        this.props.menuItemList,
        this.props.categoryList
      );
      this.props.setFilteredItemList({
        filteredItemList: filteredItemList,
      });
    } else {      
      var filtercate = [];
      var filterindx = [];
      const { menuItemList } = this.props;
      this.props.categoryList.map((category, index) => {
        var filter = filterTheMenuWithCategory(menuItemList, category);

        if (filter && filter.length > 0) {
          filtercate.push(category);
          filterindx.push(index);
        }
      });

      this.props.updateFilterObject({
        searchQuery: event.target.value,
        activeKey: filterindx[0],
        selectedCategory: filtercate[0],
        selectedSubCategory: null,
        isSubCategory: null,
      });
    }
  };

  render() {
    const { filteredObject, filteredItemList, categoryList, cartObjects } =
      this.props;
    //console.log("Fitler List & Object" ,filteredItemList, filteredObject);
    let count = 0;

    _.forEach(filteredItemList, (each) => {
      if (filteredObject.selectedCategory && each.item){
          if(filteredObject.selectedCategory.categoryCode === each.item.categoryCode){
            count = count + _.size(each.filterMenu);
          }
      }
    });

    const { filteredLength } = this.state;
    return (
      <div className="">
        <section className="Category-header">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12 cato">
                <div className="col-md-3 col-sm-3 col-xs-12">
                  <div className="filter-header">
                    <h3>Categories</h3>
                  </div>
                </div>
                <div className="col-md-9 col-sm-9 col-xs-12 cato2">
                  <div className="category-filter">
                    {filteredObject.selectedCategory !== null && (
                      <>
                        {filteredObject.selectedSubCategory === null && (
                          <h3>
                            {filteredObject.selectedCategory?.name}
                            {" ("}
                            {/* {filteredObject.minPrice &&
                            filteredObject.maxprice &&
                            filteredObject.isVeg
                              ? filteredItemList && filteredItemList.length
                              : this.state.filteredLength &&
                                this.state.filteredLength.length} */}
                            {count}
                            {")"}
                          </h3>
                        )}
                        {filteredObject.selectedSubCategory !== null && (
                          <h3>
                            {filteredObject.selectedSubCategory.name}
                            {" ("}
                            {filteredObject.minPrice &&
                            filteredObject.maxprice &&
                            filteredObject.isVeg
                              ? filteredItemList && filteredItemList.length
                              : this.state.filteredLength &&
                                this.state.filteredLength.length}
                            {")"}
                          </h3>
                        )}
                      </>
                    )}
                    {filteredObject.selectedCategory === null && (
                      <h3>
                        All products {" ("}
                        {filteredItemList && filteredItemList.length}
                        {")"}{" "}
                        {filteredObject.isVeg && filteredObject.isVeg === true && (
                          <small>
                            <img
                              style={{ width: "20px" }}
                              src={
                                require("../../assets/images/Veg.svg").default
                              }
                            />
                            Veg
                          </small>
                        )}
                        {filteredObject.isNonVeg &&
                          filteredObject.isNonVeg === true && (
                            <small>
                              <img
                                style={{ width: "20px" }}
                                src={
                                  require("../../assets/images/Non Veg.svg")
                                    .default
                                }
                              />
                              Non-Veg
                            </small>
                          )}
                        {filteredObject.isNonVeg &&
                          filteredObject.isNonVeg === true &&
                          filteredObject.isVeg &&
                          filteredObject.isVeg === true && <small>All</small>}
                      </h3>
                    )}
                  </div>

                  <div className="category-search">
                    <div className="input-group" id="adv-search">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Search"
                        value={this.state.searchQuery}
                        onChange={this.updateTheSearchQuery}
                      />

                      <button type="button" className="btn btn-primary">
                        <span
                          className="glyphicon glyphicon-search"
                          aria-hidden="true"
                        ></span>
                      </button>
                    </div>
                  </div>
                  <div className="popular-category">
                    <button
                      id=""
                      className="service-filter detailHeader"
                      onClick={this.openFilterModal}
                    >
                      <div className="SubCategoryIcon">
                        <span className="Filter detailHeader">Filter</span>
                        <FontAwesomeIcon
                          icon={faSlidersH}
                          size="sm"
                          className="filter-icon"
                        />
                      </div>
                    </button>
                    <button
                      id=""
                      className="service-filter showFooter"
                      onClick={() => this.props.history.push("/filter")}
                    >
                      <div className="SubCategoryIcon">
                        <span className="Filter detailHeader">Filter</span>
                        <FontAwesomeIcon
                          icon={faSlidersH}
                          size="sm"
                          className="filter-icon"
                        />
                      </div>
                    </button>
                    <FilterModal
                      showModal={this.state.showPopup}
                      closePopUp={this.closePopUp}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          {this.state.isShow ? (
            <CategoryModel
              style={{ zIndex: "10000" }}
              show={this.state.isShow}
              categ={this.state.options}
              onClose={this.flipAddAddressState}
            />
          ) : (
            ""
          )}
          <div
            class="btn-group-fab "
            role="group"
            aria-label="FAB Menu"
            style={{ marginBottom: "75px", zIndex: "99" }}
          >
            <div>
              <button
                style={
                  this.props.cartObject &&
                  this.props.cartObject.cartItems &&
                  this.props.cartObject.cartItems.length > 0
                    ? styles
                    : styles2
                }
                type="button"
                onClick={this.flipAddAddressState}
                class="btn btn-main btn-primary has-tooltip "
                data-placement="center"
                title="Menu"
              >
                Categories{" "}
              </button>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;

  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const filteredObject = reducerObj.filteredObject.filteredObject;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  const filteredItemList =
    reducerObj.filteredItemList.filteredItemList.filteredItemList;
  return {
    filteredObject,
    filteredItemList,
    categoryList,
    menuItemList,
    cartObject,
  };
};

export default connect(mapStateToProps, {
  updateFilterObject,
  setFilteredItemList,
  filterTheMenuWithCategory,
})(withRouter(CatlogHeader));

CatlogHeader.propTypes = {
  filteredObject: PropTypes.object.isRequired,
  updateFilterObject: PropTypes.func.isRequired,
};

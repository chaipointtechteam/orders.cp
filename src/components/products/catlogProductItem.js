import { PropTypes } from "prop-types";
import React, { Component, useState, useEffect } from "react";
import { isMobileOnly } from "react-device-detect";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { updateCartItem, updateCartObject } from "../../actions/CartAction";
import { Modal } from "react-bootstrap";
import {
  updateCouponObject,
  updateSelectedProduct,
} from "../../actions/CouponAction";
import { updateMenuItem } from "../../actions/MenuItemsAction";
import {
  CheckAvaibilityOfProduct,
  checkOutofStock,
  containsObjectInCart,
  ModifiersQuantity,
  updateSubTotal,
  VariantQuantity
} from "../../utils/common";
import toast from "../common/commonToast";
import ProductModal from "../product/ProductModal";
import IncrementDecrementAction from "./incrementDecrementAction";
import IncrementDecrementActionModifier from "./incrementDecrementActionModifier";

import ReadMore from './readMore';

class CatlogProductItem extends Component {
  state = {
    item: null,
    showProductModal: false,
    showReadMore: false,
    modalData: {},
  };
  routingFunction = () => {
    this.props.updateSelectedProduct(this.props.product);
    this.props.history.push(`/productDetails`);
  };
  updateCartAndMenuList = (quantity) => {
    //update quantity value in menu list reducer
    this.props.updateMenuItem(
      this.props.menuItemList,
      this.props.product,
      quantity,
      this.props.inventory
    );
    //update quantity value in cart list reducer
    this.props.updateCartItem(
      this.props.cartItemList,
      this.props.product,
      quantity
    );
  };

  updateCartWithProduct = () => {
    if (
      (this.props.product.productModifierList &&
        this.props.product.productModifierList[0] !== undefined &&
        this.props.product.productModifierList[0].modifierProductList.length !==
          0) ||
      this.props.product.productVariantList.length !== 0
    ) {
      if (isMobileOnly) {
        this.routingFunction();
      } else {
        this.setState({
          showProductModal: true,
        });
      }
      //need to check if product is already added
      // var cartList = this.props.cartObject.cartItems;
      // if (!containsObjectInCart(this.props.product, cartList)) {
      //   cartList.push(this.props.product);
      //   this.props.product.quantity = this.props.product.quantity + 1;
      //   this.updateCartAndMenuList(this.props.product.quantity);
      //   //need to update the quantity in menu list as well
      // } else {

      //if product already there this button wont be visible
      // }

      // this.props.updateCartObject({
      //   cartItems: cartList,
      //   subTotal: updateSubTotal(cartList),
      // });
    } else if (this.props.product.productVariantList.length === 0) {
      var m = CheckAvaibilityOfProduct(
        this.props.inventory,
        this.props.product
      );

      var cartList = this.props.cartObject.cartItems;
      if (!containsObjectInCart(this.props.product, cartList)) {
        if (
          CheckAvaibilityOfProduct(this.props.inventory, this.props.product)
        ) {
          cartList.push(this.props.product);
          this.props.product.quantity = this.props.product.quantity + 1;
          this.updateCartAndMenuList(this.props.product.quantity);
        } else {
          toast.warning(
            `you can't add ${this.props.product.displayName},it's quantity is  now out of stock`
          );
        }
        //need to update the quantity in menu list as well
      } else {
        //if product already there this button wont be visible
      }

      this.props.updateCartObject({
        cartItems: cartList,
        subTotal: updateSubTotal(cartList),
      });
    }
  };

  closeTheModal = () => {
    this.setState({
      showProductModal: false,
    });
  };
  showModal = () => {
    this.setState({ showProductModal: true });
  };
  render() {
    const { product } = this.props;

    return (
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div
          class="Recommended-products"
          style={{ marginTop: !isMobileOnly ? "0px" : "10px" }}
        >
          {this.state.showProductModal ? (
            <ProductModal
              showModal={this.state.showProductModal}
              closeModal={this.closeTheModal}
              selectedProduct={product}
            />
          ) : (
            ""
          )}
          {product.images && product.images.length > 0 && (
            <img
              onClick={isMobileOnly ? () => null : this.showModal}
              onClick={this.props.clickImage}
              src={product.images[0].imageUrl}
            />
          )}
          {product.images.length === 0 && (
            <img src={require("../../assets/images/Offers-04.png").default} />
          )}

          <div class="veg-end">
            {product.isVegetarian && (
              <span>
                <img src={require("../../assets/images/Veg.svg").default} />
              </span>
            )}
            {!product.isVegetarian && (
              <span>
                <img src={require("../../assets/images/Non Veg.svg").default} />
              </span>
            )}
          </div>
          <div class="product-price">
            <div className="product-dis">
              <div>
                <h4>{product.displayName}</h4>
              </div>
              {/* CategoryCode - {product.category && product.category.categoryCode}
            <br />
            SubCategoryCode -{" "}
            {product.subCategory && product.subCategory.categoryCode} */}
              <div>
                <span class="price">
                  {/* <strike>₹135</strike> */} ₹{product.price}
                </span>
              </div>
            </div>
            <div>
              <ReadMore
                children={product}
                setReadMore={(visible, modalData) =>
                  this.setState({ showReadMore: visible, modalData })
                }
                itemList={this.props.menuItemList}
                // clickImage={this.props.clickImage}
                clickImage={
                  isMobileOnly
                    ? !checkOutofStock(this.props.inventory, product)
                      ? this.routingFunction
                      : this.routingFunction
                    : !checkOutofStock(this.props.inventory, product)
                    ? this.props.clickImage
                    : this.props.clickImage
                }

                // clickImage={isMobileOnly ? !checkOutofStock(this.props.inventory, product) ? this.routingFunction : this.routingFunction : !checkOutofStock(this.props.inventory, product) ? "" : this.props.clickImage}
              />
            </div>
            {product.quantity === 0 ? (
              <>
                {product.quantity === 0 &&
                product.productModifierList.length === 0 &&
                product.productVariantList.length === 0 ? (
                  <div>
                    {checkOutofStock(this.props.inventory, product) ? (
                      <div className="product-add-button">
                        <button
                          class="add-button"
                          onClick={this.updateCartWithProduct}
                        >
                          add +
                        </button>
                      </div>
                    ) : (
                      <div className="product-add-button">
                        <button
                          style={{
                            height: "38px",
                            backgroundColor: "#878b8b",
                            border: "1px solid #878b8b",
                            fontWeight: 100,
                            fontSize: "10px",
                            textTransform: "none",
                            padding: "7px 0px",
                          }}
                        >
                          Next available at 11am
                        </button>
                      </div>
                    )}
                  </div>
                ) : (
                  <div>
                    {(
                      product.productVariantList.length > 0
                        ? VariantQuantity(product, this.props.cartItemList) == 0
                        : ModifiersQuantity(
                            this.props.inventory,
                            product,
                            this.props.cartItemList
                          ) == 0
                    ) ? (
                      <div>
                        {checkOutofStock(this.props.inventory, product) ? (
                          <div className="product-add-button">
                            <button
                              class="add-button"
                              onClick={this.updateCartWithProduct}
                            >
                              add +{" "}
                              <span
                                style={{ fontSize: "7px", fontWeight: "500" }}
                              >
                                {" "}
                                customize
                              </span>
                            </button>
                          </div>
                        ) : (
                          <div className="product-add-button">
                            <button
                              style={{
                                height: "38px",
                                backgroundColor: "#878b8b",
                                border: "1px solid #878b8b",
                                fontWeight: 100,
                                fontSize: "10px",
                                textTransform: "none",
                                padding: "7px 0px",
                              }}
                            >
                              Next available at 11am
                            </button>
                          </div>
                        )}
                      </div>
                    ) : (
                      <>
                        {/* <h1> {ModifiersQuantity(this.props.inventory, product, this.props.cartItemList)}</h1> */}
                        <IncrementDecrementActionModifier product={product} />
                      </>
                    )}
                  </div>
                )}
              </>
            ) : (
              <div>
                {
                  product.productVariantList?.length > 0 &&
                  product.quantity > 0 ? (
                    <>
                      {VariantQuantity(product, this.props.cartItemList) > 0 ? (
                        <IncrementDecrementActionModifier product={product} />
                      ) : (
                        <div>
                          {checkOutofStock(this.props.inventory, product) ? (
                            <div className="product-add-button">
                              <button
                                class="add-button"
                                onClick={this.updateCartWithProduct}
                              >
                                add +{" "}
                                <span
                                  style={{ fontSize: "7px", fontWeight: "500" }}
                                >
                                  {" "}
                                  customize
                                </span>
                              </button>
                            </div>
                          ) : (
                            <div className="product-add-button">
                              <button
                                style={{
                                  height: "38px",
                                  backgroundColor: "#878b8b",
                                  border: "1px solid #878b8b",
                                  fontWeight: 100,
                                  fontSize: "10px",
                                  textTransform: "none",
                                  padding: "7px 0px",
                                }}
                              >
                                Next available at 11am
                              </button>
                            </div>
                          )}
                        </div>
                      )}
                    </>
                  ) : (
                    product.quantity > 0 &&
                    product.productVariantList.length == 0 && (
                      <IncrementDecrementAction
                        product={product}
                        openModal={() =>
                          this.setState({
                            showProductModal: true,
                          })
                        }
                      />
                    )
                  )

                  // <div className="product-add-button">
                  //   <button class="add-button" onClick={this.updateCartWithProduct}>
                  //     add + <span style={{ fontSize: "6px", fontWeight: "500" }}>{" "}customize</span>
                  //   </button>
                  // </div>
                }
              </div>
            )}
          </div>
          {this.state.showReadMore && (
            <Modal
              id="myModalview"
              className="modal-opacity product-modal"
              show={this.state.showReadMore}
              onHide={() => this.setState({ showReadMore: false })}
            >
              <div className="pro-details product-detail-modal">
                <div className="pro-img">
                  <img src={this.state.modalData.images[0].imageUrl} />
                </div>
                <div className="pro-details">
                  <h4>{this.state.modalData.displayName}</h4>
                  <span className="pro-price"></span>
                  <div className="product-detail-wrapper">
                    <small>{this.state.modalData.description}</small>
                  </div>
                </div>
              </div>
            </Modal>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const inventory = reducerObj.menuItemList.menuItemList.inventory;
  const cartItemList = reducerObj.cartObject.cartObject.cartItems;
  const CouponObject = reducerObj.CouponObject.CouponObject;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;

  return {
    cartObject,
    menuItemList,
    cartItemList,
    CouponObject,
    selectedChannel,
    selectedStore,
    inventory,
    categoryList
  };
};

export const CatlogProduct =  connect(mapStateToProps, {
  updateCartObject,
  updateCartItem,
  updateMenuItem,
  updateCouponObject,
  updateSelectedProduct,
})(withRouter(CatlogProductItem));

const ExpandMore = connect(mapStateToProps)(ReadMore);

CatlogProductItem.propTypes = {
  cartObject: PropTypes.object.isRequired,
  updateCartObject: PropTypes.func.isRequired,
};

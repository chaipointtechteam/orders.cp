import React, { Component, useState, useEffect } from "react";
import {connect} from 'react-redux';
import $ from "jquery";
import { isMobileOnly } from "react-device-detect";
import {setFilteredItemList,updateFilterObject} from '../../actions/FilterItemsAction'
import {setMenuItemList} from '../../actions/MenuItemsAction'
import {
    filterTheMenu,
    filterTheMenuWithCategory
  } from "../../utils/common";

const ReadMore = (props) => {
    let text = '';
    const [data, setdata] = useState(props.children);
    const [isReadMore, setIsReadMore] = useState(false);
    const [itemLists, setItemList] = useState([])
    const [children, setChildren] = useState({})    
    
    const { 
        menuItemList, 
        filteredObject, 
        categoryList,
        filteredItemList,
        itemList, 
        inventory 
      } = props;
    
    // console.log('Menu itemList', itemList)
    //setItemList(menuItemList);
    
    const {
      selectedCategory,
      selectedSubCategory,
      searchQuery,
      minPrice,
      maxPrice,
      isVeg,
      isNonVeg,
    } = filteredObject;

    // useEffect(() => {
    //   console.log("dfgjsdklfgd");
    // }, [isReadMore]);
    
   
    useEffect(() => {
      text = props.children.description;
      //console.log('in here')
     //setItemList(props.itemList);
    //   setChildren(props.children)
    });

    useEffect(() => {
      // console.log('in here i am')
      setItemList(itemList);
    }, [itemList]);



    const toggleReadMore = (id) => {
      props.itemList.map((item) => {
        if (item.productId == id) {
          return (item.ReadMore = !item.ReadMore);
        } else {
          return (item.ReadMore = true);
        }
      });
      var top = $(`#${id}`).offset().top;
      console.log(top);
      $(window).scrollTop($(window).scrollTop() + 10);
    };
  
    const expandMore = (data) => {
      let { menuItemList, categoryList } = props;
      
      let newFilteredItemList= []
      newFilteredItemList = itemList.map(item => {
        if(item.productId === data.productId)
          return {...item,showMore:true}
        else
          return {...item,showMore:false}
      });
     
      let myItemList = filterTheMenu(newFilteredItemList, categoryList);
      // console.log('New filteredItemList', myItemList);
      props.setMenuItemList({
        menuItemList: newFilteredItemList,
        inventory:inventory
      });
     
      // setItemList(newFilteredItemList);           
       //console.log('New filteredItemList', filteredItemList);
    }
  
    return (
      <div>
        {
          isMobileOnly ? (
          <>
            {
                itemLists.map((data) => {
                //console.log('props.children.productId',data.productId, props.children.productId, data.showMore)
                if (data.productId == props.children.productId) { 
                    text = data.ReadMore && data.description ? data.description:''
                    return (
                    <>
                        <p
                        className="text"
                        id={`${data.productId}`}
                        style={{ textOverflow: "ellipsis" }}
                        >
                        {/* {data.ReadMore
                            ? text.slice(0, 45)
                            : window.innerWidth < 760
                            ? text
                            : text.slice(0, 60)} */}                            
                        {
                          data.showMore ===true ? text:(
                            data.ReadMore
                            ? text.slice(0, 45)
                            : window.innerWidth < 760
                            ? text
                            : text.slice(0, 60)
                          )
                        }

                        {text.length > 85 ? (
                            <span className="read-or-hide">
                            <span
                                onClick={() => {
                                //props.setReadMore(true, data);
                                //toggleReadMore(props.children.productId);
                                expandMore(data)
                                // setIsReadMore(!isReadMore);
                                }}
                            >
                                {data.showMore ===true ?"":"...Read more"}
                            </span>
                            </span>
                        ) : (
                            <span>{text.slice(55, text.length)}</span>
                        )}
                        </p>
                    </>
                    );
              }
            })}
          </>
        ) : (
          <div>
            {
                (
                  <>
                    {
                        itemLists.map((data) => {
                        //console.log('props.children.productId',data.productId, props.children.productId, data.showMore)
                        if (data.productId == props.children.productId) { 
                            text = data.ReadMore && data.description ? data.description:''
                            return (
                            <>
                                <p
                                className="text"
                                id={`${data.productId}`}
                                style={{ textOverflow: "ellipsis" }}
                                >
                                {/* {data.ReadMore
                                    ? text.slice(0, 45)
                                    : window.innerWidth < 760
                                    ? text
                                    : text.slice(0, 60)} */}                            
                                {
                                  data.showMore ===true ? text:(
                                    data.ReadMore
                                    ? text.slice(0, 45)
                                    : window.innerWidth < 760
                                    ? text
                                    : text.slice(0, 60)
                                  )
                                }
        
                                {text.length > 85 ? (
                                    <span className="read-or-hide">
                                    <span
                                        onClick={() => {
                                        //props.setReadMore(true, data);
                                        //toggleReadMore(props.children.productId);
                                        expandMore(data)
                                        // setIsReadMore(!isReadMore);
                                        }}
                                    >
                                        {data.showMore ===true ?"":"...Read more"}
                                    </span>
                                    </span>
                                ) : (
                                    <span>{text.slice(55, text.length)}</span>
                                )}
                                </p>
                            </>
                            );
                      }
                    })}
                  </>
                )
            }            
          </div>
        )}
      </div>
    );
  };

  const mapStateToProps = (reducerObj) => {
    const cartObject = reducerObj.cartObject.cartObject;
    const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
    const inventory = reducerObj.menuItemList.menuItemList.inventory;
    const cartItemList = reducerObj.cartObject.cartObject.cartItems;
    const CouponObject = reducerObj.CouponObject.CouponObject;
    const selectedChannel =
      reducerObj.selectedChannel.selectedChannel.selectedChannel;
    const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
    const categoryList = reducerObj.categoryList.categoryList.categoryList;
    const filteredItemList = reducerObj.filteredItemList.filteredItemList;
    const filteredObject = reducerObj.filteredObject.filteredObject;
  
    return {
      cartObject,
      menuItemList,
      cartItemList,
      CouponObject,
      selectedChannel,
      selectedStore,
      inventory,
      categoryList,
      filteredItemList,
      filteredObject
    };
  };

  export default connect(mapStateToProps,{setFilteredItemList,updateFilterObject,setMenuItemList})(ReadMore);
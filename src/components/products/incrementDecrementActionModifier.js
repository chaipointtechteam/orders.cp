import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { updateCartItem, removeCartItem } from "../../actions/CartAction";
import {
    updateMenuItem,
    upateMenuItemAfterCartItemRemoval,
} from "../../actions/MenuItemsAction";
import ProductModal from "../product/ProductModal";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";
import toast from "../common/commonToast"
import { CheckAvaibilityOfProduct, ModifiersQuantity,VariantQuantity} from "../../utils/common";
import { withRouter } from 'react-router-dom'
import "../../../node_modules/@syncfusion/ej2-base/styles/material.css";
import "../../../node_modules/@syncfusion/ej2-react-popups/styles/material.css";
class IncrementDecrementActionModifier extends Component {
    state = {
        showProductModal: false,
        modal: false,
    }

    closeTheModal = () => {
        this.setState({
            showProductModal: false
        })
    }

    increment = () => {
        if (window.innerWidth >= 760) {
            this.setState({
            showProductModal: true
        })
        } else {
            this.props.history.push("/productDetails")
        }
        
    };
    decrement = () => {
        this.setState({
            modal: true
        })
    };
    handleClick() {
        if (this.buttonElement.getAttribute('data-tooltip-id')) {
            this.tooltipInstance.close();
        }
        else {
            this.tooltipInstance.open(this.buttonElement);
        }
    }

    render() {
        let margin = {
            margin: '40px'
        };
        // debugger
        const { product } = this.props;
        return (
            <div class="input-group" style={{ width: "70%" }}>
                {this.state.showProductModal ? <ProductModal
                    also="hello"
                    showModal={this.state.showProductModal}
                    closeModal={this.closeTheModal}
                    selectedProduct={product}
                /> : ""}
                <span class="input-group-btn" onClick={this.decrement}>
                    <TooltipComponent
                        
                        position="BottomLeft"
                        className="wrap" ref={t => this.tooltipInstance = t} opensOn='custom'
                        content='You have selected multiple customizations for this product. Please remove the specific customization from the Cart'
                    >
                        <button
                            id="box"
                            type="button"
                            className="quantity-left-minus btn btn-danger btn-number e-btn"
                            ref={d => this.buttonElement = d}
                            onClick={this.handleClick.bind(this)}
                        >
                            <span class="glyphicon glyphicon-minus"></span>
                        </button>
                    </TooltipComponent>
                </span>
                <input
                    style={{ cursor: "pointer" }}
                    disabled
                    readOnly="true"
                    type="text"
                    id="quantity"
                    name="quantity"
                    class="form-control input-number mw-11"
                    value={product.productVariantList.length>0?VariantQuantity(product, this.props.cartItemList) 
                        :ModifiersQuantity(this.props.inventory, product, this.props.cartItemList)}
                    min="1"
                    max="99"
                />
                <span class="input-group-btn" onClick={this.increment}>
                    <button
                        type="button"
                        class="quantity-right-plus btn btn-success btn-number"
                        data-type="plus"
                        data-field=""
                    >
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                </span>
            </div>

        );
    }
}

const mapStateToProps = (reducerObj) => {
    const cartItemList = reducerObj.cartObject.cartObject.cartItems;
    const cartObject = reducerObj.cartObject.cartObject;
    const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
    const inventory = reducerObj.menuItemList.menuItemList.inventory;
    return { cartItemList, menuItemList, cartObject, inventory };
};

export default connect(mapStateToProps, {
    updateCartItem,
    updateMenuItem,
    removeCartItem,
    upateMenuItemAfterCartItemRemoval,
})(withRouter(IncrementDecrementActionModifier));

IncrementDecrementActionModifier.propTypes = {
    updateCartItem: PropTypes.func.isRequired,
    updateMenuItem: PropTypes.func.isRequired,
    removeCartItem: PropTypes.func.isRequired,
    upateMenuItemAfterCartItemRemoval: PropTypes.func.isRequired,
};

import React, { Component } from "react";
import { connect } from "react-redux";
import { isMobileOnly } from "react-device-detect";

import { updateCartItem } from "../../actions/CartAction";
import { updateMenuItem } from "../../actions/MenuItemsAction";
import { CheckAvaibilityOfProduct, checkAvaibilityOfProductWithAddond } from "../../utils/common";
import toast from "../common/commonToast";
import IncrementDecrementAction from "./incrementDecrementAction";
import ProductModal from '../product/ProductModal';

class MyCartItem extends Component {
  state = { addOnsName: "", price: 0,showProductModal:false };
  componentDidMount = () => {
     
    const { cartItem } = this.props;
    // var addOns = cartItem.productModifierList[0]?.modifierProductList.map((k) => {
    //   if (k.isChecked) return k.productName.displayName;
    //   else return;
    // });
    var addOns = [];
    cartItem.productModifierList.modifierProductList?.map((addOnItem) => {
      if (addOnItem) {

        addOns.push(addOnItem.productName.displayName);
      }
    });

    addOns = addOns?.filter(function (element) {
      return element !== undefined;
    });

    // var addOnsPrice = cartItem.productModifierList[0]?.modifierProductList.map((k) => {
    //   if (k.isChecked) return k.productName.price;
    //   else return;
    // });
    // console.log(addOnsPrice, "dlgamsgamf")
    var addOnsPrice = [];
    cartItem.productModifierList.modifierProductList?.map((addOnItem) => {
      if (addOnItem) {
        addOnsPrice.push(addOnItem.productName.price);
      }
    });


    addOnsPrice = addOnsPrice?.filter(function (element) {
      return element !== undefined;
    });

    var sum = addOnsPrice?.reduce(function (a, b) {
      return a + b;
    }, 0);

    this.setState({
      addOnsName: addOns?.join(","),
      price: sum ? sum + cartItem.price : cartItem.price,
    });
  };

  componentDidUpdate = (prevProps) => {
    const { cartItem } = this.props;
    if (cartItem != prevProps.cartItem) {
      var addOns = [];
      cartItem.productModifierList.modifierProductList?.map((addOnItem) => {
        if (addOnItem) {          
          addOns.push(addOnItem.productName.displayName);
        }
      });
      addOns = addOns?.filter(function (element) {
        return element !== undefined;
      });
      var addOnsPrice = [];
      cartItem.productModifierList.modifierProductList?.map((addOnItem) => {
        if (addOnItem) {
          addOnsPrice.push(addOnItem.productName.price);
        }
      });


      addOnsPrice = addOnsPrice?.filter(function (element) {
        return element !== undefined;
      });

      var sum = addOnsPrice?.reduce(function (a, b) {
        return a + b;
      }, 0);

      this.setState({
        addOnsName: addOns?.join(","),
        price: sum ? sum + cartItem.price : cartItem.price,
      });
    }
  }

  updateCartWithProduct = () => {
    
    if (isMobileOnly) {
      this.routingFunction();
    } else {
      this.setState({
        showProductModal: true,
      });
    }
    if (this.props.cartItem.productModifierList.length === 0) {
      if (CheckAvaibilityOfProduct(this.props.inventory, this.props.cartItem)) {
        this.updateCartAndMenuList(this.props.cartItem.quantity + 1);
      } else {
        toast.warning(`You can't add ${this.props.cartItem.displayName},it's quantity is  now out of stock`)
      }
    } else {
      var message = checkAvaibilityOfProductWithAddond(this.props.inventory, this.props.cartItem, this.props.cartItemList)
     
      if (message.check) {
        this.updateCartAndMenuList(this.props.cartItem.quantity + 1);
      } else {
        toast.warning(`You can't add ${message.addonItem.displayName},it's quantity is  now out of stock`)
      }
    }


  };
  closeTheModal = () => {
    this.setState({
      showProductModal: false,
    });
  };

  updateCartAndMenuList = (quantity) => {
    //update quantity value in menu list reducer
    this.props.updateMenuItem(
      this.props.menuItemList,
      this.props.cartItem,
      quantity,
      this.props.inventory
    );
    //update quantity value in cart list reducer
    this.props.updateCartItem(
      this.props.cartItemList,
      this.props.cartItem,
      quantity
    );
  };
  render() {
    const { cartItem, cartObject } = this.props;
    console.log('cartItem', cartItem)
    return (
      <>
        {
          this.state.showProductModal && (
            <ProductModal          
              showModal={this.state.showProductModal}
              closeModal={this.closeTheModal}
              selectedProduct={cartItem.selectedProduct}
            />
          )
        }        
        <div class="col-xs-12 np">
          <div class="col-xs-5">
            <h4 class="product-name">
            <img src={
                cartItem.isVegetarian ? require("../../assets/images/Veg.svg").default:require("../../assets/images/Non Veg.svg").default
              }/> 
              <strong>{cartItem.displayName}</strong>
            </h4>                                    
          </div>
          <div class="col-xs-4">
            <IncrementDecrementAction
              product={cartItem}
              openModal={this.updateCartWithProduct}
            />
          </div>
          <div class="col-xs-3 cart-padding">
            {cartObject.subTotal && (
              <h4 class="product-name">
                <strong>
                  ₹{(this.state.price * cartItem.quantity).toFixed(2)}
                </strong>
              </h4>
            )}
          </div>
          {
              cartItem.productModifierList.modifierProductList?.length > 0 &&
              this.state.addOnsName != "" && cartItem.productModifierList.modifierProductList.map(item => (
                <div>
                  <h4>
                    <small style={{ color: "#adafb1" }}>
                      {item.productName.displayName}
                      <span style={{float:"right"}}>₹{item.productName.price}</span>
                    </small>                    
                  </h4>                  
                </div>
              ))
            }
            {
              cartItem.productModifierList.modifierProductList?.length>0&&
              <small onClick={this.updateCartWithProduct}>Customized</small>
            }
        </div>
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const cartItemList = reducerObj.cartObject.cartObject.cartItems;
  const cartObject = reducerObj.cartObject.cartObject;
  const inventory = reducerObj.menuItemList.menuItemList.inventory;
  return { menuItemList, cartItemList, cartObject, inventory };
};

export default connect(mapStateToProps, {
  updateCartItem,
  updateMenuItem,
})(MyCartItem);

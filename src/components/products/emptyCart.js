import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class EmptyCart extends Component {

  
  render() {
    return (
      <div class="empty-cart">
        <img src={require("../../assets/images/Empty Cart.svg").default} />
        <h5>Your cart is empty</h5>
        <Link to="/products">
          {!this.props.show ?
            <button type="primary" title="Login" className=" back-btn ">
              Go To Menu
        </button>:""}</Link>
      </div>
    );
  }
}

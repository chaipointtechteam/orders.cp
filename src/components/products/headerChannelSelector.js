import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Select from "react-select";
import { emptyCartItem, resetCartObject } from "../../actions/CartAction";
import { updateFilterObject } from "../../actions/FilterItemsAction";
import {
  loadInventoryList,
  loadMenuItemList, resetMenuItemList, setCategoryMenuList, setMenuItemList, updateMenuItem
} from "../../actions/MenuItemsAction";
import { setFilteredItemList } from "../../actions/FilterItemsAction";
import {
  resetChannel, updateSelectedChannel
} from "../../actions/StoreListAction";
import { filterTheMenuWithCategory,filterTheMenu} from "../../utils/common";
import { oraginzationId } from "../../utils/constants";
import toast from "../common/commonToast";
const customStyles = {
  menu: (styles) => ({
    ...styles,
    zIndex: 9999,
    width: 150,
    "@media only screen and (max-width: 762px)": {
      ...styles["@media only screen and (max-width: 762px)"],
      fontSize: "12px",
    },
  }),
  placeholder: (styles) => {
    return {
      ...styles,
      color: "#ffff",
      cursor: "pointer",
    };
  },
  control: (styles) => ({
    ...styles,
    cursor: "pointer",
    width: "100%",
    paddingLeft: 30,
    border: "none",
    color: "#279599",
    borderRadius: "0px",
    boxShadow: "none",

    "@media only screen and (max-width: 375px)": {
      ...styles["@media only screen and (max-width:375px)"],
      paddingLeft: "0px",
    },
    "@media only screen and (max-width: 762px)": {
      ...styles["@media only screen and (max-width: 762px)"],
      paddingLeft: "0px",
    },
  }),
  singleValue: (styles) => ({
    ...styles,
    cursor: "pointer",
    color: "#279599",
    "@media only screen and (max-width: 762px)": {
      ...styles["@media only screen and (max-width: 762px)"],
      fontSize: "12px",
    },
  }),
  option: () => {
    return {
      listStyle: "none",
      cursor: "pointer",
      padding: "12px 20px",
      borderBottom: "1px solid #f3f4f4",
      color: "#279599",
    };
  },
  indicatorsContainer: () => {
    return {
      color: "#279599",
    };
  },
  dropdownIndicator: () => {
    return {
      color: "#279599",
    };
  },
};

class HeaderChannelSelector extends Component {
  state = {
    selectedValue: {},
  };
  componentDidMount = () => {
    this.updateOptions();
    this.updateSelectedValue();
    this.setState({ selectedChannel: {} });
  };
  updateSelectedValue = () => {
    const { selectedChannel, channelList } = this.props;
    //  
    if (channelList) {
      channelList.forEach((element, index) => {
        if (element.sellerId === selectedChannel?.sellerId) {
        //   
          this.setState({
            selectedValue: {
              value: element.sellerId,
              label: element.displayName,
              name: "selectedChannel",
              index: index,
            }
          }, () => this.getAllAvailableMenuItems());
        }
      });
    }
  };
  componentDidUpdate(prevProps) {
    if (
      this.props.channelList !== prevProps.channelList &&
      this.props.selectedStore !== prevProps.selectedStore
    ) {
      this.updateOptions();

      this.props.updateFilterObject({
        activeKey: -1,
        selectedCategory: this.props.filteredObject.selectedCategory,
        selectedSubCategory: null,
        isSubCategory: null,
      });

      // this.getAllAvailableMenuItems()
      // this.props.resetChannel();

      // this.setState(
      //   {
      //     selectedValue: {},
      //   },
      //   () => this.getAllAvailableMenuItems()
      // );

      //  
      this.updateSelectedValue()

    }
  }

  // updateSelectedValue = () => {
  //   var select = this.props.channelList.find(this.selectedChannel.sellerId)
  //   console.log(select, "sdkgjaslkdgjals");
  // }

  updateOptions = () => {
    var options = [];
    if (this.props.channelList) {
      this.props.channelList.forEach((element, index) => {
        options.push({
          value: element.sellerId,
          label: element.displayName,
          name: "selectedChannel",
          index: index,
        });
      });
    }

    this.setState({
      options: options,
    });
  };
  resetTheCart = () => {
    this.props.cartObject.cartItems.forEach((cart) => {
      this.props.emptyCartItem(this.props.cartObject.cartItems, cart);
    });
    this.props.resetMenuItemList(this.props.menuItemList);
  };

  handleChannelChange = (event) => {
    this.setState(
      {
        [event.name]: this.props.channelList[event.index],
        selectedValue: {
          value: this.props.channelList[event.index].sellerId,
          label: this.props.channelList[event.index].displayName,
          name: "selectedChannel",
          index: event.index,
        },
      },
      () => this.getAllAvailableMenuItems()
    );
    if (event.name === "selectedChannel") {
      this.props.updateSelectedChannel(
        {
          selectedChannel: this.props.channelList[event.index],
        }
        // this.getAllAvailableMenuItems()
      );
      this.props.updateFilterObject({
        activeKey: -1,
        selectedCategory: this.props.filteredObject.selectedCategory,
        selectedSubCategory: null,
        isSubCategory: null,
      });

      toast.warning("Your previous cart will be removed");
      // this.props.resetCartObject();
      // this.props.resetCartObject(); //called 2 times intenstionally as one time was not removing the data
      // this.props.resetMenuItemList(this.props.menuItemList);

      this.resetTheCart();
    }

    this.props.updateSelectedChannel(
      {
        selectedChannel: this.props.channelList[event.index],
      }
      // this.getAllAvailableMenuItems()
    );
  };

  getAllAvailableMenuItems = () => {
    //Getting the  menu items will be called
    this.props.loadInventoryList(
      {
        sellerId: this.state.selectedValue
          ? this.state.selectedValue.value
          : null,
        siteId: this.props.selectedStore.siteId,
      },
      (inventoryData) => {
        this.props.loadMenuItemList(
          {
            cityId: this.props.selectedStore.cityId,
            includeProductRecipe: true,
            organizationId: oraginzationId,
            sellerIdList: [this.props.selectedChannel?.sellerId],
            siteId: this.props.selectedStore.siteId,
          },
          (catLogData) => {
            if (catLogData) {
              var categoryList =
                catLogData.data.sellerProductCatalogueList[0].productCatalogue
                  .categoryList;
              this.props.setCategoryMenuList({
                categoryList: categoryList,
              });
              var menuItemList = [];
              var inventory = []
              catLogData.data.sellerProductCatalogueList[0].productCatalogue.productList.forEach(
                (product) => {
                  inventory = inventoryData.data
                  if (product.productVariantList.length > 0) {
                    
                    product.quantity = 0;
                    menuItemList.push(product);
                  }
                  inventoryData.data.forEach((inventory) => {
                    if (
                      product.productCode === inventory.code
                    ) {
                      product.quantity = 0;
                      product.ReadMore=true;
                      menuItemList.push(product);
                    }
                  });
                }
              );
              categoryList.forEach((category) => {
                category.productCodes.forEach((productCode) => {
                  menuItemList.forEach((product) => {
                    if (product.productCode === productCode) {
                      product.category = category;
                    }
                  });
                });
                category.subCategories.forEach((subCategory) => {
                  subCategory.productCodes.forEach((productCode) => {
                    menuItemList.forEach((product) => {
                      if (product.productCode === productCode) {
                        product.subCategory = subCategory;
                        product.category = category;
                      }
                    });
                  });
                });
              });

              if (this.props.filteredObject.activeKey === -1) {
                var filtercate = [];
                var filterindx = [];
                this.props.categoryList.map((category, index) => {
                  var filter = filterTheMenuWithCategory(menuItemList, category);

                  if (filter && filter.length > 0) {
                    filtercate.push(category);
                    filterindx.push(index);
                  }
                });

                this.props.updateFilterObject({
                  activeKey: filterindx[0],
                  selectedCategory: filtercate[0],
                  selectedSubCategory: null,
                  isSubCategory: null,
                });
              }


             let filteredItemList = filterTheMenu(menuItemList, categoryList);
             this.props.setFilteredItemList({
              filteredItemList:filteredItemList
             })
              this.props.setMenuItemList({
                menuItemList: menuItemList,
                inventory: inventory
              });
              if (this.props.cartObject.cartItems?.length > 0) {
                this.props.cartObject.cartItems.map((item) => {
                  this.props.updateMenuItem(menuItemList, item, item.quantity, inventory)
                })

              }
            } else {
              // this.props.setCategoryMenu{
              //   categoryList: [],
              // });List(
              this.props.setMenuItemList({
                menuItemList: [],
              });
            }

            //here the complete process will take place of concatination of all the inventory items and menu items
            //this.props.history.push("/products"); //as we are already on that page
          }
        );
      }
    );
  };

  render() {
    return (
      <>
        <span className="detailHeader">
          <img src={require("../../assets/images/Channel.svg").default} />
        </span>

        <div class="header-custom-select">
          <Select
            isSearchable={false}
            placeholder="Select Channel"
            value={this.state.selectedValue}
            options={this.state.options}
            styles={customStyles}
            onChange={this.handleChannelChange}
          />
        </div>
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const filteredObject = reducerObj.filteredObject.filteredObject;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const channelList = reducerObj.channelList.channelList.channelList;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  const userObject = reducerObj.userObject.userObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  return {
    filteredObject,
    selectedStore,
    selectedChannel,
    channelList,
    userObject,
    menuItemList,
    cartObject,
    categoryList,
  };
};

export default connect(mapStateToProps, {
  updateSelectedChannel,
  loadMenuItemList,
  loadInventoryList,
  setMenuItemList,
  resetChannel,
  setCategoryMenuList,
  resetCartObject,
  resetMenuItemList,
  setFilteredItemList,
  emptyCartItem,
  updateFilterObject,
  updateMenuItem,
})(withRouter(HeaderChannelSelector));

HeaderChannelSelector.propTypes = {
  updateSelectedChannel: PropTypes.func.isRequired,
  loadMenuItemList: PropTypes.func.isRequired,
  loadInventoryList: PropTypes.func.isRequired,
  setMenuItemList: PropTypes.func.isRequired,
  setCategoryMenuList: PropTypes.func.isRequired,
};

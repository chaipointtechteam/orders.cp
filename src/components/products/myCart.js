import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import {
  resetCartObject,
  removeCartItem,
  emptyCartItem,
} from "../../actions/CartAction";
import { resetMenuItemList } from "../../actions/MenuItemsAction";
import MyCartItem from "./myCartItem";
import { withRouter } from "react-router-dom";

class MyCart extends Component {
  resetTheCart = () => {
    //this.props.resetCartObject();
    // this.props.resetCartObject();

    this.props.cartObject.cartItems.forEach((cart) => {
      this.props.emptyCartItem(this.props.cartObject.cartItems, cart);
    });
    this.props.resetMenuItemList(this.props.menuItemList, this.props.inventory);
    //reset all the menu items as well
  };

  redirectToCheckoutPage = () => {
    this.props.history.push("/checkout");
  };

  render() {
    const { cartObject } = this.props;
    return (
      <div class="cart-details">
        <h2><a href="#" onClick={()=>this.props.history.goBack()}><img src="/static/media/left-arrow.2d863411.svg" className="backcart"></img></a> cart </h2>
        <p> {cartObject.cartItems.length} items</p>
        <div class="row">
          <div class="seperate-cart-section">
          {cartObject &&
            cartObject.cartItems &&
            cartObject.cartItems.map((item, index) => (
              <MyCartItem key={index} cartItem={item} />
            ))}
          </div>
          <div class="col-xs-12 cart-subtotal">
            <div class="col-xs-8">
              <h4 class="product-name">
                <strong>SubTotal</strong>
              </h4>
              <h4>
                <small>Delivery charge may apply</small>
              </h4>
            </div>
            {cartObject && cartObject.subTotal && (
              <div class="col-xs-4">
                <h4 class="product-subtotal">
                  <strong>₹{cartObject.subTotal}</strong>
                </h4>
              </div>
            )}
          </div>
          <div class="col-xs-12 checkoutBtn">
            <button onClick={this.redirectToCheckoutPage}>checkout 
             <img src={
                require("../../assets/images/right-arrows.png").default
              }/> 
            </button>
          </div>
          <div onClick={this.resetTheCart} class="col-xs-12 resetBtn">
            <button>reset</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const inventory = reducerObj.menuItemList.menuItemList.inventory;
  return { cartObject, menuItemList, inventory };
};

export default connect(mapStateToProps, {
  resetCartObject,
  resetMenuItemList,
  removeCartItem,
  emptyCartItem,
})(withRouter(MyCart));

MyCart.propTypes = {
  cartObject: PropTypes.object.isRequired,

  resetCartObject: PropTypes.func.isRequired,
  resetMenuItemList: PropTypes.func.isRequired,
};

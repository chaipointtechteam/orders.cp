import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { updateCartItem, removeCartItem } from "../../actions/CartAction";
import {
  updateMenuItem,
  upateMenuItemAfterCartItemRemoval,
} from "../../actions/MenuItemsAction";
import toast from "../common/commonToast"
import { CheckAvaibilityOfProduct } from "../../utils/common";

class IncrementDecrementAction extends Component {
  updateCartAndMenuList = (quantity) => {
    //update quantity value in menu list reducer
    this.props.updateMenuItem(
      this.props.menuItemList,
      this.props.product,
      quantity,
      this.props.inventory,
    );
    //update quantity value in cart list reducer
    this.props.updateCartItem(
      this.props.cartItemList,
      this.props.product,
      quantity
    );
  };

  increment = () => {
    
    if (this.props.product.quantity < 99) {
      if (
        this.props.product.productModifierList &&
       this.props.product.productModifierList.length == 0 &&
      this.props.product.productVariantList.length === 0 &&
        this.props.product.quantity >= 0 &&
        this.props.product.quantity < 99
      ) {
if (CheckAvaibilityOfProduct(this.props.inventory, this.props.product)) {
        this.updateCartAndMenuList(this.props.product.quantity + 1);
      } else {
        toast.info(`You can't add ${this.props.product.displayName},it's quantity is  now out of stock`)
      }

      } else {
        if (this.props.openModal) {
          this.props.openModal();
        } else {
          this.updateCartAndMenuList(this.props.product.quantity + 1);
        }
      }
    }
  };
  decrement = () => {
    if (this.props.product.quantity > 1) {
      this.updateCartAndMenuList(this.props.product.quantity - 1);
    } else {
      this.props.removeCartItem(this.props.cartItemList, this.props.product);
      this.props.upateMenuItemAfterCartItemRemoval(
        this.props.menuItemList,
        this.props.product,
        this.props.inventory
      );
    }
  };
  render() {
    const { product } = this.props;
    return (
      <div class="input-group" style={{ width: "70%" }}>
        <span class="input-group-btn" onClick={this.decrement}>
          <button
            type="button"
            class="quantity-left-minus btn btn-danger btn-number"
            data-type="minus"
            data-field=""
          >
            <span class="glyphicon glyphicon-minus"></span>
          </button>
        </span>
        <input
          style={{ cursor: "pointer" }}
          disabled
          readOnly="true"
          type="text"
          id="quantity"
          name="quantity"
          class="form-control input-number mw-11"
          value={product.quantity}
          min="1"
          max="99"
        />
        <span class="input-group-btn" onClick={this.increment}>
          <button
            type="button"
            class="quantity-right-plus btn btn-success btn-number"
            data-type="plus"
            data-field=""
          >
            <span class="glyphicon glyphicon-plus"></span>
          </button>
        </span>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartItemList = reducerObj.cartObject.cartObject.cartItems;
  const cartObject = reducerObj.cartObject.cartObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const inventory = reducerObj.menuItemList.menuItemList.inventory;
  return { cartItemList, menuItemList, cartObject, inventory };
};

export default connect(mapStateToProps, {
  updateCartItem,
  updateMenuItem,
  removeCartItem,
  upateMenuItemAfterCartItemRemoval,
})(IncrementDecrementAction);

IncrementDecrementAction.propTypes = {
  updateCartItem: PropTypes.func.isRequired,
  updateMenuItem: PropTypes.func.isRequired,
  removeCartItem: PropTypes.func.isRequired,
  upateMenuItemAfterCartItemRemoval: PropTypes.func.isRequired,
};

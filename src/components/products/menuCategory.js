import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import {
  filterTheMenuWithCategory,
  filterTheMenuWithSubCategory,
  filterByValue,
  filterFromModal,
} from "../../utils/common";
import { updateFilterObject } from "../../actions/FilterItemsAction";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { Accordion, Card, Button, Collapse } from "react-bootstrap";
import $ from 'jquery';
class MenuCategory extends Component {
  state = {
    categoryList : this.props.categoryList,
    activeKey: this.props.filteredObject.activeKey,
    selectedCategory: this.props.filteredObject.selectedCategory,
    selectedSubCategory: this.props.filteredObject.selectedSubCategory,
    isSubCategory: this.props.filteredObject.isSubCategory,
  };
    componentDidUpdate=(prevProps)=>{
      if(this.props.filteredObject!==prevProps.filteredObject||
         this.props.filteredItemList!==prevProps.filteredItemList
        ){
        this.setState({
         activeKey: this.props.filteredObject.activeKey,
         selectedCategory: this.props.filteredObject.selectedCategory,
         selectedSubCategory: this.props.filteredObject.selectedSubCategory,
         isSubCategory: this.props.filteredObject.isSubCategory,
        })
      }
  }
  setItActive = (key, selectedCategory, isSubCat, selectedSubCategory) => {

    // this.props.updateFilterObject({
    //   activeKey: key,
    //   selectedCategory: selectedCategory,
    //   selectedSubCategory: selectedSubCategory,
    //   isSubCategory: isSubCat,
    // });

    this.setState({
      activeKey: key,
      selectedCategory: selectedCategory,
      selectedSubCategory: selectedSubCategory,
      isSubCategory: isSubCat,
    });
    
    if (this.props.close&&selectedSubCategory) {
       this.props.close()
    }
  };

  onClick= (e) => {
    const element = document.getElementById(e.currentTarget.dataset.target)
    
    element.scrollIntoView({
      behaviour :'smooth'
    })    
    
  }

  render() {
    const { categoryList } = this.props;
    

    const { activeKey, selectedCategory, selectedSubCategory, isSubCategory } =
      this.state;
    return (
      <div
        class="col-md-3 col-sm-12 col-xs-12 caterogy-sticky " id="sidebar">
        <ul class="nav nav-tabs tabs-left sideways" >
        <Accordion style={{ width: "100%" }}>
          {categoryList &&
            categoryList.map((category, index) => {
              const { menuItemList } = this.props;
              var filtermenu = filterTheMenuWithCategory(
                menuItemList,
                category
              );
              var m=[]
              //console.log(this.props.filteredObject)
              //console.log(this.props.filteredItemList)
              m= this.props.filteredItemList?.filter((o)=> o.filterMenu!==undefined? o.filterMenu?.length>0:true)
              // console.log(m)
              var  k = m!==undefined? m.find((o)=>o?.item?.categoryCode==category.categoryCode):true;
              //console.log(k)
              var fil =k==true?true:k
              if (filtermenu?.length !== 0  && fil!==undefined) {
                return (
                  <>
                    {category.subCategories.length === 0 && (
                      
                        <Card className = "abc-card" style={{ border: "none"}}>
                          <Card.Header
                            style={{
                              border: "none",
                             
                            }}
                          >
                            <Accordion.Toggle
                              as={Button}
                              variant="link"
                              eventKey="0"
                            >
                              <li
                                className={`first-li  ${activeKey === index &&
                                  selectedCategory &&
                                  !selectedSubCategory
                                  ? "active"
                                  : ""
                                  }`}
                                key={index}
                                onClick={
                                  () => {
                                    this.setItActive(
                                      index,
                                      category,
                                      false,
                                      null
                                    );
                                  }
                                  // this.setItActive(index, category, false, null)
                                }
                              >
                                <>
                                  {activeKey === index &&
                                    selectedCategory &&
                                    !selectedSubCategory && (
                                      <FontAwesomeIcon
                                        icon={faChevronRight}
                                        size="sm"
                                        className="arrow-icon"
                                      />
                                    )}

                                  <a href={`#${category.name}` } data-toggle="collapse" className="arrowHeader" data-target={category.name} onClick={this.onClick}>  {!(
                                    activeKey === index &&
                                    selectedCategory &&
                                    !selectedSubCategory
                                  ) && (
                                      <span>
                                        <FontAwesomeIcon
                                          icon={faChevronRight}
                                          size="sm"
                                          // style={ }
                                          className="star-icon star-icon-inactive"
                                        /></span>
                                    )}<span>{category.name}</span></a>
                                </>
                              </li>
                            </Accordion.Toggle>
                          </Card.Header>
                        </Card>
                      
                    )}

                    {category.subCategories.length > 0 && (
                      <>
                        
                          <Card style={{ border: "none" }}>
                            <Card.Header
                              style={{
                                border: "none",
                               
                              }}
                            >
                              <Accordion.Toggle
                                as={Button}
                                variant="link"
                                eventKey={index + 1}
                              >                                
                                <li
                                  className={`${activeKey === index &&
                                    selectedCategory &&
                                    !selectedSubCategory
                                    ? "active"
                                    : ""
                                    }`}
                                  key={index}
                                  onClick={(e) =>
                                    this.setItActive(                              
                                      index,
                                      category,
                                      false,
                                      null
                                    )
                                  }
                                >
                                  <>
                                    {activeKey === index &&
                                      selectedCategory &&
                                      !selectedSubCategory && (
                                        <FontAwesomeIcon
                                          icon={faChevronRight}
                                          size="sm"
                                          className="arrow-icon"
                                        />
                                      )}
                                    {/* {!(
                                      activeKey === index &&
                                      selectedCategory &&
                                      !selectedSubCategory
                                    ) && (
                                        <FontAwesomeIcon
                                          icon={faChevronRight}
                                          size="sm"
                                          className="arrow-icon star-icon-inactive"
                                        />
                                      )} */}

                                    <a href={`#${category.name}` } data-toggle="collapse" className="arrowHeader" data-target={category.name} onClick={this.onClick}>  {!(
                                      activeKey === index &&
                                      selectedCategory &&
                                      !selectedSubCategory
                                    ) ? (
                                      <span>
                                        <FontAwesomeIcon
                                          icon={faChevronRight}
                                          size="sm"
                                          // style={ }
                                          className="star-icon star-icon-inactive"
                                        /></span>
                                    ) : ""}<span>{category.name}</span></a>               
                                  </>
                                </li>
                              </Accordion.Toggle>
                            </Card.Header>
                              <Accordion.Collapse eventKey={index + 1}>
                              <>                                
                                {category.subCategories.map((subCat, index) => {
                                  var filtersubcat =
                                    filterTheMenuWithSubCategory(
                                      menuItemList,
                                      subCat,
                                      category
                                    );
                                  if (filtersubcat?.length > 0) {
                                    return (
                                      <>
                                        {subCat.subCategories.length == 0 && (
                                          <>
                                            <li
                                              className={`inner-li ${activeKey === index &&
                                                isSubCategory &&
                                                selectedCategory &&
                                                selectedCategory.categoryCode ===
                                                category.categoryCode &&
                                                selectedSubCategory
                                                ? "active"
                                                : ""
                                                }`}
                                              key={index}
                                              onClick={() =>
                                                this.setItActive(
                                                  index,
                                                  category,
                                                  true,
                                                  subCat
                                                )
                                              }
                                            >
                                              <>
                                                <div className="SubCategoryIcon">
                                                  {activeKey === index &&
                                                    isSubCategory &&
                                                    selectedCategory &&
                                                    selectedCategory.categoryCode ===
                                                    category.categoryCode &&
                                                    selectedSubCategory && (
                                                      <FontAwesomeIcon
                                                        icon={faChevronRight}
                                                        size="lg"
                                                        className="arrow-icon"
                                                      />
                                                    )}

                                                  <a href={`#${subCat.name}${index}` }
                                                    data-toggle="collapse"
                                                    class="arrowHeader Substar-icon-a"
                                                    data-target={`${subCat.name}${index}`} onClick={this.onClick}
                                                  >
                                                    {!(
                                                      activeKey === index &&
                                                      isSubCategory &&
                                                      selectedCategory &&
                                                      selectedCategory.categoryCode ===
                                                      category.categoryCode &&
                                                      selectedSubCategory
                                                    ) && (
                                                        <span>
                                                          <FontAwesomeIcon
                                                            icon={faChevronRight}
                                                            size="sm"
                                                            // style={ }
                                                            className="arrowh star-icon star-icon-inactive"
                                                          /></span>
                                                      )}
                                                    {subCat.name}
                                                  </a>
                                                </div>
                                              </>
                                            </li>
                                          </>
                                        )
                                        }
                                      </>
                                    );
                                  }
                                })}
                              </>
                            </Accordion.Collapse>
                          </Card>
                       
                      </>
                    )
                    }
                  </>
                );
              }
            })}
            </Accordion>
        </ul>
      </div >

      
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  const filteredObject = reducerObj.filteredObject.filteredObject;
  const filteredItemList = reducerObj.filteredItemList.filteredItemList.filteredItemList;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  return { categoryList, filteredObject, menuItemList,filteredItemList };
};

export default connect(mapStateToProps, { updateFilterObject })(MenuCategory);

MenuCategory.propTypes = {
  categoryList: PropTypes.array,
  filteredObject: PropTypes.object.isRequired,
  updateFilterObject: PropTypes.func.isRequired,
};

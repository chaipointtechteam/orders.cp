import React, { Component } from "react";
import LoginModal from "../landing/LoginModal";
import SignUpModal from "../landing/SignUpModal";
import { updateUserObject } from "../../actions/UserAction";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

class ProductLoginHeader extends Component {
  state = { showLoginPopup: false, showSignUpPopup: false };
  openLoginPopup = () => {
    this.setState({ showLoginPopup: true });
    this.props.updateUserObject({
      showLoginPopup: true,
    });
  };
  openSignUpPopup = () => {
    this.setState({ showSignUpPopup: true });
    this.props.updateUserObject({
      showSignUpPopup: true,
    });
  };
  closePopUp = () => {
    this.setState({
      showLoginPopup: false,
      showSignUpPopup: false,
    });
    this.props.updateUserObject({
      showLoginPopup: false,
      showSignUpPopup: false,
    });
  };
  closePopUpAndOpenLogin = () => {
    this.setState({
      showLoginPopup: true,
      showSignUpPopup: false,
    });
    this.props.updateUserObject({
      showLoginPopup: true,
      showSignUpPopup: false,
    });
  };
  closePopUpAndOpenSignUp = () => {
    this.setState({
      showLoginPopup: false,
      showSignUpPopup: true,
    });
    this.props.updateUserObject({
      showLoginPopup: false,
      showSignUpPopup: true,
    });
  };
  render() {
    return (
      <>
        <div class="user-profile">
          <button
            onClick={this.openLoginPopup}
            class="btn btn-primary dropdown-toggle"
            type="button"
            data-toggle="dropdown"
          >
            <span>
              <img
                src={require("../../assets/images/Profile_Green.svg").default}
              />
            </span>
            <span class="user">Login</span>
          </button>
        </div>
        <LoginModal
          showModal={this.state.showLoginPopup}
          closePopUp={this.closePopUp}
          closePopUpAndOpenSignUp={this.closePopUpAndOpenSignUp}
        />
        <SignUpModal
          showModal={this.state.showSignUpPopup}
          closePopUp={this.closePopUp}
          closePopUpAndOpenLogin={this.closePopUpAndOpenLogin}
        />
      </>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, {
  updateUserObject,
})(ProductLoginHeader);

ProductLoginHeader.propTypes = {
  updateUserObject: PropTypes.func.isRequired,
};

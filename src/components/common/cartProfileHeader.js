import { PropTypes } from "prop-types";
import React, { Component } from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { updateUserObject } from "../../actions/UserAction";
import CartModal from "../landing/CartModal";
import ProfileDropdown from "./profileDropdown";

class CartProfileHeader extends Component {
  state = { showPopup: false };
  openPopup = () => {
    this.setState({ showPopup: true });
  };
  closePopUp = () => {
    this.setState({
      showPopup: false,
    });
  };
  render() {
    return (
      <>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="category-section">
            <div class="row detailHeader">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-12 search-profile empty-profile">
                  <div class="simplemenu">
                    <button class="dropbtn" onClick={this.openPopup}>
                      <span>
                        {_.size(_.get(this.props, "cartObject.cartItems", "")) >
                          0 && (
                          <span className="badge badge-light cart-badge">
                            {_.size(
                              _.get(this.props, "cartObject.cartItems", "")
                            )}
                          </span>
                        )}

                        <img
                          src={require("../../assets/images/Cart2.svg").default}
                        />
                      </span>
                      <span>Cart</span>
                    </button>
                  </div>
                </div>
                <ProfileDropdown />
              </div>
            </div>
          </div>
        </div>
        <CartModal
          showModal={this.state.showPopup}
          closePopUp={this.closePopUp}
        />
      </>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const cartObject = reducerObj.cartObject.cartObject;
  return { userObject, cartObject };
};

export default connect(mapStateToProps, {
  updateUserObject,
})(CartProfileHeader);

CartProfileHeader.propTypes = {
  updateUserObject: PropTypes.func.isRequired,
};

import React from "react";
import Geocode from "react-geocode";
import Autocomplete from "react-google-autocomplete";
import {
  GoogleMap, InfoWindow,
  Marker, withGoogleMap, withScriptjs
} from "react-google-maps";
import { googleMapApi } from "../../../common/constants/index";
import {
  getArea,
  getCity, getCurrentLatLong,
  getPinCode, getState
} from "../../../utils/address_helper";


const GoogleMapsAPI = googleMapApi;
Geocode.setApiKey(GoogleMapsAPI);
Geocode.enableDebug();
/* global google */
// export default class Map extends React.PureComponent {
export default class Map extends React.Component {
  state = { selectedLocationText: null };

  shouldComponentUpdate(nextProps, nextState) {
    const { current } = this.props;
    const { current: nextCurrent } = nextProps;
    if (nextCurrent.lat != current.lat || nextCurrent.lng != current.lng) {
      return true;
    }
    return false;
  }

  onInfoWindowClose = (event) => { };

  async componentDidMount() {
    try {
      const { lat, long } = await getCurrentLatLong();
      var lati=lat;
      var longi = long;
    } catch (error) {
      var lati = this.props.editData?.latitude ;
      var longi = this.props.editData?.longitude;
    }
    // const { lat, long } = await getCurrentLatLong();
    // var lati = this.props.editData?.latitude || lat;
    // var longi = this.props.editData?.longitude || long;
    Geocode.fromLatLng(lati, longi).then(
      (response) => {
        const address = response.results[0].formatted_address;
        const addressArray = response.results[0].address_components;
        const city = getCity(addressArray);
        const area = getArea(addressArray);
        const state = getState(addressArray);
        const flatNo = this.props.editData?.streetAddress1;
        if (this.props.editData) {

          this.onAddressChange({
            address: this.props.editData?.streetAddress2,
            flatNo,
            city,
            area,
            state,
            lati: this.props.editData?.latitude,
            lng: this.props.editData?.longitude,
            pincode: `${this.props.editData?.zipcode}`,
            landmark: this.props.editData?.landmark
          });
        } else {
          this.onAddressChange({ address, city, area, state, lati, lng: longi });
        }
      },
      (error) => {
        console.error("Error", error);
      }
    );
  }

  onAddressChange = (mapData = {}) => {
    const {
      area,
      address,
      city,
      state,
      lati,
      lng,
      flatNo,
      pincode,
      landmark,
    } = mapData;
    console.log(mapData)

    const current = {
      address: address ? address : "",
      area: area ? area : "",
      city: city ? city : "",
      state: state ? state : "",
      lat: lati ? lati : mapData.lat,
      lng: lng ? lng : "",
      streetAddress1: flatNo ? flatNo : "",
      pincode: pincode ? pincode : "",
      landmark: landmark ? landmark : "",
    };
    this.props.onAddressChange(current);
  };

  onMarkerDragEnd = (event) => {
    const lat = event.latLng.lat();
    const lng = event.latLng.lng();
    console.log(lat, lng)
    Geocode.fromLatLng(lat, lng).then(
      (response) => {
        const address = response.results[0].formatted_address;
        const addressArray = response.results[0].address_components;
        const city = getCity(addressArray);
        const area = getArea(addressArray);
        const state = getState(addressArray);
        const pincode = getPinCode(addressArray);
        this.onAddressChange({ address, city, area, state, lat, lng, pincode });
      },
      (error) => {
        console.error(error);
      }
    );
  };

  onPlaceSelected = (place) => {

    const address = place.formatted_address;
    const addressArray = place.address_components;
    const city = getCity(addressArray);
    const area = getArea(addressArray);
    const state = getState(addressArray);
    const pincode = getPinCode(addressArray);
    const lat = place.geometry.location.lat();
    const lng = place.geometry.location.lng();

    this.onAddressChange({ address, city, area, state, lat, lng, pincode });
  };

  render() {
    const { selectedLocationText } = this.state;
    const { current = {}, editData } = this.props;
    const AsyncMap = withScriptjs(
      withGoogleMap((props) => (
        <>
          <GoogleMap
            google={this.props.google}
            defaultZoom={this.props.zoom}
            defaultCenter={{
              lat: editData?.latitude || current.lat,
              lng: editData?.longitude || current.lng,
            }}
          >
            <InfoWindow
              onClose={this.onInfoWindowClose}
              position={{
                lat: current.lat + 0.0018,
                lng: current.lng,
              }}
            >
              <div>
                <span style={{ padding: 0, margin: 0 }}>{current.address}</span>
              </div>
            </InfoWindow>
            <Marker
              google={this.props.google}
              // name={"Dolores park"}
              draggable={true}
              onDragEnd={this.onMarkerDragEnd}
              position={{
                lat: current.lat,
                lng: current.lng,
              }}
            />
            <Marker />
          </GoogleMap>
          <div>
            <Autocomplete
              style={{
                width: "100%",
                height: "40px",
                paddingLeft: "4px",
                marginTop: "2px",
                marginBottom: "500px",
              }}
              defaultValue={this.props.editData?.streetAddress2}
              apiKey={googleMapApi}
              onPlaceSelected={this.onPlaceSelected}
              types={["(regions)"]}
            />
          </div>
        </>
      ))
    );
    let map;
    if (current.lat !== undefined) {
      map = (
        <div>
          <AsyncMap
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${googleMapApi}&libraries=places`}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: this.props.height }} />}
            mapElement={<div style={{ height: `100%` }} />}
          />
        </div>
      );
    } else {
      map = <div style={{ height: this.props.height }} />;
    }
    return map;
  }
}

import React, { Component } from "react";
import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";
import styles from "./styles/address-modal.module.scss";
import Map from "./map";
import { getCurrentLatLong } from "../../../utils/address_helper";
import ProfileService from "../../../utils/services/profile/profile-service";
import { connect } from "react-redux";
import { getProfile } from "../../../../src/actions/LoginAction";
import { updateUserObject } from "../../../../src/actions/UserAction";
import toast from "../commonToast";
import { withRouter } from "react-router-dom";
class AddressContainer extends Component {
  state = {
    current: {
      addressType: 0,
      lat: 0,
      lng: 0,
      address: "",
      streetAddress2: "",
      streetAddress1: "",
      landmark: "",
      pincode: "",
      fullName: "",
      phoneNo: "",
    },
    error: "",
  };
  async componentDidMount() {    
    try {
      const { lat, long } = await getCurrentLatLong();
      this.setState({
        current: { lat, long, ...this.state.current },
      });
    } catch (error) {}
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    // this.setState({current:{addressTypeId:value,...this.state.current}});
  };
  handleInputChange = ({ target = {} }) => {    
    const { name = "" } = target;
    const { current = {} } = this.state;
    let value = target.value;
    if (name === "pincode" && isNaN(target.value)) {
      value = current.pincode;
    }

    const newCurrent = { ...current, [name]: value };
    this.setState({ current: newCurrent });
  };

  isInvalidForm = () => {
    const { current = {} } = this.state;
    ["streetAddress1", "streetAddress2", "pincode", "landmark"].forEach((i) => {
      if (!current[i] || !current[i].trim()) {
        return true;
      }
    });
    if (
      current["streetAddress1"]?.length == 0 ||
      current["streetAddress1"] == null
    ) {
      return true;
    } else if (current["streetAddress2"].length == 0) {
      return true;
    } else if (current["landmark"].length == 0) {
      return true;
    } else if (current["pincode"].length != 6) {
      return true;
    } else if(current['addressType'] === 0){
        return true
    }else return false;
  };

  handleSave = async () => {
    const { userObject = {} } = this.props;
    const { emailId, phoneNo, name } = userObject;
    const {
      addressType,
      streetAddress1,
      streetAddress2,
      pincode,
      fullName,
      landmark,
      lat,
      lng,
    } = this.state.current;
    const payload = {
      addressTypeId: addressType,
      emailId: emailId,
      fullName: name,
      landmark,
      latitude: lat,
      longitude: lng,
      phoneNo: Number(phoneNo),
      pincode: Number(pincode),
      streetAddress1,
      isDefault: true,
      streetAddress2,
    };

    const { sessionId } = this.props.userObject.newHeader;

    const op = await ProfileService.addUpdateAddress(payload, sessionId)

      .then((e) => {
        ProfileService.defaultAddress(e, sessionId);
        setTimeout(() => {
          this.props.getProfile(`${phoneNo}`, sessionId);
        }, 200);
        if (e.status === 400) {
          this.setState({ error: e.data.message });
        } else {
          this.props.onClose();
          if (this.props.checkout && window.innerHeight < 760) {
            this.props.history.push("/checkout");
          }
        }
      })
      .catch((e) => {
        // console.log("hello2",e)
      });
  };

  render() {
    const { show, onClose } = this.props;
    const { current = {}, onSave } = this.state;
    const isInvalidForm = this.isInvalidForm();

    return (
      <Modal
        className={styles["modal-container"]}
        show={show}
        onHide={onClose}
        centered
        contentClassName={styles["modal-content"]}
      >
        <Modal.Body>
          <Modal.Header closeButton>
            <Modal.Title>Add Address</Modal.Title>
          </Modal.Header>
          <div className="add-new-address">
            <Map
              height="150px"
              zoom={15}
              onAddressChange={(current) => {                
                this.setState({
                  current: {
                    ...this.state.current,
                    ...current,                    
                    streetAddress2: current.address,
                    pincode: current.pincode,
                    landmark: current.area,
                  },
                },()=>console.log('current', current));
              }}
              current={current}
            />
            <div className="mapAddress">
              {[
                { field: "streetAddress2", placeholder: "Full Address" },
                { field: "streetAddress1", placeholder: "Flat no/ Door No" },
                { field: "landmark", placeholder: "Landmark" },
                { field: "pincode", placeholder: "Pincode", maxLength: "6" },
              ].map((i) => {
                const { field, ...rest } = i;

                return (
                  <div key={field}>
                    <input
                      required
                      {...rest}
                      value={current[field]}
                      name={field}
                      onChange={this.handleInputChange}
                    />
                  </div>
                );
              })}

              <div className="Address-category bbb">
                <input
                  type="radio"
                  id="home"
                  name="addressType"
                  value={1}
                  onChange={this.handleInputChange}                  
                />
                <label htmlFor="home">Home</label>
                <input
                  type="radio"
                  id="work"
                  name="addressType"
                  value={2}
                  onChange={this.handleInputChange}
                />
                <label htmlFor="work">Office</label>
                <input
                  type="radio"
                  id="other"
                  name="addressType"
                  value={3}
                  onChange={this.handleInputChange}
                />
                <label htmlFor="other">Other</label>
              </div>
              <div>
                <Button
                  variant="primary"
                  onClick={this.handleSave}
                  disabled={isInvalidForm}
                >
                  Save Address
                </Button>
                <p style={{ color: "red" }}>{this.state.error}</p>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};
const mapDispatchToProps = (dispatchEvent, { sessionId }) => {
  return {
    getProfile: (payload, sessionId) =>
      dispatchEvent(getProfile(payload, sessionId)),
    updateUserObject: (payload) => dispatchEvent(updateUserObject(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AddressContainer));

import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  updateAboutUs,
  updatePartners,
  updatePrivacyPolicy,
  updateTandC,
} from "../../actions/FooterAction";
import {
  getCMS,
  getFooter,
  MenuItem,
  subscribeLetter,
} from "../../actions/internalActions/LandingActions";
import appStoreIcon from "../../assets/images/App-Store.png";
import facebookIcon from "../../assets/images/FB.svg";
import mailIcon from "../../assets/images/Mail.svg";
import playStoreIcon from "../../assets/images/play-Store.png";
import twitterIcon from "../../assets/images/Twitter.svg";
import { isValidEmail } from "../../utils/common";
import toast from "../common/commonToast";
import _ from "lodash";
import { Modal, Input } from "antd";
import "antd/lib/modal/style/index.css";
import "antd/lib/button/style/index.css";
import "antd/lib/input/style/index.css";

const paths = {
  "chaiPoint.com": {
    "Contact Us": "Contact",
    "Privacy Policy": "PrivacyPolicy",
    "Terms  & Conditions": "TearmAndConditions",
    Partners: "Partners",
    "Help/FAQs": "FAQ",
    "About us": "aboutus",
    Careers: "Careers",
  },

  "shop.chaipoint.com": {
    "Contact Us": "Contact",
    "Privacy Policy": "PrivacyPolicy",
    "Terms  & Conditions": "TearmAndConditions",
    Partners: "Partners",
    "Help/FAQs": "FAQ",
    "About us": "aboutus",
    Careers: "Careers",
  },

  "order.chaipoint.com": {
    "Contact Us": "Contact",
    "Privacy Policy": "PrivacyPolicy",
    "Terms  & Conditions": "TearmAndConditions",
    Partners: "Partners",
    "Help/FAQs": "FAQ",
    "About us": "aboutus",
    Careers: "Careers",
  },
};

class Footer extends Component {
  state = {
    BU: "",
    footerData: {
      facebook: "https://www.facebook.com/chaipoint",
      twitter: "https://twitter.com/Chai_Point",
      instagram: "https://www.instagram.com/chaipoint/",
      business_time_info: "9 AM to 10 PM",
      contact: "8880141000",
      google_play_url: "",
      email: "feedback@chaipoint.com",
    },
    TandC: [],
    PrivacyPolicy: [],
    FooterMenuitem: [],
    pathway: [
      { name: "Contact Us", path: "Contact" },
      { name: "Privacy Policy", path: "PrivacyPolicy" },
      { name: "Terms  & Conditions", path: "TearmAndConditions" },
      { name: "Partners", path: "Partners" },
      { name: "Help/FAQs", path: "FAQ" },
      { name: "About us", path: "aboutus" },
    ],
  };

  componentDidMount = () => {
    this.props.getFooter((data) => {
      if (data) {
        this.setState({
          footerData: data,
        });
      }
    });

    var BU = "order.chaipoint.com";
    var state = window.location.href.split("/");
    var index = state.length - 1;
    if (state[index] == "home") {
      BU = "chaiPoint.com";
    } else if (state[index] == "Shop") {
      BU = "shop.chaipoint.com";
    } else {
      BU = "order.chaipoint.com";
    }

    this.props.MenuItem(BU, (data) => {
      if (data) {
        console.log("Menuitem", data.data);
        this.setState({
          FooterMenuitem: data.data,
          BU,
        });
      }
    });
  };

  redirctOnMenu = (menu) => {
    switch (menu) {
      case "Newsletter":
        Modal.info({
          title: "Please enter your email address",
          content: <Input></Input>,
          onOk: () => true,
        });
        break;
      default:
        this.props.history.push(`/${_.get(paths, [this.state.BU, menu])}`);
        break;
    }
  };

  updateInputValue = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  submitToSubscribeLetter = () => {
    //need to check if email is valid or not
    if (this.validate()) {
      this.props.subscribeLetter(this.state.subscribe, (responseMessage) => {
        toast.success(responseMessage, {});
      });
    } else {
      toast.error("Please enter valid email address", {});
    }
  };
  validate = () => {
    return this.state.subscribe && isValidEmail(this.state.subscribe);
  };
  routeCheck = () => {};

  redirect = (ro) => {
    var route;
    console.log("dddd", ro);
    this.state.pathway.map((dd) => {
      console.log("dddd0", dd.name);
      if (dd.name == ro) {
        console.log("dddd1", dd.name);
        route = dd.path;
      }
    });
    console.log("route", route);
    var url = window.location.href.split("/");
    this.props.history.push({ pathname: `/${route}`, state: { url: url } });
  };

  render() {
    const legalMenus = _.filter(
      _.get(
        _.find(this.state.FooterMenuitem, ["name", "Legal"]),
        "child_menus"
      ),
      (each) => each.status === "A"
    );

    const companyMenus = _.filter(
      _.get(
        _.find(this.state.FooterMenuitem, ["name", "Company"]),
        "child_menus"
      ),
      (each) => each.status === "A"
    );

    return (
      <>
        <section className="footer-details">
          <div className="container">
            <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="col-md-5 col-sm-5 col-xs-12">
                  <div className="subs-nl">
                    <h3 className="title">Subscribe to our Newsletter</h3>
                    <p>
                      We'll send you news and special offers <br />
                      twice a month
                    </p>
                    <form action="#">
                      <input
                        type="text"
                        className="email-nl"
                        placeholder="Email Address"
                        value={this.state.subscribe}
                        name="subscribe"
                        placeholder="Email to subscribe"
                        onChange={this.updateInputValue}
                      />
                      <input
                        type="button"
                        className="subs-btn"
                        className="subs-btn"
                        value="Subscribe"
                        onClick={this.submitToSubscribeLetter}
                      />
                    </form>
                    <p>
                      We deliver from {this.state.footerData.business_time_info}
                    </p>
                    <p>Call us on {this.state.footerData.contact}</p>
                  </div>
                </div>
                <div className="col-md-2 col-sm-2 col-xs-12">
                  <div className="company">
                    <h3 className="title">Company</h3>
                    <ul>
                      {_.map(companyMenus, (data) => (
                        <>
                          <li
                            // onClick={() => {
                            //   this.props.history.push(
                            //     `/${_.get(paths, [this.state.BU, data.name])}`
                            //   );
                            // }}
                            onClick={() => this.redirctOnMenu(data.name)}
                          >
                            {data.name}
                          </li>
                        </>
                      ))}
                    </ul>
                  </div>
                </div>
                <div className="col-md-2 col-sm-2 col-xs-12">
                  <div className="legal">
                    <h3 className="title">Legal</h3>
                    <ul>
                      {_.map(legalMenus, (data) => (
                        <>
                          <li
                            onClick={() => {
                              this.props.history.push(
                                `/${_.get(paths, [this.state.BU, data.name])}`
                              );
                            }}
                          >
                            {data.name}
                          </li>
                        </>
                      ))}
                    </ul>
                  </div>
                </div>
                <div className="col-md-3 col-sm-3 col-xs-12">
                  <div className="apps">
                    <h3 className="title">Apps</h3>
                    <img
                      href={this.state.footerData.google_play_url}
                      src={playStoreIcon}
                      alt="Play store"
                    />
                    <img src={appStoreIcon} alt="Apps store" />
                  </div>
                  <div className="follow-us">
                    <h3 className="title">Follow Us</h3>
                    <ul>
                      <li>
                        <a href={this.state.footerData.facebook}>
                          <img src={facebookIcon} alt="facebook" />
                        </a>
                      </li>
                      <li>
                        <img
                          href={this.state.footerData.twitter}
                          src={twitterIcon}
                          alt="twitter"
                        />
                      </li>
                      <li>
                        <a href={`mailto:${this.state.footerData.email}`}>
                          <img src={mailIcon} alt="mail" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="down-section">
                  <p>
                    © Mountain Trail Foods Pvt Ltd version build on 30th
                    March,2021_v17.00
                    <span>Powered by Carmatec</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="footer-details">
          <div className="container-fluid">
            <div className="row"></div>
          </div>
        </section>
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const isLoggedin = reducerObj.userObject.isLoggedIn;
  return { isLoggedin };
};

export default connect(mapStateToProps, {
  getFooter,
  subscribeLetter,
  getCMS,
  updateTandC,
  updatePrivacyPolicy,
  updateAboutUs,
  updatePartners,
  MenuItem,
})(withRouter(Footer));
Footer.propTypes = {
  getFooter: PropTypes.func.isRequired,
};

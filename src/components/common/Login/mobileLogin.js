import React from 'react'
import  Login from "./login"
import {isMobileOnly} from "react-device-detect"
import {Redirect} from "react-router-dom"
function mobileLogin() {
    return (
        
        <div>
            {isMobileOnly?
           <div>
            <Login />
            </div>
            :
            <Redirect to="/"/>  
            }
             
        </div>
    )
}

export default mobileLogin

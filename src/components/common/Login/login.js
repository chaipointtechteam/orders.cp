import React, { Component } from "react";
import LoginImage from "../../../assets/images/login-illustration1.svg";
import { generateOtp, verifyOtp } from "../../../actions/LoginAction";
import { updateUserObject } from "../../../actions/UserAction";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { applicationId, oraginzationId } from "../../../utils/constants";
import { withRouter } from "react-router";
import { header } from "../../../utils/apiUtil";
import toast from "../../common/commonToast";
import { timer } from "rxjs";
import { getProfile } from "../../../actions/LoginAction";
import { isMobileOnly } from "react-device-detect";
import { throwStatement } from "@babel/types";
class Login extends Component {
  constructor( props){
    super()
    this.myRef=React.createRef()
    this.updateStep = this.updateStep.bind(this)
  }
  state = {
    step: 1,
    phoneNo: "",
    otp: "",
    isReadOnly: false,
    timer: 30,
    isEnable: false,
  };

  componentDidMount(){
    setTimeout( () => {
             this.myRef.current.focus();
      }, 1)
  }
  updateStep = (e) => {
    e.preventDefault();
    var patt = new RegExp(/^[6-9]/i);
    //call action to send
    if (
      this.state.phoneNo &&
      this.state.phoneNo !== "" &&
      patt.test(this.state.phoneNo) &&
      this.state.phoneNo.length === 10
    ) {
      this.setState({ timer: 30, isEnable: false });
      this.props.generateOtp(
        {
          phoneNumber: `91${this.state.phoneNo}`,
          applicationId: applicationId,
        },
        this.setState({
          step: 2,
          isReadOnly: true,
        })
      );
      var m = setInterval(() => {
        if (this.state.timer > 0) {
          this.setState({
            timer: this.state.timer - 1,
          });
        } else {
          this.setState({ isEnable: true });
          clearInterval(m);
        }
      }, 1000);
    } else {
      if (this.state.phoneNo === "") {
        toast.error("Please enter Phone number", { closable: true });
      } else if (this.state.phoneNo.length !== 10) {
        toast.error("Phone no. must have 10 digit", { closable: true });
      } else if (!patt.test(this.state.phoneNo)) {
        toast.error("Phone no. must start from 6,7,8,9 ", { closable: true });
      }
      //  else if (this.state.phoneNo === "") {
      //   toast.error("Please enter Phone number")
      // }

      // toast.error("Please enter valid phone no", {});
    }
  };
   

  goBack=()=>{
    this.props.history.goBack();
  }
  verifyOtp = (e) => {
    e.preventDefault();
    const { userObject } = this.props;
    if (
      this.state.otp &&
      this.state.otp !== "" &&
      this.state.otp.length === 6 &&
      this.state.phoneNo &&
      this.state.phoneNo !== "" &&
      this.state.phoneNo.length === 10
    ) {
      this.props.verifyOtp(
        {
          phoneNumber: `91${this.state.phoneNo}`,
          applicationId: applicationId,
          organisationId: oraginzationId,
          otp: this.state.otp,
        },
        (responseData) => {
          if (responseData && !responseData.errorCode) {
            var phone = this.state.phoneNo;
            var session = responseData.sessionId;
            //old header to get update
            const newHeader = header;
            newHeader.sessionId = responseData.sessionId;
            //save no in user object as well
            this.props.updateUserObject({
              phoneNo: `91${this.state.phoneNo}`,
              email: responseData.email,
              name: responseData.name,
              customerId: responseData.customerId,
              headerSessionId: responseData.sessionId,
              newHeader: newHeader,
            });
            //close the modal
            this.props.closePopUp();
            toast.success("Welcome to Chai point", { closable: true });
            if (responseData?.email === null || responseData?.name === null || responseData?.email === "" || responseData?.name === "") {
              this.props.history.push("/profile")
              toast.warning(
                "Please edit your profile details",
                { closable: true }
                
              );
              
            }
            this.props.getProfile(`${phone}`, session).then((res) => {
              if (res) {
                this.props.updateUserObject(res);
              }
            });
           
            //redirect to product catlog page
            //check if location and shop and channel selected then only redirect to products page else keep it on here only with header changes
            if (
              Object.keys(this.props.selectedChannel).length !== 0 &&
              Object.keys(this.props.selectedLocation).length !== 0 &&
              Object.keys(this.props.selectedStore).length !== 0
            ) {
              //call to get the profile
            //   if (isMobileOnly) {
            //     this.props.history.push({
            //       pathname: "/products",
            //     })
            // }
            }
          }else if(responseData.errorCode){
            toast.warning(
              `${responseData.errorCode} ${responseData.message}`,
              { closable: true }
            );
          } else {
            if (responseData?.email === null && responseData?.name === null) {
              
              window.location.href = "/profile";
            }
            toast.warning(
              "Please select location and store to see the catalog",
              { closable: true }
            );
          
            if (isMobileOnly) {
              this.props.history.goBack();
              // this.goBack()
          }
          
            
          }
        }
      );
    } else {
      if (this.state.otp.length !== 6 || this.state.otp === "") {
        toast.error("otp must have 6 digit", { closable: true });
      } else {
        toast.error("Please enter valid phone no and otp", { closable: true });
      }
      // toast.error("Please enter valid phone no and otp", {});
    }
    //success
    //user logged in or sign up successfully
    //failure
    //as to enter valid otp
  };
  isNum = (evt) => {
    var ch = String.fromCharCode(evt.which);
    if (!/[0-9]/.test(ch)) {
      evt.preventDefault();
    }
  };

  updateInputValue = (event) => {
    // this.setState({ [event.target.name]: event.target.value });
    if (event.target.name === "phoneNo") {
      if (event.target.value.length <= 10) {
        this.setState({ phoneNo: event.target.value });
      }
    }
    if (event.target.name === "otp") {
      if (event.target.value.length <= 6) {
        this.setState({ otp: event.target.value });
      }
    }
  };
  redirectToSignUp = () => {
  this.props.history.push("/signup")
}
  render() {
    return (
      <>
        <img src={LoginImage} alt="Chai point" />
        {!isMobileOnly ? (
          <div class="modal-header">
            <button
              onClick={this.props.closePopUp}
              type="button"
              class="close"
              data-dismiss="modal"
              aria-hidden="true"
            >
              &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">
              Login
            </h4>
          </div>
        ) : (
          <>
            <div className="login">
              <h4 class="Login-title" id="myModalLabel">
                Login
              </h4>
            </div>
            <div
              style={{
                marginLeft: "10px",
                fontSize: "12px",
                color: "#5f5e60",
                marginTop: "5px",
              }}
            >
              Enter Your Phone No. to continue{" "}
            </div>
          </>
        )}
        <div class="modal-body">
          <div class="otp-verification">
            <form 
              method="post" 
              id="veryfyotp" 
              action="" 
              onKeyUp={(e)=>{
                if(e.keyCode===13){
                  if(this.state.step === 2){
                    this.verifyOtp(e)
                  }else{
                    this.updateStep(e)
                  }
                }
              }}>
              <div class="row">
                <div class="form-group">
                  <span>+91</span>
                  <span> |</span>
                  <input
                    type="Number"
                    class="form-control nmaeid"
                    name="phoneNo"
                    placeholder="Phone Number"
                    ref={input => input && input.focus()}
                    ref={this.myRef}
                    required=""
                    onclick="this.select()"
                    onKeyPress={(event) => this.isNum(event)}
                    onChange={this.updateInputValue}
                    readOnly={this.state.isReadOnly}
                    value={this.state.phoneNo}
                  />
                </div>
                {this.state.step === 1 && (
                  <button
                    onClick={this.updateStep}
                    type="submit"
                    class="btn btn-primary"
                  >
                    Send OTP
                  </button>
                )}

                {this.state.step === 2 && (
                  <>
                    <div class="form-group ot">
                      <input
                        type="Number"
                        class="form-control"
                        name="otp"
                        ref={this.myRef}
                        ref={input => input && input.focus()}
                        onKeyPress={(event) => this.isNum(event)}
                        value={this.state.otp}
                        maxLength={6}
                        onChange={this.updateInputValue}
                        placeholder="Enter OTP"
                        required=""
                      />
                    </div>
                    <button
                      onClick={this.verifyOtp}
                      // type="submit"
                      type="submit"
                      class="btn btn-primary"
                    >
                      Verify OTP
                    </button>
                  </>
                )}
              </div>
            </form>
            {this.state.step === 1 && (
              <>
              <div className="showFooter" ><p>
                New to Chai Point?{" "}
                <span onClick={this.redirectToSignUp}>
                  create Account
                </span>
                </p>
                {isMobileOnly?<span onClick={()=>this.props.history.goBack()} style={{float:"right",marginRight:"20px",marginTop:"20px",color:"#12767f"}}>
             {" "}
          GO BACK
           </span>:""}
              </div>
               <div className="detailHeader" ><p>
               New to Chai Point?{" "}
               <span onClick={this.props.closePopUpAndOpenSignUp}>
                 create Accounts
               </span>
               </p>
               {isMobileOnly?<span onClick={()=>this.props.history.goBack()} style={{float:"right",marginRight:"20px",marginTop:"20px",color:"#12767f"}}>
            {" "}
         GO BACK
          </span>:""}
                </div>
                </>
            )}
            {this.state.step === 2 && (
              <p>
                {this.state.isEnable ? (
                  <span onClick={this.state.isEnable ? this.updateStep : ""}>
                    Resend OTP{" "}
                  </span>
                ) : (
                  <>
                    <span style={{ color: "gray", cursor: "wait" }}>
                      Resend OTP{" "}
                      {!this.state.isEnable ? `(${this.state.timer})` : ""}
                    </span>
                  </>
                )}
              </p>
            )}
            {this.state.step === 2 && (
              <div className="GOBACK">
                <p>
                  <span
                    onClick={() =>
                      this.setState({
                        step: 1,
                        isReadOnly: false,
                      })
                    }
                  >
                    {" "}
                    GO BACK
                  </span>
                </p>
              </div>
            )}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const selectedLocation =
    reducerObj.selectedLocation.selectedLocation.selectedLocation;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  return { selectedChannel, selectedLocation, selectedStore, userObject };
};
export default connect(mapStateToProps, {
  getProfile,
  generateOtp,
  verifyOtp,
  updateUserObject,
})(withRouter(Login));

Login.propTypes = {
  generateOtp: PropTypes.func.isRequired,
  verifyOtp: PropTypes.func.isRequired,
  updateUserObject: PropTypes.func.isRequired,
};

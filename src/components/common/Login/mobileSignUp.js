import React from 'react'
import {isMobileOnly} from "react-device-detect"
import {Redirect} from "react-router-dom"
import SignUp from "./signUp"
function mobileSignUp() {
    return (
        <div>
             {isMobileOnly?
           <div>
            <SignUp />
            </div>
            :
            <Redirect to="/"/>
            
            }
        </div>
    )
}

export default mobileSignUp

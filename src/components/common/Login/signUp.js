import React, { Component } from "react";
import SignUpImage from "../../../assets/images/login-illustration1.svg";
import {
  generateOtp,
  verifyOtp,
  updateProfile,
} from "../../../actions/LoginAction";
import { updateUserObject } from "../../../actions/UserAction";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { applicationId, oraginzationId } from "../../../utils/constants";
import { withRouter } from "react-router";
import toast from "../commonToast";
import { header } from "../../../utils/apiUtil";
import { isValidEmail } from "../../../utils/common";
import { isMobileOnly } from "react-device-detect";

class SignUp extends Component {
  constructor( props){
    super()
    this.myRef=React.createRef()
  }
  state = {
    step: 1,
    phoneNo: "",
    otp: "",
    name: "",
    email: "",
    timer: 30,
    isEnable: false,
    isReadOnly: false,
  };
  
  componentDidMount(){
    setTimeout( () => {
             this.myRef.current.focus();
      }, 1)
  }

  

  updateStep = () => {
    var patt = new RegExp(/^[6-9]/i);
    //call action to send
    if (
      this.state.phoneNo &&
      this.state.phoneNo !== "" &&
      patt.test(this.state.phoneNo) &&
      this.state.phoneNo.length === 10 &&
      this.state.name !== "" &&
      this.state.email !== "" &&
      isValidEmail(this.state.email)
    ) {
      this.setState({ timer: 30, isEnable: false });
      this.props.generateOtp(
        {
          phoneNumber: `91${this.state.phoneNo}`,
          applicationId: applicationId,
        },
        this.setState({
          step: 2,
          isReadOnly: true,
        })
      );
      var m = setInterval(() => {
        if (this.state.timer > 0) {
          this.setState({
            timer: this.state.timer - 1,
          });
        } else {
          this.setState({ isEnable: true });
          clearInterval(m);
        }
      }, 1000);


    } else {
      if (this.state.phoneNo == "" && this.state.name == "" && !isValidEmail(this.state.email)) {
        toast.error("Please enter form details", { closable: true });
      } else {
        if (this.state.phoneNo == "") {
          toast.error("Please enter phone number", { closable: true });
        }
        else if (this.state.phoneNo.length !== 10) {
          toast.error("Phone number must have  10 digit", { closable: true });
        }
        else if (!patt.test(this.state.phoneNo)) {
          toast.error("Phone number must start from 6,7,8,9", { closable: true });
        }

        if (this.state.name == "") {
          toast.error("Please enter proper name", { closable: true });
        }
        if (!isValidEmail(this.state.email)) {
          toast.error("Please enter valid email", { closable: true });
        }
        /* else{
          toast.error("Please enter valid details", {});
        } */
      }
    }
  };
  isNum=(evt)=>{
    var ch = String.fromCharCode(evt.which);
    if(!(/[0-9]/.test(ch))){
        evt.preventDefault();
    }

  }

  verifyOtp = () => {
    if (
      this.state.otp &&
      this.state.otp !== "" &&
      this.state.otp.length === 6 &&
      this.state.phoneNo &&
      this.state.phoneNo !== "" &&
      this.state.phoneNo.length === 10
    ) {
      this.props.verifyOtp(
        {
          phoneNumber: `91${this.state.phoneNo}`,
          applicationId: applicationId,
          organisationId: oraginzationId,
          otp: this.state.otp,
        },
        (responseData) => {
          //old header to get update
          if(responseData.errorCode){
            const {errorCode, message} = responseData;
            toast.error(`${errorCode} ${message}`, {closable: true});
            return
          }
          const newHeader = header;
         
          newHeader.sessionId = responseData.sessionId;
         
          //save no in user object as well
          this.props.updateUserObject({
            phoneNo: `91${this.state.phoneNo}`,
            name: this.state.name,
            email: this.state.email,
            headerSessionId: responseData.sessionId,
            newHeader: newHeader,
          });
          //close the modal
          if(isMobileOnly){
            this.props.history.push({
             pathname: "/products",
           });
         }else{
           this.props.closePopUp();
         }
 
          toast.success("Welcome to chai point", {closable: true});

          //redirect to product catlog page
          this.props.history.push({
            pathname: "/products/",
          });
          this.props.updateProfile(
            newHeader,
            { name: this.state.name, email: this.state.email },
            (newresponse) => {
              // toast.success("Profile updated successfully", {});
            }
          );
        }
      );
    } else {
      if (this.state.otp.length !== 6 || this.state.otp === "") {
        toast.error("otp must have 6 digit", {closable: true});
      } else {
        toast.error("Please enter valid phone no and otp", {closable: true});
      }
      // toast.error("Please enter valid phone No and otp", {});
    }
    //success
    //user logged in or sign up successfully
    //failure
    //as to enter valid otp
  };

  updateInputValue = (event) => {
    ["name", "email"].map(i => {
      if (event.target.name === i)
        this.setState({ [event.target.name]: event.target.value });
    })

    if (event.target.name === "phoneNo") {
      if (event.target.value.length <= 10) {
        this.setState({ phoneNo: event.target.value })
      }
    }
    if (event.target.name === "otp") {
      if (event.target.value.length <= 6) {
        this.setState({ otp: event.target.value })
      }
    }

  };
  redirectToLogin = () => {
    this.props.history.push("/login")
  }
  render() {

    return (
      <>
        <img src={SignUpImage} alt="Sign Up" />
       {!isMobileOnly?<div class="modal-header">
          <button
            type="button"
            class="close"
            onClick={this.props.closePopUp}
            data-dismiss="modal"
            aria-hidden="true"
          >
            &times;
          </button>
          {this.state.step === 2 && (
            <h4 class="modal-title" id="mysignModalLabel">
              Verify Mobile Number
            </h4>
          )}
          {this.state.step === 1 && (
            <h4 class="modal-title" id="mysignModalLabel">
              Sign Up
            </h4>
          )}
        </div>:<div className="signup">
        {this.state.step === 2 && (
            <h4 class="modal-title" id="mysignModalLabel">
              Verify Mobile Number
            </h4>
          )}
          {this.state.step === 1 && (
            <h4 class="modal-title" id="mysignModalLabel">
              Sign Up
            </h4>
          )}
          </div>}
        <div class="modal-body">
          <div class="otp-verification signup">
            <form method="post" id="veryfysignup" action="" onKeyUp={(e)=>e.keyCode===13 &&this.updateStep(e)}>
              <div class="row">
                <div class="form-group">
                  <span>+91</span>
                  <input
                    type="Number"
                    class="form-control"
                    name="phoneNo"
                    onKeyPress={(event)=>this.isNum(event)}
                    ref={this.myRef}
                    placeholder="Phone Number"
                    value={this.state.phoneNo}
                    onChange={this.updateInputValue}
                    readOnly={this.state.isReadOnly}
                    
                  />
                </div>
                {this.state.step === 1 && (
                  <>
                    <div class="form-group">
                      <input
                        type="text"
                        class="form-control"
                        name="name"
                        value={this.state.name}
                        onChange={this.updateInputValue}
                        placeholder="Name"
                        maxLength={50}
                      />
                    </div>
                    <div class="form-group">
                      <input
                        type="email"
                        class="form-control"
                        name="email"
                        value={this.state.email}
                        placeholder="Email Address"
                        onChange={this.updateInputValue}
                        maxLength={200}
                      />
                    </div>
                    <button
                      onClick={this.updateStep}
                      type="button"
                      class="btn btn-primary"
                    >
                      Create Account
                    </button>
                  </>
                )}
                {this.state.step === 2 && (
                  <>
                    <div class="form-group">
                      <input
                        type="number"
                        class="form-control"
                        name="otp"
                        onKeyPress={(event)=>this.isNum(event)}

                        placeholder="One Time Password"
                        value={this.state.otp}
                        onChange={this.updateInputValue}
                      />
                    </div>
                    {/* <p class="otp-resendd" >Resend OTP</p> */}

                    <button
                      type="button"
                      class="btn btn-primary"
                      onClick={this.verifyOtp}
                    >
                      Verify OTP
                    </button>
                  </>
                )}
              </div>
            </form>
            {this.state.step === 1 &&
              (
              <>
              <div className="showFooter">
              <p
                    class="login-account"
                    onClick={this.redirectToLogin}
               
              >
                <span>Login into your account</span>
              </p>
              {isMobileOnly?<span onClick={()=>this.props.history.goBack()} style={{float:"right",marginRight:"20px",marginTop:"20px",color:"#12767f"}}>
              {" "}
           GO BACK
            </span>:""}
            </div>  
              <div className="detailHeader">
              <p
                class="login-account"
                onClick={this.props.closePopUpAndOpenLogin}
              >
                <span>Login into your account</span>
              </p>
              {isMobileOnly?<span onClick={()=>this.props.history.goBack()} style={{float:"right",marginRight:"20px",marginTop:"20px",color:"#12767f"}}>
              {" "}
           GO BACK
            </span>:""}
                </div>
                </>
              )}
            {this.state.step === 2 && (
              <p>
                {this.state.isEnable ? (
                  <span onClick={this.state.isEnable ? this.updateStep : ""}>
                    Resend OTP{" "}

                  </span>
                ) : (
                  <span style={{ color: "gray", cursor: "wait" }}>Resend OTP {!this.state.isEnable ? `(${this.state.timer})` : ""}</span>
                )}
              </p>
            )}
{this.state.step === 2 && (
              <div className="GOBACK">
                <p>
                  <span
                    onClick={() =>
                      this.setState({
                        step: 1,
                        isReadOnly: false,
                      })
                    }
                  >
                    {" "}
                    GO BACK
                  </span>
                </p>
              </div>
            )}

            {/* 
              {this.state.step === 2 &&
            (<p
              class="login-account"
              onClick={this.updateStep}
            >
              <span>{this.state.isEnable?"Resend OTP":"" } ({this.state.time})</span>
            </p>)} */}

          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  return {};
};
export default connect(mapStateToProps, {
  generateOtp,
  verifyOtp,
  updateUserObject,
  updateProfile,
})(withRouter(SignUp));

SignUp.propTypes = {
  generateOtp: PropTypes.func.isRequired,
  verifyOtp: PropTypes.func.isRequired,
  updateUserObject: PropTypes.func.isRequired,
  updateProfile: PropTypes.func.isRequired,
};

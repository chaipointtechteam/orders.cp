import { Helmet } from 'react-helmet';
const ReactHelment =(props)=>{
return(
    <>
<Helmet>
    <title>{props.title}</title>
    <meta name="description" content={props.description} />
</Helmet>
    </>
)
}
export default ReactHelment
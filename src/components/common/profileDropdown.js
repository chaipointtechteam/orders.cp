import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { Dropdown } from "react-bootstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
// import { connect } from "react-redux";
import { getProfile } from "../../../src/actions/LoginAction";
import { updateUserObject } from "../../../src/actions/UserAction";
import { emptyCartItem, resetCartObject } from "../../actions/CartAction";
import { clearAuth, logout } from "../../actions/LoginAction";
import { emptyMenuItemList, resetMenuItemList } from "../../actions/MenuItemsAction";
import {
  resetChannel, resetLocation, resetStore,
  updateSelectedChannel, updateSelectedLocation, updateSelectedStore
} from "../../actions/StoreListAction";
import { resetUserObject } from "../../actions/UserAction";
import { header } from "../../utils/apiUtil";
import ProductLoginHeader from "../common/ProductLoginHeader";
import toast from "./commonToast";


class ProfileDropdown extends Component {
  componentDidMount = () => {
    const { userObject } = this.props;
    if (!(userObject.headerSessionId === "")) {
      const { phoneNo, headerSessionId } = userObject;
      this.props.getProfile(`${phoneNo}`, headerSessionId).then((res) => {
        if (res) {
          this.props.updateUserObject(res);
        }

      });
    }
  };
  resetTheCart = () => {

    this.props.cartObject.cartItems.forEach((cart) => {
      this.props.emptyCartItem(this.props.cartObject.cartItems, cart);
    });
    // this.props.resetMenuItemList(this.props.menuItemList);

  };
  redirectByAddress = () => {
    this.props.history.push({
      pathname: `/profile`,
      state: { Tab: 1 }
    });
  }

  redirectToProfile = () => {
    this.props.history.push("/profile");
  };
  logout = () => {
    const { userObject } = this.props;
    const newHeader = header;
    newHeader.sessionId = userObject.headerSessionId;
    this.props.logout(newHeader, (data) => {
      if (data) {
        this.props.clearAuth();
        this.resetTheCart()
        this.props.resetUserObject();
        this.props.emptyMenuItemList();
        this.props.resetCartObject();
        this.props.resetStore();
        this.props.resetChannel();
        this.props.resetLocation();
        this.props.updateSelectedStore({
          selectedStore: {},
        });
        this.props.updateSelectedChannel({
          selectedChannel: {},
        });
        this.props.updateSelectedLocation({
          selectedLocation: {},
        });
        localStorage.clear();
        this.props.history.push("/");
        toast.success("Logged out successfully", {});
      } else {
        this.resetTheCart()
        this.props.clearAuth();
        this.props.resetUserObject();
        this.props.resetUserObject();
        this.props.emptyMenuItemList();
        this.props.resetCartObject();
        this.props.resetStore();
        this.props.resetChannel();
        this.props.resetLocation();
        this.props.updateSelectedStore({
          selectedStore: {},
        });
        this.props.updateSelectedChannel({
          selectedChannel: {},
        });
        this.props.updateSelectedLocation({
          selectedLocation: {},
        });
        localStorage.clear();
        this.props.history.push("/");
        toast.error(
          "Something went wrong while loging out, please contact admin or try after some time",
          {}
        );
      }
    });
    //reset the cart
    //reset the user object
    //logout the user
    //redirect to home page
  };
  render() {
    const { userObject } = this.props;
    return (
      <>
        <div class="col-md-8 col-sm-3 col-xs-12 u-p">
          {userObject.headerSessionId === "" && <ProductLoginHeader />}
          {userObject.headerSessionId !== "" && (
            <div class="user-profile">
              <Dropdown>
                <Dropdown.Toggle>
                  <label
                    type="text"
                  >
                    <span>
                      <img
                        src={
                          require("../../assets/images/Profile_Green.svg")
                            .default
                        }
                      />
                    </span>
                    <span class="user" title={userObject.name}>{userObject.name}</span>
                  </label>
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <div class="arrow-profile"></div>
                  <ul>
                    <li
                      onClick={() => this.props.history.push("/")
                      }>
                      <Dropdown.Item >Home</Dropdown.Item>
                    </li>
                    <li onClick={this.redirectToProfile}>
                      <Dropdown.Item>Profile</Dropdown.Item>
                    </li>
                    <li onClick={this.redirectToProfile}>
                      <Dropdown.Item>Orders</Dropdown.Item>
                    </li>
                    <li onClick={this.redirectByAddress}>
                      <Dropdown.Item>Address</Dropdown.Item>
                    </li>
                    {/* <li onClick={this.redirectToProfile}>
                      <Dropdown.Item href="/profile">Profile</Dropdown.Item>
                    </li> */}
                    {/* <li onClick={()=>sessionStorage.clear()}>
                      <Dropdown.Item >Checkout</Dropdown.Item>
                    </li> */}
                    {/* <li>
                      <Dropdown.Item href="">Favourites</Dropdown.Item>
                    </li> */}
                    <li>
                      <Dropdown.Item href="" onClick={this.logout}>
                        Logout
                      </Dropdown.Item>
                    </li>
                  </ul>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          )}
        </div>

        {/* <div class="col-md-3 col-sm-3 col-xs-12 u-p">
<div class="user-profile">
   <div class="dropdown">
       <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><span><img src="assets/images/Profile_Green.svg"></span>
       <span class="user">Alexander</span></button>
       <ul class="dropdown-menu">
         <li><a href="#">Profile</a></li>
         <li><a href="#">Orders</a></li>
         <li><a href="#">Favourites</a></li>
         <li><a href="#">Logout</a></li>
       </ul>
     </div>

</div>
</div> */}
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const userObject = reducerObj.userObject.userObject;
  return { userObject, cartObject };
};
export default connect(
  mapStateToProps,

  {
    resetMenuItemList,
    getProfile,
    updateUserObject,
    resetUserObject,
    resetCartObject,
    resetStore,
    resetChannel,
    emptyMenuItemList,
    updateSelectedChannel,
    updateSelectedStore,
    updateSelectedLocation,
    logout,
    resetLocation,
    emptyCartItem,
    clearAuth
  }
)(withRouter(ProfileDropdown));

ProfileDropdown.propTypes = {
  resetUserObject: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
};

import React, { Component } from "react";
// import LoginModal from "../landing/LoginModal";
// import SignUpModal from "../landing/SignUpModal";
import { updateUserObject } from "../../actions/UserAction";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { withRouter } from "react-router";
import isMobile, { isMobileOnly } from "react-device-detect"
// import { withRouter } from "react-router";

class LoginHeader extends Component {
  openLoginPopup = () => {
    // this.setState({ showLoginPopup: true });
    this.props.updateUserObject({ showLoginPopup: true });
  };

  openSignUpPopup = () => {
    this.props.updateUserObject({ showSignUpPopup: true });
  };

  closePopUp = () => {
    this.props.updateUserObject({ showLoginPopup: false, showSignUpPopup: false });
  };

  closePopUpAndOpenLogin = () => {
    this.props.updateUserObject({ showLoginPopup: true, showSignUpPopup: false });
  };

  closePopUpAndOpenSignUp = () => {
    this.props.updateUserObject({ showLoginPopup: false, showSignUpPopup: true });
  };

  login=()=>{
    // console.log("sdfgksdfklhjsldfkhjsdkljhklsdffh")
      this.props.history.push("/login")
  }

  SignUp=()=>{
    this.props.history.push("/signUp")
  }
 
  render() {
    return (
      <>
        <div className="col-md-6 col-sm-6 col-xs-12">
          <div className="login-service">
            <button onClick={isMobileOnly?this.login:this.openLoginPopup} className="service-login">
              login
            </button>
           <button onClick={isMobileOnly?this.SignUp:this.openSignUpPopup} className="service-signup">
              sign up
            </button>
          </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, {
  updateUserObject,
})(withRouter(LoginHeader));

LoginHeader.propTypes = {
  updateUserObject: PropTypes.func.isRequired,
};

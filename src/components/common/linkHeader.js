import React, { Component } from "react";
import { updateUserObject } from "../../actions/UserAction";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import LoginHeader from "./loginHeader";
import CartProfileHeader from "./cartProfileHeader";

class LinkHeader extends Component {
  render() {
    const { userObject, cartObject } = this.props;
    return (
      <>
        {userObject.headerSessionId === "" && <LoginHeader />}
        {userObject.headerSessionId !== "" && (
          <CartProfileHeader cartObject={cartObject} />
        )}
      </>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, {
  updateUserObject,
})(LinkHeader);

LinkHeader.propTypes = {
  updateUserObject: PropTypes.func.isRequired,
};

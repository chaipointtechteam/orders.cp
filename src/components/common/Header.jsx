import React from "react";
import { useHistory, Link } from "react-router-dom";
import _ from "lodash";
import Logo from "../../assets/images/Chaipoint-01.svg";

const routeMappings = {
  "About Us": "aboutus",
  Partners: "Partners",
  "Store Locator": "",
  Careers: "career-landing",
  Blog: "",
  "F.A.Qs": "FAQ",
  "Contact Us": "Contact",
  Company: "",
  Legal: "",
};

const Header = (props) => {
  const history = useHistory();
  const { menuItems, selectedItem } = props;

  const headerMenuItems = menuItems?.filter((ele) => ele.header_menu === "Yes");
  const topItems = headerMenuItems?.filter((ele) => ele.sequence < 4);
  const bottomItems = headerMenuItems?.filter((ele) => ele.sequence > 3);

  return (
    <div className="header-wrapper">
      <header className="header-mother-page">
        {_.map(_.sortBy(topItems, ["sequence"]), (ele) => (
          <Link
            to={`/${_.get(routeMappings, [ele.name], "")}`}
            className={
              selectedItem === ele.name
                ? "selected-header-item header-item"
                : "header-item"
            }
          >
            {ele.name}
          </Link>
        ))}
      </header>
      <div className="logo">
        <img className="logo-img" src={Logo} />
      </div>
      <header className="header-mother-page">
        {_.map(_.sortBy(bottomItems, ["sequence"]), (ele) => (
          <Link
            to={`/${_.get(routeMappings, [ele.name], "")}`}
            className={
              selectedItem === ele.name
                ? "selected-header-item header-item"
                : "header-item"
            }
          >
            {ele.name}
          </Link>
        ))}
      </header>
    </div>
  );
};

export default Header;

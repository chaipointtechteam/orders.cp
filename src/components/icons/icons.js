import React from 'react';
import { ICON_TYPES } from '../../common/constants'
import Icon from '@ant-design/icons';

const Profile = () => require('../../assets/images/Profile_Green.svg');
const Location = require('../../assets/images/Location-02.svg');
const Payment = require('../../assets/images/Payment-01.svg');
const Coupon = require('../../assets/images/Coupon.svg');
const LoyaltyPoints = require('../../assets/images/Loyalty Points.svg');
const Veg = require('../../assets/images/Veg.svg');
const NonVeg = require('../../assets/images/Non Veg.svg');


export const ChaiAppIcons = props => {
    const { name = '' } = props;
    switch (name) {
        case ICON_TYPES.ACCOUNT:
            return <Icon component={Profile} {...props} />;

        case ICON_TYPES.LOCATION:
            return <Icon component={Location} {...props} />;

        case ICON_TYPES.PAYMENT:
            return <Icon component={Payment} {...props} />;

        case ICON_TYPES.COUPNS:
            return <Icon component={Coupon} {...props} />;

        case ICON_TYPES.LOYAL_POINTS:
            return <Icon component={LoyaltyPoints} {...props} />;

        case ICON_TYPES.VEG:
            return <Icon component={Veg} {...props} />;

        case ICON_TYPES.NON_VEG:
            return <Icon component={NonVeg} {...props} />;

        default:
            return null;
    }
};

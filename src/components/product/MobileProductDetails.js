import $ from "jquery";
import React, { Component } from "react";
import { isMobile } from "react-device-detect";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { updateCartItem, updateCartObject } from "../../actions/CartAction";
import { updateMenuItem } from "../../actions/MenuItemsAction";
import offer from "../../assets/images/Offers-04.png";
import veg from "../../assets/images/Veg.svg";
import {
  CheckAvaibilityOfProduct,
  checkAvaibilityOfProductWithAddond,
  checkOutofStock,
  constainsObjectWithModifierList,
  containsObject,
  deepEqual,
  getProductNameFromProductCode,
  updateSubTotal
} from "../../utils/common";
import toast from "../common/commonToast";
import MobileFooterCart from "../products/MobileFooterCart";
const styles2 = {
  bottom: "0px",
};
const styles = {
  bottom: "40px",
};
class MobileProductDetails extends Component {
  state = {
    rate: this.props.CouponObject.selectedProduct
      ? this.props.CouponObject.selectedProduct.price.toFixed(2)
      : 0,

    addOns: [],
    count: [0, 0, 0],
    click: 0,
    variants: [],
  };

  componentDidMount = () => {
    this.setState({ click: 0, count: [0, 0, 0] });

    const { selectedProduct } = this.props.CouponObject;
    if (selectedProduct) {
      this.processTheAddOns();
      this.processTheVariants();
    }
  };

  componentDidUpdate = (prevProps,prevState) => {
    if(this.state.variants && this.props.CouponObject.selectedProduct){
      console.log(this.state.variants ,prevState.variants,this.props.CouponObject.selectedProduct)
      if(this.state.variants!=prevState.variants){
      
          // debugger
        if(this.props.CouponObject.selectedProduct.productVariantList.length>0){
          const m=this.props.CouponObject.selectedProduct.productVariantList.find((o)=>o.isChecked===true)
          
          this.setState({
            rate: m ? m.productName.price : 0,
            count: [0, 0, 0],
          });
        }else{
          this.setState({
            rate: this.props.CouponObject.selectedProduct ? this.props.CouponObject.selectedProduct.price : 0,
            count: [0, 0, 0],
          });
        
      }
      }       
    }
    if (this.props.CouponObject.selectedProduct !== prevProps.CouponObject.selectedProduct) {
      console.log(this.props.CouponObject.selectedProduct,"component Did Mount")
     
      this.processTheAddOns();
      this.processTheVariants();
     
      if(this.props.CouponObject.selectedProduct.productVariantList>0){
        const m=this.props.CouponObject.selectedProduct.find((o)=>o.isChecked===true)
        
        this.setState({
          rate: m ? m.productName.price : 0,
          count: [0, 0, 0],
        });
      }else{
        this.setState({
          rate: this.props.CouponObject.selectedProduct ? this.props.CouponObject.selectedProduct.price : 0,
          count: [0, 0, 0],
        });
      }
    }
  };

  // componentDidUpdate = (prevProps) => {
  //   const { selectedProduct } = this.props.CouponObject;
  //   if (this.props.CouponObject.selectedProduct !== prevProps.selectedProduct) {
  //     this.setState({
  //       rate: selectedProduct ? selectedProduct.price : 0,
  //     });
  //     this.processTheAddOns();
  //     this.processTheVariants();
  //   }
  // };
  counterValue = (item, e) => {
    let counter = Array.from(this.state.count);
    if (counter[item] <= this.state.addOns[item].maxCount && e.target.checked) {
      counter[item] = counter[item] + 1;
      this.setState({
        count: counter,
      });
      return false;
    } else {
      counter[item] = counter[item] - 1;
      this.setState({
        count: counter,
      });
    }
  };
  isDisabled = (id) => {
    let isinclude = this.state.addOns[0].modifierProductList.filter((md) => {
      return md.isChecked !== false;
    });

    var selectedId = isinclude.map((is) => {
      if (is.productCode === id) {
        return true;
      }
    });

    var selectedId = selectedId.filter((sd) => {
      return sd !== undefined;
    });

    return this.state.count > 1 && !selectedId.length > 0;
  };

  back = () => {
    this.props.history.goBack();
  };

  checkDisable(index) {
    // let sum = 0
    // for (let i = 0; i < this.state.addOns.length; i++) {
    //   sum = sum + this.state.addOns[i].maxCount
    // }
    let maximum = this.state.addOns[index].maxCount;

    let count = this.state.count[index];

    let cls = "chk" + index;

    if (maximum === 0) {
      return true;
    } else {
      $(`input[name=${cls}]`).change(function () {
        var max = maximum;
        var chked = count;
        if (chked == max) {
          $(`input[name=${cls}]`).attr("disabled", "disabled");
          $(`input[name=${cls}]:checked`).removeAttr("disabled");
        } else {
          $(`input[name=${cls}]`).removeAttr("disabled");
        }
      });
    }
  }

  toggleAddOn = (item, e) => {
    this.counterValue(item.productIndex, e);

    this.checkDisable(item.productIndex);
    var newAddOn = this.state.addOns;
    newAddOn[item.productIndex].modifierProductList[item.Itemindex].isChecked =
      !newAddOn[item.productIndex].modifierProductList[item.Itemindex]
        .isChecked;

    this.setState({
      addOns: newAddOn,
    });
    if (
      newAddOn[item.productIndex].modifierProductList[item.Itemindex].isChecked
    ) {
      this.setState({
        rate: (
          parseFloat(this.state.rate) + item.modifierProduct.productName.price
        ).toFixed(2),
      });
    } else {
      this.setState({
        rate: (
          parseFloat(this.state.rate) - item.modifierProduct.productName.price
        ).toFixed(2),
      });
    }
    //update the rate in modal pop up as per selection of
  };
  updateCartAndMenuList = (quantity) => {
    this.setState({ click: 0, count: [0, 0, 0] });
    //update quantity value in menu list reducer
    if (this.props.CouponObject.selectedProduct.productVariantList.length > 0) {
      var variant = this.state.variants.find(
        (variant) => variant.isChecked === true
      );

      var m = [];

      variant.productName.productModifierList.map((modifier) => {
        modifier.modifierProductList.map((o, index) => {
          if (o.isChecked === true) {
            m.push(o);
          }
        });
      });
      var select = {
        selectedProduct: this.props.CouponObject.selectedProduct.displayName,
        ...variant.productName,
        productModifierList: {
          modifierProductList: m !== undefined ? m : [],
        },
      };

      this.props.updateMenuItem(
        this.props.menuItemList,
        select,
        quantity,
        this.props.inventory
      );
      this.props.updateCartItem(this.props.cartItemList, select, quantity);
    } else {
      this.props.updateMenuItem(
        this.props.menuItemList,
        this.props.CouponObject.selectedProduct,
        quantity,
        this.props.inventory
      );

      this.props.updateCartItem(
        this.props.cartItemList,
        this.props.CouponObject.selectedProduct,
        quantity
      );
    }
  };

  updateCartWithProduct = () => {
    this.setState({ click: 0, count: [0, 0, 0] });
    $("input[type=checkbox]").prop("checked", false);
    if (
      // this.props.product.productAddonList.length  !== 0 &&
      this.props.CouponObject.selectedProduct.productModifierList &&
      this.props.CouponObject.selectedProduct.productModifierList[0]
        ?.modifierProductList.length !== undefined &&
      this.props.CouponObject.selectedProduct.productVariantList.length === 0
    ) {
      var cartList = this.props.cartObject.cartItems;

      var m = [];
      this.props.CouponObject.selectedProduct.productModifierList.map(
        (modifier) => {
          modifier.modifierProductList.map((o, index) => {
            if (o.isChecked === true) {
              m.push(o);
            }
          });
        }
      );

      // console.log(m)

      if (m.length !== undefined) {
        var select = {
          ...this.props.CouponObject.selectedProduct,
          productModifierList: {
            modifierProductList: m !== undefined ? m : [],
          },
        };
        if (
          !constainsObjectWithModifierList(
            this.props.CouponObject.selectedProduct,
            cartList
          )
        ) {
          var message = checkAvaibilityOfProductWithAddond(
            this.props.inventory,
            select,
            this.props.cartItemList
          );
          // debugger
          // var m = this.props.CouponObject.selectedProduct.productModifierList[0]?.modifierProductList.filter((o, index) => o.isChecked === tru
          if (message.check) {
            cartList.push(select);
            this.props.CouponObject.selectedProduct.quantity =
              this.props.CouponObject.selectedProduct.quantity + 0;
            this.updateCartAndMenuList(
              this.props.CouponObject.selectedProduct.quantity
            );
          } else {
            toast.info(
              `You can't add ${message.addonItem.displayName},it's quantity is now out of stock`
            );
          }
        } else {
          var message = checkAvaibilityOfProductWithAddond(
            this.props.inventory,
            select,
            this.props.cartItemList
          );
          if (message.check) {
            var m = [];
            this.props.CouponObject.selectedProduct.productModifierList.map(
              (modifier) => {
                modifier.modifierProductList.map((o, index) => {
                  if (o.isChecked === true) {
                    m.push(o);
                  }
                });
              }
            );
            var select = {
              ...this.props.CouponObject.selectedProduct,
              productModifierList: {
                modifierProductList: m !== undefined ? m : [],
              },
            };
            var cart = this.props.cartItemList.find(
              (o) => o.productId == select.productId
            );
            var list = this.props.cartItemList.filter(
              (o) => o.productId == cart.productId
            );
            var product;
            list.map((o) => {
              if (
                deepEqual(o.productModifierList, select.productModifierList)
              ) {
                product = o;
              }
            });
            this.updateCartAndMenuList(product.quantity + 1);
          } else {
            toast.info(
              `You can't add ${message.addonItem.displayName},it's quantity is  now out of stock`
            );
          }
        }
      }
    } else if (
      this.props.CouponObject.selectedProduct.productModifierList.length ===
        0 &&
      this.props.CouponObject.selectedProduct.productVariantList.length === 0
    ) {
      var cartList = this.props.cartItemList;
      if (!containsObject(this.props.CouponObject.selectedProduct, cartList)) {
        // debugger
        if (
          CheckAvaibilityOfProduct(
            this.props.inventory,
            this.props.CouponObject.selectedProduct
          )
        ) {
          cartList.push(this.props.CouponObject.selectedProduct);
          this.props.CouponObject.selectedProduct.quantity =
            this.props.CouponObject.selectedProduct.quantity + 1;
          this.updateCartAndMenuList(
            this.props.CouponObject.selectedProduct.quantity
          );
        }
      } else {
        if (
          CheckAvaibilityOfProduct(
            this.props.inventory,
            this.props.CouponObject.selectedProduct
          )
        ) {
          this.updateCartAndMenuList(
            this.props.CouponObject.selectedProduct.quantity + 1
          );
        }
      }
    } else {
      // debugger
      var cartList = this.props.cartObject.cartItems;
      // console.log(this.props.CouponObject.selectedProduct)
      // console.log(this.state.variants)
      var variant = this.state.variants.find(
        (variant) => variant.isChecked === true
      );
      // console.log(variant)
      var m = [];

      variant.productName.productModifierList.map((modifier) => {
        modifier.modifierProductList.map((o, index) => {
          if (o.isChecked === true) {
            m.push(o);
          }
        });
      });

      if (m.length !== undefined) {
        var select = {
          selectedProduct: this.props.CouponObject.selectedProduct.displayName,
          ...variant.productName,
          displayName: `${
            this.props.CouponObject.selectedProduct.displayName
          }${"-"}${" "}${variant.productName.displayName}`,
          // displayName: `asjhdkjashjkaghkgha`,
          productModifierList: {
            modifierProductList: m !== undefined ? m : [],
          },
        };
        if (!constainsObjectWithModifierList(variant.productName, cartList)) {
          var message = checkAvaibilityOfProductWithAddond(
            this.props.inventory,
            select,
            this.props.cartItemList
          );
          if (message.check) {
            cartList.push(select);
            variant.productName.quantity = variant.productName.quantity + 0;
            this.updateCartAndMenuList(variant.productName.quantity);
          } else {
            toast.warning(
              `you can't add ${message.addonItem.displayName},it's quantity is  now out of stock`
            );
          }
        }
       else {
        this.updateCartAndMenuList(
          variant.productName.quantity + 1
        );
        //if product already there this button wont be visible
      }
    }
      this.props.updateCartObject({
        cartItems: cartList,
        subTotal: updateSubTotal(cartList),
      });
    }
    this.props.history.goBack();
  };
  processTheAddOns = (adons) => {
    const { selectedProduct } = this.props.CouponObject;

    const { menuItemList } = this.props;
    var modifierAddons = [];

    if (selectedProduct !== undefined) {
      selectedProduct.productModifierList.map((item) =>
        modifierAddons.push(item)
      );
      modifierAddons?.map((item, index) => {
        item.modifierProductList.map((i, index) => {
          var m = menuItemList.find((o) => o.productId === i.productId);
          if (m !== undefined) {
            i.productName = getProductNameFromProductCode(
              menuItemList,
              i.productCode
            );
            i.isChecked = false;
            i.index = index;
          }
        });
      });
    }
    if (adons !== undefined) {
      adons.productModifierList.map((item) => modifierAddons.push(item));
      modifierAddons?.map((item, index) => {
        item.modifierProductList.map((i, index) => {
          i.productName = getProductNameFromProductCode(
            menuItemList,
            i.productCode
          );
          i.isChecked = false;
          i.index = index;
        });
      });
    }
    this.setState({
      addOns: modifierAddons,
    });
  };
  selectTheVarient = (selctedVariant) => {
    // debugger
    var selected;
    if (selctedVariant.target?.id === undefined) {
      selected = selctedVariant[0]?.productId;
    } else {
      selected = selctedVariant.target.id;
    }

    if (selected !== undefined) {
      const { menuItemList, selectedProduct } = this.props;
      var varient = menuItemList.find((item) => item.productId == selected);
      if (varient) {
        if (varient.productModifierList) {
          this.processTheAddOns(varient, (res) => {
            console.log(this.state.addOns);
          });
        }
      }

      if (selctedVariant?.length >= 0) {
        selctedVariant.map((item) => {
          if (item.productId == selected) {
            item.isChecked = true;
          } else {
            item.isChecked = false;
          }
        });
        this.setState({
          variants: selctedVariant,
        });
      } else {
        var m = [];

        this.props.CouponObject.selectedProduct.productVariantList.map((item) => {
          if (item.productId == selected) {
            item.isChecked=true
            m.push({ ...item, isChecked: true });
          } else {
            item.isChecked=false
            m.push({ ...item, isChecked: false });
          }
        });

        this.setState({
          variants: m,
        });
      }
    }
  };
  processTheVariants = () => {
    const { menuItemList } = this.props;
    const { selectedProduct } = this.props.CouponObject;
    var variantsInSelectedProduct =
      selectedProduct !== undefined ? selectedProduct.productVariantList : [];
    variantsInSelectedProduct.forEach((item, index) => {
      var m = getProductNameFromProductCode(
        menuItemList,
        item.productCode
      );
      item.productName={...m,selectedProduct}
      item.isChecked = false;
    });

    // variantsInSelectedProduct.push({ productId: 12, displayName: "samosa", productName: { price: 180, quantity: 0 } })
    // variantsInSelectedProduct.push({ productId: 13, displayName: "samosa2", productName: { price: 160, quantity: 0 } })
    if (variantsInSelectedProduct.length > 1) {
      variantsInSelectedProduct.sort(
        (a, b) => a.productName.price - b.productName.price
      );
    }

    // variantsInSelectedProduct.map((item) => {

    // })

    this.setState({
      variants: variantsInSelectedProduct,
    });

    this.selectTheVarient(variantsInSelectedProduct);
  };

  render() {
    const { selectedProduct } = this.props.CouponObject;

    return (
      <>
        {isMobile ? (
          <>
            <section class="cmp-mobile">
              <div class="col-sm-12 mobileproduct-alldetails">
                <div class="mobileproduct-image">
                  <div style={{ width: "12%", padding: "8px" }}>
                    <img
                      width="30px"
                      height="30px"
                      onClick={this.back}
                      className="back-arrow"
                      src={
                        require("../../assets/images/left-arrow.svg").default
                      }
                    />{" "}
                  </div>
                  {selectedProduct.images && (
                    <img
                      width="100%"
                      src={
                        selectedProduct.images.length > 0 &&
                        selectedProduct.images
                          ? selectedProduct.images[0].imageUrl
                          : { offer }
                      }
                    />
                  )}
                </div>

                <div class="col-sm-12 mobileproduct-details">
                  <span>
                    <div class="veg-end">
                      {selectedProduct?.isVegetarian && (
                        <span>
                          <img src={require("../../assets/images/Veg.svg").default} />
                        </span>
                      )}
                      {!selectedProduct?.isVegetarian && (
                        <span>
                          <img src={require("../../assets/images/Non Veg.svg").default} />
                        </span>
                      )}
                    </div>
                  </span>
                  <label>

                    {selectedProduct.displayName}</label>
                  <small>₹ {selectedProduct.productVariantList.length > 0 ?
                    //  console.log(selectedProduct.productVariantList.find(o=>o.isChecked==true))
                    selectedProduct.productVariantList.map((o) => {
                      if (o.isChecked) {
                        return o.productName.price
                      }
                    })
                    : selectedProduct.price}</small>
                </div>
                <div class="col-sm-12 mobileproduct-content">
                  <p>{selectedProduct.description}</p>
                </div>
                {this.state.variants && this.state.variants.length > 0 && (
                  <div className="mobileproduct-content">
                    <p className="Size">Variants </p>
                    <div onChange={this.selectTheVarient}>
                      {this.state.variants.map((variant, VariatnIndex) => (
                        <div>
                          {checkOutofStock(
                            this.props.inventory,
                            variant.productName
                          ) ? (
                            <div className="row">
                              <div className="col-2">
                                <input
                                  style={{
                                    marginTop: "9px",
                                    marginLeft: "20px",
                                  }}
                                  className=""
                                  checked={variant.isChecked}
                                  type="radio"
                                  id={variant.productId}
                                  name={variant.productName}
                                  value={variant.displayName}
                                />{" "}
                              </div>
                              <div className="col-10">
                                <label
                                  for={variant.productId}
                                  className="lables"
                                >
                                  {variant.displayName}
                                </label>{" "}
                              </div>
                            </div>
                          ) : (
                            <div className="row">
                              <div className="col-2">
                                <input
                                  style={{
                                    marginTop: "9px",
                                    marginLeft: "20px",
                                  }}
                                  className=""
                                  checked={variant.isChecked}
                                  disabled={true}
                                  type="radio"
                                  id={variant.productId}
                                  name={variant.productName}
                                  value={variant.displayName}
                                />{" "}
                              </div>
                              <div className="col-10">
                                <label
                                  for={variant.productId}
                                  style={{ color: "#a9acac" }}
                                  className="lables"
                                >
                                  {variant.displayName}
                                </label>{" "}
                              </div>
                            </div>
                          )}
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              
                {this.state.addOns.map((modifierList, productIndex) => (
                  <div class="col-sm-12 add-ons">
                    <label class="mobileaddon">
                      {modifierList.modifierName}{" "}
                      <small>
                        {" "}
                        {this.state.count[productIndex]}/{modifierList.maxCount}
                      </small>
                    </label>
                    <div>
                      {modifierList.modifierProductList.map(
                        (modifierProduct, Itemindex) => (
                          <>
                           
                            {checkOutofStock(
                              this.props.inventory,
                              modifierProduct.productName
                            ) && modifierList.maxCount !== 0 ? (
                              <div class="add-ons-list">
                                <label style={{ width: "100%" }} class="add-on-box">
                                  <span>
                                    <div class="veg-end">
                                      {modifierProduct.productName?.isVegetarian && (
                                        <span>
                                          <img src={require("../../assets/images/Veg.svg").default} />
                                        </span>
                                      )}
                                      {!modifierProduct.productName?.isVegetarian && (
                                        <span>
                                          <img src={require("../../assets/images/Non Veg.svg").default} />
                                        </span>
                                      )}
                                    </div>
                                  </span>
                                  {modifierProduct.productName?.displayName}
                                  <small>
                                    ₹ {modifierProduct.productName?.price}
                                  </small>
                                  <input
                                    name={`chk` + productIndex}
                                    type="checkbox"
                                    disabled={this.checkDisable(productIndex)}
                                    onChange={(e) =>
                                      this.toggleAddOn(
                                        {
                                          modifierProduct,
                                          Itemindex,
                                          productIndex,
                                        },
                                        e
                                      )
                                    }
                                  />
                                  <span class="checkmark"></span>
                                </label>
                              </div>
                            ) : (
                              <div class="add-ons-list d-flex align-items-center">
                                <label
                                  style={{
                                    width: "100%",
                                    color: "#A9ACAC",
                                    textDecoration: "line-through",
                                    textDecorationColor: "#505050",
                                  }}
                                  class="add-on-box"
                                >
                                  <span>
                                    <div class="veg-end">
                                      {modifierProduct.productName?.isVegetarian && (
                                        <span>
                                          <img src={require("../../assets/images/Veg.svg").default} />
                                        </span>
                                      )}
                                      {!modifierProduct.productName?.isVegetarian && (
                                        <span>
                                          <img src={require("../../assets/images/Non Veg.svg").default} />
                                        </span>
                                      )}
                                    </div>
                                  </span>
                                  {modifierProduct.productName?.displayName}
                                  <small >
                                    ₹ {modifierProduct.productName?.price}
                                  </small>
                                  {/* <input
                                    name={`chk` + productIndex}
                                    type={null}
                                    disabled={this.disabled}
                                    onChange={(e) =>
                                      this.toggleAddOn(
                                        {
                                          modifierProduct,
                                          Itemindex,
                                          productIndex,
                                        },
                                        e
                                      )
                                    }

                                  /> */}
                                  <span class="checkmark"></span>
                                </label>
                              </div>
                            )}
                          </>
                        )
                      )}
                    </div>
                  </div>
                ))}

                {selectedProduct.nutrientList.length > 0 && (
                  <>
                    <h4 class="nutrient">Nutrient Information</h4>
                    <ul class="nutri-points">
                      {selectedProduct.nutrientList.map((item, index) => (
                        <li>
                          {item.value} {item.name}
                        </li>
                      ))}
                    </ul>
                  </>
                )}
              </div>

              {checkOutofStock(this.props.inventory, selectedProduct) ? (
                <div
                  class="footer-cpm"
                  style={
                    this.props.cartObject &&
                    this.props.cartObject.cartItems &&
                    this.props.cartObject.cartItems.length > 0
                      ? styles
                      : styles2
                  }
                >
                  <button onClick={this.updateCartWithProduct}>Add Item</button>
                </div>
              ) : (
                <div></div>
              )}
              <MobileFooterCart />
            </section>
          </>
        ) : (
          <Redirect to="/products" />
        )}
      </>
    );
  }
}
const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  const menuItemList = reducerObj.menuItemList.menuItemList.menuItemList;
  const cartItemList = reducerObj.cartObject.cartObject.cartItems;
  const CouponObject = reducerObj.CouponObject;
  const inventory = reducerObj.menuItemList.menuItemList.inventory;
  return { cartObject, menuItemList, cartItemList, CouponObject, inventory };
};

export default connect(mapStateToProps, {
  updateCartObject,
  containsObject,
  updateCartItem,
  updateMenuItem,
})(MobileProductDetails);

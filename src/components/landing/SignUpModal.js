import React, { Component } from "react";
import SignUp from "../common/Login/signUp";
import { isMobileOnly } from "react-device-detect";
import { Modal } from "react-bootstrap";

export default class SignUpModal extends Component {
  render() {
    if(!isMobileOnly){
    return (
      <Modal
        className="modal-opacity signup"
        id="mysignModal"
        show={this.props.showModal}
        onHide={this.props.closePopUp}
      >
        <SignUp
          closePopUp={this.props.closePopUp}
          closePopUpAndOpenLogin={this.props.closePopUpAndOpenLogin}
        />
      </Modal>
    );
  }else {
    return ""
  }
}
}

import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import {
  updateSelectedStore,
  updateSelectedChannel,
} from "../../actions/StoreListAction";
import { setServiceList } from "../../actions/ChannelListAction";
import { resetCartObject } from "../../actions/CartAction";
import Select from "react-select";
const customStyles = {
  control: (styles) => ({
    ...styles,
    cursor: "pointer",
    paddingLeft: 15,
    boxShadow: "none",
    marginBottom: 25,
    borderColor: "transparent",
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  menu: styles => ({ ...styles, width: "110%",marginTop:"15px", left: -14, cursor: "pointer","@media only screen and (max-width: 762px)": {
    ...styles["@media only screen and (max-width: 762px)"],
    width: "114%", left: -9, }
  }),
  menuList: () => ({
    cursor: "pointer",
    padding: 0,
    paddingTop: 0,
    paddingBottom: 0,
  }),
  indicatorsContainer: () => {
    return {

      color: "#279599",
    };
  },
  option: () => {
    return {
      cursor: "pointer",
      listStyle: "none",
      padding: "12px 20px",
      borderBottom: "1px solid #f3f4f4",
      color: "#2b2728",
    };
  },
  dropdownIndicator: () => {
    return {
      border: "none",
      color: "#279599",
    };
  },
};

class StoreSelector extends Component {
  state = {
    selectedStore: null,
    nearestStore: null,
  };
  componentDidMount = () => {
    
    let obj = localStorage.getItem('persist:ChaiPoint')
    let obj2 = JSON.parse(obj);
    let location = JSON.parse(obj2.selectedLocation)
    let mainLatitude = location.selectedLocation.selectedLocation?.geometry?.location.lat;
    let mainLongitude = location.selectedLocation.selectedLocation?.geometry?.location.lng
    let min = this.getDistanceFromLatLonInKm(mainLatitude, mainLongitude, this.props.storeList[0]?.latitude, this.props.storeList[0]?.longitude)
    let nearestStore = this.props.storeList[0];
    let selectedChannel = {};
    for (let i = 0; i < this.props.storeList.length; i++) {
      let nearLat = this.props.storeList[i]?.latitude;
      let nearlng = this.props.storeList[i]?.longitude;
      let dist = this.getDistanceFromLatLonInKm(mainLatitude, mainLongitude, nearLat, nearlng);
      if (dist < min) {
        min = dist;
        nearestStore = this.props.storeList[i]
      }
      this.setState({
        selectedStore: {
          value: nearestStore.siteId,
          label: nearestStore.name,
          index: i
        }
      })
      for (let j = 0; j < this.props.storeList[i].sellerOptions.length; j++) {
        if (this.props.storeList[i].sellerOptions[j].displayName === "Delivery") {
          selectedChannel = this.props.storeList[i].sellerOptions[j]
        }
      }
    }
    this.props.updateSelectedChannel({
      selectedChannel: selectedChannel
    });
    this.props.updateSelectedStore({
      selectedStore: nearestStore,
    });

    this.props.setServiceList({
      channelList: nearestStore?.sellerOptions,
    });

    this.updateOptions();
    const { selectedStore, storeList } = this.props;
    storeList.forEach((element, index) => {
      if (selectedStore && element.siteId === selectedStore.siteId) {
        this.setState({
          selectedStore: {
            value: element.siteId,
            label: element.name,
            index: index,
          },
        });
      }
    });


  };

  autoFillNearestStore=()=>{
    let obj = localStorage.getItem('persist:ChaiPoint')
    let obj2 = JSON.parse(obj);
    let location = JSON.parse(obj2.selectedLocation)
    let mainLatitude = location.selectedLocation.selectedLocation?.geometry?.location.lat;
    let mainLongitude = location.selectedLocation.selectedLocation?.geometry?.location.lng
    let min = this.getDistanceFromLatLonInKm(mainLatitude, mainLongitude, this.props.storeList[0]?.latitude, this.props.storeList[0]?.longitude)
    let nearestStore = this.props.storeList[0];
    let selectedChannel = {};
    for (let i = 0; i < this.props.storeList.length; i++) {
      let nearLat = this.props.storeList[i]?.latitude;
      let nearlng = this.props.storeList[i]?.longitude;
      let dist = this.getDistanceFromLatLonInKm(mainLatitude, mainLongitude, nearLat, nearlng);
      if (dist < min) {
        min = dist;
        nearestStore = this.props.storeList[i]
      }
      this.setState({
        selectedStore: {
          value: nearestStore.siteId,
          label: nearestStore.name,
          index: i
        }
      })
      for (let j = 0; j < this.props.storeList[i].sellerOptions.length; j++) {
        if (this.props.storeList[i].sellerOptions[j].displayName === "Delivery") {
          selectedChannel = this.props.storeList[i].sellerOptions[j]
        }
      }
    }
    this.props.updateSelectedChannel({
      selectedChannel: selectedChannel
    });
    this.props.updateSelectedStore({
      selectedStore: nearestStore,
    });

    this.props.setServiceList({
      channelList: nearestStore?.sellerOptions,
    });

    this.updateOptions();
    const { selectedStore, storeList } = this.props;
    storeList.forEach((element, index) => {
      if (selectedStore && element.siteId === selectedStore.siteId) {
        this.setState({
          selectedStore: {
            value: element.siteId,
            label: element.name,
            index: index,
          },
        });
      }
    });
  }
  componentDidUpdate(prevProps) {
    if (this.props.storeList !== prevProps.storeList) {
      this.autoFillNearestStore()
      this.updateOptions();
    }
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180)
  }


  //using Haversine formula.
  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  updateOptions = () => {
    var options = [];
    if (this.props.storeList) {
      this.props.storeList.forEach((element, index) => {
        options.push({
          value: element.siteId,
          label: element.name,
          index: index,
        });
      });
      this.setState({
        options: options,
      });
    }
  };

  handleStoreChange = (item) => {
    const { storeList } = this.props;
    this.setState({
      selectedStore: {
        value: item.value,
        label: item.label,
        index: item.index,
      },
    });

    this.setState({
      sellerOptions: storeList[item.index].sellerOptions,
    });
    this.props.updateSelectedStore({
      selectedStore: storeList[item.index],
    });
    this.props.setServiceList({
      channelList: storeList[item.index].sellerOptions,
    });
    //need to empty the cart
    this.props.resetCartObject();
  };

  render() {
    return (
      <>
        <span>
          <img src={require("../../assets/images/Store.svg").default} />
        </span>
        <div
          style={{ background: "none", border: "none" }}
          className="custom-select"
        >
          <Select
            isSearchable={false}
            placeholder={"Select Store"}
            options={this.state.options}
            styles={customStyles}
            onChange={this.handleStoreChange}
            autoFocus={false}
            value={this.state.selectedStore}
          />
        </div>
        {/* <select name="selectedStore" onChange={this.handleStoreChange}>
          <option>Select Store</option>
          {storeList.map((option, index) => (
            <option key={index} value={index}>
              {option.name}
            </option>
          ))}
        </select> */}
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const storeList = reducerObj.storeList.storeList;
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  return { storeList, selectedStore };
};

export default connect(mapStateToProps, {
  updateSelectedStore,
  setServiceList,
  updateSelectedChannel,
  resetCartObject,
})(StoreSelector);

StoreSelector.propTypes = {
  storeList: PropTypes.array,
  updateSelectedStore: PropTypes.func.isRequired,
  setServiceList: PropTypes.func.isRequired,
  resetCartObject: PropTypes.func.isRequired,
};

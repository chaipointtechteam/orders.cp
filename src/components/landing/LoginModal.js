import React, { Component } from "react";
import Login from "../common/Login/login";
import {Redirect} from "react-router-dom"
import { Modal } from "react-bootstrap";
import { isMobileOnly } from "react-device-detect";

export default class LoginModal extends Component {
  render() {
    if(!isMobileOnly){
    return (
      <Modal
        id="myModal"
        className="modal-opacity login"
        show={this.props.showModal}
        onHide={this.props.closePopUp}
      >
        <Login
          closePopUp={this.props.closePopUp}
          closePopUpAndOpenSignUp={this.props.closePopUpAndOpenSignUp}
        />
      </Modal>
    );
    }else{
      return ""
    }
  }
}

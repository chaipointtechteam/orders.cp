import React, { Component } from "react";
import { Modal } from "react-bootstrap";
import EmptyCart from "../products/emptyCart";
import MyCart from "../products/myCart";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

class CartModal extends Component {
  render() {
    const { cartObject } = this.props;
    
    return (
      <Modal
        className="modal-opacity cart cart-modal"
        show={this.props.showModal}
        onHide={this.props.closePopUp}
      >
        <div class="arrow-cart"></div>
        {cartObject &&
          cartObject.cartItems &&
          cartObject.cartItems.length === 0 && <EmptyCart />}
        {cartObject &&
          cartObject.cartItems &&
          cartObject.cartItems.length > 0 && <MyCart />}
      </Modal>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const cartObject = reducerObj.cartObject.cartObject;
  return { cartObject };
};

export default connect(mapStateToProps, {})(CartModal);

CartModal.propTypes = {
  cartObject: PropTypes.object.isRequired,
};

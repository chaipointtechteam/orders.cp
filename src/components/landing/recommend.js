import _ from "lodash";
import { PropTypes } from "prop-types";
import React, { Component } from "react";
import { connect } from "react-redux";
//my carousel
import Slider from "react-slick";
import { getMarkettingOffers } from "../../actions/internalActions/LandingActions";
import "../../assets/slick/slick-theme.css";
import "../../assets/slick/slick.css";

var settings = {
  autoplay: true,
  arrows: true,
  infinite: true,
  speed: 500,

  slidesToScroll: 1,
  // initialSlide: 4,
  rtl: true,
  dots: true,
  dotsClass: "owl-dots",
  responsive: [
    {
      breakpoint: 1295,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 1100,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: false,
        //dots: true,
      },
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        //dots: true,
      },
    },
    {
      breakpoint: 720,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
  ],
};

class Recommend extends Component {
  state = {
    carouselData: [],
    mediaPath: "",
  };
  componentDidMount = () => {
    this.props.getMarkettingOffers((data, mediaPath) => {
      let carouselData = [];
      if (data) {
        data.forEach((d) => {
          carouselData.push({ ...d, media: JSON.parse(d.media) });
        });

        this.setState({
          carouselData: carouselData,
          mediaPath: mediaPath,
        });
      }
    });
  };
  render() {
    const settingsModified = {
      ...settings,
      slidesToShow: _.size(this.state.carouselData),
    };

    console.log("best", this.state.carouselData);
    return (
      <section className="recommended-service Category">
        <div className="container">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="recommended-text">
                <h3>Best sellers</h3>
              </div>
              <div className="brand-carousel section-margin">
                <Slider {...settingsModified}>
                  {this.state.carouselData &&
                    this.state.carouselData.length > 0 &&
                    _.sortBy(this.state.carouselData, ["sequence"]).map(
                      (data, index) => {
                        return (
                          <div class="single-logo" key={index}>
                            <img
                              src={`${this.state.mediaPath}${data.media[0].image}`}
                              className="carausalImage"
                              alt={data.title}
                            />
                          </div>
                        );
                      }
                    )}
                </Slider>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

export default connect(mapStateToProps, {
  getMarkettingOffers,
})(Recommend);

Recommend.propTypes = {
  getMarkettingOffers: PropTypes.func.isRequired,
};

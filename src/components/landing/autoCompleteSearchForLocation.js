import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { PropTypes } from "prop-types";
import React from "react";
import Geocode from "react-geocode";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { resetCartObject } from "../../actions/CartAction";
import { setServiceList } from "../../actions/ChannelListAction";
import { emptyMenuItemList } from "../../actions/MenuItemsAction";
import {
  getStoreList,
  loadStoreList,
  resetChannel,
  resetLocation,
  resetStore,
  updateSelectedChannel,
  updateSelectedLocation,
  updateSelectedStore,
} from "../../actions/StoreListAction";
import { resetUserObject } from "../../actions/UserAction";
import toast from "../common/commonToast";
import ChannelSelector from "./channelSelector";
import StoreSelector from "./storeSelector";
import _ from "lodash";
/* global google */

class LocationSelector extends React.Component {
  state = {
    isLogin: false,
    showStore: false,
    options: [],
    selectedLocationText: "",
    allowToFindChai: false,
    allowFindChai: true,
    lat: 0,
    lng: 0,
  };
  constructor(props) {
    super(props);
    this.autocompleteInput = React.createRef();
    this.autocomplete = null;
    this.handlePlaceChanged = this.handlePlaceChanged.bind(this);
  }

  componentDidMount() {
    // const { isLoggedIn, userObject } = this.props.userObject
    // const {email,name}=userObject
    // if (email === null && name === null) {
    //   // alert("Please edit your profile details")
    //   window.location.href = "/profile";
    // }
    const data = [
      {
        siteId: 3698,
        name: "Chai Point HSR Sector -3",
        latitude: 12.91163,
        longitude: 77.638014,
        startTime: "00:00:00",
        endTime: "23:00:00",
        cityId: 549,
        cityName: null,
        zipcode: null,
        outletId: 42,
        oldStoreId: null,
        enableInventoryCheck: true,
        sellerOptions: [
          {
            sellerId: 1,
            name: "WALK_IN",
            displayName: "Dine In",
            captureCustomerAddress: false,
          },
          {
            sellerId: 12,
            name: "DELIVERY",
            displayName: "Delivery",
            captureCustomerAddress: true,
          },
          {
            sellerId: 13,
            name: "PICKUP",
            displayName: "Take Away",
            captureCustomerAddress: false,
          },
        ],
      },
      {
        siteId: 873,
        name: "Chai Point BTM - DH",
        latitude: 12.913488,
        longitude: 77.610017,
        startTime: "08:00:00",
        endTime: "23:00:00",
        cityId: 549,
        cityName: null,
        zipcode: null,
        outletId: 28,
        oldStoreId: null,
        enableInventoryCheck: true,
        sellerOptions: [
          {
            sellerId: 1,
            name: "WALK_IN",
            displayName: "Dine In",
            captureCustomerAddress: false,
          },
          {
            sellerId: 13,
            name: "PICKUP",
            displayName: "Take Away",
            captureCustomerAddress: false,
          },
          {
            sellerId: 12,
            name: "DELIVERY",
            displayName: "Delivery",
            captureCustomerAddress: true,
          },
        ],
      },
      {
        siteId: 839,
        name: "Chai Point Koramangala 3",
        latitude: 12.930798,
        longitude: 77.632912,
        startTime: "00:00:00",
        endTime: "23:55:00",
        cityId: 549,
        cityName: null,
        zipcode: null,
        outletId: 76,
        oldStoreId: null,
        enableInventoryCheck: true,
        sellerOptions: [
          {
            sellerId: 1,
            name: "WALK_IN",
            displayName: "Dine In",
            captureCustomerAddress: false,
          },
          {
            sellerId: 12,
            name: "DELIVERY",
            displayName: "Delivery",
            captureCustomerAddress: true,
          },
          {
            sellerId: 13,
            name: "PICKUP",
            displayName: "Take Away",
            captureCustomerAddress: false,
          },
        ],
      },
      {
        siteId: 835,
        name: "Chai Point HSR 27th Main Road",
        latitude: 12.9121934,
        longitude: 77.65251,
        startTime: "08:00:00",
        endTime: "23:00:00",
        cityId: 549,
        cityName: null,
        zipcode: null,
        outletId: 134,
        oldStoreId: null,
        enableInventoryCheck: true,
        sellerOptions: [
          {
            sellerId: 1,
            name: "WALK_IN",
            displayName: "Dine In",
            captureCustomerAddress: false,
          },
          {
            sellerId: 12,
            name: "DELIVERY",
            displayName: "Delivery",
            captureCustomerAddress: true,
          },
          {
            sellerId: 13,
            name: "PICKUP",
            displayName: "Take Away",
            captureCustomerAddress: false,
          },
        ],
      },
    ];
    localStorage.setItem("locationData", JSON.stringify(data));

    this.autocomplete = new google.maps.places.Autocomplete(
      this.autocompleteInput.current,
      { types: ["geocode"] }
    );

    this.autocomplete.addListener("place_changed", this.handlePlaceChanged);

    if (
      this.props.selectedLocation &&
      this.props.selectedLocation.formatted_address
    ) {
      this.setState({
        showStore: true,
        storesCount: this.props.storeList ? this.props.storeList.length : 0,
        selectedLocationText: this.props.selectedLocation.formatted_address,
      });
    }
  }
  //   componentDidUpdate=(prevProps)=> {
  //     if (this.props.userObject!== prevProps.userObject) {
  //         this.getTheCurrentLatLng()
  //    }
  // }
  componentDidUpdate = (prevProps) => {
    if (this.props.userObject.isLoggedIn !== prevProps.userObject.isLoggedIn) {
      this.getTheCurrentLatLng();
    }

    if (this.props.selectedLocation !== prevProps.selectedLocation) {
      this.setState({
        showStore: false,
        selectedLocationText: null,
      });
    }
    //  if (this.props.userObject.isLoggedIn !== prevProps.userObject.isLoggedIn) {
    //       this.setState({isLogin:!this.state.isLogin})
    //     }
    //     if (this.props.userObject.isLoggedIn !== this.state.isLogin) {
    //       this.getTheCurrentLatLng()

    //     }
  };

  handleFindChai = (value) => {
    this.setState({
      allowFindChai: value,
    });
  };

  handlePlaceChanged() {
    const place = this.autocomplete.getPlace();
    console.log("place changes", place.geometry.location.lat());

    this.props.updateSelectedLocation({
      selectedLocation: place,
    });

    this.setState({
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
      selectedLocationText: place.formatted_address,
    });
    //backend will becalled
    //load the stores
    this.props.loadStoreList(
      {
        latitude: place.geometry.location.lat(),
        longitude: place.geometry.location.lng(),
      },
      (data) => {
        this.setState({
          showStore: true,
          storesCount: data ? data.length : 0,
        });
      }
    );
  }

  getTheCurrentLatLng = () => {
    const location = window.navigator && window.navigator.geolocation;

    if (location) {
      location.getCurrentPosition(
        (position) => {
          Geocode.fromLatLng(
            position.coords.latitude,
            position.coords.longitude
          ).then((response) => {
            const address = response.results[0].formatted_address;

            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              selectedLocationText: address,
            });
          });

          //load the stores
          this.props.loadStoreList(
            {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            },
            (data) => {
              this.setState({
                showStore: true,
                storesCount: data ? data.length : 0,
              });
            }
          );
        },
        (error) => {
          this.setState({
            latitude: "err-latitude",
            longitude: "err-longitude",
          });
        }
      );
    }
  };

  updateInput = (event) => {
    if (event.target.value === "") {
      this.setState({
        showStore: false,
        options: [],
        selectedLocationText: null,
      });
      this.props.emptyMenuItemList();
      this.props.resetCartObject();
      this.props.resetStore();
      this.props.resetChannel();
      this.props.resetLocation();
      this.props.updateSelectedStore({
        selectedStore: {},
      });
      this.props.updateSelectedChannel({
        selectedChannel: {},
      });
      this.props.updateSelectedLocation({
        selectedLocation: {},
      });
      this.props.setServiceList([]);
      this.props.getStoreList([]);

      //reset selected store
      //reset selected channel
      //reset menu
      //reset cart
      //perform localstorage.empty in short
      //update  all props
      //hide the select store and select channel and find chai button
    }
    this.setState({ [event.target.name]: event.target.value });
  };

  onFindStore = () => {
    let obj = localStorage.getItem("persist:ChaiPoint");
    let lat = _.get(JSON.parse(_.get(JSON.parse(obj), "selectedStore")), [
      "selectedStore",
      "selectedStore",
      "latitude",
    ]);
    let long = _.get(JSON.parse(_.get(JSON.parse(obj), "selectedStore")), [
      "selectedStore",
      "selectedStore",
      "longitude",
    ]);

    console.log("altitudes", lat, long);

    if (!_.isEmpty(this.state.selectedLocationText)) {
      this.props.loadStoreList(
        {
          latitude: lat,
          longitude: long,
        },
        (data) => {
          this.setState({
            showStore: true,
            storesCount: data ? data.length : 0,
          });
        }
      );
    }
  };

  clearFun = () => {
    this.props.emptyMenuItemList();
    this.props.resetCartObject();
    this.props.resetStore();
    this.props.resetChannel();
    this.props.resetLocation();
    this.props.updateSelectedStore({
      selectedStore: {},
    });
    this.props.updateSelectedChannel({
      selectedChannel: {},
    });
    this.props.updateSelectedLocation({
      selectedLocation: {},
    });
    this.props.setServiceList([]);
    this.props.getStoreList([]);
  };

  findStore = (e) => {
    e.preventDefault();

    if (this.state.selectedLocationText === null) {
    } else {
      this.setState({
        showStore: true,
        allowToFindChai: true,
      });
    }
  };

  clickOnFindChai = () => {
    //here the complete process will take place of concatination of all the inventory items and menu items
    if (this.state.allowFindChai) {
      toast.warning("Please select store and channel");
    } else {
      this.props.history.push("/products");
    }
  };
  render() {
    const { selectedLocationText } = this.state;
    return (
      <div className="col-md-12 col-sm-12 col-xs-12">
        <div className="search-location">
          <h2>Chai on the run?</h2>
          <p>Order chai from a Chai Point store near you</p>
          <form style={{ display: "flex" }} className="location-search-form">
            <div className="location-div">
              <span>
                <img
                  className="location-image"
                  src={require("../../assets/images/Location.svg").default}
                />
              </span>
              <input
                className="location-srch"
                ref={this.autocompleteInput}
                value={selectedLocationText}
                placeholder="Enter your address"
                type="text"
                name="selectedLocationText"
                onChange={this.updateInput}
              ></input>
              <span
                className="locate-me cross-button"
                onClick={() => {
                  this.clearFun();
                  this.autocompleteInput.current.value = "";
                  this.setState({ showStore: false, selectedLocationText: "" });
                }}
              >
                <label style={{ paddingRight: "7px" }}>
                  <FontAwesomeIcon
                    icon={faTimes}
                    size="lg"
                    className="star-icon"
                  />
                </label>
              </span>
              <span className="locate-me" onClick={this.getTheCurrentLatLng}>
                <img
                  src={require("../../assets/images/Location_GPS.svg").default}
                />
                <span>locate me</span>
              </span>
              <button
                onClick={(e) => {
                  e.preventDefault();
                  this.onFindStore();
                }}
              >
                find store
              </button>
            </div>
            <br />

            {this.state.showStore && this.state.storesCount > 0 && (
              <>
                <div className="store-select">
                  <StoreSelector />
                </div>
                <ChannelSelector findChai={this.handleFindChai} />

                {/* {this.state.allowToFindChai && ( */}
                <br />
                <div className="submit-form">
                  <input
                    onClick={this.clickOnFindChai}
                    // disabled={this.state.allowFindChai}
                    type="button"
                    className="submit-location"
                    value="find chai"
                  ></input>
                </div>
              </>
            )}

            {/* {this.state.showFindChai && this.state.selectedChannel && (
              <input
                onClick={this.clickOnFindChai}
                type="button"
                className="submit-location"
                value="find chai"
              ></input>
            )} */}

            {this.state.showStore && this.state.storesCount === 0 && (
              <p className="no-locations">
                No store available for this area, please select another area
              </p>
            )}
          </form>
          {!this.state.showStore && (
            <p className="city-locations">
              bangalore chennai delhi faridabad ghaziabad goa gurugram hyderabad
              mumbai noida pune
            </p>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject;
  const storeList = reducerObj.storeList.storeList;
  const channelList = reducerObj.channelList.channelList.channelList;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const selectedLocation =
    reducerObj.selectedLocation.selectedLocation.selectedLocation;
  return { storeList, channelList, selectedLocation, userObject };
};

export default connect(mapStateToProps, {
  loadStoreList,
  updateSelectedLocation,
  resetUserObject,
  resetCartObject,
  resetStore,
  resetChannel,
  emptyMenuItemList,
  updateSelectedChannel,
  updateSelectedStore,
  resetLocation,
  getStoreList,
  setServiceList,
})(withRouter(LocationSelector));

LocationSelector.propTypes = {
  loadStoreList: PropTypes.func.isRequired,
  updateSelectedLocation: PropTypes.func.isRequired,
};

import React from "react";
import CurrentLocation from "./currentLocation";
import { loadStoreList } from "../../actions/StoreListAction";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

/* global google */

class SearchBar extends React.Component {
  state = {};
  constructor(props) {
    super(props);
    this.autocompleteInput = React.createRef();
    this.autocomplete = null;
    this.handlePlaceChanged = this.handlePlaceChanged.bind(this);
  }

  componentDidMount() {
    this.autocomplete = new google.maps.places.Autocomplete(
      this.autocompleteInput.current,
      { types: ["geocode"] }
    );

    this.autocomplete.addListener("place_changed", this.handlePlaceChanged);
  }

  handlePlaceChanged() {
    const place = this.autocomplete.getPlace();

    this.setState({
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
    });
    //backend will be
    //load the stores
    this.props.loadStoreList({
      latitude: place.geometry.location.lat(),
      longitude: place.geometry.location.lng(),
    });
  }

  render() {
    return (
      <div className="section-margin">
        <CurrentLocation />
        <input
          className="location-srch"
          ref={this.autocompleteInput}
          id="autocomplete"
          placeholder="Enter your address"
          type="text"
        ></input>
        {this.state.lat && this.state.lng && (
          <div>
            <br />
            Selected lat lng
            <br />
            latitude:
            <input
              className="location-srch"
              type="text"
              value={this.state.lat}
            />
            <br />
            longitude:
            <input
              type="text"
              className="location-srch"
              value={this.state.lng}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const storeList = reducerObj.storeList.storeList;
  return { storeList };
};

export default connect(mapStateToProps, {
  loadStoreList,
})(SearchBar);

SearchBar.propTypes = {
  loadStoreList: PropTypes.func.isRequired,
};

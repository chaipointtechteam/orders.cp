import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { getproductAds } from "../../actions/internalActions/LandingActions";
import { Modal } from "react-bootstrap";
import _ from "lodash";

class LandingMainMenu extends Component {
  state = {
    requiredData: [],
    mediaPath: "",
    isEdit: false,
    images: [],
  };
  componentDidMount = () => {
    this.props.getproductAds((data, mediaPath) => {
      this.setState({
        requiredData: data,
        mediaPath: mediaPath,
      });
    });
  };

  render() {
    return (
      <section className="menu-details">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
              {this.state.requiredData &&
                this.state.requiredData.map((data, index) => (
                  <>
                    {_.sortBy(data.image_type, ["id"]) === "Large" && (
                      <div className="col-md-6 col-sm-6 col-xs-12" key={index}>
                        <div
                          className="breakfast-section"
                          style={{
                            backgroundImage: `url(${this.state.mediaPath}${data.image})`,
                          }}
                        ></div>
                      </div>
                    )}
                  </>
                ))}

              <div className="col-md-6 col-sm-6 col-xs-12">
                <div className="right-side-menu">
                  {this.state.requiredData &&
                    this.state.requiredData.map((data, index) => (
                      <>
                        {data.image_type === "Small" && index < 3 && (
                          <div
                            key={index}
                            className="muffins-section"
                            style={{
                              backgroundImage: `url(${this.state.mediaPath}${data.image})`,
                            }}
                          ></div>
                        )}
                      </>
                    ))}
                  {
                    // this.state.images.length > 0 && (
                    //   <>
                    //     <div
                    //       className="muffins-section"
                    //       style={{
                    //         backgroundImage: `url(${this.state.mediaPath}${this.state.images[1].image})`,
                    //       }}
                    //     ></div>
                    //     <div
                    //       className="muffins-section"
                    //       style={{
                    //         backgroundImage: `url(${this.state.mediaPath}${this.state.images[2].image})`,
                    //       }}
                    //     ></div>
                    //   </>
                    // )
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject;
  return { userObject };
};

export default connect(mapStateToProps, {
  getproductAds,
})(LandingMainMenu);

LandingMainMenu.propTypes = {
  getproductAds: PropTypes.func.isRequired,
};

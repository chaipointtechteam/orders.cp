import React from "react";
import HomeActive from "../../assets/images/Shop_Mobile Assets-06.svg";
import _ from "lodash";
import Home from "../../assets/images/Shop_Mobile Assets-03.svg";
import cart from "../../assets/images/Cart1.svg";
import cartActive from "../../assets/images/Cart2.svg";
import Profile from "../../assets/images/Shop_Mobile Assets-05.svg";
import ProfileActive from "../../assets/images/Shop_Mobile Assets-08.svg";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
const LandingFooter = (props) => {
  const { isLoggedIn } = props.userObject;

  return (
    <div className="showFooter">
      <section className="footer-mobile">
        <div className="footer-mobile-cpm">
          <ul>
            <Link to="/">
              {" "}
              <li className={props.tab === "0" ? "active" : ""}>
                <img src={props.tab === "0" ? HomeActive : Home} />
                <label>Home</label>
              </li>
            </Link>
            {/* <Link to={isLoggedIn ? "/cart" : "/login"}> */}
            <Link to="/cart">
              {" "}
              <li className={props.tab === "1" ? "active" : ""}>
                {_.size(_.get(props, "cartObject.cartObject.cartItems", "")) >
                  0 && (
                  <span className="badge badge-light cart-badge-count">
                    {_.size(
                      _.get(props, "cartObject.cartObject.cartItems", "")
                    )}
                  </span>
                )}
                <img src={props.tab === "1" ? cartActive : cart} />
                <label>Cart</label>
              </li>
            </Link>
            <Link to={isLoggedIn ? "/profile" : "/login"}>
              <li className={props.tab === "2" ? "active" : ""}>
                <img src={props.tab === "2" ? ProfileActive : Profile} />
                <label>Profile</label>
              </li>
            </Link>
          </ul>
        </div>
      </section>
    </div>
  );
};
const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject || {};
  const cartObject = reducerObj.cartObject || {};

  return { userObject, cartObject };
};

export default connect(mapStateToProps, {})(LandingFooter);

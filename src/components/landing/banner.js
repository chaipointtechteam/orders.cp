import React, { Component } from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import Logo from "../../assets/images/logo.png";
import LinkHeader from "../common/linkHeader";
import AutoCompleteSearchForLocation from "./autoCompleteSearchForLocation";
import { getBannerImage } from "../../actions/internalActions/LandingActions";
import backgroundImage from "../../assets/images/Homepage-01.png"; //default banner Image
import { Link } from "react-router-dom";
import { MenuOutlined } from "@ant-design/icons";
import MenuList from "../../pages/common/MobileMenu/HomeMobileMenu";
import { isMobileOnly } from "react-device-detect";
import moment from "moment";

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import _ from "lodash";

class Banner extends Component {
  state = {
    bannerUrl: backgroundImage,
    showMenu: false,
    media_path: "",
  };

  componentDidMount = () => {
    var sata = this.props.getBannerImage((bannerUrl, media_path) => {
      // validation based on dates
      const filteredBanner = _.filter(
        bannerUrl,
        (each) => !moment(each.end_date).isBefore(moment().format("YYYY-MM-DD"))
      );

      if (bannerUrl) {
        this.setState({
          bannerUrl: filteredBanner,
          media_path,
        });
      }
    }, "order.chaipoint.com");
  };
  render() {
    const { userObject } = this.props;

    // When the user clicks on the button, scroll to the top of the document

    return (
      <div className="service-banner">
        <Carousel
          showThumbs={false}
          autoPlay={true}
          showArrows={false}
          showStatus={false}
          showIndicators={_.size(this.state.bannerUrl) > 1}
        >
          {_.map(this.state.bannerUrl, (each) => (
            <div>
              {each.banner_type === "Video" ? (
                <video
                  autoPlay={true}
                  muted={true}
                  loop={true}
                  controls={false}
                  id="myVideo"
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                >
                  <source src={each.banner} type="video/mp4" />
                  Your browser does not support HTML5 video.
                </video>
              ) : (
                <img
                  style={{ objectFit: "cover" }}
                  src={
                    isMobileOnly
                      ? this.state.media_path + each.mobile_banner
                      : this.state.media_path + each.banner
                  }
                />
              )}
            </div>
          ))}
        </Carousel>
        <div className="row" style={{ paddingTop: "45px" }}>
          <div className="container">
            <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="col-md-6 col-sm-6 col-xs-12">
                {/* menu for mobile */}
                {isMobileOnly && (
                  <MenuOutlined
                    className="menu-icon-drawer"
                    onClick={() => this.setState({ showMenu: true })}
                  />
                )}

                <div
                  className={
                    userObject.headerSessionId !== ""
                      ? "logo-after-login logo-display"
                      : "logo-display"
                  }
                >
                  <Link to="/">
                    <img src={Logo} alt="Chai point" />
                  </Link>
                </div>
              </div>

              <LinkHeader />
              <MenuList
                showMenu={this.state.showMenu}
                onClose={() => this.setState({ showMenu: false })}
                userObject={userObject}
              />
            </div>
            <AutoCompleteSearchForLocation />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const userObject = reducerObj.userObject.userObject;
  return { userObject };
};

export default connect(mapStateToProps, {
  getBannerImage,
})(Banner);

// Banner.propTypes = {
//   getBannerImages: PropTypes.func.isRequired,
// };

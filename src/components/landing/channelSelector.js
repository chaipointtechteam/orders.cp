import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { filterTheMenuWithCategory } from "../../utils/common";
import { updateFilterObject } from "../../actions/FilterItemsAction";

import { updateSelectedChannel } from "../../actions/StoreListAction";
import {
  loadInventoryList,
  loadMenuItemList,
  setMenuItemList,
  setCategoryMenuList,
} from "../../actions/MenuItemsAction";
import { oraginzationId } from "../../utils/constants";
import Select from "react-select";
import toast from "../common/commonToast";
// const customStyles = {
//   menu: (styles) => ({
//     ...styles,
//     width: "118%",
//     left: -20,
//     cursor: "pointer",
//   }),
//   control: (styles) => ({
//     ...styles,
//     cursor: "pointer",
//     paddingLeft: 15,
//     boxShadow: "none",
//     alignItems: "center",
//     marginBottom: 25,
//     borderColor: "transparent",
//     "&:hover": {
//       borderColor: "transparent",
//     },
//   }),

//   option: () => {
//     return {
//       cursor: "pointer",
//       listStyle: "none",
//       padding: "12px 20px",
//       borderBottom: "1px solid #f3f4f4",
//       color: "#2b2728",
//     };
//   },
//   indicatorsContainer: () => {
//     return {
//       color: "#279599",
//     };
//   },
//   dropdownIndicator: () => {
//     return {
//       color: "#279599",
//     };
//   },
// };
const customStyles = {
  control: (styles) => ({
    ...styles,
    cursor: "pointer",
    paddingLeft: 15,
    boxShadow: "none",
    marginBottom: 25,
    borderColor: "transparent",
    "&:hover": {
      borderColor: "transparent",
    },
  }),
  menu: styles => ({ ...styles, width: "110%", marginTop:"15px", left: -10, cursor: "pointer","@media only screen and (max-width: 767px)": {
    ...styles["@media only screen and (max-width: 767px)"],
    width: "115%", left: -10, }
  }),

  menuList: () => ({
    cursor: "pointer",
    padding: 0,
    paddingTop: 0,
    paddingBottom: 0,
  }),
  indicatorsContainer: () => {
    return {
      
      color: "#279599",
    };
  },
  option: () => {
    return {
      cursor: "pointer",
      listStyle: "none",
      padding: "12px 20px",
      borderBottom: "1px solid #f3f4f4",
      color: "#2b2728",
    };
  },
  dropdownIndicator: () => {
    return {
      border: "none",
      color: "#279599",
    };
  },
};
class ChannelSelector extends Component {
  state = {
    selectedChannel: null,
    allowToFindChai: false,
  };
  componentDidMount = () => {
    this.updateOptions();
    this.updateSelectedValue();
    // const { selectedChannel, channelList } = this.props;
    // channelList.forEach((element, index) => {
    //   if (selectedChannel && element.sellerId === selectedChannel.sellerId) {
    //     this.setState(
    //       {
    //         selectedChannel: {
    //           value: element.sellerId,
    //           label: element.displayName,
    //           index: index,
    //         },
    //       },
    //       this.getAllAvailableMenuItems()
    //     );
    //   }
    // });
  };

  updateSelectedValue = () => {
    const { selectedChannel, channelList } = this.props;

    if (selectedChannel?.sellerId!==undefined) {
      this.setState(
        {
          selectedChannel: {
            value: selectedChannel.sellerId,
            label: selectedChannel.displayName,
            // index: index,
          },
        },
        () => {
          this.getAllAvailableMenuItems();
          if (this.state.selectedChannel) {
            this.props.findChai(false);
          }
        }
      );
    } else {
      this.setState({
        selectedChannel: null,
      });
    }
  };

  //  
  // console.log("adganlhfnakldfhkladffhnladnh")
  // channelList?.forEach((element, index) => {
  //   if (selectedChannel && element.sellerId === selectedChannel.sellerId) {
  //     // this.state.options?.map((item, index) => {
  //     //   if (item.sellerId === element.sellerId) {
  //     console.log("adganlhfnakldfhkladffhnladnha gnalkgnlasngla alkgknalkhn")
  //     this.setState(
  //       {
  //         selectedChannel: {
  //           value: element.sellerId,
  //           label: element.displayName,
  //           index: index,
  //         },
  //       }, () => this.getAllAvailableMenuItems());
  //   } else {
  //     this.setState({
  //       selectedChannel: null
  //     })
  //   }
  // })

  componentDidUpdate = (prevProps) => {
    if (
      this.props.channelList !== prevProps.channelList &&
      this.props.selectedStore !== prevProps.selectedStore
    ) {
      this.updateOptions();

      var find = this.props.channelList?.find((n) => n.sellerId === 12);
      if (find) {
        this.updateSelectedValue();

        // if (this.state.selectedChannel) {
        //   this.props.findChai(false)
        // }
      } else {
        this.setState(
          {
            selectedChannel: null,
          },
          () => {
            if (!this.state.selectedChannel) {
              this.props.findChai(true);
            }
          }
        );
      }
      // this.setState({
      //   selectedChannel: null
      // }, () => {
      //   if (!this.state.selectedChannel) {
      //     this.props.findChai(true)
      //   }
      // })

      //  this.updateSelectedValue();
      // this.getAllAvailableMenuItems();
    }
  };
  updateOptions = () => {
    var options = [];
    this.props.channelList?.forEach((element, index) => {
      options.push({
        value: element.sellerId,
        label: element.displayName,
        index: index,
      });
    });
    this.setState({
      options: options,
    });
  };

  handleChannelChange = (item) => {
    this.setState(
      {
        selectedChannel: {
          value: item.value,
          label: item.label,
          index: item.index,
        },
      },
      () => {
        if (this.state.selectedChannel) {
          this.props.findChai(false);
        }
      }
    );
    this.props.updateSelectedChannel({
      selectedChannel: this.props.channelList[item.index],
    });

    setTimeout(() => {
      this.getAllAvailableMenuItems();
    }, 100);
  };

  getAllAvailableMenuItems = () => {
    //Getting the  menu items will be called
    if (this.props.selectedChannel) {
      this.props.loadInventoryList(
        {
          sellerId: this.props.selectedChannel.sellerId,
          siteId: this.props.selectedStore.siteId,
        },
        (inventoryData) => {
          if (this.props.selectedChannel.sellerId !== undefined) {

            this.props.loadMenuItemList(
              {
                cityId: this.props.selectedStore.cityId,
                includeProductRecipe: true,
                organizationId: oraginzationId,
                sellerIdList: [this.props.selectedChannel.sellerId],
                siteId: this.props.selectedStore.siteId,
              },
              (catLogData) => {
                if (catLogData?.data) {
                  var categoryList =
                    catLogData.data.sellerProductCatalogueList[0].productCatalogue
                      .categoryList;
                  this.props.setCategoryMenuList({
                    categoryList: categoryList,
                  });
                  var menuItemList = [];
                  var inventory = []
                  catLogData.data.sellerProductCatalogueList[0].productCatalogue.productList.forEach(
                    (product) => {
                      inventory = inventoryData.data
                      if (product.productVariantList.length > 0) {
                       
                        product.quantity = 0;
                        menuItemList.push(product);
                      }
                      inventoryData.data.forEach((inventory) => {
                        if (
                          product.productCode === inventory.code

                        ) {
                          product.quantity = 0;
                          menuItemList.push(product);
                        }
                      });
                    }
                  );
                }
                if (categoryList) {
                  categoryList.forEach((category) => {
                    category.productCodes.forEach((productCode) => {
                      menuItemList.forEach((product) => {
                        if (product.productCode === productCode) {
                          product.category = category;
                        }
                      });
                    });
                    category.subCategories.forEach((subCategory) => {
                      subCategory.productCodes.forEach((productCode) => {
                        menuItemList.forEach((product) => {
                          if (product.productCode === productCode) {
                            product.subCategory = subCategory;
                            product.category = category;
                          }
                        });
                      });
                    });
                  });
                }

                var filtercate = [];
                var filterindx = [];
                this.props.categoryList.map((category, index) => {
                  var filter = filterTheMenuWithCategory(menuItemList, category);

                  if (filter && filter.length > 0) {
                    filtercate.push(category);
                    filterindx.push(index);
                  }
                });

                this.props.updateFilterObject({
                  activeKey: filterindx[0],
                  selectedCategory: filtercate[0],
                  selectedSubCategory: null,
                  isSubCategory: null,
                });

                this.props.setMenuItemList({
                  menuItemList: menuItemList,
                  inventory: inventory
                });
                this.setState({
                  allowToFindChai: true,
                });
              }
            );
          }}
      );
    }
  };

  clickOnFindChai = () => {
    //here the complete process will take place of concatination of all the inventory items and menu items
    this.props.history.push("/products");
  };

  render() {
    return (
      <>
        <div class="channel-select">
          <span>
            <img src={require("../../assets/images/Channel.svg").default} />
          </span>
          <div
            class="custom-select"
            style={{ background: "none", border: "none" }}
          >
            <Select
              isSearchable={false} 
               autoFocus={false}
              placeholder={"Select Channel"}
              options={this.state.options}
              styles={customStyles}
              onChange={this.handleChannelChange}
              value={this.state.selectedChannel}
            />
          </div>
          {/* <select name="selectedChannel" onChange={this.handleChannelChange}>
            <option>Select Channel</option>
            {channelList &&
              channelList.map((option, index) => (
                <option key={index} value={index}>
                  {option.name}
                </option>
              ))}
          </select> */}
        </div>
        {/* {this.state.allowToFindChai && (
          <input
            onClick={this.clickOnFindChai}
            type="button"
            class="submit-location"
            value="find chai"
          ></input>
        )} */}
      </>
    );
  }
}

const mapStateToProps = (reducerObj) => {
  const selectedStore = reducerObj.selectedStore.selectedStore.selectedStore;
  const selectedChannel =
    reducerObj.selectedChannel.selectedChannel.selectedChannel;
  const channelList = reducerObj.channelList.channelList.channelList;
  const categoryList = reducerObj.categoryList.categoryList.categoryList;
  const userObject = reducerObj.userObject.userObject;
  return {
    selectedStore,
    selectedChannel,
    channelList,
    userObject,
    categoryList,
  };
};

export default connect(mapStateToProps, {
  updateSelectedChannel,
  loadMenuItemList,
  loadInventoryList,
  setMenuItemList,
  setCategoryMenuList,
  updateFilterObject,
})(withRouter(ChannelSelector));

ChannelSelector.propTypes = {
  updateSelectedChannel: PropTypes.func.isRequired,
  loadMenuItemList: PropTypes.func.isRequired,
  loadInventoryList: PropTypes.func.isRequired,
  setMenuItemList: PropTypes.func.isRequired,
  setCategoryMenuList: PropTypes.func.isRequired,
};

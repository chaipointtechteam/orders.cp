import React, { useEffect } from "react";
import { Spinner } from "react-bootstrap";
import styles from "./loader.module.scss";
import loader from "../../assets/images/Loader-3_No-gradient.gif";

const Loader = (props) => {
  useEffect(() => {
    document.getElementsByTagName("body")[0].style.overflow = "hidden";
    document.getElementsByTagName("body")[0].style.maxHeight = "90vh";

    // returned function will be called on component unmount
    return () => {
      document.getElementsByTagName("body")[0].style.overflow = "scroll";
      document.getElementsByTagName("body")[0].style.maxHeight = "";
    };
  }, []);

  return (
    <div
      className={styles["loader-container"]}
      style={{ position: "fixed", overflow: "hidden", overflow: "hidden" }}
    >
      {/* <Spinner animation="border" variant="info" /> */}

      <div className={styles["loader"]}>
        <div>
          <img draggable="false" src={loader} alt="loading..." />
        </div>
        {props.changeTitle ? (
          <div className={styles["text2"]}>
            <h4>Updating Cart...</h4>
          </div>
        ) : (
          <div className={styles["text1"]}>
            {" "}
            <h4>Something is brewing for you...</h4>{" "}
          </div>
        )}
      </div>
    </div>
  );
};

export default Loader;
